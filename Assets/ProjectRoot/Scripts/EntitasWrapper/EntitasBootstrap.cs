﻿using System.Threading.Tasks;
using Zenject;

namespace vailshnast.entitaswrapper
{
    public class EntitasBootstrap : IEntitasBootstrap,
                                    ITickable,
                                    IFixedTickable,
                                    ILateTickable
    {
        private readonly DiContainer _diContainer;
        private  SystemsExecutor _systemsExecutor;

        public EntitasBootstrap(DiContainer diContainer) =>
            _diContainer = diContainer;

        public void Initialize() =>
            _systemsExecutor = _diContainer.Instantiate<SystemsExecutor>();
        
        public void StartExecution() =>
            _systemsExecutor.StartExecution();

        public async void Dispose()  
        {
            _systemsExecutor.FinishExecution();
            await Task.Yield();
            Contexts.sharedInstance.Reset();
        }

        public async Task DisposeAsync()
        {
            _systemsExecutor.FinishExecution();
            await Task.Yield();
            Contexts.sharedInstance.Reset();        
        }

        public void Tick() => _systemsExecutor?.Update();
        public void LateTick() => _systemsExecutor?.LateUpdate();
        public void FixedTick() => _systemsExecutor?.FixedUpdate();
    }
}