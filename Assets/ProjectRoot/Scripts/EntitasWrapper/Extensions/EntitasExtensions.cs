using System.Collections.Generic;
using Entitas;
using Zenject;

namespace vailshnast.entitaswrapper
{
    public static class EntitasExtensions
    {
        public static void CreateInjectedSystem<T>(this Feature feature, DiContainer diContainer) where T : ISystem
        {
            var system = (ISystem) diContainer.Instantiate<T>();
            feature.Add(system);
        }

        public static void ExecuteAndCleanup(this Feature feature)
        {
            feature.Execute();
            feature.Cleanup();
        }

        public static void InitializeDebugFeatures(this IEnumerable<IDebugFeature> debugFeatures)
        {
            foreach (var debugFeature in debugFeatures) debugFeature.InitializeDebugFeature();
        }

        public static void DisposeDebugFeatures(this IEnumerable<IDebugFeature> debugFeatures)
        {
            foreach (var debugFeature in debugFeatures) debugFeature.DisposeDebugFeature();
        }
    }
}