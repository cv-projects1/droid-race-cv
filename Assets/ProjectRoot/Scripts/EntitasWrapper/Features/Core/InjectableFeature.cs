using System.Collections;
using Entitas;
using Zenject;

namespace vailshnast.entitaswrapper
{
    public class InjectableFeature : Feature
    {
        private readonly DiContainer _diContainer;
        
        public InjectableFeature(DiContainer diContainer) =>
            _diContainer = diContainer;

        public sealed override Entitas.Systems Add(ISystem system) 
            => base.Add(system);

        public void AddSystemOrFeature<T>() where T : ISystem
        {
            ISystem system = (ISystem) _diContainer.Instantiate(typeof(T));
            Add(system);
        }
    }
}