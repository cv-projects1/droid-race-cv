using System.Collections.Generic;
using Entitas;
using MobRun;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;
using ITransform = vailshnast.entitasview.ITransform;

namespace vailshnast.entitaswrapper
{
    public class GizmosFeature : Feature, IDebugFeature
    {
        private readonly GameConfig _gameConfig;
        private readonly DiContainer _diContainer;
        private GizmosProvider _provider;
        public GizmosFeature(DiContainer diContainer) => _diContainer = diContainer;

        public void InitializeDebugFeature()
        {
            CreateFeatureOrSystem<PhyllotaxisGizmosSystem>();
            CreateGizmosProvider();
        }

        public void DisposeDebugFeature() => _provider.OnDrawGizmosEvent -= Execute;

        private void CreateFeatureOrSystem<T>() where T : ISystem
        {
            var system = _diContainer.Instantiate<T>();
            Add(system);
        }

        private void CreateGizmosProvider()
        {
            if (_provider == null)
            {
                var gizmosGameObject = new GameObject("GizmosProvider");
                _provider = gizmosGameObject.AddComponent<GizmosProvider>();
            }

            Object.DontDestroyOnLoad(_provider);
            _provider.OnDrawGizmosEvent += Execute;
        }

        public sealed override Entitas.Systems Add(ISystem system) =>
            base.Add(system);
    }

    public class PhyllotaxisGizmosSystem : IExecuteSystem
    {
        private readonly GameConfig _gameConfig;
        private readonly GameContext _gameContext;
        private readonly IGroup<GameEntity> _group;
        private readonly List<GameEntity> _buffer = new List<GameEntity>();

        public PhyllotaxisGizmosSystem(GameConfig gameConfig,
                                       GameContext gameContext)
        {
            _gameConfig = gameConfig;
            _gameContext = gameContext;
            _group = _gameContext.GetGroup(GameMatcher.CrowdUnit);
        }

        RaycastHit m_Hit;
        
        public void Execute()
        {
            var globalTargetPos = _gameContext.crowdGlobalTargetEntity.worldPosition.Value;
            var list = _group.GetEntities(_buffer);

            for (int i = 0; i < list.Count; i++)
            {
                var phyllotaxisPos = _gameConfig.GetPhyllotaxisPos(i);

                var finalPos = new Vector3(globalTargetPos.x + phyllotaxisPos.x, 0,
                                           globalTargetPos.z + phyllotaxisPos.y);

                Gizmos.color = Color.red;
                Gizmos.DrawSphere(finalPos, 0.1f);
                showBoxCast(list, i);
            }

            Gizmos.DrawWireSphere(globalTargetPos, _gameConfig.GetPhyllotaxisRadius(list.Count));
        }

        private void showBoxCast(List<GameEntity> list, int i)
        {
            var entity = list[i];
            var transform = entity.entityView.Value.GetComponentView<ITransform>().TransformValue;


            var dir = -transform.up;
            
            var m_HitDetect = Physics.BoxCast(transform.position + Vector3.up,
                                              Vector3.one, dir, out m_Hit,
                                              Quaternion.identity, 10);

            if (m_HitDetect)
            {
                Gizmos.color = Color.blue;

                //Draw a Ray forward from GameObject toward the hit
                Gizmos.DrawRay(transform.position, dir * m_Hit.distance);

                //Draw a cube that extends to where the hit exists
                Gizmos.DrawWireCube(transform.position + dir * m_Hit.distance, transform.localScale);
            }

            //If there hasn't been a hit yet, draw the ray at the maximum distance
            else
            {
                Gizmos.color = Color.red;

                
                //Draw a Ray forward from GameObject toward the maximum distance
                Gizmos.DrawRay(transform.position, dir * 10);

                //Draw a cube at the maximum distance
                Gizmos.DrawWireCube(transform.position + dir * 10, transform.localScale);
            }
        }
    }
}