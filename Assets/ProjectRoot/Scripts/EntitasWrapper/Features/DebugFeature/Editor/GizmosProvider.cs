using System;
using UnityEngine;

namespace vailshnast.entitaswrapper
{
    public class GizmosProvider : MonoBehaviour
    {
        public event Action OnDrawGizmosEvent;
        private void OnDrawGizmos() => OnDrawGizmosEvent?.Invoke();
        public void Cleanup() => OnDrawGizmosEvent = null;
    }
}