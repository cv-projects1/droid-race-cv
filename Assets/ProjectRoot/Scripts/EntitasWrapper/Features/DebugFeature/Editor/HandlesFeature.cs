#if UNITY_EDITOR
using System.Collections.Generic;
using Entitas;
using MobRun;
using UnityEditor;
using UnityEngine;
using vailshnast.entitasview;
using Zenject;

namespace vailshnast.entitaswrapper
{
    public class HandlesFeature : Feature, IDebugFeature
    {
        private readonly DiContainer _diContainer;

        public HandlesFeature(DiContainer diContainer)
        {
            _diContainer = diContainer;
            CreateFeatureOrSystem<SlidePointHandlesSystem>();
            CreateFeatureOrSystem<CrowdUnitGroundedHandlesSystem>();
            //CreateFeatureOrSystem<CrowdUnitFallDirectionSystem>();
        }

        private void CreateFeatureOrSystem<T>() where T : ISystem
        {
            var system = _diContainer.Instantiate<T>();
            Add(system);
        }

        private void StateChanged(PlayModeStateChange obj)
        {
            if (obj == PlayModeStateChange.ExitingPlayMode)
                SceneView.duringSceneGui -= Execute;
        }

        private void Execute(SceneView view) => Execute();

        public sealed override Entitas.Systems Add(ISystem system) =>
            base.Add(system);

        public void InitializeDebugFeature()
        {
            SceneView.duringSceneGui += Execute;
            EditorApplication.playModeStateChanged += StateChanged;
        }

        public void DisposeDebugFeature()
        {
            SceneView.duringSceneGui -= Execute;
            EditorApplication.playModeStateChanged -= StateChanged;
        }
    }

    public class SlidePointHandlesSystem : IExecuteSystem
    {
        public void Execute()
        {
            var gameContext = Contexts.sharedInstance.game;

            if (!gameContext.hasSlidePointActive) return;

            var closestPoint = gameContext.GetEntityWithId(
                gameContext.slidePointActive.EntityIndex).slidePoint.Value;

            var targetPos = gameContext.crowdGlobalTargetEntity.worldPosition.Value;
            Handles.DrawLine(targetPos, closestPoint, 5);
        }
    }

    /*public class CrowdUnitFallDirectionSystem : IExecuteSystem
    {
        private readonly List<GameEntity> _buffer = new List<GameEntity>();
        private readonly IGroup<GameEntity> _entities = null;
        private readonly GameContext _gameContext;
        private readonly AnimationConfig _animationConfig;

        public CrowdUnitFallDirectionSystem(GameContext gameContext, AnimationConfig animationConfig)
        {
            _gameContext = gameContext;
            _animationConfig = animationConfig;
            _entities = _gameContext.GetGroup(GameMatcher.CrowdUnit);
        }

        public void Execute()
        {
            foreach (var entity in _entities.GetEntities(_buffer))
            {
                var crowdParentPos = _gameContext.crowdGlobalTargetEntity.worldPosition.Value;
                var entityPos = entity.crowdLocalTarget.Value + crowdParentPos/*entity.entityView.Value.GetComponentView<ITransform>().TransformValue.position#1#;
                var approxPos = entityPos + (entityPos - crowdParentPos).normalized * _animationConfig.Distance;
                Handles.color = Color.green;
                Handles.DrawLine(entityPos, approxPos, 2.5f);
                Handles.color = Color.red;

                Handles.DrawLine(entityPos, crowdParentPos, 2.5f);

#if UNITY_EDITOR

                //Debug.DrawRay(transform.position, -Vector3.up * 2f, grounded ? Color.green : Color.red);
#endif
            }
        }
    }*/

    public class CrowdUnitGroundedHandlesSystem : IExecuteSystem
    {
        private readonly List<GameEntity> _buffer = new List<GameEntity>();
        private readonly IGroup<GameEntity> _entities = null;
        private readonly GameContext _gameContext;

        public CrowdUnitGroundedHandlesSystem(GameContext gameContext)
        {
            _gameContext = gameContext;
            _entities = _gameContext.GetGroup(GameMatcher.CrowdUnit);
        }

        public void Execute()
        {
            foreach (var entity in _entities.GetEntities(_buffer))
            {
                var transform = entity.entityView.Value.GetComponentView<ITransform>().TransformValue;
                var grounded = Physics.Raycast(transform.position + Vector3.up * 0.1f, -Vector3.up, 2f);
#if UNITY_EDITOR

                //Debug.DrawRay(transform.position, -Vector3.up * 2f, grounded ? Color.green : Color.red);
#endif
            }
        }
    }
}
#endif