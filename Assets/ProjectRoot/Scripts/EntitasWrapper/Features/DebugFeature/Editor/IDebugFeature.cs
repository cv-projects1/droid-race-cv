namespace vailshnast.entitaswrapper
{
    public interface IDebugFeature
    {
        void InitializeDebugFeature();
        void DisposeDebugFeature();
    }
}