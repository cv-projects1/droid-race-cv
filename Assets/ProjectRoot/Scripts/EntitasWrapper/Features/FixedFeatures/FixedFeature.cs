using Zenject;

namespace vailshnast.entitaswrapper
{
    public class FixedFeature : InjectableFeature
    {
        public FixedFeature(DiContainer diContainer) : base(diContainer)
        {
            AddSystemOrFeature<GravitySystem>();
            AddSystemOrFeature<LaunchSystem>();
            AddSystemOrFeature<AddForceOnceSystem>();
        }
    }
}