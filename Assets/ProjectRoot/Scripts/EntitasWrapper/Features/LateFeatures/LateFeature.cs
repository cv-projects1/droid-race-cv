using Zenject;

namespace vailshnast.entitaswrapper
{
    public class LateFeature : InjectableFeature
    {
        public LateFeature(DiContainer diContainer) : base(diContainer)
        {
            AddSystemOrFeature<ProcessCameraSystem>();
        }
    }
}