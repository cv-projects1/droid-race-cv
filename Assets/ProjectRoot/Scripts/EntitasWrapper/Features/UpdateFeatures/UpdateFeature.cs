using Systems;
using MobRun;
using PrototypeTemplate;
using Zenject;

namespace vailshnast.entitaswrapper
{
    public class UpdateFeature : InjectableFeature
    {
        public UpdateFeature(DiContainer diContainer, PlayerModel playerModel) : base(diContainer)
        {
            AddSystemOrFeature<InitializationFeature>();
            AddSystemOrFeature<InputFeature>();
            AddSystemOrFeature<TimeFeature>();
            AddSystemOrFeature<GameFeature>();
            AddSystemOrFeature<TutorialFeature>();
            AddSystemOrFeature<EventsFeature>();
            AddSystemOrFeature<DestroyFeature>();
        }
    }

    public class InitializationFeature : InjectableFeature
    {
        public InitializationFeature(DiContainer diContainer) : base(diContainer)
        {
            AddSystemOrFeature<IdSubscriptionSystem>();
            AddSystemOrFeature<GameStartSystem>();
        }
    }

    public class TimeFeature : InjectableFeature
    {
        public TimeFeature(DiContainer diContainer) : base(diContainer)
        {
            AddSystemOrFeature<UpdateTimeDeltaSystem>();
            AddSystemOrFeature<UpdateFixedTimeDeltaSystem>();
            AddSystemOrFeature<TimerSystem>();
        }
    }

    public class InputFeature : InjectableFeature
    {
        public InputFeature(DiContainer diContainer) : base(diContainer)
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            AddSystemOrFeature<EditorTouchProcessingSystem>();
#else
            AddSystemOrFeature<MobileTouchProcesssingSystem>();
#endif
        }
    }

    public class DotweenFeature : InjectableFeature
    {
        public DotweenFeature(DiContainer diContainer) : base(diContainer)
        {
            AddSystemOrFeature<TweenAddedSystem>();
            AddSystemOrFeature<TweenExecutingSystem>();
            AddSystemOrFeature<TweenDestructionSystem>();
        }
    }

    public class CrowdFeature : InjectableFeature
    {
        public CrowdFeature(DiContainer diContainer) : base(diContainer)
        {
            AddSystemOrFeature<CrowdSpawnSystem>();
            AddSystemOrFeature<CrowdKillSystem>();
            AddSystemOrFeature<CrowdMoveGlobalTargetSystem>();
            AddSystemOrFeature<CrowdDirectionAssistSystem>();
            AddSystemOrFeature<CrowdCalculateLocalTargetPositionSystem>();
            AddSystemOrFeature<CrowdMoveTriggerSystem>();
            AddSystemOrFeature<CrowdMoveToTargetSystem>();
            AddSystemOrFeature<CrowdUnitGroundedSystem>();
            AddSystemOrFeature<CrowdColliderRadiusSystem>();
            AddSystemOrFeature<CrowdNonGroundedUnitsSystem>();
            AddSystemOrFeature<CrowdObstacleCollisionEnterSystem>();
            AddSystemOrFeature<CrowdIndexRecalculationSystem>();
            AddSystemOrFeature<CrowdAmountCalculationSystem>();
            AddSystemOrFeature<CrowdDeathSystem>();
            AddSystemOrFeature<CrowdUnitFallenSystem>();
        }
    }

    public class SlideFeature : InjectableFeature
    {
        public SlideFeature(DiContainer diContainer) : base(diContainer)
        {
            AddSystemOrFeature<SlidePointSetActiveSystem>();
            AddSystemOrFeature<SlidePointRemoveActiveSystem>();
            AddSystemOrFeature<SlidePointMagnetizingProgressSystem>();
            AddSystemOrFeature<SlidePointStartTriggerSystem>();
            AddSystemOrFeature<SlidePointEndTriggerSystem>();
            AddSystemOrFeature<SlideProcessingSystem>();
            AddSystemOrFeature<SlideLineDrawSystem>();
            AddSystemOrFeature<SlidePointSoundSystem>();
        }
    }

    public class TutorialFeature : InjectableFeature
    {
        public TutorialFeature(DiContainer diContainer) : base(diContainer)
        {
            AddSystemOrFeature<TutorialInitializationSystem>();
            AddSystemOrFeature<TutorialHoldTapTriggerSystem>();
            AddSystemOrFeature<TutorialWaitForHoldSystem>();
            AddSystemOrFeature<TutorialKeepHoldingTouchEndSystem>();
            AddSystemOrFeature<TutorialKeepHoldingTouchBeginSystem>();
            AddSystemOrFeature<TutorialReleaseTriggerSystem>();
            AddSystemOrFeature<TutorialReleaseTouchEndSystem>();
        }
    }

    public class GameFeature : InjectableFeature
    {
        public GameFeature(DiContainer diContainer) : base(diContainer)
        {
            AddSystemOrFeature<DotweenFeature>();
            AddSystemOrFeature<PortalProcessingSystem>();
            AddSystemOrFeature<CrowdFeature>();
            AddSystemOrFeature<SlideFeature>();
            AddSystemOrFeature<FinishRowCollisionEnterSystem>();
            AddSystemOrFeature<FinishBoxUnitReachedSystem>();
            AddSystemOrFeature<FinishBoxFlyingSystem>();
            AddSystemOrFeature<FinishBoxCollectedSystem>();
            AddSystemOrFeature<FinishBoxPrizeCollisionSystem>();
            AddSystemOrFeature<FinishLineCollisionSystem>();
            AddSystemOrFeature<RotationSystem>();
            AddSystemOrFeature<LooseSystem>();
            AddSystemOrFeature<VictorySystem>();
        }
    }

    public class EventsFeature : InjectableFeature
    {
        public EventsFeature(DiContainer diContainer) : base(diContainer)
        {
            AddSystemOrFeature<GameEventSystems>();
        }
    }

    public class DestroyFeature : InjectableFeature
    {
        public DestroyFeature(DiContainer diContainer) : base(diContainer)
        {
            AddSystemOrFeature<GameCleanupSystems>();
            AddSystemOrFeature<DestroyEntitiesSystem>();
            AddSystemOrFeature<DestroyWorldSystem>();
        }
    }
}