using Entitas;
using Zenject;

namespace vailshnast.entitaswrapper
{
    public class  EntitasInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Contexts contexts = Contexts.sharedInstance;
            IContext[] contextsList = contexts.allContexts;
            
            Container.BindInterfacesAndSelfTo<EntitasBootstrap>().AsSingle();
            Container.BindInstance(contexts);
            
            for (int i = 0; i < contextsList.Length; i++)
                Container.Bind(contextsList[i].GetType()).FromInstance(contextsList[i]);
        }
    }
}