using System.Threading.Tasks;

namespace vailshnast.entitaswrapper
{
    public interface IEntitasBootstrap
    {
        void Initialize();
        void StartExecution();
        void Dispose();
        Task DisposeAsync();
    }
}