namespace vailshnast.entitaswrapper
{
    public enum ExecutionPoint
    {
        Update      = 0,
        LateUpdate  = 1,
        FixedUpdate = 2
    }
}