using System.Collections.Generic;
using MobRun;
using Zenject;

namespace vailshnast.entitaswrapper
{
    public class SystemsExecutor
    {
        private readonly Feature _rootFeature;
        private readonly Dictionary<ExecutionPoint, Feature> _featuresMap;
        private readonly DiContainer _diContainer;
        private readonly DebugConfig _debugConfig;
        private bool _executing;

#if UNITY_EDITOR
        private readonly IDebugFeature[] _debugFeatures;
#endif

        public SystemsExecutor(DiContainer diContainer, DebugConfig debugConfig)
        {
            _diContainer = diContainer;
            _debugConfig = debugConfig;
            _rootFeature = new Feature($"Root Feature");

            _featuresMap = new Dictionary<ExecutionPoint, Feature>()
            {
                {ExecutionPoint.Update, CreateFeature<UpdateFeature>()},
                {ExecutionPoint.LateUpdate, CreateFeature<LateFeature>()},
                {ExecutionPoint.FixedUpdate, CreateFeature<FixedFeature>()}
            };

#if UNITY_EDITOR
            if (_debugConfig.EnableDebugSystems)
            {
                _debugFeatures = new IDebugFeature[]
                {
                    CreateFeature<GizmosFeature>(),
                    CreateFeature<HandlesFeature>()
                };
            }
#endif
            
            foreach (var value in _featuresMap.Values)
                _rootFeature.Add(value);
        }

        private T CreateFeature<T>() where T : Feature => (T) _diContainer.Instantiate(typeof(T));
        public void Update() => ExecuteSystems(ExecutionPoint.Update);
        public void FixedUpdate() => ExecuteSystems(ExecutionPoint.FixedUpdate);

        public void LateUpdate() => ExecuteSystems(ExecutionPoint.LateUpdate);

        private void ExecuteSystems(ExecutionPoint point)
        {
            if (!_executing) return;

            _featuresMap[point].Execute();
            _featuresMap[point].Cleanup();
        }

        public void StartExecution()
        {
            _rootFeature.ActivateReactiveSystems();
            _rootFeature.Initialize();
            _executing = true;

#if UNITY_EDITOR
            if (_debugConfig.EnableDebugSystems) _debugFeatures.InitializeDebugFeatures();
#endif
        }

        public void FinishExecution()
        {
#if UNITY_EDITOR
            if (_debugConfig.EnableDebugSystems) _debugFeatures.DisposeDebugFeatures();
#endif

            _executing = false;
            
            _rootFeature.TearDown();
            _rootFeature.DeactivateReactiveSystems();
        }
    }
}