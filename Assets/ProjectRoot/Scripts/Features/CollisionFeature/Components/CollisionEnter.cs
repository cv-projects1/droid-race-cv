using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game,Cleanup(CleanupMode.DestroyEntity)]
public sealed class CollisionEnter : IComponent
{
    public int TriggerId, ListenerId;
}