using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game,Cleanup(CleanupMode.DestroyEntity)]
public sealed class CollisionExit : IComponent
{
    public int TriggerId, ListenerId;
}