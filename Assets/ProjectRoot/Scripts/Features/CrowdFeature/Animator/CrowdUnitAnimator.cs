using System.Collections.Generic;
using Entitas;
using UnityEngine;
using vailshnast.animator;
using vailshnast.entitasview;

namespace MobRun
{
    public class CrowdUnitAnimator : MonoBehaviour
    {
        [SerializeField] private Animator _animator;

        private const string RunParam = "Run",
                             HoldBoxParam = "HoldBox";

        
        public CachedAnimator<CrowdAnimationState> CachedAnimator { get; } = 
            new CachedAnimator<CrowdAnimationState>();

        public void Initialize()
        {
            CachedAnimator.Initialize(new List<AnimatorParam<CrowdAnimationState>>()
            {
                new AnimatorParam<CrowdAnimationState> {Key = CrowdAnimationState.Running, ParamName = RunParam},
                new AnimatorParam<CrowdAnimationState> {Key = CrowdAnimationState.HoldingBox, ParamName = HoldBoxParam},
            }, _animator);
        }
    }

    public enum CrowdAnimationState
    {
        Running = 0,
        HoldingBox = 1
    }
}