using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game,Event(EventTarget.Any), Unique]
public sealed class CrowdAmountComponent : IComponent
{
    public int Value;
}