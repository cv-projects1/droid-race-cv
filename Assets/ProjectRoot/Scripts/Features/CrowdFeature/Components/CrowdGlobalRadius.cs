using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique , Event(EventTarget.Any)]
public sealed class CrowdGlobalRadius : IComponent
{
    public float Value;
}