using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game,Unique]
public sealed class CrowdGlobalTargetSpeed : IComponent
{
    public float Value;
}