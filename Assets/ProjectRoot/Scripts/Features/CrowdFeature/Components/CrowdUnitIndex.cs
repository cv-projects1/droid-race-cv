using Entitas;

[Game]
public sealed class CrowdUnitIndex : IComponent
{
    public int Value;
}