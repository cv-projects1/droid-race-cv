using Entitas;
using MobRun;
using UnityEngine;
using vailshnast.entitasview;

public class CrowdAnimationListener : MonoBehaviour, IEventListener, 
                                      ICrowdUnitHoldingBoxListener
{
    [SerializeField] private CrowdUnitAnimator _crowdUnitAnimator;
    
    private GameEntity _entity;

    public void RegisterListeners(IEntity entity)
    {
        _entity = (GameEntity) entity;
        _entity.AddCrowdUnitHoldingBoxListener(this);
        _crowdUnitAnimator.Initialize();
    }

    public void RemoveListeners()
    {
        _entity.RemoveCrowdUnitHoldingBoxListener(this);
    }

    public void OnCrowdUnitHoldingBox(GameEntity entity)
    {
        _crowdUnitAnimator.CachedAnimator.SetTrigger(CrowdAnimationState.HoldingBox);
    }
}
