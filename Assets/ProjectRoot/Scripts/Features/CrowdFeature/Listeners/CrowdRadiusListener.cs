using Entitas;
using UnityEngine;
using vailshnast.entitasview;

public class CrowdRadiusListener : MonoBehaviour, IEventListener, 
                                   IAnyCrowdGlobalRadiusListener
{
    [SerializeField] private SphereCollider _collider;
    private GameEntity _entity;

    public void RegisterListeners(IEntity entity)
    {
        _entity = (GameEntity) entity;
        _entity.AddAnyCrowdGlobalRadiusListener(this);
    }

    public void RemoveListeners()
    {
        _entity.RemoveAnyCrowdGlobalRadiusListener(this);
    }

    public void OnAnyCrowdGlobalRadius(GameEntity entity, float value) =>
        _collider.radius = value;
}
