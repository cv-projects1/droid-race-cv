using Entitas;
using UnityEngine;
using vailshnast.entitasview;

public class CrowdUnitFlyingListener : MonoBehaviour, IEventListener, ICrowdUnitFlyingListener
{
    [SerializeField] private ParticleSystem[] _particleSystems;
    private GameEntity _entity;

    public void RegisterListeners(IEntity entity)
    {
        _entity = (GameEntity) entity;
        _entity.AddCrowdUnitFlyingListener(this);
    }

    public void RemoveListeners()
    {
        _entity.RemoveCrowdUnitFlyingListener(this);
    }

    public void OnCrowdUnitFlying(GameEntity entity)
    {
        foreach (var system in _particleSystems) system.Play();
    }
}
