using Entitas;
using System.Collections.Generic;

public class CrowdAmountCalculationSystem : MultiReactiveSystem<GameEntity, Contexts>
{
    private readonly GameContext _gameContext;
    private readonly IGroup<GameEntity> _crowdGroup;

    public CrowdAmountCalculationSystem(Contexts contexts) : base(contexts)
    {
        _gameContext = contexts.game;
        _crowdGroup = _gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.CrowdUnit).NoneOf(GameMatcher.Destroyed));
    }

    protected override ICollector[] GetTrigger(Contexts contexts)
    {
        return new ICollector[]
        {
            /*contexts.game.CreateCollector(GameMatcher.AllOf(GameMatcher.CrowdUnit,
                                                            GameMatcher.Destroyed).AddedOrRemoved()),*/
            contexts.game.CreateCollector(GameMatcher.AllOf(GameMatcher.CrowdUnit).AddedOrRemoved())
        };
    }

    protected override bool Filter(GameEntity entity) => true;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            // do stuff to the matched entities
            _gameContext.ReplaceCrowdAmount(_crowdGroup.count);
        }
    }
}