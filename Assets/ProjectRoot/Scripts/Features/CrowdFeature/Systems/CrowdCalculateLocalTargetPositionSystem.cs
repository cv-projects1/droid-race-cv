using System.Collections.Generic;
using Entitas;
using MobRun;
using UnityEngine;
using vailshnast.math;

public class CrowdCalculateLocalTargetPositionSystem : IExecuteSystem
{
    private readonly List<GameEntity> _buffer = new List<GameEntity>();
    private readonly IGroup<GameEntity> _entities = null;
    private readonly GameContext _gameContext;
    private readonly GameConfig _gameConfig;

    public CrowdCalculateLocalTargetPositionSystem(GameContext gameContext,
                                              GameConfig gameConfig)
    {
        _gameContext = gameContext;
        _gameConfig = gameConfig;
        _entities = _gameContext.GetGroup(GameMatcher.CrowdUnitIndex);
    }

    public void Execute()
    {
        foreach (var entity in _entities.GetEntities(_buffer))
        {
            var localPosition = Phyllotaxis.Calculate(
                _gameConfig.PhyllotasixAngle,
                _gameConfig.PhyllotaxisScale, entity.crowdUnitIndex.Value);

            entity.ReplaceCrowdLocalTarget(new Vector3(localPosition.x, 0, localPosition.y));
        }
    }
}