using Entitas;
using MobRun;

public class CrowdColliderRadiusSystem : IExecuteSystem
{
    private readonly IGroup<GameEntity> _entities = null;
    private readonly GameContext _gameContext;
    private readonly GameConfig _gameConfig;

    public CrowdColliderRadiusSystem(GameContext gameContext, GameConfig gameConfig)
    {
        _gameContext = gameContext;
        _gameConfig = gameConfig;

        _entities = _gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.CrowdUnit).
                                                       NoneOf(GameMatcher.Destroyed));
    }

    public void Execute()
    {
        var targetEntity = _gameContext.crowdGlobalTargetEntity;
        if (targetEntity == null) return;
        
        var radius = _gameConfig.GetPhyllotaxisRadius(_entities.count);

        _gameContext.ReplaceCrowdGlobalRadius(radius);
    }
}