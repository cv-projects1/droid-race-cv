using Entitas;
using System.Collections.Generic;
using MobRun;
using UnityEngine;
using vailshnast.entitasview;
using vailshnast.pooledfactories;

public class CrowdDeathSystem : ReactiveSystem<GameEntity>
{
    private readonly IHapticsService _hapticsService;
    private readonly IGameSoundService _gameSoundService;
    private readonly IBasePooledFactory _basePooledFactory;
    private readonly HapticConfig _hapticConfig;
    private readonly SoundsContainerConfig _soundsContainerConfig;
    private float _lastDeathSoundTime = 0;
    
    public CrowdDeathSystem(Contexts contexts,
                            HapticConfig hapticConfig,
                            SoundsContainerConfig soundsContainerConfig,
                            IHapticsService hapticsService,
                            IGameSoundService gameSoundService,
                            IBasePooledFactory basePooledFactory) : base(contexts.game)
    {
        _hapticsService = hapticsService;
        _gameSoundService = gameSoundService;
        _basePooledFactory = basePooledFactory;
        _hapticConfig = hapticConfig;
        _soundsContainerConfig = soundsContainerConfig;

        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.Destroyed);
    protected override bool Filter(GameEntity entity) => entity.isDestroyed && entity.isCrowdUnit;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            // do stuff to the matched entities
            _hapticsService.CreateHaptic(_hapticConfig.RobotDeath);
            
            if (Time.time > _lastDeathSoundTime + _soundsContainerConfig.DeathSoundDelay)
            {
                _gameSoundService.PlaySound(SoundType.RobotDeath);
                _lastDeathSoundTime = Time.time;
                var effect = _basePooledFactory.CreatePoolObject<PooledEffect>(PooledConstants.DeathEffect);
                effect.transform.position = entity.entityView.Value.GetComponentView<ITransform>().TransformValue.position;
            }
        }
    }
}
