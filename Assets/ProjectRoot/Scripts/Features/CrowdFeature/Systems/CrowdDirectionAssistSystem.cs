using Entitas;
using MobRun;
using UnityEngine;

public class CrowdDirectionAssistSystem : IExecuteSystem
{
    private readonly GameContext _gameContext;
    private readonly GameConfig _gameConfig;

    public CrowdDirectionAssistSystem(GameContext gameContext, GameConfig gameConfig)
    {
        _gameContext = gameContext;
        _gameConfig = gameConfig;
    }

    public void Execute()
    {
        var targetEntity = _gameContext.crowdGlobalTargetEntity;

        if (_gameContext.hasSlidePointActive && _gameContext.hasTouchHold) return;
        if (targetEntity == null) return;

        var delta = _gameContext.timeDelta.Value;
        var targetTransform = targetEntity.transformView.Value.TransformValue;
        var forward = targetTransform.forward;
        var closestDirection = GameDirections.GetClosestDirection(forward);
        var targetRotation = Quaternion.LookRotation(closestDirection);
        var currentRotation = targetTransform.rotation;

        targetTransform.rotation =
            Quaternion.RotateTowards(currentRotation, targetRotation, delta * _gameContext.crowdAssistSpeed.Value);

        //Debug.Log($"{forward} / {GameDirections.GetClosestDirection(forward)}");
    }
}

public class GameDirections
{
    public static readonly Vector3[] Directions =
    {
        new Vector3(0, 0, 1),
        new Vector3(1, 0, 0),
        new Vector3(0, 0, -1),
        new Vector3(-1, 0, 0)
    };

    public static Vector3 GetClosestDirection(Vector3 dir)
    {
        var closestDir = Directions[0];

        for (int i = 0; i < Directions.Length; i++)
        {
            var iteratedDir = Directions[i];
            var distance1 = Vector3.Distance(closestDir, dir);
            var distance2 = Vector3.Distance(iteratedDir, dir);
            if (distance2 < distance1) closestDir = iteratedDir;
        }

        return closestDir;
    }
}