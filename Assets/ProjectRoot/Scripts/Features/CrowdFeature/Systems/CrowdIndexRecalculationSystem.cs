using Entitas;
using System.Collections;
using System.Collections.Generic;
using MobRun;
using UnityEngine;

public class CrowdIndexRecalculationSystem : MultiReactiveSystem<GameEntity,Contexts>
{
    private readonly GameContext _gameContext;
    private readonly IGroup<GameEntity> _entitiesGroup;
    private readonly List<GameEntity> _bufffer = new List<GameEntity>();
    private readonly GameConfig _gameConfig;

    public CrowdIndexRecalculationSystem(Contexts contexts,
                                         GameConfig gameConfig) : base(contexts)
    {
        // pass the context of interest to the base constructor
        _gameConfig = gameConfig;
        _gameContext = contexts.game;

        _entitiesGroup = _gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.CrowdUnit).NoneOf(
                                                   GameMatcher.Destroyed,
                                                   GameMatcher.CrowdRecalculationLock));
    }

    protected override ICollector[] GetTrigger(Contexts contexts)
    {
        return new ICollector[]
        {
            contexts.game.CreateCollector(GameMatcher.AllOf(GameMatcher.CrowdUnit).AddedOrRemoved())
        };
    }

    protected override bool Filter(GameEntity entity) => entity.isDestroyed ||
                                                         !entity.isCrowdUnit;

    protected override void Execute(List<GameEntity> destroyedEntities)
    {
        var unitsGroup = _entitiesGroup.GetEntities(_bufffer);

        foreach (var entity in destroyedEntities)
        {
            //Debug.Log(entity.crowdUnitIndex.Value);
            var unitsTotalCount = unitsGroup.Count;

            for (int i = 0; i < unitsTotalCount; i++)
            {
                var phyllotoxisPosition = _gameConfig.GetPhyllotaxisPos(i);

                var finalPos = new Vector3(phyllotoxisPosition.x,
                                           0,phyllotoxisPosition.y);

                var closestEntity = GetClosestEntity(finalPos);

                if (closestEntity == null) return;
                closestEntity.isCrowdRecalculationLock = true;
                closestEntity.ReplaceCrowdUnitIndex(i);
            }

            // do stuff to the matched entities
        }
    }

    private GameEntity GetClosestEntity(Vector3 pos)
    {
        GameEntity closestEntity = null;
        
        foreach (var crowdUnit in _entitiesGroup.GetEntities(_bufffer))
        {
            if (closestEntity == null)
            {
                closestEntity = crowdUnit;
                continue;
            }
            
            var dist1 = Vector3.Distance(closestEntity.localPosition.Value, pos);
            var dist2 = Vector3.Distance(crowdUnit.localPosition.Value, pos);

            if (dist2 < dist1) closestEntity = crowdUnit;
        }

        return closestEntity;
    }
}