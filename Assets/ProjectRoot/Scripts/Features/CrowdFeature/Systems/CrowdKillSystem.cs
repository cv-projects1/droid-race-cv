using Entitas;
using System.Collections;
using System.Collections.Generic;
using MobRun;
using UnityEngine;
using vailshnast.entitasview;
using vailshnast.pooledfactories;

public class CrowdKillSystem : ReactiveSystem<GameEntity>
{
    private readonly IBasePooledFactory _basePooledFactory;
    private readonly IGroup<GameEntity> _group;
    
    private readonly List<GameEntity> _entityBuffer = new List<GameEntity>();
    private readonly GameContext _gameContext;

    public CrowdKillSystem(Contexts contexts,
                           IBasePooledFactory basePooledFactory) :
        base(contexts.game)
    {
        _basePooledFactory = basePooledFactory;

        _gameContext = contexts.game;
        _group = _gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.CrowdUnit)
                                                  .NoneOf(GameMatcher.Destroyed));

        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.CrowdKill);

    protected override bool Filter(GameEntity entity) =>
        entity.hasCrowdKill;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            var removedCount = 0;

            var effect = _basePooledFactory.CreatePoolObject<PooledEffect>(PooledConstants.DeathEffectPortal);
            effect.transform.position = _gameContext.crowdGlobalTargetEntity.worldPosition.Value;
            
            foreach (var crowdEntity in _group.GetEntities(_entityBuffer))
            {
                if (removedCount < entity.crowdKill.Count)
                {
                    crowdEntity.isDestroyed = true;
                    removedCount++;
                }
            }
        }
    }
}