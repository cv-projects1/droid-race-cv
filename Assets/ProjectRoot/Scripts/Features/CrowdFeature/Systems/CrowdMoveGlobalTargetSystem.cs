using Entitas;
using MobRun;
using UnityEngine;

public class CrowdMoveGlobalTargetSystem : IExecuteSystem
{
    private readonly GameContext _gameContext;
    private readonly GameConfig _gameConfig;
    private readonly AnimationConfig _animationConfig;
    private readonly IGroup<GameEntity> _unitsGroup;

    public CrowdMoveGlobalTargetSystem(GameContext gameContext,
                                       GameConfig gameConfig,
                                       AnimationConfig animationConfig)
    {
        _gameContext = gameContext;
        _gameConfig = gameConfig;
        _animationConfig = animationConfig;
        _unitsGroup = _gameContext.GetGroup(GameMatcher.CrowdUnit);
    }

    public void Execute()
    {
        if (_unitsGroup.count <= 0) return;
        if (_gameContext.hasSlidePointActive && _gameContext.hasTouchHold) return;
        //if(_gameContext.isFinishLineReachedTween) return;

        var delta = _gameContext.timeDelta.Value;
        var targetEntity = _gameContext.crowdGlobalTargetEntity;
        var transform = targetEntity.transformView.Value.TransformValue;
        
        var pos = targetEntity.worldPosition.Value;
        var direction = transform.forward;
        var speed = _gameContext.crowdGlobalTargetSpeed.Value;

        var finalPos = pos + direction.normalized * speed * delta;

        if (_gameContext.hasFinishLineReached)
            finalPos.x = Mathf.MoveTowards(finalPos.x, _gameContext.finishLineReached.CenterPos.x,
                                           delta * _animationConfig.CenteringSpeed);
        
        targetEntity.ReplaceWorldPosition(finalPos);
    }
}