using System.Collections.Generic;
using Entitas;
using MobRun;
using UnityEngine;
using vailshnast.entitasview;

public class CrowdMoveToTargetSystem : IExecuteSystem
{
    private readonly GameConfig _gameConfig;
    private readonly IGroup<GameEntity> _entities = null;
    private readonly List<GameEntity> _buffer = new List<GameEntity>();
    private readonly GameContext _gameContext;

    public CrowdMoveToTargetSystem(Contexts contexts, GameConfig gameConfig)
    {
        _gameConfig = gameConfig;

        _entities = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.CrowdUnit,
                                                             GameMatcher.EntityView));

        _gameContext = contexts.game;
    }

    public void Execute()
    {
        var delta = _gameContext.timeDelta.Value;

        foreach (var entity in _entities.GetEntities(_buffer))
        {
            //var rigidbody = entity.entityView.Value.GetComponentView<IRigidbody>().RigidbodyValue;
            var entityPosition = entity.localPosition.Value;

            //var speed = _gameContext.crowdSpeed.Value;
            var formationSpeed = _gameConfig.CrowdFormationSpeed;
            var targetPosition = entity.crowdLocalTarget.Value;

            var finalPosition = new Vector3(targetPosition.x,
                                            entityPosition.y,
                                            targetPosition.z);

            var pos = Vector3.MoveTowards(entityPosition, finalPosition, delta * formationSpeed);
            entity.ReplaceLocalPosition(pos);
        }
    }
}