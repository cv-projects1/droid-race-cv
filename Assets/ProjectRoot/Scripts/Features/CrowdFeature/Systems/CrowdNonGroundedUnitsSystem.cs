using Entitas;
using System.Collections.Generic;
using MobRun;
using UnityEngine;
using vailshnast.entitasview;
using vailshnast.extensions;

public class CrowdNonGroundedUnitsSystem : ReactiveSystem<GameEntity>
{
    private readonly IEntityCreationService _entityCreationService;
    private readonly ITweenEntityService _tweenEntityService;
    private readonly AnimationConfig _animationConfig;
    private readonly GameContext _gameContext;

    public CrowdNonGroundedUnitsSystem(Contexts contexts,
                                       IEntityCreationService entityCreationService,
                                       ITweenEntityService tweenEntityService,
                                       AnimationConfig animationConfig) : base(contexts.game)
    {
        _entityCreationService = entityCreationService;
        _tweenEntityService = tweenEntityService;
        _animationConfig = animationConfig;
        _gameContext = contexts.game;

        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.CrowdUnitGrounded.Removed());

    protected override bool Filter(GameEntity entity) => entity.isCrowdUnit && !entity.isCrowdUnitGrounded;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            _entityCreationService.RemoveCrowdUnit(entity);

            var tweenEntity = _gameContext.CreateEntity();
            var entityPos = entity.entityView.Value.GetComponentView<ITransform>().TransformValue.position;
            var finalPos = new Vector3(entityPos.x, _animationConfig.CrowdFlyHeight, entityPos.z);
            entity.AddWorldPosition(entityPos);

            var tween = _tweenEntityService.CreateMoveTweenFromEntity(entity, finalPos,
                                                                      _animationConfig.CrowdFallAnimSettings.Duration);

            tween.ApplyCurve(_animationConfig.CrowdFallAnimSettings);
            tweenEntity.AddTween(tween);
            tweenEntity.isCrowdUnitFallen = true;
        }
    }
}