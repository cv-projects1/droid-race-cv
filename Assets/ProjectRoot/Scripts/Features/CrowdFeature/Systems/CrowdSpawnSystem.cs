using Entitas;
using System.Collections.Generic;
using MobRun;
using UnityEngine;
using UnityEngine.Rendering;
using vailshnast.entitasview;
using vailshnast.extensions;

public class CrowdSpawnSystem : ReactiveSystem<GameEntity>
{
    private readonly GameConfig _gameConfig;
    private readonly AnimationConfig _animationConfig;
    private readonly HapticConfig _hapticConfig;
    private readonly IGroup<GameEntity> _group;
    private readonly IGameFactoryService _gameFactoryService;
    private readonly IHapticsService _hapticsService;
    private readonly IGameSoundService _gameSoundService;
    private readonly GameContext _gameContext;

    public CrowdSpawnSystem(Contexts contexts,
                            GameConfig gameConfig,
                            AnimationConfig animationConfig,
                            HapticConfig hapticConfig,
                            IGameFactoryService gameFactoryService,
                            IHapticsService hapticsService,
                            IGameSoundService gameSoundService) :
        base(contexts.game)
    {
        _gameConfig = gameConfig;
        _animationConfig = animationConfig;
        _hapticConfig = hapticConfig;
        _gameFactoryService = gameFactoryService;
        _hapticsService = hapticsService;
        _gameSoundService = gameSoundService;
        _gameContext = contexts.game;

        _group = _gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.CrowdUnit)
                                                  .NoneOf(GameMatcher.Destroyed));

        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.CrowdSpawn);

    protected override bool Filter(GameEntity entity) =>
        entity.hasCrowdSpawn;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            _hapticsService.CreateHaptic(_hapticConfig.RobotAdd);
            _gameSoundService.PlaySound(SoundType.RobotSpawn);

            for (int i = 0; i < entity.crowdSpawn.Count; i++)
            {
                var unitsCount = _group.count;
                CreateCrowdUnit(unitsCount, entity);
            }
        }
    }

    private void CreateCrowdUnit(int index, GameEntity spawnEntity)
    {
        var phyllatoxisPos = _gameConfig.GetPhyllotaxisPos(index);
        var pos = new Vector3(phyllatoxisPos.x, 0, phyllatoxisPos.y);
        var entityView = _gameFactoryService.CreateCrowdUnit();
        var unitEntity = _gameContext.CreateEntity();
        
        entityView.Link(unitEntity);

        var rigidbody = entityView.GetComponentView<IRigidbody>();
        rigidbody.RigidbodyValue.isKinematic = false;

        rigidbody.RigidbodyValue.constraints =
            (RigidbodyConstraints) 120;
        
        unitEntity.AddEntityView(entityView);
        unitEntity.AddLocalPosition(pos);
        unitEntity.AddCrowdLocalTarget(pos);
        unitEntity.AddCrowdUnitIndex(index);
        unitEntity.AddRigidbody(rigidbody);
        unitEntity.AddShadow(ShadowCastingMode.On);
        unitEntity.isCrowdUnitGrounded = true;
        unitEntity.isCrowdUnit = true;

        if (!spawnEntity.isCrowdStartUnit)
        {
            var transform = entityView.GetComponentView<ITransform>().TransformValue;
            var tweenEntity = _gameContext.CreateEntity();
            var tween = _animationConfig.UnitSpawnAnimSettings.CreatePunchScaleTweenFromSettings(transform);
            tweenEntity.AddTween(tween);
        }
    }
}