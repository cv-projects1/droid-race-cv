using Entitas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowdUnitFallenSystem : ReactiveSystem<GameEntity>
{
    public CrowdUnitFallenSystem(Contexts contexts) : base(contexts.game)
    {
        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.TweenRunning.Removed());

    protected override bool Filter(GameEntity entity) =>
        entity.isCrowdUnitFallen && !entity.isTweenRunning;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            // do stuff to the matched entities
            entity.isDestroyed = true;
        }
    }
}
