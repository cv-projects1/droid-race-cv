using Entitas;
using UnityEngine;
using vailshnast.entitasview;

public class EntityViewDestroyListener : MonoBehaviour, IEventListener, IDestroyedListener
{
    private GameEntity _entity;

    public void RegisterListeners(IEntity entity)
    {
        _entity = (GameEntity) entity;
        _entity.AddDestroyedListener(this);
    }

    public void RemoveListeners()
    {
        _entity.RemoveDestroyedListener(this);
    }

    public void OnDestroyed(GameEntity entity) => Destroy(gameObject);
}
