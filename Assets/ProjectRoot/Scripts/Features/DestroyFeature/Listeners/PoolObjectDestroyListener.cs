using Entitas;
using UnityEngine;
using vailshnast.entitasview;
using vailshnast.pool;

public class PoolObjectDestroyListener : MonoBehaviour, IEventListener, IDestroyedListener
{
    [SerializeField] private PoolBehaviour _basePooledObject;
    private GameEntity _entity;

    public void RegisterListeners(IEntity entity)
    {
        _entity = (GameEntity) entity;
        _entity.AddDestroyedListener(this);
    }

    public void RemoveListeners() => _entity.RemoveDestroyedListener(this);
    public void OnDestroyed(GameEntity entity) => _basePooledObject.Destroy();
}
