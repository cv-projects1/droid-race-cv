using System.Collections.Generic;
using Entitas;
using vailshnast.entitasview;

namespace Systems
{
    public class DestroyEntitiesSystem : ReactiveSystem<GameEntity>
    {
        public DestroyEntitiesSystem(Contexts contexts) : base(contexts.game) { }
        
        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
            context.CreateCollector(GameMatcher.Destroyed);

        protected override bool Filter(GameEntity entity) => entity.isDestroyed;

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                var entityView = entity.hasEntityView ? entity.entityView.Value : null;
                
                if(entity.hasDestroyedListener)
                    foreach (var destroyedListener in entity.destroyedListener.value)
                        destroyedListener.OnDestroyed(entity);
                
                entityView?.Unlink();
                entity.Destroy();
            }
        }
    }
}