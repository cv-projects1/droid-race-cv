using System.Collections.Generic;
using Entitas;

namespace Systems
{
    public class DestroyWorldSystem : ITearDownSystem
    {
        private readonly List<GameEntity> _buffer = new List<GameEntity>();
        private readonly IGroup<GameEntity> _entities;

        public DestroyWorldSystem(Contexts contexts) =>
            _entities = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.EntityView).NoneOf(GameMatcher.Destroyed));

        public void TearDown()
        {
            foreach (var entity in _entities.GetEntities(_buffer))
            {
                var entityView = entity.hasEntityView ? entity.entityView.Value : null;

                if (entity.hasDestroyedListener)
                    foreach (var destroyedListener in entity.destroyedListener.value)
                        destroyedListener.OnDestroyed(entity);

                entityView?.Unlink();
                entity.Destroy();
            }
        }
    }
}