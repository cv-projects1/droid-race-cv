using DG.Tweening;
using Entitas;

[Game]
public sealed class TweenComponent : IComponent
{
    public Tween Value;
}