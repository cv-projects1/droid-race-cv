using Entitas;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TweenAddedSystem : ReactiveSystem<GameEntity>
{
    public TweenAddedSystem(Contexts contexts) : base(contexts.game)
    {
        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.Tween.Added());

    protected override bool Filter(GameEntity entity) => entity.hasTween &&
                                                         !entity.isTweenRunning;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            // do stuff to the matched entities
            entity.isTweenRunning = true;
            entity.tween.Value.SetUpdate(UpdateType.Manual);
        }
    }
}
