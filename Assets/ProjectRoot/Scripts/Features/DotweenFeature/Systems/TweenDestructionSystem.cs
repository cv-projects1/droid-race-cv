using System.Collections.Generic;
using DG.Tweening;
using Entitas;

public class TweenDestructionSystem : ITearDownSystem
{
    private readonly List<GameEntity> _buffer = new List<GameEntity>();
    private readonly IGroup<GameEntity> _entities;

    public TweenDestructionSystem(Contexts contexts) =>
        _entities = contexts.game.GetGroup(GameMatcher.Tween);

    public void TearDown()
    {
        foreach (var entity in _entities.GetEntities(_buffer))
        {
            var tween = entity.tween;
            
            tween.Value?.Kill();
            entity.Destroy();
        }
    }
}
