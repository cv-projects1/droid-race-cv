using System.Collections.Generic;
using DG.Tweening;
using Entitas;
using UnityEngine;

public class TweenExecutingSystem : IExecuteSystem
{
    private readonly List<GameEntity> _buffer = new List<GameEntity>();
    private readonly IGroup<GameEntity> _entities = null;
    private readonly GameContext _gameContext;


    public TweenExecutingSystem(GameContext gameContext)
    {
        _gameContext = gameContext;

        _entities = _gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.Tween,
                                                            GameMatcher.TweenRunning));
    }

    public void Execute()
    {
        var delta = _gameContext.timeDelta.Value;
        
        foreach (var entity in _entities.GetEntities(_buffer))
        {
            var tween = entity.tween.Value;
            tween.ManualUpdate(delta, Time.unscaledDeltaTime);

            if (!tween.IsActive() /*|| !tween.IsPlaying() || tween.IsComplete()*/)
            {
                entity.isTweenRunning = false;
                entity.isDestroyed = true;
            }
        }
    }
}