using System.Collections.Generic;
using Zenject;
using UnityEngine;
using vailshnast.entitasview;

namespace MobRun
{
    public class FinishPrizeBoxBlueprint : MonoBehaviour, IEntityBlueprint
    {
        [SerializeField] private EntityView _entityView;
        
        private GameContext _gameContext;

        [Inject]
        private void Construct(IEntityBlueprintService entityBlueprintService,
                               GameContext gameContext)
        {
            _gameContext = gameContext;
            entityBlueprintService.AddBlueprint(this);
        }

        public void Initialize()
        {
            var entity = _gameContext.CreateEntity();

            entity.isFinishPrizeBox = true;
            entity.AddEntityView(_entityView);
            entity.AddWorldPosition(transform.position);

            _entityView.Link(entity);
        }
    }
}