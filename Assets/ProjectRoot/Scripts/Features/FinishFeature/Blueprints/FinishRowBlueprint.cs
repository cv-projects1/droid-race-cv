using System.Collections.Generic;
using UnityEngine;
using vailshnast.entitasview;
using Zenject;

namespace MobRun
{
    public class FinishRowBlueprint : MonoBehaviour, IEntityBlueprint
    {
        [SerializeField] private EntityView _selfView;
        [SerializeField] private EntityView[] _boxes;

        private GameContext _gameContext;

        [Inject]
        public void Construct(IEntityBlueprintService entityBlueprintService,
                              GameContext gameContext)
        {
            _gameContext = gameContext;
            entityBlueprintService.AddBlueprint(this);
        }

        public void Initialize()
        {
            var rowEntity = _gameContext.CreateEntity();
            rowEntity.isFinishRow = true;
            rowEntity.AddEntityView(_selfView);
            rowEntity.AddWorldPosition(_selfView.transform.position);
            _selfView.Link(rowEntity);

            for (int i = 0; i < _boxes.Length; i++)
            {
                var entityView = _boxes[i];
                if(entityView == null) continue;
                var boxEntity = _gameContext.CreateEntity();

                boxEntity.AddFinishBox(rowEntity.id.Value);
                boxEntity.AddFinishBoxMovingUnits(0);
                boxEntity.AddFinishBoxReachedUnitsAmount(0);
                boxEntity.AddEntityView(entityView);
                boxEntity.AddWorldPosition(entityView.transform.position);
                boxEntity.AddFinishBoxUnits(new List<int>());
                //boxEntity.AddWorldPosition(entityView.transform.position);

                entityView.Link(boxEntity);
            }
        }
    }
}