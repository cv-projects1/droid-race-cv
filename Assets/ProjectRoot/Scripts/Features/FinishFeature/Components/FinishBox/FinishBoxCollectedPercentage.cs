using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique, Event(EventTarget.Any)]
public sealed class FinishBoxCollectedPercentage : IComponent
{
    public float Value;
}