using Entitas;

[Game]
public sealed class FinishBoxReachedUnitsAmount : IComponent
{
    public int Value;
}