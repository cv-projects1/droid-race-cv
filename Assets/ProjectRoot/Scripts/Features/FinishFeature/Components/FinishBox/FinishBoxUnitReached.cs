using Entitas;

[Game]
public sealed class FinishBoxUnitReached : IComponent
{
    public int BoxID;
    public int UnitID;
}