using System.Collections.Generic;
using Entitas;

[Game]
public sealed class FinishBoxUnitsComponent : IComponent
{
    public List<int> UnitsIndexes = new List<int>();
}