using Entitas;
using UnityEngine;

[Game]
public sealed class FinishLineCollision : IComponent
{
    public Vector3 CenterPosition;
}