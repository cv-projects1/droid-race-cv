using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Event(EventTarget.Any), Cleanup(CleanupMode.RemoveComponent)]
public sealed class FinishRowCollision : IComponent
{
    public int FinishRowIndex;
}