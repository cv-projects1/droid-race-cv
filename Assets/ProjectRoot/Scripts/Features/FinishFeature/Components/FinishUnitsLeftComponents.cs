using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public sealed class FinishUnitsLeftComponents : IComponent
{
    public int Value;
}