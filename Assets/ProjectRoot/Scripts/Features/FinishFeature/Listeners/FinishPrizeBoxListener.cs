using System.Collections.Generic;
using DG.Tweening;
using Entitas;
using UnityEngine;
using vailshnast.entitasview;

public class FinishPrizeBoxListener : MonoBehaviour,
                                      IEventListener,
                                      IAnyFinishPrizeBoxReachedListener
{
    [SerializeField] private DOTweenAnimation _animation;
    [SerializeField] private List<ParticleSystem> _particleSystems;
    
    private GameEntity _entity;

    public void RegisterListeners(IEntity entity)
    {
        _entity = (GameEntity) entity;
        _entity.AddAnyFinishPrizeBoxReachedListener(this);
    }

    public void RemoveListeners()
    {
        _entity.RemoveAnyFinishPrizeBoxReachedListener(this);
    }

    public void OnAnyFinishPrizeBoxReached(GameEntity entity)
    {
        foreach (var particle in _particleSystems) particle.Play();

        _animation.DOPlay();
    }
}
