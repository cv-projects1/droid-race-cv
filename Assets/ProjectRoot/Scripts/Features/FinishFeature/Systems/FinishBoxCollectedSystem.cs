using Entitas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishBoxCollectedSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext _gameContext;
    private readonly IGroup<GameEntity> _collectedBoxesGroup;
    private readonly IGroup<GameEntity> _totalBoxesGroup;

    public FinishBoxCollectedSystem(Contexts contexts) : base(contexts.game)
    {
        _gameContext = contexts.game;

        _collectedBoxesGroup = _gameContext.GetGroup(GameMatcher.FinishBoxFlying);
        _totalBoxesGroup = _gameContext.GetGroup(GameMatcher.FinishBox);

        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.FinishBoxFlying);

    protected override bool Filter(GameEntity entity) =>
        entity.isFinishBoxFlying;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            var collectedCount = _collectedBoxesGroup.count;
            var totalCount = _totalBoxesGroup.count;

            _gameContext.ReplaceFinishBoxCollectedPercentage((float) collectedCount / totalCount);
            // do stuff to the matched entities
        }
    }
}