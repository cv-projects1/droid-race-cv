using Entitas;
using System.Collections.Generic;
using DG.Tweening;
using MobRun;
using UnityEngine;
using UnityEngine.Rendering;
using vailshnast.extensions;

public class FinishBoxFlyingSystem : ReactiveSystem<GameEntity>
{
    private readonly GameConfig _gameConfig;
    private readonly AnimationConfig _animationConfig;
    private readonly GameContext _gameContext;
    private readonly PlayerModel _playerModel;
    private readonly IUIAnimationService _uiAnimationService;
    private readonly ITweenEntityService _tweenEntityService;
    private readonly IGroup<GameEntity> _unitsAmount;

    public FinishBoxFlyingSystem(Contexts contexts,
                                 GameConfig gameConfig,
                                 AnimationConfig animationConfig,
                                 GameContext gameContext,
                                 PlayerModel playerModel,
                                 IUIAnimationService uiAnimationService,
                                 ITweenEntityService tweenEntityService) : base(contexts.game)
    {
        _gameConfig = gameConfig;
        _animationConfig = animationConfig;
        _gameContext = gameContext;
        _playerModel = playerModel;
        _uiAnimationService = uiAnimationService;
        _tweenEntityService = tweenEntityService;
        _unitsAmount = _gameContext.GetGroup(GameMatcher.CrowdUnit);

        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.FinishBoxReachedUnitsAmount);

    protected override bool Filter(GameEntity entity) =>
        entity.hasFinishBoxReachedUnitsAmount && !entity.isFinishBoxFlying;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (_unitsAmount.count > 0)
                if (entity.finishBoxReachedUnitsAmount.Value < _gameConfig.CrowdAmountPerBox)
                    continue;

            var units = entity.finishBoxUnits.UnitsIndexes;

            foreach (var unit in units)
            {
                var unitEntity = _gameContext.GetEntityWithId(unit);
                unitEntity.isCrowdUnitFlying = true;
            }

            var currencyEntityTween = _gameContext.CreateEntity();

            var currencyTween = _uiAnimationService.CreateAnimatedCogs(entity.worldPosition.Value, 0.07f, 0.5f,
                                                                       _gameConfig.SmallBoxCurrencyAmount,
                                                                       AddPlayerCurrency);

            currencyEntityTween.AddTween(currencyTween);
            currencyEntityTween.isFinishCurrencyTween = true;
            entity.isFinishBoxFlying = true;
            CreateFlyTween(entity);
        }
    }

    private void CreateFlyTween(GameEntity entity)
    {
        var finishRowPos = _gameContext.GetEntityWithId(entity.finishBox.RowIndex).worldPosition.Value;
        var startDirection = Vector3.forward;
        var centerDirection = (entity.worldPosition.Value - finishRowPos).normalized;
        var rotatedDirection = Quaternion.Euler(_animationConfig.FinishBoxRotation,/* centerDirection.x * 45*/0, 0) * startDirection;
        var finalPos = entity.worldPosition.Value + rotatedDirection * _animationConfig.FinishBoxFlyDistance;
        var tweenEntity = _gameContext.CreateEntity();

        //var endPos = entity.worldPosition.Value + Vector3.up * _animationConfig.FinishBoxFlyDistance;

        var tween = _tweenEntityService.CreateMoveTweenFromEntity(entity, finalPos,
                                                                  _animationConfig.FinishBoxAnimSettings.Duration)
                                       .OnComplete(() => entity.ReplaceShadow(ShadowCastingMode.Off));

        tween.ApplyCurve(_animationConfig.FinishBoxAnimSettings);
        tweenEntity.AddTween(tween);
        tweenEntity.isFinishBoxEndedFly = true;
    }

    private void AddPlayerCurrency()
    {
        _playerModel.AdjustCurrencyAmount(1);
        _gameContext.ReplaceFinishCurrency(_gameContext.finishCurrency.Value + 1);
    }
}