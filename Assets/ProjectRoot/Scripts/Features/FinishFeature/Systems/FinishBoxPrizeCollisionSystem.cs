using Entitas;
using System.Collections.Generic;
using DG.Tweening;
using Lofelt.NiceVibrations;
using MobRun;
using vailshnast.statemachines;

public class FinishBoxPrizeCollisionSystem : ReactiveSystem<GameEntity>
{
    private readonly IUIAnimationService _uiAnimationService;
    private readonly IStateMachine _stateMachine;
    private readonly IHapticsService _hapticsService;
    private readonly IGameSoundService _gameSoundService;
    private readonly GameContext _gameContext;
    private readonly PlayerModel _playerModel;
    private readonly GameConfig _gameConfig;
    private readonly HapticConfig _hapticConfig;

    public FinishBoxPrizeCollisionSystem(Contexts contexts,
                                         PlayerModel playerModel,
                                         GameConfig gameConfig,
                                         HapticConfig hapticConfig,
                                         IUIAnimationService uiAnimationService,
                                         IStateMachine stateMachine,
                                         IHapticsService hapticsService,
                                         IGameSoundService gameSoundService) : base(
        contexts.game)
    {
        _playerModel = playerModel;
        _gameConfig = gameConfig;
        _hapticConfig = hapticConfig;
        _uiAnimationService = uiAnimationService;
        _stateMachine = stateMachine;
        _hapticsService = hapticsService;
        _gameSoundService = gameSoundService;
        _gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.AllOf(GameMatcher.CollisionEnter,
                                                  GameMatcher.FinishPrizeBoxCollision));

    protected override bool Filter(GameEntity entity) =>
        entity.isFinishPrizeBoxCollision && entity.hasCollisionEnter;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            // do stuff to the matched entities
            var crowdTargetEntity = _gameContext.GetEntityWithId(entity.collisionEnter.ListenerId);

            if (!crowdTargetEntity.isCrowdGlobalTarget) continue;

            _gameSoundService.PlaySound(SoundType.FinishPrizeBoxReached);
            _hapticsService.CreateHaptic(_hapticConfig.FinishPrizeBox);

            crowdTargetEntity.crowdGlobalTargetSpeed.Value = 0;
            _gameContext.CreateEntity().isFinishPrizeBoxReached = true;

            var prizePos = _gameContext.finishPrizeBoxEntity
                                       .worldPosition
                                       .Value;

            var tween = _uiAnimationService.CreateAnimatedCogs(prizePos, 0.07f, 0.5f,
                                                               _gameConfig.BigBoxCurrencyAmount,
                                                               AddPlayerCurrency);

            _gameContext.ReplaceFinishUnitsLeftComponents(_gameContext.crowdAmount.Value);

            tween.OnComplete(() => _stateMachine.EnterState<GameVictoryState>());
        }
    }

    private void AddPlayerCurrency()
    {
        _playerModel.AdjustCurrencyAmount(1);
        _gameContext.ReplaceFinishCurrency(_gameContext.finishCurrency.Value + 1);
    }
}