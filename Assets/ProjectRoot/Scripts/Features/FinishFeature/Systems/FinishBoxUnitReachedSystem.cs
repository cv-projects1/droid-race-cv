using Entitas;
using System.Collections.Generic;
using MobRun;

public class FinishBoxUnitReachedSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext _gameContext;
    private readonly HapticConfig _hapticConfig;
    private readonly IHapticsService _hapticsService;

    public FinishBoxUnitReachedSystem(Contexts contexts,
                                      GameContext gameContext,
                                      HapticConfig hapticConfig,
                                      IHapticsService hapticsService) : base(contexts.game)
    {
        _gameContext = gameContext;
        _hapticConfig = hapticConfig;
        _hapticsService = hapticsService;

        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.TweenRunning.Removed());
    }

    protected override bool Filter(GameEntity entity)
    {
        return !entity.isTweenRunning && entity.hasFinishBoxUnitReached;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            _hapticsService.CreateHaptic(_hapticConfig.RobotBoxReached);

            // do stuff to the matched entities
            var box = _gameContext.GetEntityWithId(entity.finishBoxUnitReached.BoxID);
            var unit = _gameContext.GetEntityWithId(entity.finishBoxUnitReached.UnitID);

            box.ReplaceFinishBoxReachedUnitsAmount(
                box.finishBoxReachedUnitsAmount.Value + 1);

            var units = box.finishBoxUnits.UnitsIndexes;
            units.Add(unit.creationIndex);
            box.ReplaceFinishBoxUnits(units);
            
            unit.isCrowdUnitHoldingBox = true;
            unit.isCrowdPrizeUnit = false;
        }
    }
}