using Entitas;
using System.Collections.Generic;
using DG.Tweening;
using MobRun;

public class FinishLineCollisionSystem : ReactiveSystem<GameEntity>
{
    private readonly ITweenEntityService _tweenEntityService;
    private readonly IHapticsService _hapticsService;
    private readonly IGameSoundService _gameSoundService;
    private readonly AnimationConfig _animationConfig;
    private readonly HapticConfig _hapticConfig;
    private readonly GameContext _gameContext;

    public FinishLineCollisionSystem(Contexts contexts,
                                     AnimationConfig animationConfig,
                                     HapticConfig hapticConfig,
                                     ITweenEntityService tweenEntityService,
                                     IHapticsService hapticsService,
                                     IGameSoundService gameSoundService) : base(contexts.game)
    {
        _tweenEntityService = tweenEntityService;
        _hapticsService = hapticsService;
        _gameSoundService = gameSoundService;
        _animationConfig = animationConfig;
        _hapticConfig = hapticConfig;
        _gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.AllOf(GameMatcher.CollisionEnter,
                                                  GameMatcher.FinishLineCollision));

    protected override bool Filter(GameEntity entity) =>
        entity.hasCollisionEnter && entity.hasFinishLineCollision;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            // do stuff to the matched entities
            var listenerEntity = _gameContext.GetEntityWithId(entity.collisionEnter.ListenerId);

            if (!listenerEntity.isCrowdGlobalTarget) continue;

            _hapticsService.CreateHaptic(_hapticConfig.FinishLine);
            _gameSoundService.PlaySound(SoundType.FinishLineReached);

            /*var tween = _tweenEntityService.CreateMoveTweenFromEntity(
                listenerEntity, entity.finishLineCollision.CenterPosition, _animationConfig.CenteringSpeed);

            var tweenEntity = _gameContext.CreateEntity();
            
            tweenEntity.AddTween(tween);
            tweenEntity.isFinishLineReachedTween = true;

            tween.SetSpeedBased();*/

            _gameContext.SetFinishLineReached(entity.finishLineCollision.CenterPosition);
        }
    }
}