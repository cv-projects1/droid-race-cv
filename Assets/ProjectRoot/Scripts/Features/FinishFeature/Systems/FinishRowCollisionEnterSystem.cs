using Entitas;
using System.Collections.Generic;
using DG.Tweening;
using MobRun;
using UnityEngine;
using UnityEngine.Rendering;
using vailshnast.entitasview;

public class FinishRowCollisionEnterSystem : ReactiveSystem<GameEntity>
{
    private readonly GameConfig _gameConfig;
    private readonly IEntityCreationService _entityCreationService;
    private readonly GameContext _gameContext;
    private readonly IGroup<GameEntity> _group;
    private readonly List<GameEntity> _buffer = new List<GameEntity>();

    public FinishRowCollisionEnterSystem(Contexts contexts, GameConfig gameConfig,
                                         IEntityCreationService entityCreationService) : base(contexts.game)
    {
        _gameConfig = gameConfig;
        _entityCreationService = entityCreationService;
        _gameContext = contexts.game;

        _group = _gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.FinishBox)
                                                  .NoneOf(GameMatcher.FinishBoxFlying));
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.AllOf(GameMatcher.CollisionEnter,
                                                  GameMatcher.FinishRowCollision));

    protected override bool Filter(GameEntity entity) =>
        entity.hasFinishRowCollision && entity.hasCollisionEnter;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            // do stuff to the matched entities
            var crowdUnitEntity = _gameContext.GetEntityWithId(entity.collisionEnter.ListenerId);

            if (!crowdUnitEntity.isCrowdUnit) continue;

            var crowdEntityView = crowdUnitEntity.entityView.Value;
            var crowdUnitTransform = crowdEntityView.GetComponentView<ITransform>().TransformValue;
            var closestBox = GetClosestBox(entity.finishRowCollision.FinishRowIndex, crowdUnitTransform.position);

            if (closestBox == null) continue;

            var runningUnitsAmount = closestBox.finishBoxMovingUnits.Value;
            closestBox.ReplaceFinishBoxMovingUnits(closestBox.finishBoxMovingUnits.Value + 1);

            var closestBoxTransform = closestBox.entityView.Value.GetComponentView<ITransform>()
                                                .TransformValue;

            _entityCreationService.RemoveCrowdUnit(crowdUnitEntity);
            
            crowdUnitEntity.isCrowdPrizeUnit = true;
            crowdUnitTransform.SetParent(closestBoxTransform);
            var targetPos = GetUnitPosition(runningUnitsAmount);

            var tween = crowdUnitTransform.DOLocalMove(targetPos, _gameConfig.CrowdFinishMoveSpeed)
                                          .SetSpeedBased().OnComplete(() => crowdUnitEntity.ReplaceShadow(ShadowCastingMode.Off));

            var tweenEntity = _gameContext.CreateEntity();
            tweenEntity.AddTween(tween);

            tweenEntity.AddFinishBoxUnitReached(closestBox.creationIndex,
                                                crowdUnitEntity.creationIndex);
        }
    }

    private Vector3 GetUnitPosition(int index)
    {
        var offset = 1.35f;
        index++;
        var side = index % 2 == 0 ? 1 : -1;
        var finalOffset = side * Mathf.CeilToInt(index / 2) * offset;
        var startPos = new Vector3(finalOffset, 0, -3);

        return startPos;
    }

    private GameEntity GetClosestBox(int finishRowIndex, Vector3 pos)
    {
        var entitiesList = _group.GetEntities(_buffer);

        if (entitiesList.Count <= 0) return null;

        GameEntity closestEntity = null;

        foreach (var entity in _group.GetEntities(_buffer))
        {
            if (entity.finishBox.RowIndex != finishRowIndex) continue;

            if (closestEntity == null)
            {
                if (entity.finishBoxMovingUnits.Value < _gameConfig.CrowdAmountPerBox)
                    closestEntity = entity;

                continue;
            }

            if (entity.finishBoxMovingUnits.Value >= _gameConfig.CrowdAmountPerBox)
                continue;
            
            if (entity.finishBoxMovingUnits.Value > closestEntity.finishBoxMovingUnits.Value)
                closestEntity = entity;
        }

        return closestEntity;
    }
}