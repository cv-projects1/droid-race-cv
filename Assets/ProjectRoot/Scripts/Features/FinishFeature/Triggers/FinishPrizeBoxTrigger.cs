using System;
using UnityEngine;
using Zenject;

public class FinishPrizeBoxTrigger : MonoBehaviour
{
    private IEntityProviderService _entityService;

    [Inject]
    private void Construct(IEntityProviderService entityProviderService) =>
        _entityService = entityProviderService;
    
    private void OnTriggerEnter(Collider other)
    {
        if (gameObject == null || other.gameObject == null) return;

        var entityProvider = _entityService.GetEntityProvider(other.gameObject.GetInstanceID());

        if (entityProvider == null) return;

        var collisionEntity = Contexts.sharedInstance.game.CreateEntity();
        collisionEntity.AddCollisionEnter(-1, entityProvider.GetEntity<GameEntity>().creationIndex);
        collisionEntity.isFinishPrizeBoxCollision = true;
    }
}