using UnityEngine;
using vailshnast.entitasview;
using Zenject;

public class FinishRowTrigger : MonoBehaviour
{
    [SerializeField] private EntityView _entityView;
    private IEntityProviderService _entityService;

    [Inject]
    private void Construct(IEntityProviderService entityProviderService) =>
        _entityService = entityProviderService;

    private void OnTriggerEnter(Collider other)
    {
        if (gameObject == null || other.gameObject == null) return;

        var entityProvider = _entityService.GetEntityProvider(other.gameObject.GetInstanceID());

        if (entityProvider == null) return;

        var collisionEntity = Contexts.sharedInstance.game.CreateEntity();

        collisionEntity.AddCollisionEnter(_entityView.GetEntity<GameEntity>().creationIndex,
                                          entityProvider.GetEntity<GameEntity>().creationIndex);
        
        collisionEntity.AddFinishRowCollision(_entityView.GetEntity<GameEntity>().creationIndex);
    }
}