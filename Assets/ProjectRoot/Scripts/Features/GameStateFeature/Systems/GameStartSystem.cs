using System.Collections.Generic;
using Entitas;
using MobRun;
using UnityEngine;
using vailshnast.entitasview;

public class GameStartSystem : IInitializeSystem
{
    private readonly GameContext _gameContext;
    private readonly GameConfig _gameConfig;
    private readonly IGameModuleService _gameModuleService;
    private readonly IUpgradeService _upgradeService;

    public GameStartSystem(GameContext gameContext,
                           GameConfig gameConfig,
                           IGameModuleService gameModuleService,
                           IUpgradeService upgradeService)
    {
        _gameContext = gameContext;
        _gameConfig = gameConfig;
        _gameModuleService = gameModuleService;
        _upgradeService = upgradeService;
    }

    public void Initialize()
    {
        var moduleParent = _gameModuleService.Module.ModuleParent;
        var entityView = moduleParent.GetComponent<EntityView>();

        var entity = _gameContext.CreateEntity();

        entityView.Link(entity);

        entity.isCrowdGlobalTarget = true;
        entity.AddEntityView(entityView);
        entity.AddCrowdAmount(0);
        entity.AddCrowdGlobalTargetSpeed(0);
        entity.AddCrowdGlobalRadius(0);
        entity.AddWorldPosition(Vector3.zero);
        entity.AddSlidePointMagnetizing(0);
        entity.AddSlidePointsTriggeredList(new List<int>());
        entity.AddFinishBoxCollectedPercentage(0);
        entity.AddFinishUnitsLeftComponents(0);
        entity.AddFinishCurrency(0);
        entity.AddTransformView(entityView.GetComponentView<ITransform>());

        var timerEntity = _gameContext.CreateEntity();
        timerEntity.AddTimer(_gameConfig.CrowdStartMoveDelay);
        timerEntity.isTimerRunning = true;
        timerEntity.isStartMoveDelayTimer = true;

        _upgradeService.ApplyGameUpgrades(entity);
    }
}