using Entitas;
using System.Collections.Generic;
using MobRun;
using vailshnast.statemachines;

public class LooseSystem : IExecuteSystem
{
    private readonly IStateMachine _stateMachine;
    private readonly IGroup<GameEntity> _group;
    private readonly GameContext _gameContext;

    public LooseSystem(IStateMachine stateMachine, GameContext gameContext)
    {
        _stateMachine = stateMachine;

        _gameContext = gameContext;
        _group = gameContext.GetGroup(GameMatcher.AnyOf(GameMatcher.CrowdUnit,
                                                        GameMatcher.CrowdPrizeUnit,
                                                        GameMatcher.CrowdUnitHoldingBox));

        // pass the context of interest to the base constructor
    }

    public void Execute()
    {
        if (!_stateMachine.IsActiveStateTypeOf<GameLoopState>()) return;

        if (_group.count <= 0)
        {
            _gameContext.isGameIsEnded = true;
            _stateMachine.EnterState<GameLossState>();
        }
    }
}