using Entitas;
using MobRun;
using vailshnast.statemachines;

public class VictorySystem : IExecuteSystem
{
    private readonly IStateMachine _stateMachine;
    private readonly GameContext _gameContext;
    private readonly IGroup<GameEntity> _aliveUnitsGroup;

    public VictorySystem(Contexts contexts, IStateMachine stateMachine)
    {
        _stateMachine = stateMachine;
        _gameContext = contexts.game;

        _aliveUnitsGroup = _gameContext.GetGroup(GameMatcher.AnyOf(GameMatcher.CrowdUnit,
                                                                   GameMatcher.CrowdPrizeUnit,
                                                                   GameMatcher.FinishCurrencyTween));

        // pass the context of interest to the base constructor
    }

    public void Execute()
    {
        if (!_stateMachine.IsActiveStateTypeOf<GameLoopState>()) return;
        if (!_gameContext.hasFinishLineReached) return;
        if (_aliveUnitsGroup.count > 0) return;

        _gameContext.isGameIsEnded = true;
        _stateMachine.EnterState<GameVictoryState>();
    }
}