using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Meta]
public sealed class IdComponent : IComponent
{
    [PrimaryEntityIndex] public int Value;
}