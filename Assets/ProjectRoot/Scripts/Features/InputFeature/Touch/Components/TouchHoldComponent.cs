using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public sealed class TouchHoldComponent : IComponent
{
    public float HoldTime;
}