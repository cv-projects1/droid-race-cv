using Entitas;
using UnityEngine;
using vailshnast.eventsystem;

public class EditorTouchProcessingSystem : IExecuteSystem
{
    private readonly IEventSystemService _eventSystemService;
    private readonly GameContext _gameContext;
    private float _beginTouchTime;

    public EditorTouchProcessingSystem(GameContext gameContext, IEventSystemService eventSystemService)
    {
        _gameContext = gameContext;
        _eventSystemService = eventSystemService;
    }

    public void Execute()
    {
        if (_eventSystemService.IsPointerOverUIObject()) return;
        if (!IsMouseOverGameWindow) return;

        if (Input.GetMouseButtonDown(0))
        {
            _beginTouchTime = Time.time;
            _gameContext.CreateEntity().isTouchBegin = true;
        }
        else if (Input.GetMouseButton(0))
        {
            if (!_gameContext.hasTouchHold)
                _gameContext.SetTouchHold(0);

            _gameContext.touchHold.HoldTime = Time.time - _beginTouchTime;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            //if (_beginSingleTouchTime > 0f && Time.time - _beginSingleTouchTime > _inputConfig.TouchEndTime) return;
            _beginTouchTime = 0f;
            _gameContext.CreateEntity().isTouchEnd = true;

            if (_gameContext.hasTouchHold)
                _gameContext.RemoveTouchHold();
        }
    }

    private bool IsMouseOverGameWindow =>
        !(0 > Input.mousePosition.x || 0 > Input.mousePosition.y
                                    || Screen.width < Input.mousePosition.x
                                    || Screen.height < Input.mousePosition.y);
}