using Entitas;
using UnityEngine;
using vailshnast.eventsystem;

public class MobileTouchProcesssingSystem : IExecuteSystem
{
    private readonly GameContext _gameContext;
    private readonly IEventSystemService _eventSystemService;
    private int _touchCount = 0;
    private float _beginTouchTime;

    public MobileTouchProcesssingSystem(GameContext gameContext, IEventSystemService eventSystemService)
    {
        _gameContext = gameContext;
        _eventSystemService = eventSystemService;
    }

    public void Execute()
    {
        _touchCount = UnityEngine.Input.touchCount;

        if (_touchCount <= 0) return;
        if (_eventSystemService.IsPointerOverUIObject()) return;

        MobileTouchProcessing();
    }

    private void MobileTouchProcessing()
    {
        var touch = UnityEngine.Input.GetTouch(0);

        switch (touch.phase)
        {
            case TouchPhase.Began:
                _beginTouchTime = UnityEngine.Time.time;
                _gameContext.CreateEntity().isTouchBegin = true;
                _gameContext.ReplaceTouchHold(0);

                break;
            case TouchPhase.Moved:
            {
                break;
            }
            case TouchPhase.Stationary:
            {
                _gameContext.ReplaceTouchHold(UnityEngine.Time.time - _beginTouchTime);

                break;
            }
            case TouchPhase.Ended:
            {
                //if (_beginSingleTouchTime > 0f && Time.time - _beginSingleTouchTime > _inputConfig.TouchEndTime) return;
                _beginTouchTime = 0f;
                _gameContext.CreateEntity().isTouchEnd = true;

                if (_gameContext.hasTouchHold)
                    _gameContext.RemoveTouchHold();

                break;
            }
        }

        //Debug.Log(touch.phase);
    }
}