using Zenject;
using UnityEngine;
using vailshnast.entitasview;

namespace MobRun
{
    public class PortalBlueprint : MonoBehaviour, IEntityBlueprint
    {
        [SerializeField] private PortalTrigger[] _portalTriggers;
        [SerializeField] private EntityView _entityView;
        private GameContext _gameContext;

        [Inject]
        private void Construct(IEntityBlueprintService entityBlueprintService,
                               GameContext gameContext)
        {
            _gameContext = gameContext;
            entityBlueprintService.AddBlueprint(this);
        }

        public void Initialize()
        {
            var entity = _gameContext.CreateEntity();
            entity.AddEntityView(_entityView);
            _entityView.Link(entity);

            for (int i = 0; i < _portalTriggers.Length; i++)
            {
                _portalTriggers[i].OnPortalTriggerEvent += OnPortalTrigger;
                _portalTriggers[i].Initialize();
            }
        }

        private void OnPortalTrigger(object sender, PortalArgs portalArgs)
        {
            var collisionEntity = _gameContext.CreateEntity();

            collisionEntity.AddCollisionEnter(_entityView.GetEntity<GameEntity>().creationIndex, portalArgs
                                                 .EntityProvider.GetEntity<GameEntity>()
                                                 .creationIndex);

            collisionEntity.isPortalCollision = true;
            collisionEntity.AddPortalAmount(portalArgs.Amount);
            collisionEntity.AddPortalSign(portalArgs.PortalType);
            collisionEntity.AddPortalTrigger(portalArgs.PortalEntityIndex);
        }
    }
}