using Entitas;

[Game]
public sealed class PortalAmountComponent : IComponent
{
    public int Value;
}