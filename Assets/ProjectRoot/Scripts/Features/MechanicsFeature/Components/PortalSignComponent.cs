using Entitas;
using MobRun;

[Game]
public sealed class PortalSignComponent : IComponent
{
    public PortalType Value;
}