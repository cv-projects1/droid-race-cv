using Entitas;

[Game]
public sealed class PortalTriggerComponent : IComponent
{
    public int EntityIndex;
}