namespace MobRun
{
    public enum PortalType
    {
        Add          = 0,
        Substract    = 1,
        Multiplicate = 2,
        Divide       = 3
    }
}