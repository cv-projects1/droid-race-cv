using Entitas;
using UnityEngine;
using vailshnast.entitasview;

public class PortalToggledListener : MonoBehaviour, IEventListener, IPortalToggledListener
{
    [SerializeField] private Collider[] _colliders; 
        
    private GameEntity _entity;

    public void RegisterListeners(IEntity entity)
    {
        _entity = (GameEntity) entity;
        _entity.AddPortalToggledListener(this);
    }

    public void RemoveListeners()
    {
        _entity.RemovePortalToggledListener(this);
    }

    public void OnPortalToggled(GameEntity entity)
    {
        foreach (var portalCollider in _colliders) 
            portalCollider.enabled = false;
    }
}
