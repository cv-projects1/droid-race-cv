using System;
using Entitas;
using System.Collections.Generic;
using MobRun;

public class PortalProcessingSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext _gameContext;
    private readonly IGroup<GameEntity> _group;

    public PortalProcessingSystem(Contexts contexts) : base(contexts.game)
    {
        _gameContext = contexts.game;
        _group = _gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.CrowdUnit).NoneOf(GameMatcher.Destroyed));

        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.AllOf(GameMatcher.CollisionEnter,
                                                         GameMatcher.PortalCollision));
    }

    protected override bool Filter(GameEntity entity) =>
        entity.isPortalCollision &&
        entity.hasCollisionEnter &&
        entity.hasPortalAmount &&
        entity.hasPortalSign;

    protected override void Execute(List<GameEntity> entities)
    {
        int GetMultiplicativeAmount(GameEntity entity) =>
            (_group.count * entity.portalAmount.Value) - _group.count;

        int GetDivideAmount(GameEntity entity) =>
            _group.count - (_group.count / entity.portalAmount.Value);

        foreach (var entity in entities)
        {
            // do stuff to the matched entities
            var triggerEntity = _gameContext.GetEntityWithId(entity.collisionEnter.ListenerId);
            var portalEntity = _gameContext.GetEntityWithId(entity.collisionEnter.TriggerId);
            var portalTrigger = _gameContext.GetEntityWithId(entity.portalTrigger.EntityIndex);

            if (portalEntity.isPortalToggled) return;
            if (!triggerEntity.isCrowdGlobalTarget) continue;
            
            switch (entity.portalSign.Value)
            {
                case PortalType.Add:
                    _gameContext.CreateEntity().AddCrowdSpawn(entity.portalAmount.Value);

                    break;
                case PortalType.Substract:
                    _gameContext.CreateEntity().AddCrowdKill(entity.portalAmount.Value);

                    break;
                case PortalType.Multiplicate:
                    _gameContext.CreateEntity().AddCrowdSpawn(GetMultiplicativeAmount(entity));

                    break;
                case PortalType.Divide:
                    _gameContext.CreateEntity().AddCrowdKill(GetDivideAmount(entity));

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            portalEntity.isPortalToggled = true;
            portalTrigger.isPortalToggled = true;
        }
    }
}