using System;
using DG.Tweening;
using MobRun;
using TMPro;
using UnityEngine;
using Zenject;

namespace MobRun
{
    public class PortalTrigger : MonoBehaviour , IPortalToggledListener
    {
        [SerializeField] private PortalType _portalType;
        [SerializeField] private int _amount;
        [SerializeField] private TMP_Text _text;
        private IEntityProviderService _entityService;
        public EventHandler<PortalArgs> OnPortalTriggerEvent;
        private GameContext _gameContext;
        private GameEntity _entity;

        [Inject]
        private void Construct(IEntityProviderService entityProviderService, GameContext gameContext)
        {
            _gameContext = gameContext;
            _entityService = entityProviderService;
        }

        public void Initialize()
        {
            _entity = _gameContext.CreateEntity();
            _entity.AddPortalToggledListener(this);
        }

        public void OnPortalToggled(GameEntity entity)
        {
            transform.DOShakeScale(1, 0.5f);
        }

        private void Start()
        {
            switch (_portalType)
            {
                case PortalType.Add:
                    _text.SetText($"+{_amount}");

                    break;
                case PortalType.Substract:
                    _text.SetText($"-{_amount}");

                    break;
                case PortalType.Multiplicate:
                    _text.SetText($"x{_amount}");

                    break;
                case PortalType.Divide:
                    _text.SetText($"<sprite=15> {_amount}");

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (gameObject == null || other.gameObject == null) return;

            var entityProvider = _entityService.GetEntityProvider(other.gameObject.GetInstanceID());

            if (entityProvider == null) return;

            OnPortalTriggerEvent?.Invoke(this, new PortalArgs
            {
                EntityProvider = entityProvider,
                Amount = _amount,
                PortalType = _portalType,
                PortalEntityIndex = _entity.creationIndex
            });
        }
    }

    public class PortalArgs : EventArgs
    {
        public IEntityProvider EntityProvider;
        public PortalType PortalType;
        public int PortalEntityIndex;
        public int Amount;
    }
}