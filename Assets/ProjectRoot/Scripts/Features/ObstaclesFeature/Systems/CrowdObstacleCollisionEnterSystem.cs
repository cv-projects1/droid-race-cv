using System.Collections.Generic;
using Entitas;

public class CrowdObstacleCollisionEnterSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext _gameContext;
    
    public CrowdObstacleCollisionEnterSystem(Contexts contexts) : base(contexts.game) =>
        _gameContext = contexts.game;

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.AllOf(GameMatcher.CollisionEnter, 
                                                  GameMatcher.ObstacleCollision));

    protected override bool Filter(GameEntity entity) =>
        entity.hasCollisionEnter && entity.isObstacleCollision;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            // do stuff to the matched entities
            var listenerEntity = _gameContext.GetEntityWithId(entity.collisionEnter.ListenerId);

            if (listenerEntity.isCrowdUnit)
                listenerEntity.isDestroyed = true;
        }
    }
}