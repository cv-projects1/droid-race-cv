using System;
using UnityEngine;
using Zenject;

public class ObstacleTrigger : MonoBehaviour
{
    private IEntityProviderService _entityService;
    private GameContext _gameContext;

    [Inject]
    private void Construct(IEntityProviderService entityProviderService,
                           GameContext gameContext)
    {
        _entityService = entityProviderService;
        _gameContext = gameContext;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (gameObject == null || other.gameObject == null) return;

        var entityProvider = _entityService.GetEntityProvider(other.gameObject.GetInstanceID());

        if (entityProvider == null) return;

        var collisionEntity = _gameContext.CreateEntity();

        collisionEntity.AddCollisionEnter(-1, entityProvider.GetEntity<GameEntity>()
                                                            .creationIndex);

        collisionEntity.isObstacleCollision = true;
    }
}