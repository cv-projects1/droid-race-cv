using Entitas.Unity;
using MobRun;
using Zenject;
using UnityEngine;
using vailshnast.entitasview;

namespace UnityEngine
{
    public class TestBlueprint : MonoBehaviour, IEntityBlueprint
    {
        private GameContext _gameContext;

        [Inject]
        private void Construct(IEntityBlueprintService entityBlueprintService,
                               GameContext gameContext)
        {
            _gameContext = gameContext;
            entityBlueprintService.AddBlueprint(this);
        }

        public void Initialize()
        {
            var entity = _gameContext.CreateEntity();
            entity.AddRigidbody(GetComponent<IRigidbody>());
            entity.AddLaunch(transform.forward * 10, transform.position, 2, 45);
            entity.AddGravityScale(2);
            gameObject.Link(entity);
        }
    }
}