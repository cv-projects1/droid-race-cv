using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Game]
public sealed class ForceConstantComponent : IComponent
{
    public Vector3 Value;
}