using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Game, Cleanup(CleanupMode.RemoveComponent)]
public sealed class ForceOnceComponent : IComponent
{
    public Vector3 Value;
}