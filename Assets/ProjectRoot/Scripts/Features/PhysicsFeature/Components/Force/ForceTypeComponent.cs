using Entitas;
using UnityEngine;

[Game]
public sealed class ForceModeComponent : IComponent
{
    public ForceMode Value;
}