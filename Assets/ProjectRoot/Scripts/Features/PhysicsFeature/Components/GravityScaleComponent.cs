using Entitas;

[Game]
public sealed class GravityScaleComponent : IComponent
{
    public float Value;
}