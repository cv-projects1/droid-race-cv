using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Game/*, Cleanup(CleanupMode.RemoveComponent)*/]
public sealed class LaunchComponent : IComponent
{
    public Vector3 TargetPoint, StartPoint;
    public float GravityScale;
    public float Angle;
}