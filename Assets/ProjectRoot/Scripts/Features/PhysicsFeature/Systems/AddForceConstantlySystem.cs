using System.Collections.Generic;
using Entitas;
using Zenject;

public class AddForceConstantlySystem : IExecuteSystem
{
    private readonly List<GameEntity> _buffer = new List<GameEntity>();
    private readonly IGroup<GameEntity> _entities = null;

    //[Inject] private InputContext _inputContext;

    public AddForceConstantlySystem(Contexts contexts)
    {
        _entities = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Rigidbody, 
                                                             GameMatcher.ForceConstant, 
                                                             GameMatcher.ForceMode));
    }

    public void Execute()
    {
        //var delta = _inputContext.timeFixedDelta.Value;

        foreach (var entity in _entities.GetEntities(_buffer))
        {
            if (entity.hasRigidbody)
                entity.rigidbody.Value.RigidbodyValue.AddForce(entity.forceConstant.Value, entity.forceMode.Value);
        }
    }
}