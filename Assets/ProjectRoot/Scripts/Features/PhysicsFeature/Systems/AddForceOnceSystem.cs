using Entitas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForceOnceSystem : ReactiveSystem<GameEntity>
{
    public AddForceOnceSystem(Contexts contexts) : base(contexts.game)
    {
        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.AllOf(GameMatcher.Rigidbody, 
                                                         GameMatcher.ForceOnce,
                                                         GameMatcher.ForceMode));

    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasRigidbody && entity.hasForceOnce && entity.hasForceMode;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            entity.rigidbody.Value.RigidbodyValue.AddForce(entity.forceOnce.Value, entity.forceMode.Value);
        }
    }
}
