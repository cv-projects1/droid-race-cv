using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class GravitySystem : IExecuteSystem
{
    private readonly List<GameEntity> _buffer = new List<GameEntity>();
    private readonly IGroup<GameEntity> _entities = null;


    public GravitySystem(Contexts contexts)
    {
        _entities = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Rigidbody,GameMatcher.GravityScale));
    }

    public void Execute()
    {
        foreach (var entity in _entities.GetEntities(_buffer))
        {
            var gravity = Physics.gravity.y * entity.gravityScale.Value * Vector3.up;
            entity.rigidbody.Value.RigidbodyValue.AddForce(gravity,ForceMode.Acceleration);
        }
    }
}