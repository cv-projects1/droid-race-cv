using Entitas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchSystem : ReactiveSystem<GameEntity>
{
    public LaunchSystem(Contexts contexts) : base(contexts.game)
    {
        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.AllOf(GameMatcher.Rigidbody,
                                                         GameMatcher.Launch));

    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasRigidbody && entity.hasLaunch;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            var velocity = PhysicsExtensions.GetLaunchVelocity(
                entity.launch.StartPoint,
                entity.launch.TargetPoint,
                entity.launch.Angle,
                entity.launch.GravityScale);

            var rigidbody = entity.rigidbody.Value.RigidbodyValue;
            rigidbody.velocity = velocity;
            rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            rigidbody.isKinematic = false;
            
             //Debug.Log(velocity);
             entity.RemoveLaunch();
        }
    }
}
