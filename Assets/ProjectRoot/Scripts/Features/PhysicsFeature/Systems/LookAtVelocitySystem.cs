using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class LookAtVelocitySystem : IExecuteSystem
{
    private readonly List<GameEntity> _buffer = new List<GameEntity>();
    private readonly IGroup<GameEntity> _entities = null;

    //[Inject] private InputContext _inputContext;

    public LookAtVelocitySystem(Contexts contexts)
    {
        _entities = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Rigidbody,GameMatcher.Rotation));
    }

    public void Execute()
    {
        //var delta = _inputContext.timeDelta.Value;

        foreach (var entity in _entities.GetEntities(_buffer))
        {
            if (entity.hasRigidbody)
                entity.ReplaceRotation(Quaternion.LookRotation(entity.rigidbody.Value.RigidbodyValue.velocity));
        }
    }
}