using UnityEngine;
using vailshnast.entitasview;
using Zenject;

namespace MobRun
{
    public class SlidePointBlueprint : MonoBehaviour, IEntityBlueprint
    {
        [SerializeField] private EntityView _entityView;
        //[SerializeField] private float _radius;
        //[SerializeField] private Vector3 _offset;
        [SerializeField] private SlideDirection slideDirection;
        //public float Radius => _radius;
        //public Vector3 Offset => _offset;
        
        [Inject]
        private void Construct(IEntityBlueprintService entityBlueprintService) =>
            entityBlueprintService.AddBlueprint(this);
        
        public void Initialize()
        {
            var gameContext = Contexts.sharedInstance.game;
            var entity = gameContext.CreateEntity();
            
            entity.AddSlidePoint(transform.position);
            entity.AddSlideRotationPoint(transform.position);
            //entity.AddSlideRadius(Radius);
            entity.AddSlidePointDirection(slideDirection);
            entity.AddSlidePointActiveStatus(false);
            entity.AddEntityView(_entityView);
            
            _entityView.Link(entity);
        }
    }
}