using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public sealed class SlidePointActiveComponent : IComponent
{
    public int EntityIndex;
}