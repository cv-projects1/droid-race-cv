using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Event(EventTarget.Self)]
public sealed class SlidePointActiveStatus : IComponent
{
    public bool Value;
}