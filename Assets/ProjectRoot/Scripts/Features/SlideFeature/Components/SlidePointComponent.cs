using Entitas;
using UnityEngine;

[Game]
public sealed class SlidePointComponent : IComponent
{
    public Vector3 Value;
}