using System;
using Entitas;
using UnityEngine;

[Game]
public sealed class SlidePointDirectionComponent : IComponent
{
    public SlideDirection Value;
}

public enum SlideDirection
{
    Left  = 0,
    Right = 1
}

