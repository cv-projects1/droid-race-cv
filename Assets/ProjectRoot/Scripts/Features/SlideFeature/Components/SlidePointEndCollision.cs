using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Cleanup(CleanupMode.RemoveComponent)]
public sealed class SlidePointEndCollision : IComponent
{
    public int EntityIndex;
}