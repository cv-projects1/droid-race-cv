using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game,Cleanup(CleanupMode.RemoveComponent)]
public sealed class SlidePointStartCollision : IComponent
{
    public int EntityIndex;
}