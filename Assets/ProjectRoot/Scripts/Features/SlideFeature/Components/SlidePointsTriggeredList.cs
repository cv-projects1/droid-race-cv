using System.Collections.Generic;
using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game,Unique]
public sealed class SlidePointsTriggeredList : IComponent
{
    public List<int> EntityIndexes = new List<int>();
}