using Entitas;

[Game]
public sealed class SlideRadiusComponent : IComponent
{
    public float Value;
}