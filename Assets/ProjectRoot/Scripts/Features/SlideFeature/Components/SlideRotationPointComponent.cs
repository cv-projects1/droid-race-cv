using Entitas;
using UnityEngine;

[Game]
public sealed class SlideRotationPointComponent : IComponent
{
    public Vector3 Value;
}