using Entitas;
using UnityEngine;
using vailshnast.entitasview;

public class SlidePointListener : MonoBehaviour, IEventListener,
                                  ISlidePointActiveStatusListener
{
    [SerializeField] private ParticleSystem _particleSystem;
    [SerializeField] private MeshRenderer _meshRenderer;
    [SerializeField] private Color _defaultColor, _activeColor;
    private GameEntity _entity;

    public void RegisterListeners(IEntity entity)
    {
        _entity = (GameEntity) entity;
        _entity.AddSlidePointActiveStatusListener(this);
    }

    public void RemoveListeners()
    {
        _entity.RemoveSlidePointActiveStatusListener(this);
    }

    public void OnSlidePointActiveStatus(GameEntity entity, bool value)
    {
        if (value)
            _particleSystem.Play();
        else _particleSystem.Stop(true,ParticleSystemStopBehavior.StopEmittingAndClear);

        ChangeColor(value ? _activeColor : _defaultColor);
    }

    private void ChangeColor(Color color) =>
        _meshRenderer.material.color = color;
}