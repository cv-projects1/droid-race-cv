using Entitas;
using MobRun;
using UnityEngine;

public class SlideLineDrawSystem : IExecuteSystem , ITearDownSystem
{
    private readonly GameContext _gameContext;
    private readonly HapticConfig _hapticConfig;
    private readonly IGameModuleService _gameModuleService;
    private readonly IHapticsService _hapticsService;

    public SlideLineDrawSystem(GameContext gameContext,
                               HapticConfig hapticConfig,
                               IGameModuleService gameModuleService,
                               IHapticsService hapticsService)
    {
        _gameContext = gameContext;
        _gameModuleService = gameModuleService;
        _hapticConfig = hapticConfig;
        _hapticsService = hapticsService;
    }

    public void Execute()
    {
        var lineRenderer = _gameModuleService.Module.LineRenderer;
        
        if (_gameContext.hasSlidePointActive && _gameContext.hasTouchHold)
        {
            _hapticsService.CreateHaptic(_hapticConfig.Magnet);
            
            var slidePointIndex = _gameContext.slidePointActive.EntityIndex;
            var slidePointPos = _gameContext.GetEntityWithId(slidePointIndex).slidePoint.Value;
            var slideOffsetPos = slidePointPos + new Vector3(0, 0.5f, 0);
            var crowdPos = _gameContext.crowdGlobalTargetEntity.worldPosition.Value;
            lineRenderer.SetPosition(0, slideOffsetPos);
            lineRenderer.SetPosition(1, crowdPos);
            lineRenderer.enabled = true;
        }
        else
            lineRenderer.enabled = false;
    }

    public void TearDown()
    {
        var lineRenderer = _gameModuleService.Module.LineRenderer;
        lineRenderer.enabled = false;
    }
}