using Entitas;
using System.Collections.Generic;
using UnityEngine;

public class SlidePointEndTriggerSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext _gameContext;

    public SlidePointEndTriggerSystem(Contexts contexts, GameContext gameContext) : base(contexts.game)
    {
        _gameContext = gameContext;

        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.SlidePointEndCollision);

    protected override bool Filter(GameEntity entity) =>
        entity.hasSlidePointEndCollision && entity.hasSlidePoint;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var slidePointEntity in entities)
        {
            // do stuff to the matched entities
            var triggerEntity = _gameContext.GetEntityWithId(slidePointEntity.slidePointEndCollision.EntityIndex);

            if (triggerEntity.isCrowdGlobalTarget)
            {
                var slidePoints = _gameContext.slidePointsTriggeredList.EntityIndexes;
                var slidePointIndex = slidePointEntity.creationIndex;

                if (slidePoints.Contains(slidePointIndex))
                    slidePoints.Remove(slidePointIndex);

                _gameContext.ReplaceSlidePointsTriggeredList(slidePoints);
                
                /*if (!_gameContext.hasSlidePointActive) continue;

                var activeIndex = _gameContext.slidePointActive.EntityIndex;
                if (slidePointIndex == activeIndex) _gameContext.RemoveSlidePointActive();*/
            }
        }
    }
}