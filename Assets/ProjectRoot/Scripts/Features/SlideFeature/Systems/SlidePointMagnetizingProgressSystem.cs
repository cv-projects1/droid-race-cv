using Entitas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidePointMagnetizingProgressSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext _gameContext;

    public SlidePointMagnetizingProgressSystem(Contexts contexts) : base(contexts.game)
    {
        // pass the context of interest to the base constructor
        _gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.SlidePointActive.AddedOrRemoved());

    protected override bool Filter(GameEntity entity) => true;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            _gameContext.ReplaceSlidePointMagnetizing(0);
            // do stuff to the matched entities
        }
    }
}
