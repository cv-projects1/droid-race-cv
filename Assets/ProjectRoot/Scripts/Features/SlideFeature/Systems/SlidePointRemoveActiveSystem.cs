using Entitas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidePointRemoveActiveSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext _gameContext;

    public SlidePointRemoveActiveSystem(Contexts contexts) : base(contexts.game)
    {
        // pass the context of interest to the base constructor
        _gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.TouchEnd);

    protected override bool Filter(GameEntity entity) => entity.isTouchEnd;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            // do stuff to the matched entities
            if (!_gameContext.hasSlidePointActive) continue;

            var slidePoint = _gameContext.GetEntityWithId(_gameContext.slidePointActive.EntityIndex);
            slidePoint.ReplaceSlidePointActiveStatus(false);
            
            _gameContext.RemoveSlidePointActive();
        }
    }
}