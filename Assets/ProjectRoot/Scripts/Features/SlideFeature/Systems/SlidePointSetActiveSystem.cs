using System.Collections.Generic;
using Entitas;

public class SlidePointSetActiveSystem : IExecuteSystem
{
    private readonly GameContext _gameContext;

    public SlidePointSetActiveSystem(GameContext gameContext)
    {
        _gameContext = gameContext;

        //_entities = _gameContext.GetGroup(GameMatcher.);
    }

    public void Execute()
    {
        if (_gameContext.hasSlidePointActive) return;
        if (!_gameContext.hasTouchHold) return;

        var slidePointsTriggered = _gameContext.slidePointsTriggeredList.EntityIndexes;

        if (slidePointsTriggered.Count <= 0) return;

        var index = slidePointsTriggered[0];
        _gameContext.ReplaceSlidePointActive(index);
        var slidePoint = _gameContext.GetEntityWithId(index);
        slidePoint.ReplaceSlidePointActiveStatus(true);
    }
}