using Entitas;
using System.Collections.Generic;
using MobRun;

public class SlidePointSoundSystem : MultiReactiveSystem<GameEntity, Contexts>
{
    private readonly IGameSoundService _gameSoundService;
    private readonly GameContext _gameContext;

    public SlidePointSoundSystem(Contexts contexts, IGameSoundService gameSoundService) : base(contexts)
    {
        _gameSoundService = gameSoundService;
        // pass the context of interest to the base constructor
        _gameContext = contexts.game;
    }

    protected override ICollector[] GetTrigger(Contexts contexts)
    {
        return new ICollector[]
        {
            contexts.game.CreateCollector(GameMatcher.SlidePointActive),
            //contexts.game.CreateCollector(GameMatcher.TouchBegin.Added()),
        };
    }

    protected override bool Filter(GameEntity entity) => entity.hasSlidePointActive;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (!_gameContext.hasSlidePointActive) continue;
            if (!_gameContext.hasTouchHold) continue;

            _gameSoundService.PlaySound(SoundType.MagnetStart);
        }
    }
}