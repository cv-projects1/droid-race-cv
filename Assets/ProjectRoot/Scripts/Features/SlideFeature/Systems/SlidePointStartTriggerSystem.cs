using Entitas;
using System.Collections.Generic;

public class SlidePointStartTriggerSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext _gameContext;

    public SlidePointStartTriggerSystem(Contexts contexts, GameContext gameContext) : base(contexts.game)
    {
        _gameContext = gameContext;

        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.SlidePointStartCollision);

    protected override bool Filter(GameEntity entity) =>
        entity.hasSlidePointStartCollision && entity.hasSlidePoint;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var slidePointEntity in entities)
        {
            // do stuff to the matched entities
            var triggerEntity = _gameContext.GetEntityWithId(slidePointEntity.slidePointStartCollision.EntityIndex);

            if (triggerEntity.isCrowdGlobalTarget)
            {
                var slidePoints = _gameContext.slidePointsTriggeredList.EntityIndexes;
                
                slidePoints.Add(slidePointEntity.creationIndex);
                _gameContext.ReplaceSlidePointsTriggeredList(slidePoints);
            }
        }
    }
}
