using DG.Tweening;
using Entitas;
using MobRun;
using UnityEngine;

public class SlideProcessingSystem : IExecuteSystem
{
    private readonly GameContext _gameContext;
    private readonly GameConfig _gameConfig;
    private readonly IGameModuleService _gameModuleService;

    public SlideProcessingSystem(GameContext gameContext,
                                 GameConfig gameConfig,
                                 IGameModuleService gameModuleService)
    {
        _gameContext = gameContext;
        _gameConfig = gameConfig;
        _gameModuleService = gameModuleService;
    }

    public void Execute()
    {
        if (!_gameContext.hasSlidePointActive) return;
        if (!_gameContext.hasTouchHold) return;

        var delta = _gameContext.timeDelta.Value;

        var pointEntity = _gameContext.GetEntityWithId(
            _gameContext.slidePointActive.EntityIndex);

        var pointPosition = pointEntity.slideRotationPoint.Value;

        var pointDirection = pointEntity.slidePointDirection.Value
                                        .GetDirection();

        var targetEntity = _gameContext.crowdGlobalTargetEntity;
        var targetTransform = targetEntity.transformView.Value.TransformValue;
        var targetPosition = targetEntity.worldPosition.Value;
        var distance = Vector3.Distance(targetPosition, pointPosition);
        var magnetizingComponent = _gameContext.slidePointMagnetizing;

        var magnetizingProgress =
            Mathf.Clamp(magnetizingComponent.Progress + delta / _gameConfig.MagnetizingAccelerationDuration, 0, 1);

        /*var magnetizingForce =
            (_gameConfig.MagnetizingCurve.Evaluate(magnetizingProgress) * _gameConfig.MagnetizingForceInUnits * 10) / distance;*/
        var magnetizingForce = (_gameConfig.MagnetizingForceInUnits * 10) / distance;
        
        _gameContext.ReplaceSlidePointMagnetizing(magnetizingProgress);

        var rotatedPos = RotateAround(
            targetPosition,
            pointPosition,
            Vector3.up,
            delta *
            magnetizingForce
            * pointDirection);

        var rotation = Quaternion.LookRotation(rotatedPos - targetTransform.position);

        targetTransform.rotation = Quaternion.Lerp(targetTransform.rotation, rotation,
                                                   delta * _gameConfig.MagnetizingRotationSpeed);

        //targetTransform.LookAt(rotatedPos);
        _gameContext.crowdGlobalTargetEntity.ReplaceWorldPosition(rotatedPos);
    }

    private Vector3 RotateAround(Vector3 currentPosition, Vector3 point, Vector3 axis, float angle)
    {
        Vector3 position = currentPosition;
        Vector3 vector3 = Quaternion.AngleAxis(angle, axis) * (position - point);

        return point + vector3;
    }
}