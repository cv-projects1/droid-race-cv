using Entitas;
using UnityEngine;
using vailshnast.entitasview;

public class SlidePointTrigger : MonoBehaviour, IEventListener
{
    [SerializeField] private EntityView _entityView;
    [SerializeField] private EntityProviderTriggerEvent _startTrigger,
                                                        _endTrigger;

    private void OnStartTriggered(IEntityProvider entityProvider)
    {
        var slidePointEntity = _entityView.GetEntity<GameEntity>();
        var triggerEntity = entityProvider.GetEntity<GameEntity>();
        
        slidePointEntity.AddSlidePointStartCollision(triggerEntity.creationIndex);
    }

    private void OnEndTriggered(IEntityProvider entityProvider)
    {
        var slidePointEntity = _entityView.GetEntity<GameEntity>();
        var triggerEntity = entityProvider.GetEntity<GameEntity>();
        
        slidePointEntity.AddSlidePointEndCollision(triggerEntity.creationIndex);
    }

    public void RegisterListeners(IEntity entity)
    {
        _startTrigger.OnTriggerEnterEvent += OnStartTriggered;
        _endTrigger.OnTriggerEnterEvent += OnEndTriggered;
    }

    public void RemoveListeners()
    {
        _startTrigger.OnTriggerEnterEvent -= OnStartTriggered;
        _endTrigger.OnTriggerEnterEvent -= OnEndTriggered;
    }
}