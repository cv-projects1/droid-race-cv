using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public class FixedTimeDeltaComponent : IComponent
{
    public float Value;
}