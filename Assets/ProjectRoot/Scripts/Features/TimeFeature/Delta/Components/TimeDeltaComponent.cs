using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public sealed class TimeDeltaComponent : IComponent
{
    public float Value;
}