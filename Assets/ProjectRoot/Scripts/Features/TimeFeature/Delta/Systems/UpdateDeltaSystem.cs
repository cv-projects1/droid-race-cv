using Entitas;

public class UpdateTimeDeltaSystem : IExecuteSystem,
                                     IInitializeSystem
{
    private readonly GameContext _gameContext;

    public UpdateTimeDeltaSystem(GameContext gameContext)
    {
        _gameContext = gameContext;
    }

    public void Initialize()
    {
        //Make it bulletproof
        Execute();
    }

    public void Execute()
    {
        if (!_gameContext.isPause)
            _gameContext.ReplaceTimeDelta(UnityEngine.Time.deltaTime);
        else _gameContext.ReplaceTimeDelta(0);
    }
}