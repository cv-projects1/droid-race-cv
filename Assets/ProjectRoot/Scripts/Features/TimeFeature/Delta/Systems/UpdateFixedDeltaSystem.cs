using Entitas;
using UnityEngine;

public class UpdateFixedTimeDeltaSystem : IExecuteSystem,
                                          IInitializeSystem
{
    private readonly GameContext _gameContext;

    public UpdateFixedTimeDeltaSystem(GameContext gameContext)
    {
        _gameContext = gameContext;
    }

    public void Initialize()
    {
        //Make it bulletproof
        Execute();
    }

    public void Execute()
    {
        if (!_gameContext.isPause)
            _gameContext.ReplaceFixedTimeDelta(Time.fixedDeltaTime);
        else _gameContext.ReplaceTimeDelta(0);
    }
}