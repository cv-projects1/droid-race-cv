using Entitas;
using System.Collections;
using System.Collections.Generic;
using MobRun;
using UnityEngine;

public class CrowdMoveTriggerSystem : ReactiveSystem<GameEntity>
{
    private readonly GameConfig _gameConfig;
    private readonly GameContext _gameContext;

    public CrowdMoveTriggerSystem(Contexts contexts , GameConfig gameConfig) : base(contexts.game)
    {
        _gameConfig = gameConfig;
        _gameContext = contexts.game;

        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.TimerRunning.Removed());

    protected override bool Filter(GameEntity entity)
    {
        return !entity.isTimerRunning && entity.isStartMoveDelayTimer;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            // do stuff to the matched entities
            _gameContext.ReplaceCrowdGlobalTargetSpeed(_gameConfig.CrowdMoveSpeed);
        }
    }
}