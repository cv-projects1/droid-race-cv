using Entitas;

public class TimerSystem : IExecuteSystem 
{
    private readonly GameContext _gameContext;
    private readonly IGroup<GameEntity> _runningTimers;

    public TimerSystem(Contexts contexts)
    {
        _gameContext = contexts.game;
        
        _runningTimers = _gameContext.GetGroup
        (
            GameMatcher.AllOf
            (
                GameMatcher.Timer,
                GameMatcher.TimerRunning
            )
        );
    }

    public void Execute()
    {
        var delta = _gameContext.timeDelta.Value;
        foreach (var e in _runningTimers.GetEntities())
            // For each running timer...
        {
            e.timer.Remaining -= delta;

            if (!(e.timer.Remaining <= 0.0f)) continue;

            e.timer.Remaining = 0.0f;
            e.isTimerRunning = false;

            if (e.willDestroyWhenTimerExpires)
                e.isDestroyed = true; //Probably destroy with a system
        }
    }
}