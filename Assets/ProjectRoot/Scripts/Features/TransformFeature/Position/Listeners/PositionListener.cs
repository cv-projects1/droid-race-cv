using Entitas;
using UnityEngine;
using vailshnast.entitasview;

public class PositionListener : MonoBehaviour,
                                IEventListener,
                                IWorldPositionListener,
                                ILocalPositionListener
{
    private GameEntity _entity;

    public void RegisterListeners(IEntity entity)
    {
        _entity = (GameEntity) entity;
        _entity.AddWorldPositionListener(this);
        _entity.AddLocalPositionListener(this);
    }

    public void RemoveListeners()
    {
        //if (_entity.hasWorldPositionListener)
            _entity.RemoveWorldPositionListener(this);

        //if (_entity.hasLocalPositionListener)
            _entity.RemoveLocalPositionListener(this);
    }

    public void OnWorldPosition(GameEntity entity, Vector3 value)
    {
        transform.position = value;
    }

    public void OnLocalPosition(GameEntity entity, Vector3 value)
    {
        transform.localPosition = value;
    }
}