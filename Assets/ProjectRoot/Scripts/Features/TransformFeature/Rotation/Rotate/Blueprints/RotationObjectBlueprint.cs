using Zenject;
using UnityEngine;
using vailshnast.entitasview;

namespace MobRun
{
    public class RotationObjectBlueprint : MonoBehaviour, IEntityBlueprint
    {
        [SerializeField] private EntityView _entityView;
        [SerializeField] private Vector3 _rotationAxis;
        [SerializeField] private float _rotationSpeed;
        
        private GameContext _gameContext;

        [Inject]
        public void Construct(IEntityBlueprintService entityBlueprintService,
                              GameContext gameContext)
        {
            _gameContext = gameContext;
            entityBlueprintService.AddBlueprint(this);
        }

        public void Initialize()
        {
            var entity = _gameContext.CreateEntity();
            
            entity.AddRotation(transform.rotation);
            entity.AddRotationAxis(_rotationAxis);
            entity.AddRotationSpeed(_rotationSpeed);
            entity.AddEntityView(_entityView);
            
            _entityView.Link(entity);
        }
    }
}