using Entitas;
using UnityEngine;

[Game]
public sealed class RotationAxisComponent : IComponent
{
    public Vector3 Value;
}