using Entitas;

[Game]
public sealed class RotationSpeedComponent : IComponent
{
    public float Value;
}