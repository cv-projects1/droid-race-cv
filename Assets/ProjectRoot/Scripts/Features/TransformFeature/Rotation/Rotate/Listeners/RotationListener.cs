using Entitas;
using UnityEngine;
using vailshnast.entitasview;

public class RotationListener : MonoBehaviour, IEventListener, IRotationListener
{
    private GameEntity _entity;

    public void RegisterListeners(IEntity entity)
    {
        _entity = (GameEntity) entity;
        _entity.AddRotationListener(this);
    }

    public void RemoveListeners() 
        => _entity.RemoveRotationListener(this);

    public void OnRotation(GameEntity entity, Quaternion value) 
        => transform.rotation = value;
}