using Entitas;
using UnityEngine;
using Zenject;

public class RotationSystem : IExecuteSystem
{
    private readonly GameContext _gameContext;
    private readonly IGroup<GameEntity> _entities = null;

    public RotationSystem(Contexts contexts)
    {
        _gameContext = contexts.game;
        _entities = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.RotationSpeed,
                                                             GameMatcher.Rotation,
                                                             GameMatcher.RotationAxis));
    }

    public void Execute()
    {
        var delta = _gameContext.timeDelta.Value;

        foreach (var entity in _entities)
        {
            var rotationOffset = entity.rotationAxis.Value * entity.rotationSpeed.Value * delta;

            var rotation = entity.rotation.Value *
                           Quaternion.Euler(rotationOffset.x, rotationOffset.y, rotationOffset.z);

            entity.ReplaceRotation(rotation);
        }
    }
}