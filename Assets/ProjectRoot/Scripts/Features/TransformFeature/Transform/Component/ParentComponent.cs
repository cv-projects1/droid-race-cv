using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Game, Event(EventTarget.Self), Cleanup(CleanupMode.RemoveComponent)]
public sealed class ParentComponent : IComponent
{
    public Transform Value;
}