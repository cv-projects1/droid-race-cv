using Entitas;
using UnityEngine;
using vailshnast.entitasview;

namespace PrototypeTemplate
{
    public class ParentListener : MonoBehaviour, IEventListener, IParentListener
    {
        private GameEntity _entity;

        public void RegisterListeners(IEntity entity)
        {
            _entity = (GameEntity) entity;
            _entity.AddParentListener(this);
        }

        public void RemoveListeners()
        {
            _entity.RemoveParentListener(this);
        }

        public void OnParent(GameEntity entity, Transform value) => transform.SetParent(value);
    }
}