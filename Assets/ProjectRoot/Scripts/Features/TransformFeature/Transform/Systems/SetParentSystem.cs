using System.Collections.Generic;
using Entitas;
using vailshnast.entitasview;

namespace PrototypeTemplate
{
    public class SetParentSystem : ReactiveSystem<GameEntity>
    {
        public SetParentSystem(Contexts contexts) : base(contexts.game)
        {
            // pass the context of interest to the base constructor
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
            context.CreateCollector(GameMatcher.Parent);

        protected override bool Filter(GameEntity entity) => entity.hasParent && entity.hasEntityView;

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                // do stuff to the matched entities
                var view = entity.entityView.Value;
                var viewTransform = view.GetComponentView<ITransform>().TransformValue; 
                var newParent = entity.parent.Value;
            
                viewTransform.SetParent(newParent);
            }
        }
    }
}