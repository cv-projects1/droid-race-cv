using Entitas;
using Entitas.CodeGeneration.Attributes;
using MobRun;

[Game, Event(EventTarget.Any), Unique]
public sealed class TutorialStepComponent : IComponent
{
    public TutorialStepType Value;
}