namespace MobRun
{
    public enum TutorialStepType
    {
        TapAndHold    = 0,
        WaitForHold   = 1,
        KeepHolding   = 2,
        ReleaseFinger = 3,
        Completed     = 4
    }
}