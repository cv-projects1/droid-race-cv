using Entitas;
using System.Collections;
using System.Collections.Generic;
using MobRun;
using UnityEngine;
using vailshnast.windows;

public class TutorialHoldTapTriggerSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext _gameContext;
    private readonly IWindowsService _windowsService;
    private readonly IAnalyticsService _analyticsService;

    public TutorialHoldTapTriggerSystem(Contexts contexts,
                                        GameContext gameContext,
                                        IWindowsService windowsService,
                                        IAnalyticsService analyticsService) : base(contexts.game)
    {
        _gameContext = gameContext;
        _windowsService = windowsService;
        _analyticsService = analyticsService;

        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.SlidePointStartCollision);

    protected override bool Filter(GameEntity entity) =>
        entity.hasSlidePointStartCollision && entity.hasSlidePoint;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (!_gameContext.hasTutorialStep) continue;
            if (_gameContext.tutorialStep.Value != TutorialStepType.TapAndHold) continue;

            _analyticsService.LogEvent(new EventData(){EventName = AnalyticsConstants.TutorialStarted});
            
            _gameContext.isPause = true;

            _windowsService.RequestShowWindow(new GameTutorialWindowController
                                                  {TutorialViewType = TutorialViewType.TapAndHold});

            _gameContext.ReplaceTutorialStep(TutorialStepType.WaitForHold);
        }
    }
}