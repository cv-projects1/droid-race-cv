using Entitas;
using MobRun;

public class TutorialInitializationSystem : IInitializeSystem
{
    private readonly GameContext _gameContext;
    private readonly PlayerModel _playerModel;

    public TutorialInitializationSystem(GameContext gameContext, PlayerModel playerModel)
    {
        _gameContext = gameContext;
        _playerModel = playerModel;
    }
    
    public void Initialize()
    {
        if (_playerModel.PlayerSave.TutorialCompleted) return;

        var entity = _gameContext.CreateEntity();
        entity.AddTutorialStep(TutorialStepType.TapAndHold);
    }
}