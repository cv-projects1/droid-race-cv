using Entitas;
using System.Collections;
using System.Collections.Generic;
using MobRun;
using UnityEngine;
using vailshnast.windows;

public class TutorialKeepHoldingTouchBeginSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext _gameContext;
    private readonly IWindowsService _windowsService;

    public TutorialKeepHoldingTouchBeginSystem(Contexts contexts,
                                               GameContext gameContext,
                                               IWindowsService windowsService) : base(contexts.game)
    {
        _gameContext = gameContext;
        _windowsService = windowsService;

        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.TouchBegin);

    protected override bool Filter(GameEntity entity) =>
        entity.isTouchBegin;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (!_gameContext.hasTutorialStep) continue;
            if (_gameContext.tutorialStep.Value != TutorialStepType.KeepHolding) continue;

            _gameContext.isPause = false;
            _windowsService.RequestCloseWindow<GameTutorialWindowController>();
        }
    }
}