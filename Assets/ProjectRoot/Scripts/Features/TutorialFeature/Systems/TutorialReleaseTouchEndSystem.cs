using Entitas;
using System.Collections;
using System.Collections.Generic;
using MobRun;
using UnityEngine;
using vailshnast.windows;

public class TutorialReleaseTouchEndSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext _gameContext;
    private readonly PlayerModel _playerModel;
    private readonly IWindowsService _windowsService;
    private readonly ISaveService _saveService;
    private readonly IAnalyticsService _analyticsService;

    public TutorialReleaseTouchEndSystem(Contexts contexts,
                                         GameContext gameContext,
                                         PlayerModel playerModel,
                                         IWindowsService windowsService,
                                         ISaveService saveService,
                                         IAnalyticsService analyticsService) : base(contexts.game)
    {
        _gameContext = gameContext;
        _playerModel = playerModel;
        _windowsService = windowsService;
        _saveService = saveService;
        _analyticsService = analyticsService;

        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.TouchEnd);

    protected override bool Filter(GameEntity entity) =>
        entity.isTouchEnd;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (!_gameContext.hasTutorialStep) continue;
            if (_gameContext.tutorialStep.Value != TutorialStepType.ReleaseFinger) continue;

            _analyticsService.LogEvent(new EventData{EventName = AnalyticsConstants.TutorialCompleted});

            _gameContext.isPause = false;
            _windowsService.RequestCloseWindow<GameTutorialWindowController>();
            _gameContext.ReplaceTutorialStep(TutorialStepType.Completed);
            _playerModel.PlayerSave.TutorialCompleted = true;
            _saveService.Save();
        }
    }
}