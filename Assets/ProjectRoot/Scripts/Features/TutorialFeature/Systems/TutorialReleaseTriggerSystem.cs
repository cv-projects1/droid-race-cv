using Entitas;
using System.Collections;
using System.Collections.Generic;
using MobRun;
using UnityEngine;
using vailshnast.windows;

public class TutorialReleaseTriggerSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext _gameContext;
    private readonly IWindowsService _windowsService;

    public TutorialReleaseTriggerSystem(Contexts contexts,
                                        GameContext gameContext,
                                        IWindowsService windowsService) : base(contexts.game)
    {
        _gameContext = gameContext;
        _windowsService = windowsService;

        // pass the context of interest to the base constructor
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.SlidePointEndCollision);

    protected override bool Filter(GameEntity entity) =>
        entity.hasSlidePointEndCollision && entity.hasSlidePoint;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (!_gameContext.hasTutorialStep) continue;
            if (_gameContext.tutorialStep.Value != TutorialStepType.KeepHolding) continue;

            _gameContext.ReplaceTutorialStep(TutorialStepType.ReleaseFinger);

            // do stuff to the matched entities
            var triggerEntity = _gameContext.GetEntityWithId(entity.slidePointEndCollision.EntityIndex);

            if (triggerEntity.isCrowdGlobalTarget)
            {
                _windowsService.RequestShowWindow(new GameTutorialWindowController()
                                                      {TutorialViewType = TutorialViewType.ReleaseFinger});

                _gameContext.isPause = true;
            }
        }
    }
}