using Entitas;
using System.Collections;
using System.Collections.Generic;
using MobRun;
using UnityEngine;
using vailshnast.windows;

public class TutorialWaitForHoldSystem : ReactiveSystem<GameEntity>
{
    private readonly IWindowsService _windowsService;
    private readonly GameContext _gameContext;

    public TutorialWaitForHoldSystem(Contexts contexts, IWindowsService windowsService) : base(contexts.game)
    {
        _windowsService = windowsService;

        // pass the context of interest to the base constructor
        _gameContext = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
        context.CreateCollector(GameMatcher.TouchBegin);

    protected override bool Filter(GameEntity entity) => entity.isTouchBegin;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (!_gameContext.hasTutorialStep) continue;
            if (_gameContext.tutorialStep.Value != TutorialStepType.WaitForHold) continue;

            _windowsService.RequestCloseWindow<GameTutorialWindowController>();
            _gameContext.isPause = false;
            _gameContext.ReplaceTutorialStep(TutorialStepType.KeepHolding);
        }
    }
}