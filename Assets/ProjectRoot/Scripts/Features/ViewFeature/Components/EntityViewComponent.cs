using Entitas;
using vailshnast.entitasview;

[Game]
public sealed class EntityViewComponent : IComponent
{
    public IEntityView Value;
}