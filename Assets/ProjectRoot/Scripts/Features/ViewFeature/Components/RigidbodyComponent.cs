using Entitas;
using vailshnast.entitasview;

[Game]
public sealed class RigidbodyComponent : IComponent
{
    public IRigidbody Value;
}