using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine.Rendering;

[Game, Event(EventTarget.Self)]
public sealed class ShadowComponent : IComponent
{
    public ShadowCastingMode Value;
}