using Entitas;
using vailshnast.entitasview;

[Game]
public sealed class TransformViewComponent : IComponent
{
    public ITransform Value;
}