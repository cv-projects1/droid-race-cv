using Entitas;
using UnityEngine;
using UnityEngine.Rendering;
using vailshnast.entitasview;

public class MeshRendererShadowListener : MonoBehaviour, IEventListener, IShadowListener
{
    [SerializeField] private MeshRenderer _meshRenderer;
    private GameEntity _entity;

    public void RegisterListeners(IEntity entity)
    {
        _entity = (GameEntity) entity;
        _entity.AddShadowListener(this);
    }

    public void RemoveListeners()
    {
        _entity.RemoveShadowListener(this);
    }

    public void OnShadow(GameEntity entity, ShadowCastingMode value) =>
        _meshRenderer.shadowCastingMode = value;
}
