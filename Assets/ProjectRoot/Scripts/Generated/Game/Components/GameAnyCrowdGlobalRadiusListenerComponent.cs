//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public AnyCrowdGlobalRadiusListenerComponent anyCrowdGlobalRadiusListener { get { return (AnyCrowdGlobalRadiusListenerComponent)GetComponent(GameComponentsLookup.AnyCrowdGlobalRadiusListener); } }
    public bool hasAnyCrowdGlobalRadiusListener { get { return HasComponent(GameComponentsLookup.AnyCrowdGlobalRadiusListener); } }

    public void AddAnyCrowdGlobalRadiusListener(System.Collections.Generic.List<IAnyCrowdGlobalRadiusListener> newValue) {
        var index = GameComponentsLookup.AnyCrowdGlobalRadiusListener;
        var component = (AnyCrowdGlobalRadiusListenerComponent)CreateComponent(index, typeof(AnyCrowdGlobalRadiusListenerComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceAnyCrowdGlobalRadiusListener(System.Collections.Generic.List<IAnyCrowdGlobalRadiusListener> newValue) {
        var index = GameComponentsLookup.AnyCrowdGlobalRadiusListener;
        var component = (AnyCrowdGlobalRadiusListenerComponent)CreateComponent(index, typeof(AnyCrowdGlobalRadiusListenerComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveAnyCrowdGlobalRadiusListener() {
        RemoveComponent(GameComponentsLookup.AnyCrowdGlobalRadiusListener);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherAnyCrowdGlobalRadiusListener;

    public static Entitas.IMatcher<GameEntity> AnyCrowdGlobalRadiusListener {
        get {
            if (_matcherAnyCrowdGlobalRadiusListener == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.AnyCrowdGlobalRadiusListener);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherAnyCrowdGlobalRadiusListener = matcher;
            }

            return _matcherAnyCrowdGlobalRadiusListener;
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.EventEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public void AddAnyCrowdGlobalRadiusListener(IAnyCrowdGlobalRadiusListener value) {
        var listeners = hasAnyCrowdGlobalRadiusListener
            ? anyCrowdGlobalRadiusListener.value
            : new System.Collections.Generic.List<IAnyCrowdGlobalRadiusListener>();
        listeners.Add(value);
        ReplaceAnyCrowdGlobalRadiusListener(listeners);
    }

    public void RemoveAnyCrowdGlobalRadiusListener(IAnyCrowdGlobalRadiusListener value, bool removeComponentWhenEmpty = true) {
        var listeners = anyCrowdGlobalRadiusListener.value;
        listeners.Remove(value);
        if (removeComponentWhenEmpty && listeners.Count == 0) {
            RemoveAnyCrowdGlobalRadiusListener();
        } else {
            ReplaceAnyCrowdGlobalRadiusListener(listeners);
        }
    }
}
