//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public AnyFinishBoxCollectedPercentageListenerComponent anyFinishBoxCollectedPercentageListener { get { return (AnyFinishBoxCollectedPercentageListenerComponent)GetComponent(GameComponentsLookup.AnyFinishBoxCollectedPercentageListener); } }
    public bool hasAnyFinishBoxCollectedPercentageListener { get { return HasComponent(GameComponentsLookup.AnyFinishBoxCollectedPercentageListener); } }

    public void AddAnyFinishBoxCollectedPercentageListener(System.Collections.Generic.List<IAnyFinishBoxCollectedPercentageListener> newValue) {
        var index = GameComponentsLookup.AnyFinishBoxCollectedPercentageListener;
        var component = (AnyFinishBoxCollectedPercentageListenerComponent)CreateComponent(index, typeof(AnyFinishBoxCollectedPercentageListenerComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceAnyFinishBoxCollectedPercentageListener(System.Collections.Generic.List<IAnyFinishBoxCollectedPercentageListener> newValue) {
        var index = GameComponentsLookup.AnyFinishBoxCollectedPercentageListener;
        var component = (AnyFinishBoxCollectedPercentageListenerComponent)CreateComponent(index, typeof(AnyFinishBoxCollectedPercentageListenerComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveAnyFinishBoxCollectedPercentageListener() {
        RemoveComponent(GameComponentsLookup.AnyFinishBoxCollectedPercentageListener);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherAnyFinishBoxCollectedPercentageListener;

    public static Entitas.IMatcher<GameEntity> AnyFinishBoxCollectedPercentageListener {
        get {
            if (_matcherAnyFinishBoxCollectedPercentageListener == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.AnyFinishBoxCollectedPercentageListener);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherAnyFinishBoxCollectedPercentageListener = matcher;
            }

            return _matcherAnyFinishBoxCollectedPercentageListener;
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.EventEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public void AddAnyFinishBoxCollectedPercentageListener(IAnyFinishBoxCollectedPercentageListener value) {
        var listeners = hasAnyFinishBoxCollectedPercentageListener
            ? anyFinishBoxCollectedPercentageListener.value
            : new System.Collections.Generic.List<IAnyFinishBoxCollectedPercentageListener>();
        listeners.Add(value);
        ReplaceAnyFinishBoxCollectedPercentageListener(listeners);
    }

    public void RemoveAnyFinishBoxCollectedPercentageListener(IAnyFinishBoxCollectedPercentageListener value, bool removeComponentWhenEmpty = true) {
        var listeners = anyFinishBoxCollectedPercentageListener.value;
        listeners.Remove(value);
        if (removeComponentWhenEmpty && listeners.Count == 0) {
            RemoveAnyFinishBoxCollectedPercentageListener();
        } else {
            ReplaceAnyFinishBoxCollectedPercentageListener(listeners);
        }
    }
}
