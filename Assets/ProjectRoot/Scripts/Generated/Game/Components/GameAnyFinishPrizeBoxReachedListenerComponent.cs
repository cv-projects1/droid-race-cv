//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public AnyFinishPrizeBoxReachedListenerComponent anyFinishPrizeBoxReachedListener { get { return (AnyFinishPrizeBoxReachedListenerComponent)GetComponent(GameComponentsLookup.AnyFinishPrizeBoxReachedListener); } }
    public bool hasAnyFinishPrizeBoxReachedListener { get { return HasComponent(GameComponentsLookup.AnyFinishPrizeBoxReachedListener); } }

    public void AddAnyFinishPrizeBoxReachedListener(System.Collections.Generic.List<IAnyFinishPrizeBoxReachedListener> newValue) {
        var index = GameComponentsLookup.AnyFinishPrizeBoxReachedListener;
        var component = (AnyFinishPrizeBoxReachedListenerComponent)CreateComponent(index, typeof(AnyFinishPrizeBoxReachedListenerComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceAnyFinishPrizeBoxReachedListener(System.Collections.Generic.List<IAnyFinishPrizeBoxReachedListener> newValue) {
        var index = GameComponentsLookup.AnyFinishPrizeBoxReachedListener;
        var component = (AnyFinishPrizeBoxReachedListenerComponent)CreateComponent(index, typeof(AnyFinishPrizeBoxReachedListenerComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveAnyFinishPrizeBoxReachedListener() {
        RemoveComponent(GameComponentsLookup.AnyFinishPrizeBoxReachedListener);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherAnyFinishPrizeBoxReachedListener;

    public static Entitas.IMatcher<GameEntity> AnyFinishPrizeBoxReachedListener {
        get {
            if (_matcherAnyFinishPrizeBoxReachedListener == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.AnyFinishPrizeBoxReachedListener);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherAnyFinishPrizeBoxReachedListener = matcher;
            }

            return _matcherAnyFinishPrizeBoxReachedListener;
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.EventEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public void AddAnyFinishPrizeBoxReachedListener(IAnyFinishPrizeBoxReachedListener value) {
        var listeners = hasAnyFinishPrizeBoxReachedListener
            ? anyFinishPrizeBoxReachedListener.value
            : new System.Collections.Generic.List<IAnyFinishPrizeBoxReachedListener>();
        listeners.Add(value);
        ReplaceAnyFinishPrizeBoxReachedListener(listeners);
    }

    public void RemoveAnyFinishPrizeBoxReachedListener(IAnyFinishPrizeBoxReachedListener value, bool removeComponentWhenEmpty = true) {
        var listeners = anyFinishPrizeBoxReachedListener.value;
        listeners.Remove(value);
        if (removeComponentWhenEmpty && listeners.Count == 0) {
            RemoveAnyFinishPrizeBoxReachedListener();
        } else {
            ReplaceAnyFinishPrizeBoxReachedListener(listeners);
        }
    }
}
