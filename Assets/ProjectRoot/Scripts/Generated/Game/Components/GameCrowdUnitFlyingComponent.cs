//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    static readonly CrowdUnitFlying crowdUnitFlyingComponent = new CrowdUnitFlying();

    public bool isCrowdUnitFlying {
        get { return HasComponent(GameComponentsLookup.CrowdUnitFlying); }
        set {
            if (value != isCrowdUnitFlying) {
                var index = GameComponentsLookup.CrowdUnitFlying;
                if (value) {
                    var componentPool = GetComponentPool(index);
                    var component = componentPool.Count > 0
                            ? componentPool.Pop()
                            : crowdUnitFlyingComponent;

                    AddComponent(index, component);
                } else {
                    RemoveComponent(index);
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherCrowdUnitFlying;

    public static Entitas.IMatcher<GameEntity> CrowdUnitFlying {
        get {
            if (_matcherCrowdUnitFlying == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.CrowdUnitFlying);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherCrowdUnitFlying = matcher;
            }

            return _matcherCrowdUnitFlying;
        }
    }
}
