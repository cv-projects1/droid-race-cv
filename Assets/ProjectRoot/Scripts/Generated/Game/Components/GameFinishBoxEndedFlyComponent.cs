//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    static readonly FinishBoxEndedFly finishBoxEndedFlyComponent = new FinishBoxEndedFly();

    public bool isFinishBoxEndedFly {
        get { return HasComponent(GameComponentsLookup.FinishBoxEndedFly); }
        set {
            if (value != isFinishBoxEndedFly) {
                var index = GameComponentsLookup.FinishBoxEndedFly;
                if (value) {
                    var componentPool = GetComponentPool(index);
                    var component = componentPool.Count > 0
                            ? componentPool.Pop()
                            : finishBoxEndedFlyComponent;

                    AddComponent(index, component);
                } else {
                    RemoveComponent(index);
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherFinishBoxEndedFly;

    public static Entitas.IMatcher<GameEntity> FinishBoxEndedFly {
        get {
            if (_matcherFinishBoxEndedFly == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.FinishBoxEndedFly);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherFinishBoxEndedFly = matcher;
            }

            return _matcherFinishBoxEndedFly;
        }
    }
}
