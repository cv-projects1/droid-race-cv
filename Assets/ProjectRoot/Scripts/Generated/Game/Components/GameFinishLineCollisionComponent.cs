//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public FinishLineCollision finishLineCollision { get { return (FinishLineCollision)GetComponent(GameComponentsLookup.FinishLineCollision); } }
    public bool hasFinishLineCollision { get { return HasComponent(GameComponentsLookup.FinishLineCollision); } }

    public void AddFinishLineCollision(UnityEngine.Vector3 newCenterPosition) {
        var index = GameComponentsLookup.FinishLineCollision;
        var component = (FinishLineCollision)CreateComponent(index, typeof(FinishLineCollision));
        component.CenterPosition = newCenterPosition;
        AddComponent(index, component);
    }

    public void ReplaceFinishLineCollision(UnityEngine.Vector3 newCenterPosition) {
        var index = GameComponentsLookup.FinishLineCollision;
        var component = (FinishLineCollision)CreateComponent(index, typeof(FinishLineCollision));
        component.CenterPosition = newCenterPosition;
        ReplaceComponent(index, component);
    }

    public void RemoveFinishLineCollision() {
        RemoveComponent(GameComponentsLookup.FinishLineCollision);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherFinishLineCollision;

    public static Entitas.IMatcher<GameEntity> FinishLineCollision {
        get {
            if (_matcherFinishLineCollision == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.FinishLineCollision);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherFinishLineCollision = matcher;
            }

            return _matcherFinishLineCollision;
        }
    }
}
