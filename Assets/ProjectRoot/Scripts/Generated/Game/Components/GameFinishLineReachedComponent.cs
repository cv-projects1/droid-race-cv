//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentContextApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameContext {

    public GameEntity finishLineReachedEntity { get { return GetGroup(GameMatcher.FinishLineReached).GetSingleEntity(); } }
    public FinishLineReached finishLineReached { get { return finishLineReachedEntity.finishLineReached; } }
    public bool hasFinishLineReached { get { return finishLineReachedEntity != null; } }

    public GameEntity SetFinishLineReached(UnityEngine.Vector3 newCenterPos) {
        if (hasFinishLineReached) {
            throw new Entitas.EntitasException("Could not set FinishLineReached!\n" + this + " already has an entity with FinishLineReached!",
                "You should check if the context already has a finishLineReachedEntity before setting it or use context.ReplaceFinishLineReached().");
        }
        var entity = CreateEntity();
        entity.AddFinishLineReached(newCenterPos);
        return entity;
    }

    public void ReplaceFinishLineReached(UnityEngine.Vector3 newCenterPos) {
        var entity = finishLineReachedEntity;
        if (entity == null) {
            entity = SetFinishLineReached(newCenterPos);
        } else {
            entity.ReplaceFinishLineReached(newCenterPos);
        }
    }

    public void RemoveFinishLineReached() {
        finishLineReachedEntity.Destroy();
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public FinishLineReached finishLineReached { get { return (FinishLineReached)GetComponent(GameComponentsLookup.FinishLineReached); } }
    public bool hasFinishLineReached { get { return HasComponent(GameComponentsLookup.FinishLineReached); } }

    public void AddFinishLineReached(UnityEngine.Vector3 newCenterPos) {
        var index = GameComponentsLookup.FinishLineReached;
        var component = (FinishLineReached)CreateComponent(index, typeof(FinishLineReached));
        component.CenterPos = newCenterPos;
        AddComponent(index, component);
    }

    public void ReplaceFinishLineReached(UnityEngine.Vector3 newCenterPos) {
        var index = GameComponentsLookup.FinishLineReached;
        var component = (FinishLineReached)CreateComponent(index, typeof(FinishLineReached));
        component.CenterPos = newCenterPos;
        ReplaceComponent(index, component);
    }

    public void RemoveFinishLineReached() {
        RemoveComponent(GameComponentsLookup.FinishLineReached);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherFinishLineReached;

    public static Entitas.IMatcher<GameEntity> FinishLineReached {
        get {
            if (_matcherFinishLineReached == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.FinishLineReached);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherFinishLineReached = matcher;
            }

            return _matcherFinishLineReached;
        }
    }
}
