//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    static readonly TouchBeginComponent touchBeginComponent = new TouchBeginComponent();

    public bool isTouchBegin {
        get { return HasComponent(GameComponentsLookup.TouchBegin); }
        set {
            if (value != isTouchBegin) {
                var index = GameComponentsLookup.TouchBegin;
                if (value) {
                    var componentPool = GetComponentPool(index);
                    var component = componentPool.Count > 0
                            ? componentPool.Pop()
                            : touchBeginComponent;

                    AddComponent(index, component);
                } else {
                    RemoveComponent(index);
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherTouchBegin;

    public static Entitas.IMatcher<GameEntity> TouchBegin {
        get {
            if (_matcherTouchBegin == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.TouchBegin);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherTouchBegin = matcher;
            }

            return _matcherTouchBegin;
        }
    }
}
