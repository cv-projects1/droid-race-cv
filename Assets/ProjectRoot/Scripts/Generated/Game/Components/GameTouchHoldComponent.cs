//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentContextApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameContext {

    public GameEntity touchHoldEntity { get { return GetGroup(GameMatcher.TouchHold).GetSingleEntity(); } }
    public TouchHoldComponent touchHold { get { return touchHoldEntity.touchHold; } }
    public bool hasTouchHold { get { return touchHoldEntity != null; } }

    public GameEntity SetTouchHold(float newHoldTime) {
        if (hasTouchHold) {
            throw new Entitas.EntitasException("Could not set TouchHold!\n" + this + " already has an entity with TouchHoldComponent!",
                "You should check if the context already has a touchHoldEntity before setting it or use context.ReplaceTouchHold().");
        }
        var entity = CreateEntity();
        entity.AddTouchHold(newHoldTime);
        return entity;
    }

    public void ReplaceTouchHold(float newHoldTime) {
        var entity = touchHoldEntity;
        if (entity == null) {
            entity = SetTouchHold(newHoldTime);
        } else {
            entity.ReplaceTouchHold(newHoldTime);
        }
    }

    public void RemoveTouchHold() {
        touchHoldEntity.Destroy();
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public TouchHoldComponent touchHold { get { return (TouchHoldComponent)GetComponent(GameComponentsLookup.TouchHold); } }
    public bool hasTouchHold { get { return HasComponent(GameComponentsLookup.TouchHold); } }

    public void AddTouchHold(float newHoldTime) {
        var index = GameComponentsLookup.TouchHold;
        var component = (TouchHoldComponent)CreateComponent(index, typeof(TouchHoldComponent));
        component.HoldTime = newHoldTime;
        AddComponent(index, component);
    }

    public void ReplaceTouchHold(float newHoldTime) {
        var index = GameComponentsLookup.TouchHold;
        var component = (TouchHoldComponent)CreateComponent(index, typeof(TouchHoldComponent));
        component.HoldTime = newHoldTime;
        ReplaceComponent(index, component);
    }

    public void RemoveTouchHold() {
        RemoveComponent(GameComponentsLookup.TouchHold);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherTouchHold;

    public static Entitas.IMatcher<GameEntity> TouchHold {
        get {
            if (_matcherTouchHold == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.TouchHold);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherTouchHold = matcher;
            }

            return _matcherTouchHold;
        }
    }
}
