//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentContextApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameContext {

    public GameEntity tutorialStepEntity { get { return GetGroup(GameMatcher.TutorialStep).GetSingleEntity(); } }
    public TutorialStepComponent tutorialStep { get { return tutorialStepEntity.tutorialStep; } }
    public bool hasTutorialStep { get { return tutorialStepEntity != null; } }

    public GameEntity SetTutorialStep(MobRun.TutorialStepType newValue) {
        if (hasTutorialStep) {
            throw new Entitas.EntitasException("Could not set TutorialStep!\n" + this + " already has an entity with TutorialStepComponent!",
                "You should check if the context already has a tutorialStepEntity before setting it or use context.ReplaceTutorialStep().");
        }
        var entity = CreateEntity();
        entity.AddTutorialStep(newValue);
        return entity;
    }

    public void ReplaceTutorialStep(MobRun.TutorialStepType newValue) {
        var entity = tutorialStepEntity;
        if (entity == null) {
            entity = SetTutorialStep(newValue);
        } else {
            entity.ReplaceTutorialStep(newValue);
        }
    }

    public void RemoveTutorialStep() {
        tutorialStepEntity.Destroy();
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public TutorialStepComponent tutorialStep { get { return (TutorialStepComponent)GetComponent(GameComponentsLookup.TutorialStep); } }
    public bool hasTutorialStep { get { return HasComponent(GameComponentsLookup.TutorialStep); } }

    public void AddTutorialStep(MobRun.TutorialStepType newValue) {
        var index = GameComponentsLookup.TutorialStep;
        var component = (TutorialStepComponent)CreateComponent(index, typeof(TutorialStepComponent));
        component.Value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceTutorialStep(MobRun.TutorialStepType newValue) {
        var index = GameComponentsLookup.TutorialStep;
        var component = (TutorialStepComponent)CreateComponent(index, typeof(TutorialStepComponent));
        component.Value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveTutorialStep() {
        RemoveComponent(GameComponentsLookup.TutorialStep);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherTutorialStep;

    public static Entitas.IMatcher<GameEntity> TutorialStep {
        get {
            if (_matcherTutorialStep == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.TutorialStep);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherTutorialStep = matcher;
            }

            return _matcherTutorialStep;
        }
    }
}
