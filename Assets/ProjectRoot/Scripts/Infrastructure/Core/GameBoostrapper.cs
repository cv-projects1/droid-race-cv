using UnityEngine;
using vailshnast.statemachines;
using Zenject;

namespace MobRun
{
    public class GameBoostrapper : MonoBehaviour
    {
        private IStateMachine _gameStateMachine;

        [Inject]
        public void Construct(IStateMachine gameStateMachine) =>
            _gameStateMachine = gameStateMachine;

        private void Start()
        {
            _gameStateMachine.Initialize();
            _gameStateMachine.EnterState<GameBoostrapState>();
        }
    }
}