using vailshnast.statemachines;
using Zenject;

namespace MobRun
{
    public class GameStateMachine : BaseStateMachine
    {
        public GameStateMachine(DiContainer diContainer) : base(diContainer)
        {
        }

        public override void Initialize()
        {
            AddState<GameBoostrapState>();
            AddState<GameInitializationState>();
            AddState<GameStartState>();
            AddState<GameLoopState>();
            AddState<GamePreviewState>();
            AddState<GameLossState>();
            AddState<GameVictoryState>();
            AddState<GamePausedState>();
            AddState<GameRestartState>();
            AddState<GameLoadState>();
        }
    }
}