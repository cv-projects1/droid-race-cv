using vailshnast.applicationevents;
using vailshnast.applicationservice;
using vailshnast.coroutine;
using vailshnast.eventsystem;
using vailshnast.mobile.essensials;
using vailshnast.pool;
using vailshnast.pooledfactories;
using vailshnast.sceneloading;
using vailshnast.serviceparent;
using vailshnast.statemachines;
using vailshnast.timer;
using vailshnast.windows;
using Zenject;

namespace MobRun
{
    public class GameInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            BindServices();
            BindModels();
        }

        private void BindServices()
        {
            PlatformInstaller.Install(Container);
            CoreServiceInstaller.Install(Container);
            GameServiceInstaller.Install(Container);
        }

        private void BindModels()
        {
            GameModelInstaller.Install(Container);
        }
    }

    public class GameModelInstaller : Installer<GameModelInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<PlayerModel>().AsSingle();
        }
    }
    
    public class CoreServiceInstaller : Installer<CoreServiceInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<IApplicationService>().To<ApplicationService>().AsSingle();
            Container.Bind<IApplicationEventsService>().To<ApplicationEventsService>().AsSingle();
            Container.Bind<IServicesParent>().To<ServicesParent>().AsSingle();
            Container.Bind<IStateMachine>().To<GameStateMachine>().AsSingle();
            Container.Bind<ICoroutineService>().To<CoroutineService>().AsSingle();
            Container.Bind<IEventSystemService>().To<EventSystemService>().AsSingle();
            Container.Bind<ISceneLoadingService>().To<SceneLoadingService>().AsSingle();
            Container.Bind<ISaveService>().To<SaveService>().AsSingle();
            Container.Bind<IWindowsService>().To<WindowsService>().AsSingle();
            Container.BindInterfacesAndSelfTo<TimerService>().AsSingle();
        }
    }

    public class GameServiceInstaller : Installer<GameServiceInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<IPoolService>().To<PoolService>().AsSingle();
            Container.Bind<IEntityProviderService>().To<EntityProviderService>().AsSingle();
            Container.Bind<IEntityBlueprintService>().To<EntityBlueprintService>().AsSingle();
            Container.Bind<IGameModuleService>().To<GameModuleService>().AsSingle();
            Container.Bind<IGameFactoryService>().To<GameFactoryService>().AsSingle();
            Container.Bind<IBasePooledFactory>().To<PooledFactoryService>().AsSingle();
            Container.Bind<IUIAnimationService>().To<UIAnimationService>().AsSingle();
            Container.Bind<ITweenEntityService>().To<TweenEntityService>().AsSingle();
            Container.Bind<IUpgradeService>().To<UpgradeService>().AsSingle();
            Container.Bind<ILevelService>().To<LevelService>().AsSingle();
            Container.Bind<IEntityCreationService>().To<EntityCreationService>().AsSingle();
            Container.Bind<IGameSoundService>().To<GameSoundService>().AsSingle();
        }
    }
    
    public class PlatformInstaller : Installer<PlatformInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<IAnalyticsService>().To<AmplitudeService>().AsSingle();
            Container.Bind<IGameAnalyticsService>().To<GameAnalyticsService>().AsSingle();
            Container.Bind<IPerformanceService>().To<PerformanceService>().AsSingle();
            Container.Bind<IHapticsService>().To<HapticsService>().AsSingle();

#if UNITY_EDITOR || UNITY_STANDALONE
            Container.Bind<IPlatformService>().To<StandaloneService>().AsSingle();
            Container.Bind<IDebugService>().To<EditorDebugService>().AsSingle();
#elif UNITY_IOS
            Container.Bind<IPlatformService>().To<IOSService>().AsSingle();
            Container.Bind<IDebugService>().To<MobileDebugService>().AsSingle();
#elif UNITY_ANDROID
            Container.Bind<IPlatformService>().To<AndroidService>().AsSingle();
            Container.Bind<IDebugService>().To<MobileDebugService>().AsSingle();
#endif
        }
    }
}