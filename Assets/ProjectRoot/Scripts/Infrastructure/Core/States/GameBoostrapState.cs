using vailshnast.statemachines;

namespace MobRun
{
    public class GameBoostrapState : ISimpleState
    {
        private IStateMachine _stateMaching;
                    
        public void Initialize(IStateMachine stateMachine) =>
            _stateMaching = stateMachine;

        public void EnterState()
        {
            _stateMaching.EnterState<GameInitializationState>();
        }
    }
}