using vailshnast.applicationevents;
using vailshnast.applicationservice;
using vailshnast.coroutine;
using vailshnast.entitaswrapper;
using vailshnast.eventsystem;
using vailshnast.mobile.essensials;
using vailshnast.pool;
using vailshnast.pooledfactories;
using vailshnast.serviceparent;
using vailshnast.statemachines;
using vailshnast.windows;
using Zenject;

namespace MobRun
{
    public class GameInitializationState : ISimpleState
    {
        [Inject] private readonly IServicesParent _servicesParent;
        [Inject] private readonly ICoroutineService _coroutineService;
        [Inject] private readonly IEventSystemService _eventSystemService;
        [Inject] private readonly IApplicationService _applicationService;
        [Inject] private readonly IApplicationEventsService _applicationEventsService;
        [Inject] private readonly IGameFactoryService _gameFactoryService;
        [Inject] private readonly IWindowsService _windowsService;
        [Inject] private readonly IEntitasBootstrap _entitasBootstrap;
        [Inject] private readonly IEntityBlueprintService _entityBlueprintService;
        [Inject] private readonly IPoolService _poolService;
        [Inject] private readonly IBasePooledFactory _basePooledFactory;
        [Inject] private readonly ISaveService _saveService;
        [Inject] private readonly IUpgradeService _upgradeService;
        [Inject] private readonly ILevelService _levelService;
        [Inject] private readonly IDebugService _debugService;
        [Inject] private readonly IAnalyticsService _analyticsService;
        [Inject] private readonly IGameAnalyticsService _gameAnalyticsService;
        [Inject] private readonly IPlatformService _platformService;
        [Inject] private readonly IPerformanceService _performanceService;
        [Inject] private readonly IHapticsService _hapticsService;
        [Inject] private readonly IGameSoundService _gameSoundService;
        
        private IStateMachine _stateMachine;

        public void Initialize(IStateMachine stateMachine)
            => _stateMachine = stateMachine;

        public async void EnterState()
        {
            _saveService.Initialize();

            _platformService.Initialize(_saveService.IsFirstLaunch);
            
            _analyticsService.Initialize();
            _gameAnalyticsService.Initialize();

            _applicationService.Initialize();
            _servicesParent.Initialize();
            _applicationEventsService.Initialize();
            _eventSystemService.Initialize();
            _coroutineService.Initialize();
            
            _debugService.Initialize();
            _upgradeService.Initialize();
            _levelService.Initialize();

            _hapticsService.Initialize();
            _performanceService.Initialize();
            
            await _windowsService.Initialize();
            
            _poolService.Initialize();
            _basePooledFactory.Initialize();
            _gameFactoryService.Initialize();
            _entityBlueprintService.Initialize();
            _gameSoundService.Initialize();
            
            await _levelService.LoadStartScene();
            
            _entitasBootstrap.Initialize();
            _stateMachine.EnterState<GamePreviewState>();
            
            _upgradeService.ApplyStartUpgrades();
        }
    }
}