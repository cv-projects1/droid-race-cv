using vailshnast.entitaswrapper;
using vailshnast.pool;
using vailshnast.statemachines;
using vailshnast.windows;

namespace MobRun
{
    public class GameLoadState : ISimpleState
    {
        private readonly IEntityBlueprintService _entityBlueprintService;
        private readonly IPoolService _poolService;
        private readonly IWindowsService _windowsService;
        private readonly ILevelService _levelService;
        private IStateMachine _stateMachine;

        public GameLoadState(IEntityBlueprintService entityBlueprintService,
                             IPoolService poolService,
                             IWindowsService windowsService,
                             ILevelService levelService)
        {
            _entityBlueprintService = entityBlueprintService;
            _poolService = poolService;
            _windowsService = windowsService;
            _levelService = levelService;
        }

        public void Initialize(IStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }

        public async void EnterState()
        {
            _poolService.ReturnAllObjectsToPools();
            _entityBlueprintService.ClearBlueprints();
            await _levelService.LoadCurrentLevel();
            _windowsService.RequestCloseAllWindows();
            _stateMachine.EnterState<GamePreviewState>();
        }
    }
}