using vailshnast.statemachines;
using vailshnast.windows;
using Zenject;

namespace MobRun
{
    public class GameLoopState : ISimpleState, IExitableState
    {
        private readonly IWindowsService _windowsService;

        public void Initialize(IStateMachine stateMachine)
        {
            
        }

        public GameLoopState(IWindowsService windowsService)
        {
            _windowsService = windowsService;
        }
        
        
        public void EnterState()
        {
            _windowsService.RequestShowWindow(new GameWindowController());
        }

        public void ExitState()
        {
            _windowsService.RequestCloseWindow<GameWindowController>();
        }
    }
}