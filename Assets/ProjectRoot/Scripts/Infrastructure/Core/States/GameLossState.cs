using vailshnast.entitaswrapper;
using vailshnast.statemachines;
using vailshnast.windows;

namespace MobRun
{
    public class GameLossState : ISimpleState
    {
        private readonly IWindowsService _windowsService;
        private readonly IEntitasBootstrap _entitasBootstrap;
        private readonly IGameModuleService _gameModuleService;

        public GameLossState(IWindowsService windowsService,
                             IEntitasBootstrap entitasBootstrap,
                             IGameModuleService gameModuleService)
        {
            _windowsService = windowsService;
            _entitasBootstrap = entitasBootstrap;
            _gameModuleService = gameModuleService;
        }

        public void Initialize(IStateMachine stateMachine)
        {
        }

        public async void EnterState()
        {
            await _entitasBootstrap.DisposeAsync();
            _gameModuleService.Dispose();
            
            _windowsService.RequestShowWindow(new GameLossWindowController());
        }
    }
}