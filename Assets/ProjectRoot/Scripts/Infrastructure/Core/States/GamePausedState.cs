using vailshnast.statemachines;
using vailshnast.windows;

namespace MobRun
{
    public class GamePausedState : ISimpleState,IExitableState
    {
        private readonly GameContext _gameContext;
        private readonly IWindowsService _windowsService;
        private IStateMachine _stateMachine;

        public void Initialize(IStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }

        public GamePausedState(GameContext gameContext, IWindowsService windowsService)
        {
            _gameContext = gameContext;
            _windowsService = windowsService;
        }

        public void EnterState()
        {
            _gameContext.isPause = true;
            
            _windowsService.RequestShowWindow(new GamePauseWindowController());
        }

        public void ExitState()
        {
            _gameContext.isPause = false;
            
            _windowsService.RequestCloseWindow<GamePauseWindowController>();
        }
    }
}