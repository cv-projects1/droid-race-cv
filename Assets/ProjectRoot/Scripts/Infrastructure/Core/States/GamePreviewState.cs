using Cysharp.Threading.Tasks;
using vailshnast.statemachines;
using vailshnast.windows;

namespace MobRun
{
    public class GamePreviewState : ISimpleState, IExitableState
    {
        private readonly IWindowsService _windowsService;
        private readonly IGameModuleService _gameModuleService;

        public GamePreviewState(IWindowsService windowsService, 
                                IGameModuleService gameModuleService)
        {
            _windowsService = windowsService;
            _gameModuleService = gameModuleService;
        }

        public void Initialize(IStateMachine stateMachine)
        {
        }

        public async void EnterState()
        {
            _gameModuleService.Initialize();

            _windowsService.RequestShowWindow(new PreviewStateWindowController());
            
            _gameModuleService.Module.VirtualGameCamera.gameObject.SetActive(false);
            await UniTask.WaitForEndOfFrame();
            _gameModuleService.Module.VirtualPreviewCamera.gameObject.SetActive(true);
        }

        public void ExitState()
        {
            _windowsService.RequestCloseWindow<PreviewStateWindowController>();
        }
    }
}