using vailshnast.entitaswrapper;
using vailshnast.pool;
using vailshnast.statemachines;
using vailshnast.windows;

namespace MobRun
{
    public class GameRestartState : ISimpleState
    {
        private readonly IEntityBlueprintService _entityBlueprintService;
        private readonly IPoolService _poolService;
        private readonly IWindowsService _windowsService;
        private readonly ILevelService _levelService;
        private readonly IEntitasBootstrap _entitasBootstrap;
        private readonly IGameModuleService _gameModuleService;
        private IStateMachine _stateMachine;

        public GameRestartState(IEntityBlueprintService entityBlueprintService,
                                IPoolService poolService,
                                IWindowsService windowsService,
                                ILevelService levelService,
                                IEntitasBootstrap entitasBootstrap,
                                IGameModuleService gameModuleService)
        {
            _entityBlueprintService = entityBlueprintService;
            _poolService = poolService;
            _windowsService = windowsService;
            _levelService = levelService;
            _entitasBootstrap = entitasBootstrap;
            _gameModuleService = gameModuleService;
        }

        public void Initialize(IStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }

        public async void EnterState()
        {
            await _entitasBootstrap.DisposeAsync();
            _gameModuleService.Dispose();
            
            _poolService.ReturnAllObjectsToPools();
            _entityBlueprintService.ClearBlueprints();
            
            await _levelService.LoadCurrentLevel();
            
            _windowsService.RequestCloseAllWindows();
            _stateMachine.EnterState<GamePreviewState>();
        }
    }
}