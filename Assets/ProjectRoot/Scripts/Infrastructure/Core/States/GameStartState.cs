using Cysharp.Threading.Tasks;
using vailshnast.entitaswrapper;
using vailshnast.statemachines;

namespace MobRun
{
    public class GameStartState : ISimpleState
    {
        private readonly IGameModuleService _gameModuleService;
        private readonly IEntitasBootstrap _entitasBootstrap;
        private readonly IEntityBlueprintService _entityBlueprintService;
        private IStateMachine _stateMachine;

        public void Initialize(IStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }

        public GameStartState(IGameModuleService gameModuleService,
                              IEntitasBootstrap entitasBootstrap,
                              IEntityBlueprintService entityBlueprintService)
        {
            _gameModuleService = gameModuleService;
            _entitasBootstrap = entitasBootstrap;
            _entityBlueprintService = entityBlueprintService;
        }

        public async void EnterState()
        {
            _gameModuleService.Start();

            _gameModuleService.Module.VirtualPreviewCamera.gameObject.SetActive(false);
            await UniTask.WaitForEndOfFrame();
            _gameModuleService.Module.VirtualGameCamera.gameObject.SetActive(true);

            _entitasBootstrap.StartExecution();
            _entityBlueprintService.InitializeBlueprints();
            
            _gameModuleService.PostStartup();

            _stateMachine.EnterState<GameLoopState>();
        }
    }
}