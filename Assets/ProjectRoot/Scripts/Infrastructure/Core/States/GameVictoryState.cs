using vailshnast.entitaswrapper;
using vailshnast.statemachines;
using vailshnast.windows;

namespace MobRun
{
    public class GameVictoryState : ISimpleState
    {
        private readonly IWindowsService _windowsService;
        private readonly ILevelService _levelService;
        private readonly IEntitasBootstrap _entitasBootstrap;
        private readonly IGameModuleService _gameModuleService;

        public GameVictoryState(IWindowsService windowsService,
                                ILevelService levelService,
                                IEntitasBootstrap entitasBootstrap,
                                IGameModuleService gameModuleService)
        {
            _windowsService = windowsService;
            _levelService = levelService;
            _entitasBootstrap = entitasBootstrap;
            _gameModuleService = gameModuleService;
        }

        public void Initialize(IStateMachine stateMachine)
        {
            
        }

        public async void EnterState()
        {
            _windowsService.RequestShowWindow(new GameVictoryWindowController());

            await _entitasBootstrap.DisposeAsync();
            _gameModuleService.Dispose();
            
            _levelService.CompleteLevel();
        }
    }
}