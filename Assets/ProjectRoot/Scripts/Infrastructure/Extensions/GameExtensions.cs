namespace MobRun
{
    public static class GameExtensions
    {
        public static float GetDirection(this SlideDirection direction)
        {
            var dir = 0f;
            switch (direction)
            {
                case SlideDirection.Left:
                    dir =  -1;
                    break;
                case SlideDirection.Right:
                    dir = 1;
                    break;
            }

            return dir;
        }
    }
}