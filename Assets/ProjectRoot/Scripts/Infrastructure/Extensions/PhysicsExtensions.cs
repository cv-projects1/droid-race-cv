using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PhysicsExtensions
{
    public static Vector3 GetLaunchVelocity(Vector3 lauchPoint, Vector3 targetPoint, float angle, float gravityScale)
    {
        // think of it as top-down view of vectors: 
        //   we don't care about the y-component(height) of the initial and target position.
        var projectileXZPos = new Vector3(lauchPoint.x, 0.0f, lauchPoint.z);
        var targetXZPos = new Vector3(targetPoint.x, 0.0f, targetPoint.z);

        // shorthands for the formula
        var R = Vector3.Distance(projectileXZPos, targetXZPos);
        var G = Physics.gravity.y * gravityScale;
        var tanAlpha = Mathf.Tan(angle * Mathf.Deg2Rad);
        var H = targetPoint.y - lauchPoint.y;

        // calculate the local space components of the velocity 
        // required to land the projectile on the target object 
        var Vz = Mathf.Sqrt(G * R * R / (2.0f * (H - R * tanAlpha)) );
        var Vy = tanAlpha * Vz;

        // create the velocity vector in local space and get it in global space
        var rotation = Quaternion.LookRotation(targetXZPos - projectileXZPos);
        var localVelocity = new Vector3(0f, Vy, Vz);
        var globalVelocity = rotation * localVelocity;

        // launch the object by setting its initial velocity and flipping its state
        return globalVelocity;
    }
}
