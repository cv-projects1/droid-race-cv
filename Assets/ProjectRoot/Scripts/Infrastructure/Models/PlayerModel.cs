using System;
using vailshnast.observer;

namespace MobRun
{
    public class PlayerModel : Observable
    {
        public PlayerSave PlayerSave { get; private set; }

        public void SetPlayerSave(PlayerSave playerSave)
        {
            PlayerSave = playerSave;
            SetChanged();
        }

        public void AdjustCurrencyAmount(int amount)
        {
            PlayerSave.Currency += amount;
            SetChanged();
        }

        public void SetLevelIndex(int level) => PlayerSave.LevelIndex = level;
        public void SetLastExitTime() => PlayerSave.LastExitTime = DateTime.Now;

        public void SetUpgrade(UpgradeType upgradeType, int grade)
        {
            PlayerSave.Upgrades[upgradeType] = grade;
            SetChanged();
        }
    }
}
