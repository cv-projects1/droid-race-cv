using System;
using vailshnast.statemachines;
using vailshnast.windows;

namespace MobRun
{
    public class EditorDebugService : IDebugService
    {
        private readonly IStateMachine _stateMachine;
        private readonly IWindowsService _windowsService;
        private readonly DebugConfig _debugConfig;
        private readonly PlayerModel _playerModel;

        public EditorDebugService(IStateMachine stateMachine,
                                  IWindowsService windowsService,
                                  DebugConfig debugConfig,
                                  PlayerModel playerModel)
        {
            _stateMachine = stateMachine;
            _windowsService = windowsService;
            _debugConfig = debugConfig;
            _playerModel = playerModel;
        }

        public void Initialize()
        {
            _stateMachine.OnStateEntered += OnStateEnter;
        }

        private void OnStateEnter(Type type)
        {
            var typeName = type.Name;

            switch (typeName)
            {
                case nameof(GameLoopState):
                    if (_debugConfig.StopWatchEnabled)
                        _windowsService.RequestShowWindow(new GameStopWatchWindowController());
                    break;
                case nameof(GameVictoryState):
                    _windowsService.GetWindow<GameStopWatchWindow, GameStopWatchWindowController>().DisableStopWatch();

                    if (_debugConfig.LoadSameLevelEachTime)
                        _playerModel.PlayerSave.LevelIndex--;
                    
                    break;
                case nameof(GameLossState):
                    _windowsService.GetWindow<GameStopWatchWindow, GameStopWatchWindowController>().DisableStopWatch();
                    break;
            }
        }
    }
}