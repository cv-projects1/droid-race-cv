using UnityEngine;
using vailshnast.entitasview;

namespace MobRun
{
    public class EntityCreationService : IEntityCreationService
    {
        public void RemoveCrowdUnit(GameEntity crowdUnitEntity)
        {
            crowdUnitEntity.entityView.Value.GetComponentView<ITransform>().TransformValue.parent = null;
            crowdUnitEntity.entityView.Value.GetComponentView<IRigidbody>().RigidbodyValue.isKinematic = true;
            crowdUnitEntity.isCrowdUnit = false;
            crowdUnitEntity.isCrowdUnitGrounded = false;
            crowdUnitEntity.RemoveCrowdUnitIndex();
            //crowdUnitEntity.RemoveCrowdLocalTarget();
            crowdUnitEntity.RemoveLocalPosition();
        }
    }
}