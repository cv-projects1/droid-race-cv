using UnityEngine;

namespace MobRun
{
    public interface IEntityCreationService
    {
        void  RemoveCrowdUnit(GameEntity crowdUnitEntity);
    }
}