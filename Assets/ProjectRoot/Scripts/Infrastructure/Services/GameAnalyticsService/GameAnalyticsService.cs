using System;
using System.Collections.Generic;
using UnityEngine;
using vailshnast.statemachines;

namespace MobRun
{
    public class GameAnalyticsService : IGameAnalyticsService
    {
        private readonly IStateMachine _stateMachine;
        private readonly ISaveService _saveService;
        private readonly IAnalyticsService _analyticsService;
        private readonly ILevelService _levelService;
        private readonly PlayerModel _playerModel;
        private readonly GameContext _gameContext;

        public GameAnalyticsService(IStateMachine stateMachine,
                                    ISaveService saveService,
                                    IAnalyticsService analyticsService,
                                    ILevelService levelService,
                                    PlayerModel playerModel,
                                    GameContext gameContext)
        {
            _stateMachine = stateMachine;
            _saveService = saveService;
            _analyticsService = analyticsService;
            _levelService = levelService;
            _playerModel = playerModel;
            _gameContext = gameContext;
        }

        public void Initialize()
        {
            _stateMachine.OnStateEntered += OnStateEnter;
            _stateMachine.OnBeforeStateEntered += OnBeforeStateEnter;

            if (_saveService.IsFirstLaunch)
                _analyticsService.LogEvent(new EventData {EventName = AnalyticsConstants.SessionFirst});

            Debug.Log($"Is First Launch {_saveService.IsFirstLaunch}");
            //_analyticsService.LogEvent(new EventData {EventName = AnalyticsConstants.SessionStart});
        }

        private void OnBeforeStateEnter(Type type)
        {
            var typeName = type.Name;

            switch (typeName)
            {
                case nameof(GameVictoryState):
                    if (_levelService.CurrentLevelIndex != 0)
                        _analyticsService.LogEvent(GetCurrentLevelEvent(AnalyticsConstants.LevelCompleted));

                    break;
            }
        }

        private void OnStateEnter(Type type)
        {
            var typeName = type.Name;

            switch (typeName)
            {
                case nameof(GameStartState):
                    if (_levelService.CurrentLevelIndex != 0)
                        _analyticsService.LogEvent(GetCurrentLevelEvent(AnalyticsConstants.LevelStarted));

                    break;
                case nameof(GameLossState):
                    _analyticsService.LogEvent(GetCurrentLevelEvent(AnalyticsConstants.LevelFailed));

                    break;
                case nameof(GameRestartState):
                    if (!_stateMachine.IsPreviousStateTypeOf<GameVictoryState>())
                        _analyticsService.LogEvent(GetCurrentLevelEvent(AnalyticsConstants.LevelRestart));

                    break;
            }
        }

        private EventParamData GetCurrentLevelEvent(string eventName)
        {
            return new EventParamData()
            {
                EventName = eventName, EventParams = new Dictionary<string, object>()
                {
                    {AnalyticsConstants.LevelID, _levelService.CurrentLevelIndex + 1}
                }
            };
        }

        private EventParamData GetSingleEventParamed(string eventName, string paramName, object paramValue)
        {
            return new EventParamData()
            {
                EventName = eventName, EventParams = new Dictionary<string, object>()
                {
                    {paramName, paramValue}
                }
            };
        }
    }
}