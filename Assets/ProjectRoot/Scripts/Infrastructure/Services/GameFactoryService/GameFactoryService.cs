using vailshnast.entitasview;
using vailshnast.pooledfactories;

namespace MobRun
{
    public class GameFactoryService : IGameFactoryService
    {
        private readonly IGameModuleService _gameModuleService;
        private readonly IBasePooledFactory _basePooledFactory;

        public GameFactoryService(IGameModuleService gameModuleService,
                                  IBasePooledFactory basePooledFactory)
        {
            _gameModuleService = gameModuleService;
            _basePooledFactory = basePooledFactory;
        }

        public void Initialize()
        {
        }

        public IEntityView CreateCrowdUnit()
        {
            var pooledEntityObject = _basePooledFactory.
                CreatePoolObject<PooledEntityObject>(PooledConstants.CrowdUnit);

            pooledEntityObject.transform.SetParent(_gameModuleService.Module.CrowdParent);

            return pooledEntityObject.EntityView;
        }

        public PooledEffect CreateEffect(string id)
            => _basePooledFactory.CreatePoolObject<PooledEffect>(id);

        public void Cleanup()
        {
            
        }
    }
}