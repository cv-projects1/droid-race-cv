using vailshnast.entitasview;
using vailshnast.pool;

namespace MobRun
{
    public interface IGameFactoryService
    {
        void Initialize();
        void Cleanup();
        IEntityView CreateCrowdUnit();
        PooledEffect CreateEffect(string id);
    }
}