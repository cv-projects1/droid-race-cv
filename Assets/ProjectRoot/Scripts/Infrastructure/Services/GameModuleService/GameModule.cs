using Cinemachine;
using UnityEngine;

namespace MobRun
{
    public class GameModule : MonoBehaviour
    {
        [SerializeField] private Camera _gameCamera;
        [SerializeField] private Transform _moduleParent;
        [SerializeField] public Transform _crowdParent;
        [SerializeField] private LineRenderer _lineRenderer;
        
        [SerializeField] private CinemachineVirtualCamera _virtualGameCamera,
                                                          _virtualPreviewCamera;

        public Transform ModuleParent => _moduleParent;
        public LineRenderer LineRenderer => _lineRenderer;
        public Transform CrowdParent => _crowdParent;
        public Camera GameCamera => _gameCamera;
        public CinemachineVirtualCamera VirtualGameCamera => _virtualGameCamera;
        public CinemachineVirtualCamera VirtualPreviewCamera => _virtualPreviewCamera;
    }
}