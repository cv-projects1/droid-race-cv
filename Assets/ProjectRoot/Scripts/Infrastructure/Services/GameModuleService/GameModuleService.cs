using UnityEngine;
using UnityEngine.Rendering.Universal;
using vailshnast.entitasview;
using vailshnast.windows;
using Zenject;

namespace MobRun
{
    public class GameModuleService : IGameModuleService
    {
        private readonly DiContainer _diContainer;
        private readonly PrefabsConfig _prefabsConfig;
        private readonly IWindowsService _windowsService;
        public GameModule Module { get; private set; }
        private EntityListenerContainer _entityListenerContainer;

        public GameModuleService(DiContainer diContainer,
                                 PrefabsConfig prefabsConfig,
                                 IWindowsService windowsService)
        {
            _diContainer = diContainer;
            _prefabsConfig = prefabsConfig;
            _windowsService = windowsService;
        }

        public void Initialize()
        {
            var gameWindow = _windowsService.GetWindow<GameWindow, GameWindowController>();
            Module = _diContainer.InstantiatePrefabForComponent<GameModule>(_prefabsConfig.ModulePrefab);
            var moduleGameObject = Module.gameObject;
            moduleGameObject.SetActive(false);

            var moduleCamera = Module.GameCamera;
            var uiCamera = _windowsService.WindowsContainer.UICamera;
            var cameraData = moduleCamera.GetUniversalAdditionalCameraData();
            cameraData.cameraStack.Add(uiCamera);
            
            _entityListenerContainer = new EntityListenerContainer(new[]
            {
                moduleGameObject,
                gameWindow.gameObject
            });

            _entityListenerContainer.Initialize();
            
            var root = new GameObject($"Root");
            Module.transform.SetParent(root.transform);
            Module.gameObject.SetActive(true);
        }

        public void Start() => _entityListenerContainer.Startup();
        public void PostStartup() => _entityListenerContainer.PostStartup();

        public void Dispose()
        {
            _entityListenerContainer.Dispose();

            //LevelModule.gameObject.SetActive(false);
        }
    }
}