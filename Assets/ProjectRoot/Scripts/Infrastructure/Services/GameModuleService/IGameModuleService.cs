namespace MobRun
{
    public interface IGameModuleService
    {
        GameModule Module { get; }
        void Initialize();
        void Start();
        void PostStartup();
        void Dispose();
    }
}