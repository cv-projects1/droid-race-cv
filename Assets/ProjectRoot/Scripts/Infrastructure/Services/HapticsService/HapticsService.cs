using Lofelt.NiceVibrations;
using UnityEngine;
using vailshnast.applicationevents;
using vailshnast.serviceparent;

namespace MobRun
{
    public class HapticsService : IHapticsService
    {
        private readonly IServicesParent _servicesParent;
        private readonly ISaveService _saveService;
        private readonly IApplicationEventsService _applicationEventsService;
        private readonly PlayerModel _playerModel;
        private readonly HapticConfig _hapticConfig;
        private float _lastVibrateTime;
        
        public HapticsService(IServicesParent servicesParent,
                              ISaveService saveService,
                              IApplicationEventsService applicationEventsService,
                              PlayerModel playerModel,
                              HapticConfig hapticConfig)
        {
            _servicesParent = servicesParent;
            _saveService = saveService;
            _applicationEventsService = applicationEventsService;
            _playerModel = playerModel;
            _hapticConfig = hapticConfig;
        }
        
        public void Initialize()
        {
            HapticController.Init();
            HapticController.hapticsEnabled = _playerModel.PlayerSave.HapticsEnabled;
            _applicationEventsService.AddApplicationFocusSubscriber(HapticController.ProcessApplicationFocus);
        }

        public void CreateHaptic(GameHapticSettings settings)
        {
            var currentTime = Time.time;

            if (currentTime < _lastVibrateTime + _hapticConfig.HapticDelay)
                return;
            
            if(!settings.IsOn) return;
            _lastVibrateTime = Time.time;
            
            HapticPatterns.PlayPreset(settings.Pattern);
        }

        public void Disable()
        {
            _playerModel.PlayerSave.HapticsEnabled = false;
            HapticController.hapticsEnabled = false;
            _saveService.Save();
        }

        public void Enable()
        {
            _playerModel.PlayerSave.HapticsEnabled = true;
            HapticController.hapticsEnabled = true;
            _saveService.Save();
        }

        public void Toggle()
        {
            var enabled = !_playerModel.PlayerSave.HapticsEnabled;
            _playerModel.PlayerSave.HapticsEnabled = enabled;
            HapticController.hapticsEnabled = enabled;
            _saveService.Save();
        }

        public void Vibrate() => Handheld.Vibrate();
    }
}