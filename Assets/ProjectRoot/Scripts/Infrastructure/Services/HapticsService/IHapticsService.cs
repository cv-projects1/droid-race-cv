namespace MobRun
{
    public interface IHapticsService
    {
        void Initialize();
        void CreateHaptic(GameHapticSettings settings);
        void Disable();
        void Enable();
        void Toggle();
    }
}