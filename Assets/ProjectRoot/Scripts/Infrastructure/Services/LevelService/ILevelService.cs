using System.Threading.Tasks;

namespace MobRun
{
    public interface ILevelService
    {
        void Initialize();
        void CompleteLevel();
        Task LoadLevel(int index);
        Task LoadCurrentLevel();
        Task LoadStartScene();
        int CurrentLevelIndex { get; }
        string GetVisualLevelIndex { get; }
    }
}