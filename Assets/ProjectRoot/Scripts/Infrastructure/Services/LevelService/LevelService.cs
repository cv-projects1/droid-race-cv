using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using vailshnast.sceneloading;

namespace MobRun
{
    public class LevelService : ILevelService
    {
        private readonly PlayerModel _playerModel;
        private readonly SceneConfig _sceneConfig;
        private readonly DebugConfig _debugConfig;
        private readonly ISceneLoadingService _sceneLoadingService;
        private readonly ISaveService _saveService;

        public int CurrentLevelIndex => _playerModel.PlayerSave.LevelIndex;
        public string GetVisualLevelIndex => (CurrentLevelIndex + 1).ToString();
        
        public LevelService(PlayerModel playerModel,
                            SceneConfig sceneConfig,
                            DebugConfig debugConfig,
                            ISceneLoadingService sceneLoadingService,
                            ISaveService saveService)
        {
            _playerModel = playerModel;
            _sceneConfig = sceneConfig;
            _debugConfig = debugConfig;
            _sceneLoadingService = sceneLoadingService;
            _saveService = saveService;
        }

        public void Initialize()
        {
#if UNITY_EDITOR
            if (_debugConfig.DebugExecution == DebugExecution.LoadExactLevel)
                _playerModel.SetLevelIndex(_debugConfig.LevelIndex - 1);
#endif
        }

        public void CompleteLevel()
        {
            var curentLevelIndex = _playerModel.PlayerSave.LevelIndex;

            _playerModel.SetLevelIndex(curentLevelIndex + 1);
            _saveService.Save();
        }
        
        public async Task LoadLevel(int index)
        {
            var sceneName = _sceneConfig.LevelScenes[index].GetSceneName();
            await _sceneLoadingService.LoadSceneAsync(sceneName);
        }
        public async Task LoadCurrentLevel()
        {
            var curentLevelIndex = _playerModel.PlayerSave.LevelIndex;
            var maxLevelIndex = _sceneConfig.LevelScenes.Length - 1;

            curentLevelIndex = Mathf.Clamp(curentLevelIndex, 0, maxLevelIndex);

            await LoadLevel(curentLevelIndex);
        }

        public async Task LoadStartScene()
        {
#if UNITY_EDITOR
            if (_debugConfig.DebugExecution != DebugExecution.LoadCurrentScene)
                await LoadCurrentLevel();
            else await _sceneLoadingService.LoadSceneAsync(SceneManager.GetActiveScene().name);
#else
            await LoadCurrentLevel();
#endif
        }
    }
}