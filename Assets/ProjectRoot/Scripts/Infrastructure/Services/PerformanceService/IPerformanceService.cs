namespace MobRun
{
    public interface IPerformanceService
    {
        void Initialize();
        void ShowDebugger();
        void HideDebugger();
    }
}