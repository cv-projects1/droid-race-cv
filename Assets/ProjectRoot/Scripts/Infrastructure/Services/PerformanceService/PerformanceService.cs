using Tayx.Graphy;
using UnityEngine;
using vailshnast.serviceparent;

namespace MobRun
{
    public class PerformanceService : IPerformanceService
    {
        private readonly DebugConfig _debugConfig;
        private readonly IServicesParent _servicesParent;
        private GraphyManager _graphy;

        public PerformanceService(DebugConfig debugConfig, IServicesParent servicesParent)
        {
            _debugConfig = debugConfig;
            _servicesParent = servicesParent;
        }
        
        public void Initialize()
        {
            _graphy = Object.Instantiate(_debugConfig.GraphyPrefab);
            _graphy.transform.SetParent(_servicesParent.Parent);
            //_graphy.Disable();
            _graphy.gameObject.SetActive(false);
        }

        public void ShowDebugger()
        {
            //_graphy.Enable();
            _graphy.gameObject.SetActive(true);
        }

        public void HideDebugger()
        {
            //_graphy.Disable();
            _graphy.gameObject.SetActive(false);
        }
    }
}