using System;
using UnityEngine;
using vailshnast.pool;

namespace MobRun
{
    public class PooledEffect : PoolBehaviour
    {
        [SerializeField] private ParticleSystem _particleSystem;
        
        public override Type ComponentType => typeof(PooledEffect);

        //private void OnEnable() => _particleSystem.Play(true);
        private void OnParticleSystemStopped() => Destroy();
    }
}