using System.Collections.Generic;
using vailshnast.pool;
using vailshnast.pooledfactories;

namespace MobRun
{
    public class PooledFactoryService : BasePooledFactory
    {
        private readonly IBasePooledFactoryConfig _basePooledFactoryConfig;

        public PooledFactoryService(IPoolService poolService,
                                    IBasePooledFactoryConfig basePooledFactoryConfig) : base(poolService)
        {
            _basePooledFactoryConfig = basePooledFactoryConfig;
        }

        public override List<PoolSettings> GetPoolSettingsList() =>
            _basePooledFactoryConfig.PoolSettings;
    }
}