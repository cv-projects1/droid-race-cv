using System;
using UnityEngine;
using vailshnast.pool;

namespace MobRun
{
    public class PooledRectTransform : PoolBehaviour
    {
        [SerializeField] private RectTransform _rectTransform;
        
        public override Type ComponentType => typeof(PooledRectTransform);
        public RectTransform RectTransform => _rectTransform;
    }
}