namespace MobRun
{
    public interface ISaveService
    {
        bool IsFirstLaunch { get; }
        
        void Initialize();
        void Save();
        void Load();
        void SetNewProgressAndSave();
    }
}