using System;
using System.Collections.Generic;
using UnityEngine;
using vailshnast.extensions;

namespace MobRun
{
    public class SaveService : ISaveService
    {
        public bool IsFirstLaunch { get; private set; }
        private readonly PlayerModel _playerModel;
        private const string SaveKey = "save";
        public void Initialize() => Load();
        public void Save() => SaveProgress(_playerModel.PlayerSave);

        public SaveService(PlayerModel playerModel)
        {
            _playerModel = playerModel;
        }

        public void Load()
        {
            if (PlayerPrefs.HasKey(SaveKey))
            {
                try
                {
                    var loadedProgress = SaveExtensionPrefs.LoadObject<PlayerSave>(SaveKey);
                    _playerModel.SetPlayerSave(loadedProgress);
                    IsFirstLaunch = false;
                }
                catch (Exception exception)
                {
                    Debug.LogError($"{exception.Message}\nCouldnt load old progress, saving new one");
                    SetNewProgressAndSave();
                    IsFirstLaunch = true;
                }
            }
            else
            {
                Debug.Log("There were no save progress saving new one");
                SetNewProgressAndSave();
                IsFirstLaunch = true;
            }
        }

        private PlayerSave GetNewPlayerSave()
        {
            var upgrades = new Dictionary<UpgradeType, int>()
            {
                {UpgradeType.StartAmount, 0},
                {UpgradeType.Alignment, 0},
                {UpgradeType.PassiveIncome, 0}
            };

            return new PlayerSave
            {
                Currency = 0,
                LastExitTime = DateTime.Now,
                LevelIndex = 0,
                Upgrades = upgrades,
                TutorialCompleted = false,
                HapticsEnabled = true,
                AudioEnabled = true
            };
        }

        public void SetNewProgressAndSave()
        {
            var newSave = GetNewPlayerSave();
            _playerModel.SetPlayerSave(newSave);
            SaveProgress(newSave);
        }

        private void SaveProgress(PlayerSave progress) =>
            SaveExtensionPrefs.SaveObject(progress, SaveKey);
    }
}