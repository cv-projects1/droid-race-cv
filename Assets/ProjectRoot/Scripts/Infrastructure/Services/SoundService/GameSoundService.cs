using Lofelt.NiceVibrations;
using UnityEngine;
using vailshnast.pool;
using vailshnast.serviceparent;
using vailshnast.soundservice;

namespace MobRun
{
    public class GameSoundService : IGameSoundService
    {
        private readonly IPoolService _poolService;
        private readonly IServicesParent _servicesParent;
        private readonly ISaveService _saveService;
        private readonly SoundConfig _soundConfig;
        private readonly SoundsContainerConfig _soundsContainerConfig;
        private readonly PlayerModel _playerModel;

        public GameSoundService(IPoolService poolService,
                                IServicesParent servicesParent,
                                ISaveService saveService,
                                SoundConfig soundConfig,
                                SoundsContainerConfig soundsContainerConfig,
                                PlayerModel playerModel)
        {
            _poolService = poolService;
            _servicesParent = servicesParent;
            _saveService = saveService;
            _soundConfig = soundConfig;
            _soundsContainerConfig = soundsContainerConfig;
            _playerModel = playerModel;
        }

        public void Initialize()
        {
            _poolService.CreatePool(_soundConfig.SoundPlayerPrefab.gameObject,
                                    _soundConfig.InitialPoolSize,
                                    _soundConfig.IncreaseBy);


            var parent = _servicesParent.Parent;
            var listenerGameObject = new GameObject("Sound Listener");
            listenerGameObject.AddComponent<AudioListener>();
            listenerGameObject.transform.SetParent(parent);
            
            AudioListener.volume = _playerModel.PlayerSave.AudioEnabled ? 1 : 0;
            
            //InitializeBackgroundMusic();
        }

        public void PlaySound(SoundType soundType)
        {
            if (!_playerModel.PlayerSave.AudioEnabled) return;
            
            var soundGroup = _soundsContainerConfig.GetAudioMixerGroup(AudioGroupType.SoundGroup);
            var soundReference = _soundsContainerConfig.GetSoundReference(soundType);
            var soundClip = soundReference.AudioClip;
            var volume = soundReference.Volume;
            var soundPlayer = GetSoundPlayer();

            soundPlayer.Play(new SoundSettings
            {
                Volume = Vector2.one * volume,
                AudioMixerGroup = soundGroup,
                ClipsToPlay = new[] {soundClip},
                Is3DSound = false,
            });
        }

        public void InitializeBackgroundMusic()
        {
            var soundGroup = _soundsContainerConfig.GetAudioMixerGroup(AudioGroupType.MusicGroup);
            var soundPlayer = GetSoundPlayer();
            var musicClip = _soundsContainerConfig.MusicClip;

            soundPlayer.SetLoop();
            soundPlayer.Play(new SoundSettings
            {
                AudioMixerGroup = soundGroup,
                ClipsToPlay = new[] {musicClip},
                Is3DSound = false
            });
        }

        public SoundPlayer GetSoundPlayer()
        {
            var poolObject = _poolService.InstantiateFromPool(_soundConfig.SoundPlayerPrefab
                                                                          .gameObject);

            return poolObject.ResolveComponent<SoundPlayer>();
        }

        public void Toggle()
        {
            var enabled = !_playerModel.PlayerSave.AudioEnabled;
            _playerModel.PlayerSave.AudioEnabled = enabled;
            AudioListener.volume = enabled ? 1 : 0;
            _saveService.Save();
        }
    }
}