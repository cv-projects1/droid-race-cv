using vailshnast.soundservice;

namespace MobRun
{
    public interface IGameSoundService
    {
        void Initialize();
        void PlaySound(SoundType soundType);
        void InitializeBackgroundMusic();
        SoundPlayer GetSoundPlayer();
        void Toggle();
    }
}