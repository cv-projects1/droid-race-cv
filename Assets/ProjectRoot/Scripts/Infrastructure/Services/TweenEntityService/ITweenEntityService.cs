using DG.Tweening;
using UnityEngine;

namespace MobRun
{
    public interface ITweenEntityService
    {
        Tween CreateMoveTweenFromEntity(GameEntity moveEntity, Vector3 endPos, float duration);

        Tween CreateRotateTweenFromEntity(GameEntity rotateEntity,
                                          Vector3 endValue,
                                          RotateMode rotateMode,
                                          float duration);
    }
}