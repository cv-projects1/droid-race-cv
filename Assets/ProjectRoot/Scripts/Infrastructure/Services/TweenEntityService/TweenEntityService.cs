using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace MobRun
{
    public class TweenEntityService : ITweenEntityService
    {
        private readonly GameContext _gameContext;

        public TweenEntityService(GameContext gameContext)
        {
            _gameContext = gameContext;
        }

        public Tween CreateMoveTweenFromEntity(GameEntity moveEntity, Vector3 endPos, float duration)
        {
            var tween = DOTween.To(() => moveEntity.worldPosition.Value,
                                   moveEntity.ReplaceWorldPosition, endPos,
                                   duration);

            return tween;
        }

        public Tween CreateRotateTweenFromEntity(GameEntity rotateEntity,
                                                 Vector3 endValue,
                                                 RotateMode rotateMode,
                                                 float duration)
        {
            var tween = DOTween.To(() => rotateEntity.rotation.Value, rotateEntity.ReplaceRotation,
                                   endValue, duration);

            tween.plugOptions.rotateMode = rotateMode;

            return tween;
        }
    }
}