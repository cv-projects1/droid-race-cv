using System;
using DG.Tweening;
using UnityEngine;

namespace MobRun
{
    public interface IUIAnimationService
    {
        Tween CreateAnimatedCogIcon(Vector3 worldPosition, float duration, TweenCallback callback);
        Tween CreateAnimatedCogs(Vector3 startPos,
                                 float delay,
                                 float durationEach,
                                 float amount,
                                 TweenCallback onEachComplete = null);
    }
}