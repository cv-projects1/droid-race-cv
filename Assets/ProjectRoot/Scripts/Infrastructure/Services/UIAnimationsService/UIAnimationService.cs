using System;
using DG.Tweening;
using Lofelt.NiceVibrations;
using UnityEngine;
using vailshnast.extensions;
using vailshnast.pooledfactories;
using vailshnast.windows;

namespace MobRun
{
    public class UIAnimationService : IUIAnimationService
    {
        private readonly IBasePooledFactory _basePooledFactory;
        private readonly IWindowsService _windowsService;
        private readonly IGameModuleService _gameModuleService;
        private readonly IHapticsService _hapticsService;
        private readonly IGameSoundService _gameSoundService;
        private readonly HapticConfig _hapticConfig;

        public UIAnimationService(IBasePooledFactory basePooledFactory,
                                  IWindowsService windowsService,
                                  IGameModuleService gameModuleService,
                                  IHapticsService hapticsService,
                                  IGameSoundService gameSoundService,
                                  HapticConfig hapticConfig)
        {
            _basePooledFactory = basePooledFactory;
            _windowsService = windowsService;
            _gameModuleService = gameModuleService;
            _hapticConfig = hapticConfig;
            _hapticsService = hapticsService;
            _gameSoundService = gameSoundService;
        }

        public Tween CreateAnimatedCogIcon(Vector3 worldPosition, float duration, TweenCallback callback)
        {
            var window = _windowsService.GetWindow<GameWindow, GameWindowController>();
            var pointsTargetRect = window.PointsTargetRect;
            var pointsParentRect = window.PointsParentRect;
            var pooledIcon = _basePooledFactory.CreatePoolObject<PooledRectTransform>(PooledConstants.CogsAmount);
            var pooledIconRect = pooledIcon.RectTransform;
            var worldCamera = _gameModuleService.Module.GameCamera;
            var windowsContainer = _windowsService.WindowsContainer;
            var uiCamera = windowsContainer.UICamera;
            var canvasRect = windowsContainer.CanvasRect;
            var startPos = CanvasUtilities.WorldToCanvasPoint(worldPosition, canvasRect, worldCamera, uiCamera);
            var endPos = pointsTargetRect.transform.position;
            pooledIconRect.SetParent(pointsParentRect);
            pooledIconRect.anchoredPosition3D = new Vector3(startPos.x, startPos.y, 0);
            pooledIconRect.localScale = Vector3.one;
            pooledIcon.gameObject.SetActive(false);

            return pooledIconRect.DOMove(endPos, duration).OnStart(() => pooledIcon.gameObject.SetActive(true))
                                 .OnComplete(() =>
                                  {
                                      OnCogsAnimCompleted(callback, pooledIcon);
                                      _gameSoundService.PlaySound(SoundType.CurrencyEarn);
                                      _hapticsService.CreateHaptic(_hapticConfig.CurrencyCoin);
                                  });
        }

        private void OnCogsAnimCompleted(TweenCallback callback,
                                                PooledRectTransform pooledIcon)
        {
            callback?.Invoke();
            pooledIcon.Destroy();
        }

        public Tween CreateAnimatedCogs(Vector3 startPos,
                                        float delay,
                                        float durationEach,
                                        float amount,
                                        TweenCallback onEachComplete = null)
        {
            var sequence = DOTween.Sequence();

            for (int i = 0; i < amount; i++)
            {
                var tween = CreateAnimatedCogIcon(startPos,durationEach, onEachComplete);
                sequence.Insert(i * delay, tween);
            }

            return sequence;
        }
    }
}