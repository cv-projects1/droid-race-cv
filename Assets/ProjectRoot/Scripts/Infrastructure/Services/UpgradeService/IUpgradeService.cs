namespace MobRun
{
    public interface IUpgradeService
    {
        void Initialize();
        void ApplyStartUpgrades();
        void ApplyGameUpgrades(GameEntity coreEntity);
        bool BuyUpgrade(UpgradeType upgradeType);
        bool ContainsBuyableUpgrade();
        bool EnoughToBuyUpgrade(UpgradeType upgradeType);
    }
}