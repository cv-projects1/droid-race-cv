using System.Collections.Generic;
using Zenject;

namespace MobRun
{
    public class UpgradeService : IUpgradeService
    {
        private readonly DiContainer _diContainer;
        private readonly UpgradeConfig _upgradeConfig;
        private readonly PlayerModel _playerModel;
        private readonly ISaveService _saveService;
        private Dictionary<UpgradeType, IUpgrade> _upgrades;

        public UpgradeService(DiContainer diContainer,
                              UpgradeConfig upgradeConfig,
                              PlayerModel playerModel,
                              ISaveService saveService)
        {
            _diContainer = diContainer;
            _upgradeConfig = upgradeConfig;
            _playerModel = playerModel;
            _saveService = saveService;
        }

        public void Initialize()
        {
            _upgrades = new Dictionary<UpgradeType, IUpgrade>();
            AddUpgrade<PassiveIncomeUpgrade>();
            AddUpgrade<AlignmentUpgrade>();
            AddUpgrade<StartAmountUpgrade>();
        }

        public void ApplyStartUpgrades()
        {
            var playerSave = _playerModel.PlayerSave;

            foreach (var upgrade in _upgrades.Values)
            {
                var upgradeIndex = playerSave.Upgrades[upgrade.UpgradeType];
                upgrade.ApplyStartUpgrades(upgradeIndex);
            }
        }

        public void ApplyGameUpgrades(GameEntity coreEntity)
        {
            var playerSave = _playerModel.PlayerSave;

            foreach (var upgrade in _upgrades.Values)
            {
                var upgradeIndex = playerSave.Upgrades[upgrade.UpgradeType];
                upgrade.ApplyGameUpgrades(coreEntity,upgradeIndex);
            }
        }

        private void AddUpgrade<T>() where T : IUpgrade
        {
            var upgrade = _diContainer.Instantiate<T>();
            var type = upgrade.UpgradeType;
            _upgrades.Add(type, upgrade);
            var upgradeSettings = _upgradeConfig.GetSettings(type);
            upgrade.Initialize(upgradeSettings);
        }

        public bool BuyUpgrade(UpgradeType upgradeType)
        {
            var playerSave = _playerModel.PlayerSave;
            var upgrades = playerSave.Upgrades;
            var upgradeSettings = _upgradeConfig.GetSettings(upgradeType);
            var stepsAmount = upgradeSettings.UpgradeSteps.Count;
            var currentGrade = upgrades[upgradeType];
            var nextGrade = currentGrade + 1;

            if (nextGrade >= stepsAmount) return false;

            var upgradeStep = upgradeSettings.UpgradeSteps[nextGrade];
            var playerCurrency = playerSave.Currency;
            var upgradePrice = upgradeStep.Price;

            if (upgradePrice > playerCurrency) return false;

            _playerModel.SetUpgrade(upgradeType, nextGrade);
            _playerModel.AdjustCurrencyAmount(-upgradePrice);
            _saveService.Save();

            return true;
        }

        public bool EnoughToBuyUpgrade(UpgradeType upgradeType)
        {
            var playerSave = _playerModel.PlayerSave;
            var upgrades = playerSave.Upgrades;
            var upgradeSettings = _upgradeConfig.GetSettings(upgradeType);
            var stepsAmount = upgradeSettings.UpgradeSteps.Count;
            var currentGrade = upgrades[upgradeType];
            var nextGrade = currentGrade + 1;

            if (nextGrade >= stepsAmount) return false;

            var upgradeStep = upgradeSettings.UpgradeSteps[nextGrade];
            var playerCurrency = playerSave.Currency;
            var upgradePrice = upgradeStep.Price;

            return upgradePrice <= playerCurrency;
        }

        public bool ContainsBuyableUpgrade()
        {
            var playerSave = _playerModel.PlayerSave;
            var upgrades = playerSave.Upgrades;

            foreach (var upgradeType in _upgrades.Keys)
            {
                var upgradeSettings = _upgradeConfig.GetSettings(upgradeType);
                var currentGrade = upgrades[upgradeType];
                var stepsAmount = upgradeSettings.UpgradeSteps.Count;
                var nextGrade = currentGrade + 1;
                
                if (nextGrade >= stepsAmount)
                    continue;
                
                var upgradeStep = upgradeSettings.UpgradeSteps[nextGrade];
                var playerCurrency = playerSave.Currency;
                var upgradePrice = upgradeStep.Price;
                
                if (upgradePrice <= playerCurrency) return true;
            }

            return false;
        }
    }
}