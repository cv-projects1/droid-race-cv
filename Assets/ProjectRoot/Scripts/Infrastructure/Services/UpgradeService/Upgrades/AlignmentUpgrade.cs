namespace MobRun
{
    public class AlignmentUpgrade : IUpgrade
    {
        private UpgradeSettings _upgradeSettings;
        private readonly GameContext _gameContext;
        public UpgradeType UpgradeType => UpgradeType.Alignment;

        public void Initialize(UpgradeSettings upgradeSettings)
        {
            _upgradeSettings = upgradeSettings;
        }

        public AlignmentUpgrade(GameContext gameContext)
        {
            _gameContext = gameContext;
        }

        
        public void ApplyStartUpgrades(int upgradeIndex)
        {
            
        }

        public void ApplyGameUpgrades(GameEntity coreEntity, int upgradeIndex)
        {
            var upgradeStep = _upgradeSettings.UpgradeSteps[upgradeIndex];

            coreEntity.AddCrowdAssistSpeed(upgradeStep.Value);
        }
    }
}