namespace MobRun
{
    public interface IUpgrade
    {
        UpgradeType UpgradeType { get; }
        void Initialize(UpgradeSettings upgradeSettings);
        void ApplyStartUpgrades(int upgradeIndex);
        void ApplyGameUpgrades(GameEntity coreEntity, int upgradeIndex);
    }
}