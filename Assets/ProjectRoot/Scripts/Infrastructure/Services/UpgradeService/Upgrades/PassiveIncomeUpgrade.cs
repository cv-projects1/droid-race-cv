using System;
using vailshnast.windows;

namespace MobRun
{
    public class PassiveIncomeUpgrade : IUpgrade
    {
        private readonly ISaveService _saveService;
        private readonly IWindowsService _windowsService;
        private readonly PlayerModel _playerModel;
        private UpgradeSettings _upgradeSettings;
        public UpgradeType UpgradeType => UpgradeType.PassiveIncome;

        public PassiveIncomeUpgrade(ISaveService saveService,
                                    IWindowsService windowsService,
                                    PlayerModel playerModel)
        {
            _saveService = saveService;
            _windowsService = windowsService;
            _playerModel = playerModel;
        }

        public void Initialize(UpgradeSettings upgradeSettings)
        {
            _upgradeSettings = upgradeSettings;
        }

        public void ApplyStartUpgrades(int upgradeIndex)
        {
            if (_saveService.IsFirstLaunch) return;

            var upgradeStep = _upgradeSettings.UpgradeSteps[upgradeIndex];
            var timeSpan = DateTime.Now - _playerModel.PlayerSave.LastExitTime;
            var minutes = timeSpan.TotalMinutes;
            var totalCurrency = (int) (minutes * upgradeStep.Value);

            if (totalCurrency > 0)
            {
                _playerModel.PlayerSave.LastExitTime = DateTime.Now;
                _saveService.Save();
                _windowsService.RequestShowWindow(new GamePassiveIncomeWindowController {CurrencyAmount = totalCurrency});
            }
        }

        public void ApplyGameUpgrades(GameEntity coreEntity, int upgradeIndex)
        {
        }
    }
}