using vailshnast.extensions;

namespace MobRun
{
    public class StartAmountUpgrade : IUpgrade
    {
        private readonly GameContext _gameContext;
        private UpgradeSettings _upgradeSettings;
        public UpgradeType UpgradeType => UpgradeType.StartAmount;

        public void Initialize(UpgradeSettings upgradeSettings)
        {
            _upgradeSettings = upgradeSettings;
        }

        public StartAmountUpgrade(GameContext gameContext)
        {
            _gameContext = gameContext;
        }

        public void ApplyStartUpgrades(int upgradeIndex)
        {
            
        }

        public void ApplyGameUpgrades(GameEntity coreEntity, int upgradeIndex)
        {
            var upgradeStep = _upgradeSettings.UpgradeSteps[upgradeIndex];
            
            var crowdSpawnEntity = _gameContext.CreateEntity();
            crowdSpawnEntity.AddCrowdSpawn(upgradeStep.Value.ToNearestInt());
            crowdSpawnEntity.isCrowdStartUnit = true;
        }
    }
}