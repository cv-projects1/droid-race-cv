using System;
using UnityEngine;
using vailshnast.extensions;
using vailshnast.installers;

namespace MobRun
{
    [CreateAssetMenu(fileName = "AnimationConfig", menuName = "StaticData/AnimationConfig")]
    public class AnimationConfig : ScriptableObject, IScriptableService
    {
        public Type ServiceType => typeof(AnimationConfig);
        public DotweenPunchSettings UnitSpawnAnimSettings;
#if UNITY_EDITOR
        [Sirenix.OdinInspector.BoxGroup("Finish Box")]
#endif
        public DotweenSettings FinishBoxAnimSettings;
#if UNITY_EDITOR
        [Sirenix.OdinInspector.BoxGroup("Finish Box")]
#endif
        public float FinishBoxRotation = -10,
                     FinishBoxFlyDistance = 250;
        
#if UNITY_EDITOR
        [Sirenix.OdinInspector.BoxGroup("CrowdUnits")]
#endif
        public DotweenSettings CrowdFallAnimSettings;
#if UNITY_EDITOR
            [Sirenix.OdinInspector.BoxGroup("CrowdUnits")]
#endif
            public float CrowdFlyHeight = -50,
                         CenteringSpeed = 1;
    }
}