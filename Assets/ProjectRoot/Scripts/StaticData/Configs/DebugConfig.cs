using System;
using Tayx.Graphy;
using UnityEngine;
using vailshnast.installers;

namespace MobRun
{
    [CreateAssetMenu(fileName = "DebugConfig", menuName = "StaticData/DebugConfig")]
    public class DebugConfig : ScriptableObject, IScriptableService
    {
        public Type ServiceType => typeof(DebugConfig);

        public GraphyManager GraphyPrefab;
        public bool EnableDebugSystems;

        
        public bool StopWatchEnabled;
        public bool LoadSameLevelEachTime;
        public DebugExecution DebugExecution;
#if UNITY_EDITOR
        private bool ShowProp => DebugExecution == DebugExecution.LoadExactLevel;

        [Sirenix.OdinInspector.ShowIf(nameof(ShowProp))]
#endif

        public int LevelIndex;
#if UNITY_EDITOR
        [Sirenix.OdinInspector.Button]
        private void ClearSaves() => PlayerPrefs.DeleteAll();
#endif
    }

    public enum DebugExecution
    {
        LoadSavedLevel = 0,
        LoadCurrentScene = 1,
        LoadExactLevel = 2
    }
}