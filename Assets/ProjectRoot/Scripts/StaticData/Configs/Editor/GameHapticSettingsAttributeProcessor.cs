#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Reflection;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;

namespace MobRun
{
    public class GameHapticSettingsAttributeProcessor : OdinAttributeProcessor<GameHapticSettings>
    {
        public override void ProcessChildMemberAttributes(InspectorProperty parentProperty, MemberInfo member,
                                                          List<Attribute> attributes)
        {
            base.ProcessChildMemberAttributes(parentProperty, member, attributes);

            switch (member.Name)
            {
                case "IsOn":
                    attributes.Add(new BoxGroupAttribute(parentProperty.Name));

                    break;
                case "Pattern":
                    
                    attributes.Add(new ShowIfAttribute("IsOn"));
                    attributes.Add(new BoxGroupAttribute(parentProperty.Name));

                    break;
            }
        }

        public override void ProcessSelfAttributes(InspectorProperty property, List<Attribute> attributes)
        {
            base.ProcessSelfAttributes(property, attributes);
            attributes.Add(new HideLabelAttribute());
        }
    }
}

#endif