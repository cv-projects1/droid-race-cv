using System;
using UnityEngine;
using vailshnast.installers;
using vailshnast.math;

namespace MobRun
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "StaticData/GameConfig")]
    public class GameConfig : ScriptableObject, IScriptableService
    {
        public Type ServiceType => typeof(GameConfig);
#if UNITY_EDITOR
        [Sirenix.OdinInspector.Title("Crowd Spawn Shape")]
#endif
        public float PhyllotaxisScale = 0.65f;

        public float PhyllotasixAngle = 137.5f;
#if UNITY_EDITOR
        [Sirenix.OdinInspector.Title("Crowd General Settings")]
#endif
        public float CrowdMoveSpeed = 50;
        public float CrowdStartMoveDelay = 0.5f;

        public float CrowdFormationSpeed = 3;
#if UNITY_EDITOR
        [Sirenix.OdinInspector.Title("Magnetizing Settings")]
#endif
        public float MagnetizingRotationSpeed = 2;

        public float MagnetizingForceInUnits = 300;
        public float MagnetizingAccelerationDuration = 1;
#if UNITY_EDITOR
        [Sirenix.OdinInspector.Title("Crowd Finish Settings")]
#endif
        public int CrowdAmountPerBox = 3;

        public float CrowdFinishMoveSpeed = 10;

        public Vector2 GetPhyllotaxisPos(int count) =>
            Phyllotaxis.Calculate(PhyllotasixAngle,
                                  PhyllotaxisScale, count);

#if UNITY_EDITOR
        [Sirenix.OdinInspector.Title("Economics")]
#endif
        public int SmallBoxCurrencyAmount = 2;

        public int BigBoxCurrencyAmount = 10;
#if UNITY_EDITOR
        [Sirenix.OdinInspector.Title("Other")]
#endif
        public float GetPhyllotaxisRadius(int count) => Phyllotaxis.GetRadius(PhyllotaxisScale, count);
    }
}