using System;
using Lofelt.NiceVibrations;
using UnityEngine;
using vailshnast.installers;

namespace MobRun
{
    [CreateAssetMenu(fileName = "HapticConfig", menuName = "StaticData/HapticConfig")]
    public class HapticConfig : ScriptableObject, IScriptableService
    {
        public Type ServiceType => typeof(HapticConfig);
        
        public GameHapticSettings RobotAdd,
                                  RobotDeath,
                                  Magnet,
                                  FinishLine,
                                  RobotBoxReached,
                                  CurrencyCoin,
                                  FinishPrizeBox;

#if UNITY_EDITOR
        [Sirenix.OdinInspector.Title("Global Delay in seconds")]
#endif
        public float HapticDelay = 0.1f;
    }

    [Serializable]
    public class GameHapticSettings
    {
        public bool IsOn;
        public HapticPatterns.PresetType Pattern;
    }
}