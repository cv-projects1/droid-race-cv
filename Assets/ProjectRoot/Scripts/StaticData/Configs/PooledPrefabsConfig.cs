using System;
using UnityEngine;
using vailshnast.pooledfactories;

namespace MobRun
{
    [CreateAssetMenu(fileName = "PooledPrefabsConfig", menuName = "StaticData/PooledPrefabsConfig")]
    public class PooledPrefabsConfig : BasePooledFactoryConfig
    {
        public override Type ServiceType => 
            typeof(IBasePooledFactoryConfig);
    }
}