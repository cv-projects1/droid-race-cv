using System;
using UnityEngine;
using vailshnast.entitasview;
using vailshnast.installers;

namespace MobRun
{
    [CreateAssetMenu(fileName = "PrefabsConfig",menuName = "StaticData/PrefabsConfig")]
    public class PrefabsConfig : ScriptableObject , IScriptableService
    {
        [SerializeField] private GameModule _modulePrefab;
        public Type ServiceType => typeof(PrefabsConfig);
        
        public GameModule ModulePrefab => _modulePrefab;
    }
}
