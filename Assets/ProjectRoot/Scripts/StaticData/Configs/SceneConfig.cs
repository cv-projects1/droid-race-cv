using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using vailshnast.installers;
using vailshnast.windows;
using Object = System.Object;
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine.SceneManagement;

#endif

namespace MobRun
{
    [CreateAssetMenu(fileName = "SceneConfig", menuName = "StaticData/SceneConfig")]
    public class SceneConfig : ScriptableObject, IScriptableService
    {
        public Type ServiceType => typeof(SceneConfig);
        public SceneReference InitializationScene;
        public SceneReference[] LevelScenes;
#if UNITY_EDITOR
        [Sirenix.OdinInspector.FolderPath]
#endif
        public string LevelScenesFolder;

#if UNITY_EDITOR
        [Sirenix.OdinInspector.Button]
        public void SetupScenes()
        {
            var guids = AssetDatabase.FindAssets("t:scene", new[] {LevelScenesFolder});
            var toSelect = new List<SceneAsset>();
            var scenesPaths = guids.Select(AssetDatabase.GUIDToAssetPath).ToList();
            
            SceneReference GetRef(string path)
            {
                var reference = new SceneReference();
                var sceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(path);
                reference.SetAsset(sceneAsset, path);

                return reference;
            }

            LevelScenes = scenesPaths.Select(GetRef).ToArray();

            var levelScenes = new List<EditorBuildSettingsScene>
            {
                new EditorBuildSettingsScene(InitializationScene.Path, true)
            };

            foreach (var scenesPath in scenesPaths)
                levelScenes.Add(new EditorBuildSettingsScene(scenesPath, true));

            EditorBuildSettings.scenes = levelScenes.ToArray();
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
        }
#endif
    }
}