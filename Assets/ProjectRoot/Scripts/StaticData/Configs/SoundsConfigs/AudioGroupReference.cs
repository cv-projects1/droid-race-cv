using System;
using UnityEngine.Audio;

namespace MobRun
{
    [Serializable]
    public class AudioGroupReference
    {
        public AudioGroupType AudioGroupType;
        public AudioMixerGroup AudioGroup;
    }
}