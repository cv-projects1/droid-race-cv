namespace MobRun
{
    public enum AudioGroupType
    {
        SoundGroup  = 0,
        MusicGroup  = 1,
        MasterGroup = 2
    }
}