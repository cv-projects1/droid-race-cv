using System;
using UnityEngine;

namespace MobRun
{
    [Serializable]
    public class SoundReference
    {
        public SoundType SoundType;
        public AudioClip AudioClip;
        [Range(0, 1)] public float Volume;
    }
}