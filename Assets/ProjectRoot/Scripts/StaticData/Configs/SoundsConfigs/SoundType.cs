namespace MobRun
{
    public enum SoundType
    {
        ButtonClick = 0,
        WindowsOpen = 1,
        WindowsClose = 2,
        WindowsLoose = 3,
        WindowsVictory = 4,
        CurrencyEarn = 5,
        FinishPrizeBoxReached = 6,
        FinishLineReached = 7,
        MagnetStart = 8,
        RobotSpawn = 9,
        RobotDeath = 10
    }
}