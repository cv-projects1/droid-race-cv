using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;
using vailshnast.installers;

namespace MobRun
{
    [CreateAssetMenu(fileName = "SoundsContainerConfig", menuName = "StaticData/SoundsContainerConfig")]
    public class SoundsContainerConfig : ScriptableObject, IScriptableService
    {
        public Type ServiceType => typeof(SoundsContainerConfig);

        public AudioClip MusicClip;
        public SoundReference[] SoundReferences;
        public AudioGroupReference[] AudioGroupReferences;
        
#if UNITY_EDITOR
        [Sirenix.OdinInspector.Title("Sound Delay in seconds")]
#endif
        public float DeathSoundDelay = 0.1f;
        
        public SoundReference GetSoundReference(SoundType soundType) =>
            SoundReferences.FirstOrDefault(x => x.SoundType == soundType);

        public AudioMixerGroup GetAudioMixerGroup(AudioGroupType audioGroupType) =>
            AudioGroupReferences.FirstOrDefault(x => x.AudioGroupType == audioGroupType)?.AudioGroup;
    }
}
