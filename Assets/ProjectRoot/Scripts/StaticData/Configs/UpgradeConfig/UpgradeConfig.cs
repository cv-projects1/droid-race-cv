using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using vailshnast.installers;

namespace MobRun
{
    [CreateAssetMenu(fileName = "UpgradeConfig", menuName = "StaticData/UpgradeConfig")]
    public class UpgradeConfig : ScriptableObject, IScriptableService
    {
        public Type ServiceType => typeof(UpgradeConfig);
        
        [SerializeField] private List<UpgradeSettings> _upgradeSettings;
        
        public UpgradeSettings GetSettings(UpgradeType upgradeType) =>
            _upgradeSettings.FirstOrDefault(x => x.UpgradeType == upgradeType);

#if UNITY_EDITOR
        private void OnValidate()
        {
            foreach (var upgradeSetting in _upgradeSettings)
            {
                var type = upgradeSetting.UpgradeType;
                if (type != UpgradeType.StartAmount) continue;
                
                foreach (var upgradeStep in upgradeSetting.UpgradeSteps)
                    upgradeStep.Value = Mathf.RoundToInt(upgradeStep.Value);
            }
        }
#endif
    }
}