using System;
using System.Collections.Generic;

namespace MobRun
{
    [Serializable]
    public class UpgradeSettings
    {
        public UpgradeType UpgradeType;
        public List<UpgradeStep> UpgradeSteps = new List<UpgradeStep>();
    }
}