using System;

namespace MobRun
{
    [Serializable]
    public class UpgradeStep
    {
        public int Price;
        public float Value;
    }
}