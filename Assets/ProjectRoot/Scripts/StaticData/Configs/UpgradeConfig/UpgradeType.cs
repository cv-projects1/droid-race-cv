namespace MobRun
{
    public enum UpgradeType
    {
        StartAmount = 0,
        Alignment = 1,
        PassiveIncome = 2
    }
}