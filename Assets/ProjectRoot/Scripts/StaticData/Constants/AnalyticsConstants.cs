namespace MobRun
{
    public static class AnalyticsConstants
    {
        public const string SessionFirst = "session_first";
        public const string SessionStart = "session_start";

        public const string LevelStarted = "level_started";     // level_id
        public const string LevelCompleted = "level_completed"; // level_id
        public const string LevelFailed = "level_failed";       // level_id
        public const string LevelRestart = "level_restart";     // level_id
        public const string LevelID = "level_id";
        
        public const string TutorialStarted = "tutorial_started";
        public const string TutorialStepCompleted = "tutorial_step_completed"; // step
        public const string TutorialStep = "step";
        public const string TutorialCompleted = "tutorial_completed";
    }
}