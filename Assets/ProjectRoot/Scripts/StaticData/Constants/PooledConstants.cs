namespace MobRun
{
    public static class PooledConstants
    {
        public const System.String CrowdUnit = "CrowdUnit";
        public const System.String DeathEffect = "DeathEffect";
        public const System.String CogsAmount = "CogsAmount";
        public const System.String DeathEffectPortal = "DeathEffectPortal";
    }
}
