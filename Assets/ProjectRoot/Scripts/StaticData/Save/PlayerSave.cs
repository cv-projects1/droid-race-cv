using System;
using System.Collections.Generic;

namespace MobRun
{
    [Serializable]
    public class PlayerSave
    {
        public int Currency;
        public int LevelIndex;

        public bool TutorialCompleted;
        public DateTime LastExitTime;
        public Dictionary<UpgradeType, int> Upgrades;

        public bool AudioEnabled;
        public bool HapticsEnabled;
    }
}