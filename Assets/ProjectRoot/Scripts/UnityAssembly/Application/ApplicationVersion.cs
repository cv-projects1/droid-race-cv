using TMPro;
using UnityEngine;
using vailshnast.applicationservice;
using Zenject;

namespace GameCore
{
    public class ApplicationVersion : MonoBehaviour
    {
        [SerializeField] private TMP_Text _applicationVersion;
        private IApplicationConfig _applicationConfig;

        [Inject]
        private void Construct(IApplicationConfig applicationConfig)
        {
            _applicationConfig = applicationConfig;
        }

        private void Start()
        {
            var productName = Application.productName;
            var version = Application.version;
            var bundleVersion = _applicationConfig.BundleVersion;
            var releaseType = _applicationConfig.ReleaseType;
                        
            _applicationVersion.SetText($"{productName} ver: {version} bunver: {bundleVersion} type: {releaseType}");
        }
    }
}