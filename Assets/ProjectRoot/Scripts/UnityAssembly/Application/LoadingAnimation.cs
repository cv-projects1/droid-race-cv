using System.Collections;
using TMPro;
using UnityEngine;

namespace GameCore
{
    public class LoadingAnimation : MonoBehaviour
    {
        [SerializeField] private TMP_Text _loadingText;
        [SerializeField] private float _delay = 0.1f;

        private int _index = 0;
        
        private readonly string[] _loadingStrings = new[]
        {
            $"Loading",
            $"Loading.",
            $"Loading..",
            $"Loading...",
        };

        private void Start()
        {
            _loadingText.SetText(_loadingStrings[_index]);
            StartCoroutine(AnimateCoroutine());
        }

        private IEnumerator AnimateCoroutine()
        {
            while (true)
            {
                yield return new WaitForSeconds(_delay);

                if (_index == _loadingStrings.Length - 1)
                    _index = 0;
                else _index++;
                
                _loadingText.SetText(_loadingStrings[_index]);
            }
        }

        private void OnDestroy() => StopAllCoroutines();
    }
}
