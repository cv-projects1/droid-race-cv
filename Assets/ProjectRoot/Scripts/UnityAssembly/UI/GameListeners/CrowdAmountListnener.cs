using System;
using TMPro;
using UnityEngine;
using vailshnast.entitasview;
using vailshnast.extensions;
using vailshnast.windows;
using Zenject;

namespace MobRun
{
    public class CrowdAmountListnener : MonoBehaviour,
                                        IEntityListener,
                                        IAnyCrowdAmountListener,
                                        IAnyFinishLineReachedListener
    {
        [SerializeField] private TMP_Text _text;
        [SerializeField] private RectTransform _amountRect;
        private GameContext _gameContext;
        private GameEntity _entity;
        private IWindowsService _windowsService;
        private IGameModuleService _gameModuleService;

        [Inject]
        private void Construct(GameContext gameContext, 
                               IWindowsService windowsService,
                               IGameModuleService gameModuleService)
        {
            _gameContext = gameContext;
            _windowsService = windowsService;
            _gameModuleService = gameModuleService;
        }

        public void Initialize()
        {
        }

        public void Startup()
        {
            _entity = _gameContext.CreateEntity();
            _entity.AddAnyCrowdAmountListener(this);
            _entity.AddAnyFinishLineReachedListener(this);

            gameObject.SetActive(true);
        }

        public void PostStartup()
        {
        }

        public void Dispose()
        {
            /*if (_entity.hasAnyCrowdAmountListener)
                _entity.RemoveAnyCrowdAmountListener(this);*/
        }

        public void OnAnyCrowdAmount(GameEntity entity, int value) =>
            _text.SetText(value.ToString());

        public void OnAnyFinishLineReached(GameEntity entity, Vector3 centerPos) => gameObject.SetActive(false);

        private void FixedUpdate()
        {
            var entity = _gameContext.crowdGlobalTargetEntity;

            if (entity == null) return;

            var uiCam = _windowsService.WindowsContainer.UICamera;
            var gameCam = _gameModuleService.Module.GameCamera;
            
            var pos = CanvasUtilities.WorldToCanvasPoint(
                entity.worldPosition.Value + new Vector3(0, 0, _gameContext.crowdGlobalRadius.Value),
                _windowsService.WindowsContainer.CanvasRect, gameCam, uiCam);

            _amountRect.anchoredPosition = pos + new Vector2(0, 100);
        }
    }
}