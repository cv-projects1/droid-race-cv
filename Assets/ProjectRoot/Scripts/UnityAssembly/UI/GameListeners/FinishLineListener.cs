using UnityEngine;
using UnityEngine.UI;
using vailshnast.entitasview;
using vailshnast.uicomponents;
using Zenject;

namespace MobRun
{
    public class FinishLineListener : MonoBehaviour , IEntityListener , 
                                      IAnyFinishBoxCollectedPercentageListener,
                                      IAnyFinishLineReachedListener
    {
        [SerializeField] private FinishProgressBar _finishProgressBar;
        [SerializeField] private UIButtonComponent _pauseButton;
        
        private GameContext _gameContext;
        private GameEntity _entity;

        [Inject]
        private void Construct(GameContext gameContext)
        {
            _gameContext = gameContext;
        }
        
        public void Initialize()
        {
        }

        public void Startup()
        {
            _entity = _gameContext.CreateEntity();
            _entity.AddAnyFinishLineReachedListener(this);
            _entity.AddAnyFinishBoxCollectedPercentageListener(this);
            
            _pauseButton.gameObject.SetActive(true);
        }

        public void PostStartup()
        {
        }

        public void Dispose()
        {
            _finishProgressBar.CloseSlider();
        }

        public void OnAnyFinishBoxCollectedPercentage(GameEntity entity, float value) =>
            _finishProgressBar.SetProgress(value);

        public void OnAnyFinishLineReached(GameEntity entity, Vector3 centerPos)
        {
            _finishProgressBar.OpenSlider();
            _pauseButton.gameObject.SetActive(false);
        }
    }
}