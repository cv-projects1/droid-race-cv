using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using vailshnast.entitasview;
using vailshnast.extensions;
using vailshnast.uicomponents;

namespace MobRun
{
    public class FinishProgressBar : MonoBehaviour
    {
        [SerializeField] private SlicedFilledImage _sliderProgress,
                                                   _sliderMask;

        [SerializeField] private float _animationSpeed = 0.35f;
        public void Initialize() => CloseSlider();

        public void OpenSlider()
        {
            DOTween.Kill(this);
            _sliderMask.DOFillAmount(1, _animationSpeed).SetEase(Ease.Linear);
        }

        public void CloseSlider()
        {
            DOTween.Kill(this);
            _sliderMask.fillAmount = 0;
        }

        public void SetProgress(float progress)
            => _sliderProgress.fillAmount = progress;
    }
}