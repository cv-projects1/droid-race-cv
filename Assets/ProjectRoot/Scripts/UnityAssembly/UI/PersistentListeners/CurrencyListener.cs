using TMPro;
using UnityEngine;
using vailshnast.observer;
using Zenject;

namespace MobRun
{
    public class CurrencyListener : MonoBehaviour, IObserver
    {
        [SerializeField] private TMP_Text _text;
        private PlayerModel _playerModel;

        [Inject]
        private void Construct(PlayerModel playerModel)
        {
            _playerModel = playerModel;
            
            _playerModel.AddObserver(this);
            OnObjectChanged(_playerModel);
        }

        private void OnDestroy() => _playerModel.RemoveObserver(this);

        public void OnObjectChanged(IObservable observable)
        {
            _text.SetText(_playerModel.PlayerSave.Currency.ToString());
        }
    }
}
