using MobRun;
using UnityEngine;
using vailshnast.statemachines;
using vailshnast.uicomponents;
using vailshnast.windows;
using Zenject;

public class GameCheatsWindow : BaseWindowView<GameCheatsWindowController>
{
    [SerializeField] private UIButtonComponent _addMoney,
                                               _quit,
                                               _resetProgress,
                                               _enableFPS,
                                               _disableFPS;

    private PlayerModel _playerModel;
    private IWindowsService _windowsService;
    private ISaveService _saveService;
    private IStateMachine _stateMachine;
    private IPerformanceService _performanceService;

    [Inject]
    private void Construct(PlayerModel playerModel,
                           IWindowsService windowsService,
                           ISaveService saveService,
                           IStateMachine stateMachine,
                           IPerformanceService performanceService)
    {
        _playerModel = playerModel;
        _windowsService = windowsService;
        _saveService = saveService;
        _stateMachine = stateMachine;
        _performanceService = performanceService;
    }

    public override void OnCreated()
    {
        _addMoney.AddSubscriber(OnAddMoneyClicked);
        _quit.AddSubscriber(OnQuitClicked);

        _resetProgress.AddSubscriber(OnResetClicked);
        
        _enableFPS.AddSubscriber(OnEnableFPS);
        _disableFPS.AddSubscriber(OnDisableFPS);
    }

    private void OnDisableFPS() => _performanceService.HideDebugger();
    private void OnEnableFPS() => _performanceService.ShowDebugger();
    private void OnQuitClicked() => _windowsService.RequestCloseWindow(this);

    private void OnAddMoneyClicked()
    {
        _playerModel.AdjustCurrencyAmount(10000);
        _saveService.Save();
    }

    private void OnResetClicked()
    {
        _saveService.SetNewProgressAndSave();
        _stateMachine.EnterState<GameRestartState>();
    }

    public override void OnPostCreated()
    {
    }

    protected override void OnOpened()
    {
    }

    protected override void OnClosed()
    {
    }
}

public class GameCheatsWindowController : IWindowController
{
    public WindowView View { get; set; }
}