using DG.Tweening;
using Lofelt.NiceVibrations;
using MobRun;
using UnityEngine;
using UnityEngine.UI;
using vailshnast.statemachines;
using vailshnast.uicomponents;
using vailshnast.windows;
using Zenject;

public class GameLossWindow : BaseWindowView<GameLossWindowController>
{
    [SerializeField] private UIButtonComponent _restartButton;
    [SerializeField] private GameButtonAppear _gameButtonAppear;
    [SerializeField] private HapticPatterns.PresetType _openVibration;
    
    private IStateMachine _stateMachine;
    private IGameSoundService _gameSoundService;

    [Inject]
    public void Construct(IStateMachine stateMachine, 
                          IGameSoundService gameSoundService)
    {
        _stateMachine = stateMachine;
        _gameSoundService = gameSoundService;
    }

    public override void OnCreated()
    {
        _restartButton.AddSubscriber(OnButtonRestart);
    }

    public override void OnPostCreated()
    {
    }

    protected override void OnOpened()
    {
        _gameSoundService.PlaySound(SoundType.WindowsLoose);
        
        HapticPatterns.PlayPreset(_openVibration);
        _gameButtonAppear.Setup();
        _gameButtonAppear.Animate().SetDelay(0.5f);
    }

    protected override void OnClosed()
    {
    }

    private void OnButtonRestart() => _stateMachine.EnterState<GameLoadState>();
}

public class GameLossWindowController : IWindowController
{
    public WindowView View { get; set; }
}