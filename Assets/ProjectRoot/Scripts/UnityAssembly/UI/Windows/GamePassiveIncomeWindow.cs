using DG.Tweening;
using MobRun;
using TMPro;
using UnityEngine;
using vailshnast.uicomponents;
using vailshnast.windows;
using Zenject;

public class GamePassiveIncomeWindow : BaseWindowView<GamePassiveIncomeWindowController>
{
    [SerializeField] private TMP_Text _earnedText;
    [SerializeField] private UIButtonComponent _earnButton;

    [SerializeField] private GameButtonAppear _gameButtonAppear;
    [SerializeField] private GameStatPanel _gameStatPanel;
    
    private PlayerModel _playerModel;
    private IWindowsService _windowsService;
    private ISaveService _saveService;

    [Inject]
    private void Construct(PlayerModel playerModel ,
                           IWindowsService windowsService,
                           ISaveService saveService)
    {
        _playerModel = playerModel;
        _windowsService = windowsService;
        _saveService = saveService;
    }
    
    public override void OnCreated()
    {
        _earnButton.AddSubscriber(OnEarnClicked);
    }

    public override void OnPostCreated()
    {
        
    }

    protected override void OnOpened()
    {
        _gameStatPanel.Setup();
        _gameButtonAppear.Setup();

        var sequence = DOTween.Sequence();
        var statTween = _gameStatPanel.Animate(ConcreteController.CurrencyAmount);
        var gameButtonTween = _gameButtonAppear.Animate();

        sequence.Append(statTween);
        sequence.Append(gameButtonTween);

        sequence.SetDelay(0.5f);
    }

    protected override void OnClosed()
    {

    }

    private void OnEarnClicked()
    {
        _playerModel.AdjustCurrencyAmount(ConcreteController.CurrencyAmount);
        _saveService.Save();
        _windowsService.RequestCloseWindow(this);
    }
}

public class GamePassiveIncomeWindowController : IWindowController
{
    public WindowView View { get; set; }
    public int CurrencyAmount;
}