using MobRun;
using UnityEngine;
using vailshnast.statemachines;
using vailshnast.uicomponents;
using vailshnast.windows;
using Zenject;

public class GamePauseWindow : BaseWindowView<GamePauseWindowController>
{
    [SerializeField] private UIButtonComponent _closeButton,
                                               _restartButton;

    [SerializeField] private GameHapticsSwitcher _hapticsSwitcher;
    [SerializeField] private GameSoundSwitcher _gameSoundSwitcher;

    private IStateMachine _stateMachine;

    [Inject]
    private void Construct(IStateMachine stateMachine)
    {
        _stateMachine = stateMachine;
    }
    
    public override void OnCreated()
    {
        _closeButton.AddSubscriber(OnCloseClicked);
        _restartButton.AddSubscriber(OnRestartClicked);
        
        _hapticsSwitcher.OnCreated();
        _gameSoundSwitcher.OnCreated();
    }

    public override void OnPostCreated()
    {
        
    }

    protected override void OnOpened()
    {
        _hapticsSwitcher.OnOpen();
        _gameSoundSwitcher.OnOpen();
    }

    protected override void OnClosed()
    {

    }

    private void OnRestartClicked() =>
        _stateMachine.EnterState<GameRestartState>();

    private void OnCloseClicked() =>
        _stateMachine.EnterState<GameLoopState>();
}

public class GamePauseWindowController : IWindowController
{
    public WindowView View { get; set; }
}