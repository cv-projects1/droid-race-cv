using MobRun;
using UnityEngine;
using vailshnast.statemachines;
using vailshnast.uicomponents;
using vailshnast.windows;
using Zenject;

public class GamePreviewStateWindow : BaseWindowView<PreviewStateWindowController>
{
    [SerializeField] private UIButtonComponent _previewExitButton,
                                               _settingsButton,
                                               _upgradesButton,
                                               _cheatsButton;

    [SerializeField] private GameObject _upgradeAttentionMarker;
    
    private IStateMachine _stateMachine;
    private IWindowsService _windowsService;
    private IUpgradeService _upgradeService;

    [Inject]
    public void Construct(IStateMachine stateMachine,
                          IWindowsService windowsService,
                          IUpgradeService upgradeService)
    {
        _stateMachine = stateMachine;
        _windowsService = windowsService;
        _upgradeService = upgradeService;
    }

    public override void OnCreated()
    {
        _previewExitButton.AddSubscriber(OnPreviewExitClicked);
        _settingsButton.AddSubscriber(OnSettingsClicked);
        _upgradesButton.AddSubscriber(OnUpgradesClicked);
        _cheatsButton.AddSubscriber(OnCheatsClicked);
    }

    public override void OnPostCreated()
    {
    }

    protected override void OnOpened()
    {
        _upgradeAttentionMarker.SetActive(_upgradeService.ContainsBuyableUpgrade());
    }

    protected override void OnClosed()
    {
    }

    private void OnSettingsClicked()
    {
        _windowsService.RequestCloseWindow(this);
        _windowsService.RequestShowWindowAndQueue(new GameSettingsWindowController(),
                                                  new PreviewStateWindowController());
    }

    private void OnUpgradesClicked()
    {
        _windowsService.RequestCloseWindow(this);
        _windowsService.RequestShowWindowAndQueue(new GameUpgradesWindowController(),
                                                  new PreviewStateWindowController());
    }

    private void OnCheatsClicked()
    {
        _windowsService.RequestCloseWindow(this);
        _windowsService.RequestShowWindowAndQueue(new GameCheatsWindowController(),
                                                  new PreviewStateWindowController());
    }

    private void OnPreviewExitClicked() =>
        _stateMachine.EnterState<GameStartState>();
}

public class PreviewStateWindowController : IWindowController
{
    public WindowView View { get; set; }
}