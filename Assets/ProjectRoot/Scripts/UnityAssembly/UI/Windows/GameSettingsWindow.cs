using MobRun;
using UnityEngine;
using vailshnast.uicomponents;
using vailshnast.windows;
using Zenject;

public class GameSettingsWindow : BaseWindowView<GameSettingsWindowController>
{
    [SerializeField] private UIButtonComponent[] _closeButtons;
    [SerializeField] private GameHapticsSwitcher _gameHapticsSwitcher;
    [SerializeField] private GameSoundSwitcher _gameSoundSwitcher;
    private IWindowsService _windowsService;

    [Inject]
    private void Construct(IWindowsService windowsService)
    {
        _windowsService = windowsService;
    }

    public override void OnCreated()
    {
        foreach (var closeButton in _closeButtons) 
            closeButton.AddSubscriber(OnCloseClicked);
        
        _gameHapticsSwitcher.OnCreated();
        _gameSoundSwitcher.OnCreated();
    }

    public override void OnPostCreated()
    {
    }
    
    protected override void OnOpened()
    {
        _gameHapticsSwitcher.OnOpen();
        _gameSoundSwitcher.OnOpen();
    }

    protected override void OnClosed()
    {
    }

    private void OnCloseClicked() => _windowsService.RequestCloseWindow(this);
}

public class GameSettingsWindowController : IWindowController
{
    public WindowView View { get; set; }
}