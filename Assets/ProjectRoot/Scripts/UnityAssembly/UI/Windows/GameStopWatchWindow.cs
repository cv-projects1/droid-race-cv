using System;
using TMPro;
using UnityEngine;
using vailshnast.windows;

public class GameStopWatchWindow : BaseWindowView<GameStopWatchWindowController>
{
    [SerializeField] private TMP_Text _totalText, _currentText;

    private DateTime _startStopWatchData;
    private TimeSpan _totalSpan;

    private bool _stopWatchEnabled;

    public override void OnCreated()
    {
        _totalSpan = TimeSpan.FromMilliseconds(0);
    }

    public override void OnPostCreated()
    {
        
    }

    protected override void OnOpened()
    {
        _startStopWatchData = DateTime.Now;
        _stopWatchEnabled = true;
    }

    public void DisableStopWatch()
    {
        _stopWatchEnabled = false;
        _totalSpan = _totalSpan.Add(DateTime.Now - _startStopWatchData);
    }

    protected override void OnClosed()
    {

    }

    public void Update()
    {
        if (!_stopWatchEnabled) return;
        
        var timeElapsed = DateTime.Now - _startStopWatchData;
        var totalSpan = _totalSpan.Add(timeElapsed);

        _currentText.SetText(timeElapsed.ToString(@"mm\:ss\:ff"));
        _totalText.SetText(totalSpan.ToString(@"mm\:ss\:ff"));
    }
}

public class GameStopWatchWindowController : IWindowController
{
    public WindowView View { get; set; }
}