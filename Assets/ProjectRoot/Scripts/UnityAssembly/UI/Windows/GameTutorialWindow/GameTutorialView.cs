using MobRun;
using UnityEngine;

public class GameTutorialView : MonoBehaviour
{
    [SerializeField] private TutorialViewType tutorialViewType;
    public TutorialViewType TutorialViewType => tutorialViewType;
    public virtual void OnOpen() { }
    public virtual void OnClose() { }
}