using System.Collections.Generic;
using System.Linq;
using MobRun;
using UnityEngine;
using vailshnast.windows;
using Zenject;

public class GameTutorialWindow : BaseWindowView<GameTutorialWindowController>
{
    [SerializeField] private List<GameTutorialView> _tutorialViews;
    private Dictionary<TutorialViewType, GameTutorialView> _tutorialViewMap = new Dictionary<TutorialViewType, GameTutorialView>();
    private IWindowsService _windowsService;

    [Inject]
    private void Construct(IWindowsService windowsService) => _windowsService = windowsService;

    public override void OnCreated()
    {
        _tutorialViewMap = _tutorialViews.ToDictionary(x => x.TutorialViewType);
    }

    public override void OnPostCreated()
    {
        //_windowsManager.OnWindowOpened += () => Transform.Value.SetSiblingIndex(_windowsManager.WindowsCount - 3);
    }

    protected override void OnOpened()
    {
        var tutorialView = _tutorialViewMap[ConcreteController.TutorialViewType];
#if UNITY_EDITOR
        if (tutorialView == null)
        {
            Debug.LogError($"Tutorial view of type {tutorialView.TutorialViewType} is missing");

            return;
        }
#endif
        tutorialView.gameObject.SetActive(true);
        tutorialView.OnOpen();
    }

    protected override void OnClosed()
    {
        for (int i = 0; i < _tutorialViews.Count; i++)
        {
            _tutorialViews[i].OnClose();
            _tutorialViews[i].gameObject.SetActive(false);
        }
    }
}

public class GameTutorialWindowController : IWindowController
{
    public WindowView View { get; set; }
    public TutorialViewType TutorialViewType;
}