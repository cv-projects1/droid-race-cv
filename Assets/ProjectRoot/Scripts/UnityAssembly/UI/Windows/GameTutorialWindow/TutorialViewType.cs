namespace MobRun
{
    public enum TutorialViewType
    {
        TapAndHold    = 0,
        KeepHolding   = 1,
        ReleaseFinger = 2
    }
}