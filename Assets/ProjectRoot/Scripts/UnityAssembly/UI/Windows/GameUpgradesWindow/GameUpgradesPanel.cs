using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using vailshnast.uicomponents;
using Zenject;

namespace MobRun
{
    public class GameUpgradesPanel : MonoBehaviour
    {
        [SerializeField] private TMP_Text _gradeText, _priceText;
        [SerializeField] private Image[] _gradeSteps;
        [SerializeField] private Sprite _gradeOnSprite, _gradeOffSprite;
        [SerializeField] private UIButtonComponent _buyButton, _disabledButton;
        [SerializeField] private UpgradeType _upgradeType;
        
        private PlayerModel _playerModel;
        private UpgradeSettings _upgradeSettings;
        private IUpgradeService _upgradeService;

        [Inject]
        private void Construct(PlayerModel playerModel,
                               UpgradeConfig upgradeConfig,
                               IUpgradeService upgradeService)
        {
            _playerModel = playerModel;
            _upgradeService = upgradeService;
            _upgradeSettings = upgradeConfig.GetSettings(_upgradeType);
        }

        public void Initialize(UnityAction onBuyClicked)
        {
            _buyButton.AddSubscriber(OnBuyButtonClicked);
            _buyButton.AddSubscriber(onBuyClicked);
            UpdatePanel();
        }

        public void UpdatePanel()
        {
            var currentGrade = _playerModel.PlayerSave.Upgrades[_upgradeType];
            var upgradeSteps = _upgradeSettings.UpgradeSteps;
            SetGradeSteps(currentGrade);
            _gradeText.SetText($"Level {currentGrade}");
            var isMaxed = currentGrade == upgradeSteps.Count - 1;
            _priceText.SetText(isMaxed ? "MAX" : upgradeSteps[currentGrade + 1].Price.ToString());

            var enoughToBuy = _upgradeService.EnoughToBuyUpgrade(_upgradeType);
            
            _buyButton.gameObject.SetActive(enoughToBuy);
            _disabledButton.gameObject.SetActive(!enoughToBuy);
        }

        private void SetGradeSteps(int amount)
        {
            for (int i = 0; i < _gradeSteps.Length; i++)
                _gradeSteps[i].sprite = i < amount ? _gradeOnSprite : _gradeOffSprite;
        }

        private void OnBuyButtonClicked()
        {
            if (_upgradeService.BuyUpgrade(_upgradeType))
                UpdatePanel();
        }
    }
}