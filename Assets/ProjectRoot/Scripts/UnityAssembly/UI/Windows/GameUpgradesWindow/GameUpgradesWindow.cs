using MobRun;
using UnityEngine;
using vailshnast.uicomponents;
using vailshnast.windows;
using Zenject;

public class GameUpgradesWindow : BaseWindowView<GameUpgradesWindowController>
{
    [SerializeField] private GameUpgradesPanel[] _gameUpgradesPanels;
    [SerializeField] private UIButtonComponent[] _closeButtons;
    private IWindowsService _windowsService;

    [Inject]
    private void Construct(IWindowsService windowsService)
    {
        _windowsService = windowsService;
    }

    public override void OnCreated()
    {
        foreach (var closeButton in _closeButtons)
            closeButton.AddSubscriber(OnCloseClicked);

        foreach (var gameUpgrades in _gameUpgradesPanels)
            gameUpgrades.Initialize(UpdateUpgradePanels);
        
        UpdateUpgradePanels();
    }

    public override void OnPostCreated()
    {
    }

    private void UpdateUpgradePanels()
    {
        foreach (var upgradesPanel in _gameUpgradesPanels)
            upgradesPanel.UpdatePanel();
    }

    protected override void OnOpened() => UpdateUpgradePanels();

    protected override void OnClosed()
    {
    }

    private void OnCloseClicked() => _windowsService.RequestCloseWindow(this);
}

public class GameUpgradesWindowController : IWindowController
{
    public WindowView View { get; set; }
}