using DG.Tweening;
using Lofelt.NiceVibrations;
using MobRun;
using TMPro;
using UnityEngine;
using vailshnast.statemachines;
using vailshnast.uicomponents;
using vailshnast.windows;
using Zenject;

public class GameVictoryWindow : BaseWindowView<GameVictoryWindowController>
{
    [SerializeField] private TMP_Text _levelText;
    [SerializeField] private UIButtonComponent _restartButton;

    [SerializeField] private GameStatPanel _currencyPanel,
                                                 _robotsPanel;

    [SerializeField] private GameButtonAppear _nextView;
    [SerializeField] private HapticPatterns.PresetType _openVibration;
    
    
    private IStateMachine _stateMachine;
    private ILevelService _levelService;
    private IGameSoundService _gameSoundService;
    private GameContext _gameContext;

    [Inject]
    public void Construct(IStateMachine stateMachine,
                          ILevelService levelService,
                          IGameSoundService gameSoundService,
                          GameContext gameContext)
    {
        _gameSoundService = gameSoundService;
        _stateMachine = stateMachine;
        _gameContext = gameContext;
        _levelService = levelService;
    }

    public override void OnCreated()
    {
        _restartButton.AddSubscriber(OnButtonRestart);
    }

    public override void OnPostCreated()
    {
    }

    private void Animate()
    {
        _nextView.Setup();
        _currencyPanel.Setup();
        _robotsPanel.Setup();
        
        var robotsAmount = _gameContext.finishUnitsLeftComponents.Value;
        var sequence = DOTween.Sequence();
        var currency = _currencyPanel.Animate(_gameContext.finishCurrency.Value);
        var robots = robotsAmount > 0 ? _robotsPanel.Animate(robotsAmount) : null;
        var next = _nextView.Animate();
        
        sequence.Append(currency);

        if (robots != null)
            sequence.Append(robots);

        sequence.Append(next);
        sequence.SetDelay(0.5f);
    }

    protected override void OnOpened()
    {
        _gameSoundService.PlaySound(SoundType.WindowsVictory);

        HapticPatterns.PlayPreset(_openVibration);
        
        _levelText.SetText($"Level {_levelService.GetVisualLevelIndex}");
        Animate();
    }

    protected override void OnClosed()
    {
    }

    private void OnButtonRestart() => _stateMachine.EnterState<GameLoadState>();
}

public class GameVictoryWindowController : IWindowController
{
    public WindowView View { get; set; }
}