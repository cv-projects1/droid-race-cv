using MobRun;
using UnityEngine;
using vailshnast.statemachines;
using vailshnast.uicomponents;
using vailshnast.windows;
using Zenject;

public class GameWindow : BaseWindowView<GameWindowController> 
{
    [SerializeField] private RectTransform _pointsTargetRect,
                                           _pointsParentRect;
    
    [SerializeField] private UIButtonComponent _pauseButton;

    private IStateMachine _stateMachine;
    public RectTransform PointsTargetRect => _pointsTargetRect;
    public RectTransform PointsParentRect => _pointsParentRect;

    [Inject]
    private void Construct(IStateMachine stateMachine)
    {
        _stateMachine = stateMachine;
    }
    
    public override void OnCreated()
    {
        _pauseButton.AddSubscriber(OnPauseButtonClicked);
    }

    public override void OnPostCreated()
    {
        
    }

    protected override void OnOpened()
    {
    }

    protected override void OnClosed()
    {
        
    }

    private void OnPauseButtonClicked() =>
        _stateMachine.EnterState<GamePausedState>();
}

public class GameWindowController : IWindowController
{
    public WindowView View { get; set; }
}