using DG.Tweening;
using TMPro;
using UnityEngine;

namespace MobRun
{
    public class GameButtonAppear : MonoBehaviour
    {
        [SerializeField] private RectTransform _button;
        [SerializeField] private TMP_Text _canvasGroup;

        public float _scaleDuration = 0.3f, _groupDuration = 0.3f;
        
        public void Setup()
        {
            _button.localScale = new Vector3(0, 1, 1);
            _canvasGroup.alpha = 0;
        }
        
        public Tween Animate()
        {
            var sequence = DOTween.Sequence();
            
            var backTween = _button.DOScale(Vector3.one, _scaleDuration);
            var canvasGroupTween = _canvasGroup.DOFade(1, _groupDuration);
            
            sequence.Append(backTween);
            sequence.Append(canvasGroupTween);

            return sequence;
        }
    }
}