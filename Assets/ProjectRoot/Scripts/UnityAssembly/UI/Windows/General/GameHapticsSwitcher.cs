using Lofelt.NiceVibrations;
using UnityEngine;
using UnityEngine.UI;
using vailshnast.uicomponents;
using Zenject;

namespace MobRun
{
    public class GameHapticsSwitcher : MonoBehaviour
    {
        [SerializeField] private Image _image;
        [SerializeField] private Sprite _spriteOn, _spriteOff;
        [SerializeField] private UIButtonComponent _button;
        
        private PlayerModel _playerModel;
        private IHapticsService _hapticsService;

        [Inject]
        private void Construct(PlayerModel playerModel, IHapticsService hapticsService)
        {
            _playerModel = playerModel;
            _hapticsService = hapticsService;
        }

        public void OnCreated()
        {
            _button.AddSubscriber(OnButtonClicked);
        }

        public void OnOpen()
        {
            SetSprite();
        }

        private void OnButtonClicked()
        {
            _hapticsService.Toggle();
            SetSprite();

            if (_playerModel.PlayerSave.HapticsEnabled)
                _hapticsService.CreateHaptic(new GameHapticSettings {IsOn = true,
                    Pattern = HapticPatterns.PresetType.LightImpact});
        }

        public void SetSprite()
        {
            _image.sprite = _playerModel.PlayerSave.HapticsEnabled ? _spriteOn : _spriteOff;
        }
    }
}