using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace MobRun
{
    public class GameSoundButton : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] private SoundType _soundType = SoundType.ButtonClick;
        private IGameSoundService _gameSoundService;

        [Inject]
        private void Construct(IGameSoundService gameSoundService)
        {
            _gameSoundService = gameSoundService;
        }

        public void OnPointerDown(PointerEventData eventData) =>
            _gameSoundService.PlaySound(_soundType);
    }
}
