using System.Collections;
using System.Collections.Generic;
using Lofelt.NiceVibrations;
using UnityEngine;
using UnityEngine.UI;
using vailshnast.uicomponents;
using Zenject;

namespace MobRun
{
    public class GameSoundSwitcher : MonoBehaviour
    {
        [SerializeField] private Image _image;
        [SerializeField] private Sprite _spriteOn, _spriteOff;
        [SerializeField] private UIButtonComponent _button;
        
        private PlayerModel _playerModel;
        private IGameSoundService _gameSoundService;

        [Inject]
        private void Construct(PlayerModel playerModel, IGameSoundService gameSoundService)
        {
            _playerModel = playerModel;
            _gameSoundService = gameSoundService;
        }

        public void OnCreated()
        {
            _button.AddSubscriber(OnButtonClicked);
        }

        public void OnOpen()
        {
            SetSprite();
        }

        private void OnButtonClicked()
        {
            _gameSoundService.Toggle();
            SetSprite();
        }

        public void SetSprite()
        {
            _image.sprite = _playerModel.PlayerSave.AudioEnabled ? _spriteOn : _spriteOff;
        }
    }
}
