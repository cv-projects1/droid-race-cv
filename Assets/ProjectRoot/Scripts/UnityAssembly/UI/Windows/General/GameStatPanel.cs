using Cysharp.Threading.Tasks;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MobRun
{
    public class GameStatPanel : MonoBehaviour
    {
        [SerializeField] private RectTransform _backRect;
        [SerializeField] private TMP_Text _amountText;
        [SerializeField] private CanvasGroup _canvasGroup;

        [SerializeField] private float _scaleDuration = 0.3f,
                                       _groupDuration = 0.3f,
                                       _textDuration = 0.3f;

        public void Setup()
        {
            _backRect.localScale = new Vector3(0, 1, 1);
            _canvasGroup.alpha = 0;
            
        }

        public Tween Animate(int amount)
        {
            var sequence = DOTween.Sequence();
            
            var backTween = _backRect.DOScale(Vector3.one, _scaleDuration);
            var canvasGroupTween = _canvasGroup.DOFade(1, _groupDuration);
            var amountTween = GetAmountTween(_amountText, 0, amount, _textDuration);
            
            sequence.Append(backTween);
            sequence.Append(canvasGroupTween);
            sequence.Append(amountTween);

            return sequence;
        }

        private Tween GetAmountTween(TMP_Text text, int startAmount, int endAmount, float duration)
        {
            var progress = 0f;
            _amountText.SetText($"+{0}");

            var tween = DOTween.To(() => progress, x => progress = x, 1, duration)
                               .OnUpdate(() =>
                                {
                                    var value = (int) Mathf.Lerp(startAmount, endAmount, progress);
                                    text.SetText($"+{value}");
                                });

            return tween;
        }
    }
}