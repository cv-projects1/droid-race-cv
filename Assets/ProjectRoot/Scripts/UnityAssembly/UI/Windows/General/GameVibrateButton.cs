using Lofelt.NiceVibrations;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MobRun
{
    public class GameVibrateButton : MonoBehaviour , IPointerDownHandler
    {
        [SerializeField] private HapticPatterns.PresetType _presetType;
        
        public void OnPointerDown(PointerEventData eventData) =>
            HapticPatterns.PlayPreset(_presetType);
    }
}
