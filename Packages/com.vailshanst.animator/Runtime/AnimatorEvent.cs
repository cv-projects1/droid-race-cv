using System;
using UnityEngine;
using UnityEngine.Events;

namespace vailshnast.animator
{
    [RequireComponent(typeof(Animator))]
    public class AnimatorEvent<T> : MonoBehaviour
    {
        public event Action<T> OnAnimatorEvent;
        public UnityEvent<T> OnAnimatorEventUnity;

        public void Event(T param) => OnAnimatorEvent?.Invoke(param);
    }
}