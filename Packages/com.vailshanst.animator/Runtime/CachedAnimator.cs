using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace vailshnast.animator
{
    public class CachedAnimator<TKey>
    {
        private readonly Dictionary<TKey, int> _animatorKeys = new Dictionary<TKey, int>();
        private Animator _animator;
        private AnimatorControllerParameter[] _animatorParameters;

        public void Initialize(List<AnimatorParam<TKey>> animParams, Animator animator)
        {
            if (animParams == null) return;

            _animator = animator;
            _animatorParameters = _animator.parameters;
            for (int i = 0; i < animParams.Count; i++) AddParam(animParams[i]);
        }

        public void AddParam(AnimatorParam<TKey> param)
        {
            var hash = Animator.StringToHash(param.ParamName);

            if (_animatorKeys.ContainsKey(param.Key) || _animatorKeys.ContainsValue(hash)) return;

            _animatorKeys.Add(param.Key, hash);
        }

        public void SetFloat(float value, TKey key)
        {
            var hash = _animatorKeys[key];

            if (!IsParameterValid(AnimatorControllerParameterType.Float, hash)) return;

            _animator.SetFloat(hash, value);
        }

        public void SetBool(bool value, TKey key)
        {
            var hash = _animatorKeys[key];

            if (!IsParameterValid(AnimatorControllerParameterType.Bool, hash)) return;

            _animator.SetBool(hash, value);
        }

        public void SetInt(int value, TKey key)
        {
            var hash = _animatorKeys[key];

            if (!IsParameterValid(AnimatorControllerParameterType.Int, hash)) return;

            _animator.SetInteger(hash, value);
        }

        public void SetTrigger(TKey key)
        {
            var hash = _animatorKeys[key];

            if (!IsParameterValid(AnimatorControllerParameterType.Trigger, hash)) return;

            _animator.SetTrigger(hash);
        }

        private bool IsParameterValid(AnimatorControllerParameterType type, int hash)
        {
            var param = _animatorParameters.FirstOrDefault(x => x.nameHash == hash);

            if (param != null) return param.type == type;

            return false;
        }
    }

    public class AnimatorParam<TKey>
    {
        public string ParamName;
        public TKey Key;
    }
}