using UnityEngine;

namespace vailshnast.animator
{
    public class ObjectAnimatorEvent : AnimatorEvent<Object> { }
}