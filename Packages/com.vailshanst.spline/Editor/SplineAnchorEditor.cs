﻿using UnityEditor;
using UnityEngine;
using vailshanst.spline;

[CustomEditor(typeof(SplineAnchor))]
public class SplineAnchorEditor : UnityEditor.Editor
{
    //Scene GUI:
    void OnSceneGUI()
    {
        //ensure pivot is used so anchor selection has a proper transform origin:
        if (Tools.pivotMode == PivotMode.Center)
        {
            Tools.pivotMode = PivotMode.Pivot;
        }
    }

    //Gizmos:
    [DrawGizmo(GizmoType.Selected)]
    static void RenderCustomGizmo(Transform objectTransform, GizmoType gizmoType)
    {
        if (objectTransform.parent != null)
        {
            SplineEditor.RenderCustomGizmo(objectTransform.parent, gizmoType);
        }
    }
}