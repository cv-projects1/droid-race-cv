﻿namespace vailshanst.spline
{
    public struct CurveDetail
    {
        //Public Variables:
        public int currentCurve;
        public float currentCurvePercentage;

        //Constructor:
        public CurveDetail(int currentCurve, float currentCurvePercentage)
        {
            this.currentCurve = currentCurve;
            this.currentCurvePercentage = currentCurvePercentage;
        }
    }
}