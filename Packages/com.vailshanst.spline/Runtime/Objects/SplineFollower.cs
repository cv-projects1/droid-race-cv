﻿using UnityEngine;

namespace vailshanst.spline
{
    [System.Serializable]
    public class SplineFollower
    {
        public Transform Target;
        public float Percentage = -1;
        public bool FaceDirection;
        public Vector3 Offset;
        public bool UpdatePosition = true;
        public float StandTime;

        public bool WasMoved
        {
            get
            {
                if (Percentage == _previousPercentage && FaceDirection == _previousFaceDirection) return false;

                _previousPercentage = Percentage;
                _previousFaceDirection = FaceDirection;

                return true;
            }
        }

        private float _previousPercentage;
        private bool _previousFaceDirection;

        public void UpdateOrientation(Spline spline)
        {
            if (Target == null) return;
            if (!UpdatePosition) return;

            if (!spline.loop) Percentage = Mathf.Clamp01(Percentage);

            if (FaceDirection)
            {
                if (spline.direction == SplineDirection.Forward)
                    Target.LookAt(Target.position + spline.GetDirection(Percentage));
                else Target.LookAt(Target.position - spline.GetDirection(Percentage));
            }

            Target.position = spline.GetPosition(Percentage) + Offset;
        }
    }
}