using System;
using UnityEngine;

namespace vailshnast.applicationevents
{
    public class ApplicationEventsProvider : MonoBehaviour
    {
        public event Action<bool> OnApplicationFocusEvent;
        public event Action<bool> OnApplicationPauseEvent;
        public event Action OnApplicationQuitEvent;

        public void OnApplicationFocus(bool hasFocus) =>
            OnApplicationFocusEvent?.Invoke(hasFocus);

        public void OnApplicationPause(bool pauseStatus) =>
            OnApplicationPauseEvent?.Invoke(pauseStatus);

        public void OnApplicationQuit() =>
            OnApplicationQuitEvent?.Invoke();

        public void Cleanup()
        {
            OnApplicationFocusEvent = null;
            OnApplicationPauseEvent = null;
            OnApplicationQuitEvent = null;
        }
    }
}