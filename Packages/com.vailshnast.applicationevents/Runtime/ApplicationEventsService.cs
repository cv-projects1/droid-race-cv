using System;
using UnityEngine;
using vailshnast.serviceparent;

namespace vailshnast.applicationevents
{
    public class ApplicationEventsService : IApplicationEventsService
    {
        private readonly IServicesParent _servicesParent;
        private ApplicationEventsProvider _applicationEventsProvider;

        public ApplicationEventsService(IServicesParent servicesParent) =>
            _servicesParent = servicesParent;

        public void Initialize()
        {
            var serviceParent = _servicesParent.Parent;
            var providerGameObject = new GameObject("ApplicationEventsProvider");
            _applicationEventsProvider = providerGameObject.AddComponent<ApplicationEventsProvider>();
            _applicationEventsProvider.transform.SetParent(serviceParent);
        }

        public void AddApplicationPauseSubscriber(Action<bool> action) =>
            _applicationEventsProvider.OnApplicationPauseEvent += action;

        public void AddApplicationFocusSubscriber(Action<bool> action) =>
            _applicationEventsProvider.OnApplicationFocusEvent += action;

        public void AddApplicationQuitSubscriber(Action action) =>
            _applicationEventsProvider.OnApplicationQuitEvent += action;

        public void Cleanup() => _applicationEventsProvider.Cleanup();
    }
}