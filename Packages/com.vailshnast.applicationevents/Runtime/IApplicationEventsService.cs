using System;

namespace vailshnast.applicationevents
{
    public interface IApplicationEventsService
    {
        void Initialize();
        void AddApplicationPauseSubscriber(Action<bool> action);
        void AddApplicationFocusSubscriber(Action<bool> action);
        void AddApplicationQuitSubscriber(Action action);
        void Cleanup();
    }
}