using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace vailshnast.applicationservice.editor
{
    public class BundleVersionPostProcessor : IPreprocessBuildWithReport
    {
        public int callbackOrder => 0;

        public void OnPreprocessBuild(BuildReport report)
        {
            var guids = AssetDatabase.FindAssets("t:scriptableObject");
            var toSelect = new List<ScriptableObject>();

            foreach (var guid in guids)
            {
                var path = AssetDatabase.GUIDToAssetPath(guid);

                var toCheck = AssetDatabase.LoadAllAssetsAtPath(path)
                                           .ToList()
                                           .OfType<ScriptableObject>()
                                           .Where(x => x != null && x is IApplicationConfig);

                toSelect.AddRange(toCheck);
            }

            var config = (IApplicationConfig) toSelect[0];

            config.SetBundleVersion(Application.platform == RuntimePlatform.IPhonePlayer
                                        ? PlayerSettings.iOS.buildNumber
                                        : PlayerSettings.Android.bundleVersionCode.ToString());

            if (config is ScriptableObject scriptableConfig)
                EditorUtility.SetDirty(scriptableConfig);

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}