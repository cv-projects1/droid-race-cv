using System.Collections.Generic;
using UnityEngine;

namespace vailshnast.applicationservice
{
    public interface IApplicationConfig
    {
        ApplicationSettings GetSettings(RuntimePlatform platform);
        string BundleVersion { get; }
        ReleaseType ReleaseType { get; }
        void SetBundleVersion(string version);
    }
}