using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using vailshnast.installers;

namespace vailshnast.applicationservice
{
    [CreateAssetMenu(fileName = "ApplicationConfig", menuName = "StaticData/ApplicationConfig")]
    public class ApplicationConfig : ScriptableObject, IScriptableService,
                                     IApplicationConfig
    {
        [SerializeField] private List<ApplicationSettings> _applicationSettingsList;

        public ApplicationSettings GetSettings(RuntimePlatform platform) =>
            _applicationSettingsList.FirstOrDefault(x => x.Platform == platform);

        public Type ServiceType => typeof(IApplicationConfig);

        [SerializeField] private string _bundleVersion;
        [SerializeField] private ReleaseType _releaseType;

        public string BundleVersion
        {
            get
            {
#if UNITY_EDITOR
                return  Application.platform == RuntimePlatform.IPhonePlayer
                    ? UnityEditor.PlayerSettings.iOS.buildNumber
                    : UnityEditor.PlayerSettings.Android.bundleVersionCode.ToString();
#else
                return _bundleVersion;
#endif
            }
        }

        public ReleaseType ReleaseType => _releaseType;

        public void SetBundleVersion(string version) =>
            _bundleVersion = version;
    }
}