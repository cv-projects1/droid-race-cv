using UnityEngine;
using UnityEngine.SceneManagement;

namespace vailshnast.applicationservice
{
    public class ApplicationService : IApplicationService
    {
        private readonly IApplicationConfig _applicationConfig;

        public ApplicationService(IApplicationConfig applicationConfig) =>
            _applicationConfig = applicationConfig;

        public void Initialize()
        {
            var platformSettings = _applicationConfig.GetSettings(Application.platform);

            if (platformSettings == null)
            {
                Debug.LogError($"{nameof(ApplicationService)} config doesnt contains settings for current platfrom\nSetting default values..");
                
                platformSettings = new ApplicationSettings
                {
                    ApplicationFrameRate = 60,
                    IsEnabledDebug = true,
                    Platform = Application.platform
                };
            }

            ApplySettings(platformSettings);
        }

        private void ApplySettings(ApplicationSettings platformSettings)
        {
            Application.targetFrameRate = platformSettings.ApplicationFrameRate;

            if (platformSettings.IsEnabledDebug) return;

            var sceneName = SceneManager.GetActiveScene().name;
            const string scriptName = nameof(ApplicationService) + ".cs";
            Debug.LogWarning($"Logging was disabled by Scene: {sceneName}, Script: {scriptName}");
            Debug.unityLogger.logEnabled = false;
        }
    }
}