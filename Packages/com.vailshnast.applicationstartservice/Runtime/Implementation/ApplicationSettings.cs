using System;
using UnityEngine;

namespace vailshnast.applicationservice
{
    [Serializable]
    public class ApplicationSettings
    {
        public RuntimePlatform Platform;
        public bool IsEnabledDebug;
        public int ApplicationFrameRate;
    }
}