namespace vailshnast.applicationservice
{
    public enum ReleaseType
    {
        Development = 0,
        Release     = 1
    }
}