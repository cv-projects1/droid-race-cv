﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace vailshnast.tools
{
    public static class AttachAttributesUtils
    {
        private const string ContextMenuItemLabel = "CONTEXT/Component/AutoGetComponent";
        private const string ToolsMenuItemLabel = "Tools/AutoGetComponent";

        private const string EditorPrefsAttachAttributesGlobal = "IsAttachAttributesActive";

        public static bool IsEnabled
        {
            get => EditorPrefs.GetBool(EditorPrefsAttachAttributesGlobal, true);
            private set
            {
                if (value) EditorPrefs.DeleteKey(EditorPrefsAttachAttributesGlobal);
                else EditorPrefs.SetBool(EditorPrefsAttachAttributesGlobal, value); // clear value if it's equals defaultValue
            }
        }

        [MenuItem(ContextMenuItemLabel)]
        [MenuItem(ToolsMenuItemLabel)]
        private static void ToggleAction()
        {
            IsEnabled = !IsEnabled;
        }

        [MenuItem(ContextMenuItemLabel, true)]
        [MenuItem(ToolsMenuItemLabel, true)]
        private static bool ToggleActionValidate()
        {
            Menu.SetChecked(ContextMenuItemLabel, IsEnabled);
            Menu.SetChecked(ToolsMenuItemLabel, IsEnabled);
            return true;
        }

        public static string GetPropertyType(this SerializedProperty property)
        {
            var type = property.type;
            var match = Regex.Match(type, @"PPtr<\$(.*?)>");
            if (match.Success)
                type = match.Groups[1].Value;
            return type;
        }

        public static Type StringToType(this string aClassName) => AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes()).First(x => x.IsSubclassOf(typeof(Component)) && x.Name == aClassName);
    }

    #region Attribute Editors

    #endregion
}