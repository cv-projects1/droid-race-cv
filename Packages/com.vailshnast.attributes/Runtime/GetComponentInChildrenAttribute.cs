using UnityEngine;

namespace vailshnast.attributes
{
    public class GetComponentInChildrenAttribute : PropertyAttribute
    {
        public readonly string ChildName;
        public readonly bool IncludeInactive;

        public GetComponentInChildrenAttribute(string childName, bool includeInactive)
        {
            ChildName = childName;
            IncludeInactive = includeInactive;
        }
    }
}