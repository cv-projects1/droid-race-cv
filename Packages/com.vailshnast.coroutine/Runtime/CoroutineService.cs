using System;
using System.Collections;
using UnityEngine;
using vailshnast.serviceparent;

namespace vailshnast.coroutine
{
    public class CoroutineService : ICoroutineService
    {
        private readonly IServicesParent _servicesParent;
        private CoroutineProvider _coroutineProvider;

        public CoroutineService(IServicesParent servicesParent)
            => _servicesParent = servicesParent;

        public void Initialize()
        {
            GameObject coroutineGameObject = new GameObject($"Coroutine Service");
            _coroutineProvider = coroutineGameObject.AddComponent<CoroutineProvider>();
            _coroutineProvider.transform.SetParent(_servicesParent.Parent);
        }

        /// <summary>
        /// Call method after YieldInstruction
        /// </summary>
        /// <param name="action">Method name</param>
        /// <param name="time">Yield Instruction</param>
        public void Invoke(Action action, YieldInstruction time)
        {
            _coroutineProvider.StartCoroutine(InvokeCoroutine(action, time));
        }

        /// <summary>
        /// Call method after YieldInstruction
        /// </summary>
        /// <param name="action">Method name</param>
        /// <param name="time">Yield Instruction</param>
        public void Invoke(Action action, CustomYieldInstruction time)
        {
            _coroutineProvider.StartCoroutine(InvokeCoroutine(action, time));
        }

        /// <summary>
        /// Call generic method after YieldInstruction
        /// </summary>
        /// <param name="action">Method name</param>
        /// <param name="time">Yield Instruction</param>
        public void Invoke<T>(Action<T> action, T arg, YieldInstruction time)
        {
            _coroutineProvider.StartCoroutine(InvokeCoroutine(action, arg, time));
        }

        /// <summary>
        /// Call generic method after YieldInstruction
        /// </summary>
        /// <param name="action">Method name</param>
        /// <param name="time">Yield Instruction</param>
        public void Invoke<T>(Action<T> action, T arg, CustomYieldInstruction time)
        {
            _coroutineProvider.StartCoroutine(InvokeCoroutine(action, arg, time));
        }

        /// <summary>
        /// Call method after coroutine
        /// </summary>
        /// <param name="action">invoked action</param>
        /// <param name="coroutine">Coroutine method</param>
        public void Invoke(Action action, IEnumerator coroutine)
        {
            _coroutineProvider.StartCoroutine(InvokeCoroutine(action, coroutine));
        }

        /// <summary>
        /// Call generic method after coroutine
        /// </summary>
        /// <param name="coroutine">Coroutine method</param>
        public void Invoke<T>(Action<T> action, T arg, IEnumerator coroutine)
        {
            _coroutineProvider.StartCoroutine(InvokeCoroutine(action, arg, coroutine));
        }

        /// <summary>
        /// Call coroutine
        /// </summary>
        /// <param name="coroutine">Coroutine method</param>
        public void Invoke(IEnumerator coroutine)
        {
            _coroutineProvider.StartCoroutine(coroutine);
        }

        /// <summary>
        /// Stops All Coroutines
        /// </summary>
        public void StopAllCoroutines() => _coroutineProvider.StopAllCoroutines();

        private IEnumerator InvokeCoroutine(Action action, float time)
        {
            yield return new WaitForSeconds(time);

            action?.Invoke();
        }

        private IEnumerator InvokeCoroutine(Action action, YieldInstruction time)
        {
            yield return time;

            action?.Invoke();
        }

        private IEnumerator InvokeCoroutine(Action action, CustomYieldInstruction time)
        {
            yield return time;

            action?.Invoke();
        }

        private IEnumerator InvokeCoroutine<T>(Action<T> action, T arg, float time)
        {
            yield return new WaitForSeconds(time);

            action?.Invoke(arg);
        }

        private IEnumerator InvokeCoroutine<T>(Action<T> action, T arg, YieldInstruction time)
        {
            yield return time;

            action?.Invoke(arg);
        }

        private IEnumerator InvokeCoroutine<T>(Action<T> action, T arg, CustomYieldInstruction time)
        {
            yield return time;

            action?.Invoke(arg);
        }

        private IEnumerator InvokeCoroutine(Action action, IEnumerator coroutine)
        {
            yield return _coroutineProvider.StartCoroutine(coroutine);

            action?.Invoke();
        }

        private IEnumerator InvokeCoroutine<T>(Action<T> action, T arg, IEnumerator coroutine)
        {
            yield return _coroutineProvider.StartCoroutine(coroutine);

            action?.Invoke(arg);
        }
    }
}