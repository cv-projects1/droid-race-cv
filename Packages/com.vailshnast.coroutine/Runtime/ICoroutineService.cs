using System;
using System.Collections;
using UnityEngine;

namespace vailshnast.coroutine
{
    public interface ICoroutineService
    {
        void Initialize();

        /// <summary>
        /// Call method after YieldInstruction
        /// </summary>
        /// <param name="action">Method name</param>
        /// <param name="time">Yield Instruction</param>
        void Invoke(Action action, YieldInstruction time);

        /// <summary>
        /// Call method after YieldInstruction
        /// </summary>
        /// <param name="action">Method name</param>
        /// <param name="time">Yield Instruction</param>
        void Invoke(Action action, CustomYieldInstruction time);

        /// <summary>
        /// Call generic method after YieldInstruction
        /// </summary>
        /// <param name="action">Method name</param>
        /// <param name="time">Yield Instruction</param>
        void Invoke<T>(Action<T> action, T arg, YieldInstruction time);

        /// <summary>
        /// Call generic method after YieldInstruction
        /// </summary>
        /// <param name="action">Method name</param>
        /// <param name="time">Yield Instruction</param>
        void Invoke<T>(Action<T> action, T arg, CustomYieldInstruction time);

        /// <summary>
        /// Call method after coroutine
        /// </summary>
        /// <param name="coroutine">Coroutine method</param>
        void Invoke(Action action, IEnumerator coroutine);

        /// <summary>
        /// Call generic method after coroutine
        /// </summary>
        /// <param name="coroutine">Coroutine method</param>
        void Invoke<T>(Action<T> action, T arg, IEnumerator coroutine);

        /// <summary>
        /// Call coroutine
        /// </summary>
        /// <param name="coroutine">Coroutine method</param>
        void Invoke(IEnumerator coroutine);

        /// <summary>
        /// Stops All Coroutines
        /// </summary>
        void StopAllCoroutines();
    }
}