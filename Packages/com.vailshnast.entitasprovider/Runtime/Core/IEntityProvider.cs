using Entitas;

public interface IEntityProvider
{
    T GetEntity<T>() where T : IEntity;
    int GetProviderId();
}