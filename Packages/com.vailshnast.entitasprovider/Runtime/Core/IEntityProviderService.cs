public interface IEntityProviderService
{
    void Register(IEntityProvider provider);
    IEntityProvider GetEntityProvider(int instanceId);
}