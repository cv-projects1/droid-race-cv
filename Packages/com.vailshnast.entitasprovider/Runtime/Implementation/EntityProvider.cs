using Entitas;
using UnityEngine;
using vailshnast.entitasview;
using Zenject;

public class EntityProvider : MonoBehaviour , IEntityProvider
{
    [SerializeField] private EntityView _entityView;
    
    private IEntityProviderService _providerService;

    [Inject]
    private void Construct(IEntityProviderService providerService) =>
        _providerService = providerService;

    private void Start() => _providerService.Register(this);
    
    public T GetEntity<T>() where T : IEntity => _entityView.GetEntity<T>();
    public int GetProviderId() =>  gameObject.GetInstanceID();
}