using System;
using System.Collections.Generic;
using UnityEngine;

public class EntityProviderService : IEntityProviderService
{
    private readonly Dictionary<int, IEntityProvider> _providersMap = new Dictionary<int, IEntityProvider>();

    public void Register(IEntityProvider provider)
    {
#if UNITY_EDITOR
        if (provider == null)
            throw new NullReferenceException($"provider is null");

        if (_providersMap.ContainsKey(provider.GetProviderId()))
            throw new NotImplementedException($"{provider.GetProviderId()} already exists");

        if (_providersMap.ContainsValue(provider))
            throw new NotImplementedException($"{provider.GetProviderId()} already exists");
#endif
        _providersMap.Add(provider.GetProviderId(), provider);
    }

    public IEntityProvider GetEntityProvider(int instanceId)
    {
        _providersMap.TryGetValue(instanceId, out var provider);
#if UNITY_EDITOR
        if (provider == null)
            throw new NullReferenceException($"provider is null");
#endif
        return provider;
    }
}