using System;
using Entitas;
using UnityEngine;
using Zenject;

public class EntityProviderTriggerEvent : MonoBehaviour
{
    private IEntityProviderService _entityService;
    public event Action<IEntityProvider> OnTriggerEnterEvent;

    [Inject]
    private void Construct(IEntityProviderService entityProviderService) =>
        _entityService = entityProviderService;

    private void OnTriggerEnter(Collider other)
    {
        if (gameObject == null || other.gameObject == null) return;

        var entityProvider = _entityService.GetEntityProvider(other.gameObject.GetInstanceID());

        if (entityProvider == null) return;

        OnTriggerEnterEvent?.Invoke(entityProvider);
    }
}