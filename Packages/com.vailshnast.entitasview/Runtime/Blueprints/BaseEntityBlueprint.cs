using Zenject;
using UnityEngine;

namespace MobRun
{
    public class BaseEntityBlueprint : MonoBehaviour, IEntityBlueprint
    {
        //private GameContext _gameContext;

        [Inject]
        public void Construct(IEntityBlueprintService entityBlueprintService /*,*/
            /*GameContext gameContext*/)
        {
            //_gameContext = gameContext;
            entityBlueprintService.AddBlueprint(this);
        }

        public void Initialize()
        {
            //var entity = _gameContext.CreateEntity();
        }
    }
}