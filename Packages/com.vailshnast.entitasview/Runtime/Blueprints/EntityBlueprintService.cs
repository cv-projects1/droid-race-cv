using System.Collections.Generic;

namespace MobRun
{
    public class EntityBlueprintService : IEntityBlueprintService
    {
        private readonly List<IEntityBlueprint> _entityBlueprints;

        public EntityBlueprintService()
        {
            _entityBlueprints = new List<IEntityBlueprint>();
        }

        public void Initialize() => ClearBlueprints();

        public void AddBlueprint(IEntityBlueprint entityBlueprint)
        {
            if (!_entityBlueprints.Contains(entityBlueprint))
                _entityBlueprints.Add(entityBlueprint);
        }

        public void RemoveBlueprint(IEntityBlueprint entityBlueprint)
        {
            if (_entityBlueprints.Contains(entityBlueprint))
                _entityBlueprints.Remove(entityBlueprint);
        }

        public void ClearBlueprints() => _entityBlueprints.Clear();

        public void InitializeBlueprints()
        {
            foreach (var entityBlueprint in _entityBlueprints) entityBlueprint.Initialize();
        }
    }
}