namespace MobRun
{
    public interface IEntityBlueprintService
    {
        void Initialize();
        void AddBlueprint(IEntityBlueprint entityBlueprint);
        void RemoveBlueprint(IEntityBlueprint entityBlueprint);
        void ClearBlueprints();
        void InitializeBlueprints();
    }
}