using Entitas;

namespace vailshnast.entitasview
{
    public interface IEventListener
    {
        void RegisterListeners(IEntity entity);
        void RemoveListeners();
    }
}