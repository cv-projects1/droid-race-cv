using UnityEngine;

namespace vailshnast.entitasview
{
    public class ColliderView : MonoBehaviour, ICollider
    {
        [SerializeField] private Collider _collider;
        public Collider Collider => _collider;
    }
}