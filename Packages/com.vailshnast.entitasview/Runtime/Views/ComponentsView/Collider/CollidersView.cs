using UnityEngine;

namespace vailshnast.entitasview
{
    public class CollidersView : MonoBehaviour , IColliders
    {
        [SerializeField] private Collider[] _collider;
        public Collider[] Colliders => _collider;
    }
}