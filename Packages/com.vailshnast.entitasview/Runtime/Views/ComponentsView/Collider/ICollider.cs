using UnityEngine;

namespace vailshnast.entitasview
{
    public interface ICollider : IComponentView
    {
        public Collider Collider { get; }
    }
}