using UnityEngine;

namespace vailshnast.entitasview
{
    public interface IColliders : IComponentView
    {
        public Collider[] Colliders { get; }
    }
}