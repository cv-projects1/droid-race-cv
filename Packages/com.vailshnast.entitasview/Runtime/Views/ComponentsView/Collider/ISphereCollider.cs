using UnityEngine;

namespace vailshnast.entitasview
{
    public interface ISphereCollider : IComponentView
    {
        SphereCollider SphereCollider { get; }
    }
}