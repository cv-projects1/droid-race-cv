using UnityEngine;

namespace vailshnast.entitasview
{
    public class SphereColliderView : MonoBehaviour,ISphereCollider
    {
        [SerializeField] private SphereCollider _sphereCollider;
        public SphereCollider SphereCollider => _sphereCollider;
    }
}