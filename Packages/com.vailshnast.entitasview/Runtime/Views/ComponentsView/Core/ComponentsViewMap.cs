using System;
using System.Collections.Generic;

namespace vailshnast.entitasview
{
    public class ComponentsViewMap
    {
        private readonly Dictionary<Type, IComponentView> _componentViews;

        public ComponentsViewMap(IEnumerable<IComponentView> components)
        {
            _componentViews = new Dictionary<Type, IComponentView>();
        
            foreach (var component in components)
            {
                var type = component.GetType();

                if (!_componentViews.ContainsKey(type))
                    _componentViews.Add(type, component);
            }
        }

        public T GetViewComponent<T>() where T : IComponentView
        {
            var type = typeof(T);

            foreach (var viewsKey in _componentViews.Keys)
            {
                if (type.IsAssignableFrom(viewsKey))
                    return (T) _componentViews[viewsKey];
            }
        
            return default;
        }
    }
}