using UnityEngine;
using UnityEngine.AI;

namespace vailshnast.entitasview
{
    public class NavmeshAgentView : MonoBehaviour , INavmeshAgentView
    {
        [SerializeField] private NavMeshAgent _navMeshAgent;
        public NavMeshAgent NavMeshAgent => _navMeshAgent;
    }
}