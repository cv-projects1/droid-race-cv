using UnityEngine;

namespace vailshnast.entitasview
{
    public interface IRectTransform : IComponentView
    {
        RectTransform Rect { get; }
    }
}