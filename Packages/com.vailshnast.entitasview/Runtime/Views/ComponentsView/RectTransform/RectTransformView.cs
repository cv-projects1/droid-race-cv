using UnityEngine;

namespace vailshnast.entitasview
{
    public class RectTransformView : MonoBehaviour, IRectTransform
    {
        [SerializeField] private RectTransform _transform;
        public RectTransform Rect => _transform;
    }
}