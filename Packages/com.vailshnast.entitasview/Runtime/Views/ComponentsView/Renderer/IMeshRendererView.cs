using UnityEngine;

namespace vailshnast.entitasview
{
    public interface IMeshRendererView : IComponentView
    {
        MeshRenderer MeshRendererValue { get; }
    }
}