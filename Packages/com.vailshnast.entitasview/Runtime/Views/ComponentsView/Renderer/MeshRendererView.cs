using UnityEngine;

namespace vailshnast.entitasview
{
    public class MeshRendererView : MonoBehaviour , IMeshRendererView
    {
        [SerializeField] private MeshRenderer _meshRenderer;
        public MeshRenderer MeshRendererValue => _meshRenderer;
    }
}