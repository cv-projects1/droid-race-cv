using UnityEngine;

namespace vailshnast.entitasview
{
    public interface IRigidbody : IComponentView
    {
        Rigidbody RigidbodyValue { get; }
    }
}