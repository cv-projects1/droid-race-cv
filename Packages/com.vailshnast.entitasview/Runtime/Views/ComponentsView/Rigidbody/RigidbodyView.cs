using UnityEngine;

namespace vailshnast.entitasview
{
    public class RigidbodyView : MonoBehaviour, IRigidbody
    {
        [SerializeField] private Rigidbody _rigidbody;
        public Rigidbody RigidbodyValue => _rigidbody;
    }
}