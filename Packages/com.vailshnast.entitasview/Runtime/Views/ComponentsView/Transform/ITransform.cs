using UnityEngine;

namespace vailshnast.entitasview
{
    public interface ITransform : IComponentView
    {
        Transform TransformValue { get; }
    }
}