using UnityEngine;

namespace vailshnast.entitasview
{
    public class TransformView : MonoBehaviour , ITransform
    {
        [SerializeField] private Transform _transform;
    
        public Transform TransformValue => _transform;
    }
}