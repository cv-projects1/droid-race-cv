using System.Collections.Generic;
using UnityEngine;

namespace vailshnast.entitasview
{
    public class EntityListenerContainer
    {
        private readonly List<IEntityListener> _entityListeners;

        public EntityListenerContainer(GameObject[] rootGameObjects)
        {
            _entityListeners = new List<IEntityListener>();
            
            foreach (var rootGameObject in rootGameObjects)
                _entityListeners.AddRange(rootGameObject.GetComponentsInChildren<IEntityListener>(true));
        }
            

        public void Initialize()
        {
            foreach (var entityListener in _entityListeners) 
                entityListener.Initialize();
        }
        
        public void Startup()
        {
            foreach (var entityListener in _entityListeners) 
                entityListener.Startup();
        }

        public void PostStartup()
        {
            foreach (var entityListener in _entityListeners) 
                entityListener.PostStartup();
        }
        
        public void Dispose()
        {
            foreach (var entityListener in _entityListeners) 
                entityListener.Dispose();
        }
    }
}