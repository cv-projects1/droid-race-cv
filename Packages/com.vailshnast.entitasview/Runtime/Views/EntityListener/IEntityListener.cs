namespace vailshnast.entitasview
{
    public interface IEntityListener
    {
        void Initialize();
        void Startup();
        void PostStartup();
        void Dispose();
    }
}