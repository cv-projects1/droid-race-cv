using Entitas;
using Entitas.Unity;
using UnityEngine;
using vailshnast.extensions;

namespace vailshnast.entitasview
{
    public sealed class EntityView : MonoBehaviour, IEntityView
    {
        [SerializeField] private ComponentLookupType _componentLookupType =
            ComponentLookupType.InDepthWholeHierarchy;

        private IEntity _entity;
        private IEventListener[] _eventListeners;
        private ComponentsViewMap _viewMap;
        private GameObject _gameObject;
        public T GetComponentView<T>() where T : IComponentView => _viewMap.GetViewComponent<T>();
        public T GetEntity<T>() where T : IEntity => (T) _entity;
        private bool _initialized = false;

        public void Initialize()
        {
            if (_initialized) return;

            _viewMap = new ComponentsViewMap(this.SearchInterfaceList<IComponentView>(_componentLookupType));
            _eventListeners = this.SearchInterfaceArray<IEventListener>(_componentLookupType);
            _initialized = true;
        }

        public void Link(IEntity entity)
        {
            _entity = entity;
            Initialize();
            RegisterEvents();
            _gameObject = gameObject;
#if UNITY_EDITOR
            _gameObject.Link(entity);
#endif
        }

        private void RegisterEvents()
        {
            foreach (var listener in _eventListeners) listener.RegisterListeners(_entity);
        }

        public void Unlink()
        {
#if UNITY_EDITOR
            _gameObject.Unlink();
#endif
            foreach (var listener in _eventListeners) listener.RemoveListeners();
        }
    }
}