using Entitas;

namespace vailshnast.entitasview
{
    public interface IEntityView
    {
        public T GetComponentView<T>() where T : IComponentView;
    
        public T GetEntity<T>() where T : IEntity;
        public void Link(IEntity entity);
        public void Unlink();
    }
}