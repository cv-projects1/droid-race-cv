using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using vailshnast.serviceparent;

namespace vailshnast.eventsystem
{
    public class EventSystemService : IEventSystemService
    {
        private readonly IServicesParent _servicesParent;
        private EventSystem _eventSystem;
        
        public EventSystem EventSystem => _eventSystem;

        public EventSystemService(IServicesParent servicesParent) =>
            _servicesParent = servicesParent;

        public void Initialize()
        {
            GameObject eventTransform = new GameObject($"EventSystem");
            eventTransform.transform.SetParent(_servicesParent.Parent);

            _eventSystem = eventTransform.AddComponent<EventSystem>();
            eventTransform.AddComponent<StandaloneInputModule>();
        }

        public bool IsPointerOverUIObject()
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            _eventSystem.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }
    }
}