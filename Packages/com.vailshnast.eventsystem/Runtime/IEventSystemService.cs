using UnityEngine.EventSystems;

namespace vailshnast.eventsystem
{
    public interface IEventSystemService
    {
        void Initialize();
        EventSystem EventSystem { get; }
        bool IsPointerOverUIObject();
    }
}