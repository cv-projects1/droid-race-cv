using System;
using System.Collections.Generic;
using System.Reflection;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEngine;

namespace vailshnast.extensions.editor
{
    public class DotweenSettingsAttributeProcessor : OdinAttributeProcessor<DotweenSettings>
    {
        public override void ProcessChildMemberAttributes(InspectorProperty parentProperty, MemberInfo member, List<Attribute> attributes)
        {
            base.ProcessChildMemberAttributes(parentProperty, member, attributes);

            switch (member.Name)
            {
                case "UseCustomCurve":
                    attributes.Add(new FoldoutGroupAttribute(parentProperty.Name));
                    break;
                case "Ease":
                    attributes.Add(new HideIfAttribute("UseCustomCurve"));
                    attributes.Add(new FoldoutGroupAttribute(parentProperty.Name));
                    break;
                case "CustomCurve":
                    attributes.Add(new ShowIfAttribute("UseCustomCurve"));
                    attributes.Add(new FoldoutGroupAttribute(parentProperty.Name));
                    break;
                case "Duration":
                    attributes.Add(new FoldoutGroupAttribute(parentProperty.Name));
                    break;
            }
        }

        public override void ProcessSelfAttributes(InspectorProperty property, List<Attribute> attributes)
        {
            base.ProcessSelfAttributes(property, attributes);
            attributes.Add(new HideLabelAttribute());
        }
    }
    
    public class DotweenPunchSettingsAttributeProcessor : OdinAttributeProcessor<DotweenPunchSettings>
    {
        public override void ProcessChildMemberAttributes(InspectorProperty parentProperty, MemberInfo member, List<Attribute> attributes)
        {
            base.ProcessChildMemberAttributes(parentProperty, member, attributes);

            switch (member.Name)
            {
                case "UseCustomCurve":
                    attributes.Add(new FoldoutGroupAttribute(parentProperty.Name));
                    break;
                case "Punch":
                    attributes.Add(new FoldoutGroupAttribute(parentProperty.Name));
                    break;
                case "Vibrato":
                    attributes.Add(new FoldoutGroupAttribute(parentProperty.Name));
                    break;
                case "Elasticity":
                    attributes.Add(new FoldoutGroupAttribute(parentProperty.Name));
                    break;
                case "Ease":
                    attributes.Add(new HideIfAttribute("UseCustomCurve"));
                    attributes.Add(new FoldoutGroupAttribute(parentProperty.Name));
                    break;
                case "CustomCurve":
                    attributes.Add(new ShowIfAttribute("UseCustomCurve"));
                    attributes.Add(new FoldoutGroupAttribute(parentProperty.Name));
                    break;
                case "Duration":
                    attributes.Add(new FoldoutGroupAttribute(parentProperty.Name));
                    break;
            }
        }

        public override void ProcessSelfAttributes(InspectorProperty property, List<Attribute> attributes)
        {
            base.ProcessSelfAttributes(property, attributes);
            attributes.Add(new HideLabelAttribute());
        }
    }
}
