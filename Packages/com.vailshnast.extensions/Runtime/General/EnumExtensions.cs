using System;
using System.Collections.Generic;
using System.Linq;
using Random = UnityEngine.Random;

namespace vailshnast.extensions
{
    public static class EnumExtensions
    {
        public static T RandomEnumValue<T> () where T : Enum
        {
            var v = Enum.GetValues (typeof (T));
            return (T) v.GetValue(Random.Range(0, v.Length));
        }
        
        public static  List<EnumPair<TExclude1, TExclude2>> GetRandom<TExclude1, TExclude2>(params EnumPair<TExclude1,TExclude2>[] excludePairs)
            where TExclude1 : struct, IConvertible where TExclude2 : struct, IConvertible
        {
            var pairList = excludePairs.ToList();
            var type1 = Enum.GetValues(typeof(TExclude1)).Cast<TExclude1>().ToList();
            var type2 = Enum.GetValues(typeof(TExclude2)).Cast<TExclude2>().ToList();

            var typeList = new List<EnumPair<TExclude1, TExclude2>>();
        
            for (int i = 0; i < type1.Count; i++)
            {
                for (int j = 0; j < type2.Count; j++)
                {
                    var value1 = type1[i];
                    var value2 = type2[j];
                    var value = new EnumPair<TExclude1, TExclude2> {Enum1 = value1, Enum2 = value2};
                
                    if(pairList.Exists(x => x.Enum1.Equals(value1) && x.Enum2.Equals(value2))) continue;
                
                    typeList.Add(value);
                }
            }
        
            return typeList;;
        }
    }

    public class EnumPair<TEnum1, TEnum2> where TEnum1 : struct, IConvertible where TEnum2 : struct, IConvertible
    {
        public TEnum1 Enum1;
        public TEnum2 Enum2;
    }
}