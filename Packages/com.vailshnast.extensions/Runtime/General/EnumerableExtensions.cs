using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace vailshnast.extensions
{
    public static class EnumerableExtensions
    {
        public static T RandomElement<T>(this IEnumerable<T> enumerable)
        {
            var source = enumerable as T[] ?? enumerable.ToArray();
            var index = UnityEngine.Random.Range(0, source.Length);
            return source.ElementAt(index);
        }

        public static IEnumerable<T> ToEnumerable<T>(this IEnumerator<T> enumerator)
        {
            while (enumerator.MoveNext())
                yield return enumerator.Current;
        }

        public static IEnumerator<T> Cast<T>(this IEnumerator iterator)
        {
            while (iterator.MoveNext())
                yield return (T) iterator.Current;
        }
        
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
        {
            var elements = source.ToArray();
            
            for (var i = elements.Length - 1; i >= 0; i--)
            {
                var swapIndex = UnityEngine.Random.Range(0, i + 1);
                yield return elements[swapIndex];
                elements[swapIndex] = elements[i];
            }
        }

        public static T Remove<T>(this List<T> source, Predicate<T> match) where T : class
        {
            var index = source.FindIndex(match);
            if (index == -1) return null;
            
            var result = source[index];
            source.RemoveAt(index);
            
            return result;
        }
    }
}