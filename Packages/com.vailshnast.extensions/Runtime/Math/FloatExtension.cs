using UnityEngine;

namespace vailshnast.extensions
{
    public static class FloatExtension
    {
        public static float Remap(this float from, float fromMin, float fromMax, float toMin, float toMax)
        {
            var fromAbs = from - fromMin;
            var fromMaxAbs = fromMax - fromMin;
            var normal = fromAbs / fromMaxAbs;
            var toMaxAbs = toMax - toMin;
            var toAbs = toMaxAbs * normal;
            var to = toAbs + toMin;

            return to;
        }

        public static int ToNearestInt(this float value) => Mathf.RoundToInt(value);
        public static float Clamp01(this float value) => Mathf.Clamp01(value);
        public static float Clamp(this float value, float min, float max) => Mathf.Clamp(value, min, max);
    }
}