using UnityEngine;

namespace vailshnast.extensions
{
    public static class MathfHelper
    {
        public static Vector3 GetPointOnCircleXY(Vector3 center, float angle, float radius)
        {
            var x = Mathf.Cos(angle) * radius + center.x;
            var y = Mathf.Sin(angle) * radius + center.y;
            var z = center.z;

            return new Vector3(x, y, z);
        }

        public static Vector3 RotateAround(Vector3 startPoint, Vector3 center, Vector3 axis, float angle) {
            Vector3 pos = startPoint;
            Quaternion rot = Quaternion.AngleAxis(angle, axis); // get the desired rotation
            Vector3 dir = pos - center;                         // find current direction relative to center
            dir = rot * dir;                                    // rotate the direction
            return center + dir;
            // define new position
            // rotate object to keep looking at the center:
            //Quaternion myRot = transform.rotation;
            //transform.rotation *= Quaternion.Inverse(myRot) * rot * myRot;
        }
    }
}