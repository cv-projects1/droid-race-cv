using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Core.Enums;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using UnityEngine.UI;
using vailshnast.uicomponents;

namespace vailshnast.extensions
{
    public static class DoTweenExtensions
    {
        public static TweenerCore<float, float, FloatOptions> DoFade(this Image image, float targetAlpha,
                                                                     float duration, bool autoInvert = true)
        {
            if (autoInvert)
            {
                var color = image.color;
                color.a = 1 - targetAlpha;
                image.color = color;
            }

            DOGetter<float> getter = () => image.color.a;

            DOSetter<float> setter = x =>
            {
                var color = image.color;
                color.a = x;
                image.color = color;
            };

            return DOTween.To(getter, setter, targetAlpha, duration);
        }

        public static TweenerCore<float, float, FloatOptions> DoFade(this CanvasGroup canvasGroup, float targetAlpha,
                                                                     float duration, bool autoInvert = true)
        {
            if (autoInvert)
                canvasGroup.alpha = 1 - targetAlpha;

            if (targetAlpha > 0.5f)
                canvasGroup.gameObject.SetActive(true);

            DOGetter<float> getter = () => canvasGroup.alpha;
            DOSetter<float> setter = x => canvasGroup.alpha = x;
            var tween = DOTween.To(getter, setter, targetAlpha, duration);
            if (targetAlpha < 0.5f) tween.OnComplete(() => canvasGroup.gameObject.SetActive(false));

            return tween;
        }

        public static Tweener DOShakeAnchoredPosition(
            this RectTransform target,
            float duration,
            float strength = 1f,
            int vibrato = 10,
            float randomness = 90f,
            bool snapping = false,
            bool fadeOut = true)
        {
            if (duration > 0.0)
                return DOTween.Shake(() => target.anchoredPosition, x => target.anchoredPosition = x,
                                     duration, strength, vibrato, randomness, false, fadeOut).SetTarget(target)
                              .SetSpecialStartupMode(SpecialStartupMode.SetShake).SetOptions(snapping);

            if (Debugger.logPriority > 0)
                Debug.LogWarning(
                    "DOShakePosition: duration can't be 0, returning NULL without creating a tween");

            return null;
        }
        
        public static TweenerCore<float, float, FloatOptions> DOFillAmount(this SlicedFilledImage target, float endValue, float duration)
        {
            if (endValue > 1) endValue = 1;
            else if (endValue < 0) endValue = 0;
            TweenerCore<float, float, FloatOptions> t = DOTween.To(() => target.fillAmount, x => target.fillAmount = x, endValue, duration);
            t.SetTarget(target);
            return t;
        }

        public static Tween ApplyCurve(this Tween tween, DotweenSettings settings)
        {
            if (settings.UseCustomCurve)
                tween.SetEase(settings.CustomCurve);
            else tween.SetEase(settings.Ease);

            return tween;
        }

        public static Tween CreatePunchScaleTweenFromSettings(this DotweenPunchSettings settings, Transform transform)
        {
            var tween = transform.DOPunchScale(settings.Punch,
                                               settings.Duration,
                                               settings.Vibrato,
                                               settings.Elasticity = 1);

            if (settings.UseCustomCurve)
                tween.SetEase(settings.CustomCurve);
            else tween.SetEase(settings.Ease);

            return tween;
        }
        public static Tween CreatePunchRotationTweenFromSettings(this DotweenPunchSettings settings, Transform transform)
        {
            var tween = transform.DOPunchRotation(settings.Punch,
                                               settings.Duration,
                                               settings.Vibrato,
                                               settings.Elasticity = 1);

            if (settings.UseCustomCurve)
                tween.SetEase(settings.CustomCurve);
            else tween.SetEase(settings.Ease);

            return tween;
        }
        public static Tween CreatePunchPosTweenFromSettings(this DotweenPunchSettings settings, Transform transform)
        {
            var tween = transform.DOPunchPosition(settings.Punch,
                                               settings.Duration,
                                               settings.Vibrato,
                                               settings.Elasticity = 1);

            if (settings.UseCustomCurve)
                tween.SetEase(settings.CustomCurve);
            else tween.SetEase(settings.Ease);

            return tween;
        }
    }
}