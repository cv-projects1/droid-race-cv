using System;
using DG.Tweening;
using UnityEngine;

namespace vailshnast.extensions
{
    [Serializable]

    public class DotweenPunchSettings
    {
        public bool UseCustomCurve;
        public Ease Ease;
        public AnimationCurve CustomCurve;

        public Vector3 Punch = Vector3.one;
        public int  Vibrato = 10;
        public float Elasticity = 1f;
        public float Duration = 1f;
    }
}