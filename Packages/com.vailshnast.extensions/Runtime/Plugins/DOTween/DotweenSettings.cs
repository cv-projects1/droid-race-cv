using System;
using DG.Tweening;
using UnityEngine;

namespace vailshnast.extensions
{
    [Serializable]
    public class DotweenSettings
    {
        public bool UseCustomCurve;
        public Ease Ease;
        public AnimationCurve CustomCurve;
        public float Duration;
    }
}