﻿using Newtonsoft.Json;

namespace vailshnast.extensions
{
    public static class JsonExtension
    {
        public static string ToJson(this object obj, bool isPretty = false) =>
            JsonConvert.SerializeObject(obj, isPretty ? Formatting.Indented : Formatting.None);

        public static T FromJson<T>(this string json) => JsonConvert.DeserializeObject<T>(json);

        public static string ToJson(this object obj, JsonSerializerSettings settings, bool isPretty = false) =>
            JsonConvert.SerializeObject(obj, isPretty ? Formatting.Indented : Formatting.None, settings);

        public static T FromJson<T>(this string json, JsonSerializerSettings settings) =>
            JsonConvert.DeserializeObject<T>(json, settings);

        public static JsonSerializerSettings GetAutoJsonSettings() =>
            new JsonSerializerSettings {TypeNameHandling = TypeNameHandling.Auto};
    }
}