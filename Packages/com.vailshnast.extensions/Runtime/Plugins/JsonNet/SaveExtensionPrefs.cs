using Newtonsoft.Json;
using UnityEngine;

namespace vailshnast.extensions
{
    public class SaveExtensionPrefs
    {
        public static T LoadObject<T>(string prefsKey, JsonSerializerSettings settings = null)
        {
            var jsonString = PlayerPrefs.GetString(prefsKey, string.Empty);

            if (string.IsNullOrEmpty(jsonString)) Debug.LogError($"{prefsKey} json is empty");

            return jsonString.FromJson<T>(settings);
        }

        public static void SaveObject(object saveObject, string fileName, JsonSerializerSettings settings = null)
        {
            var jsonString = saveObject.ToJson(settings);
            PlayerPrefs.SetString(fileName, jsonString);
            PlayerPrefs.Save();
        }
    }
}