﻿using System;
using System.IO;
using JetBrains.Annotations;
using Newtonsoft.Json;
using UnityEngine;

namespace vailshnast.extensions
{
    public static class SaveExtensionTxt
    {
        public static T LoadObject<T>(string fileName, JsonSerializerSettings settings = null)
        {
            var path = Path.Combine(Application.persistentDataPath, $"{fileName}");
            if (!File.Exists(path)) Debug.Log($"Path: {path} doesnt exits");
            var json = File.ReadAllText(path);

            return json.FromJson<T>(settings);
        }

        public static void SaveObject(object saveObject, string fileName, JsonSerializerSettings settings = null)
        {
            var json = saveObject.ToJson(settings);
            var path = Path.Combine(Application.persistentDataPath, $"{fileName}");
            File.WriteAllText(path, json);
        }

        public static bool DataPathExists(string fileName)
        {
            var path = Path.Combine(Application.persistentDataPath, $"{fileName}");

            return File.Exists(path);
        }
    }
}