using System;
using Random = UnityEngine.Random;

namespace vailshnast.extensions
{
    [Serializable]
    public class MinMaxFloat
    {
        public float MinValue, MaxValue;

        public float GetRandom() => Random.Range(MinValue, MaxValue);
    }
}