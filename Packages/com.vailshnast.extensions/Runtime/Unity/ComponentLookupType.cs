namespace vailshnast.extensions
{
    public enum ComponentLookupType
    {
        WholeObject = 0,
        InDepthWholeHierarchy = 1,
        InDepthOnlyActiveHierarchy = 2,
        FirstEntry = 3
    }
}