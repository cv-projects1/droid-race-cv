using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace vailshnast.extensions
{
    public static class MonobehaviourExtensions
    {
        public static List<T> SearchInterfaceList<T>(this MonoBehaviour mono, ComponentLookupType componentLookupType)
        {
            switch (componentLookupType)
            {
                case ComponentLookupType.WholeObject: return mono.GetComponents<T>().ToList();
                case ComponentLookupType.InDepthWholeHierarchy: return mono.GetComponentsInChildren<T>(true).ToList();
                case ComponentLookupType.InDepthOnlyActiveHierarchy: return mono.GetComponentsInChildren<T>().ToList();
                case ComponentLookupType.FirstEntry:
                    var component = mono.GetComponent<T>();
                    return component != null ? new List<T> {component} : new List<T>();
                default: return new List<T>();
            }
        }
        
        public static T[] SearchInterfaceArray<T>(this MonoBehaviour mono, ComponentLookupType componentLookupType)
        {
            switch (componentLookupType)
            {
                case ComponentLookupType.WholeObject: return mono.GetComponents<T>().ToArray();
                case ComponentLookupType.InDepthWholeHierarchy: return mono.GetComponentsInChildren<T>(true).ToArray();
                case ComponentLookupType.InDepthOnlyActiveHierarchy: return mono.GetComponentsInChildren<T>().ToArray();
                case ComponentLookupType.FirstEntry:
                    var component = mono.GetComponent<T>();

                    return component != null ? new[] {component} : Array.Empty<T>();
                default: return Array.Empty<T>();
            }
        }
    }
}