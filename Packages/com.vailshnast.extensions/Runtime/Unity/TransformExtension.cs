using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace vailshnast.extensions
{
    public static class TransformExtension
    {
        public static Transform[] GetChilds(this Transform transform, bool includeInactive = false)
        {
            var childs = transform.GetEnumerator().Cast<Transform>().ToEnumerable();
            if (!includeInactive) childs = childs.Where(x => x.gameObject.activeInHierarchy);
            return childs.ToArray();
        }

        public static void RemoveFromDontDestroyOnLoad(this GameObject gameObject)
        {
            gameObject.transform.SetParent(null);
            SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetActiveScene());
        }
        public static void RemoveFromDontDestroyOnLoad(this MonoBehaviour mono)
        {
            mono.transform.SetParent(null);
            SceneManager.MoveGameObjectToScene(mono.gameObject, SceneManager.GetActiveScene());
        }

    }
}