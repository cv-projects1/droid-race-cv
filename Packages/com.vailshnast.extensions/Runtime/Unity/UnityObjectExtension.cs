using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

namespace vailshnast.extensions
{
    public static class UnityObjectExtension
    {
        public static bool IsNull(this Object o)
        {
            return ReferenceEquals(o, null);
        }

        public static bool IsNotNull(this Object o)
        {
            return !ReferenceEquals(o, null);
        }

        public static bool RefEquals(this Object o, Object obj)
        {
            return ReferenceEquals(o, obj);
        }

        public static bool InstanceIdEquals(this Object o, Object obj)
        {
            return o.GetInstanceID() == obj.GetInstanceID();
        }
    }
}