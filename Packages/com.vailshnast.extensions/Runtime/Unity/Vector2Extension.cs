using UnityEngine;

namespace vailshnast.extensions
{
    public static class Vector2Extension
    {
        public static float Random(this Vector2 v)
        {
            return UnityEngine.Random.Range(v.x, v.y);
        }
        
        public static float Random(this Vector2Int v)
        {
            return UnityEngine.Random.Range(v.x, v.y);
        }
    }
}