using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace vailshnast.installers.editor
{
    [CustomEditor(typeof(ScriptableObjectsInstaller))]
    public class ScriptableObjectInstallerEditor : Editor
    {
        private ScriptableObjectsInstaller _installer;
        public void OnEnable() => _installer =  target as ScriptableObjectsInstaller;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            if (GUILayout.Button("Load all scriptables"))
            {
                var guids = AssetDatabase.FindAssets("t:ScriptableObject");
                var toSelect = new List<ScriptableObject>();
            
                foreach (var guid in guids)
                {
                    var path = AssetDatabase.GUIDToAssetPath(guid);

                    var toCheck = AssetDatabase.LoadAllAssetsAtPath(path)
                                               .ToList()
                                               .OfType<ScriptableObject>()
                                               .Where(x => x is IScriptableService);
                    
                    toSelect.AddRange(toCheck);
                }
                
                _installer.SetScriptables(toSelect.ToList());
                EditorUtility.SetDirty(target);
                AssetDatabase.SaveAssets();
            }
        }
    }
}