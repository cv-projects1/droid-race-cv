using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Zenject;

namespace vailshnast.installers
{
    public class ScriptableObjectsInstaller : MonoInstaller
    {
        [SerializeField] private List<ScriptableObject> _scriptableObjects = null;

        public override void InstallBindings()
        {
#if UNITY_EDITOR
            for (var i = 0; i < _scriptableObjects.Count; i++)
            {
                if (_scriptableObjects[i] is IScriptableService) continue;

                var errorMsg =
                    $"Scriptable object with type {_scriptableObjects[i].GetType().FullName} was not implement {nameof(IScriptableService)} interface. Skipped.";

                EditorApplication.isPlaying = false;

                throw new NotSupportedException(errorMsg);
            }
#endif
            var scriptableServices = _scriptableObjects.OfType<IScriptableService>().ToList();

            foreach (var scriptableService in scriptableServices)
            {
                ScopeConcreteIdArgConditionCopyNonLazyBinder bindBuilder;

                if (scriptableService is IServiceIdentifier identifier)
                    bindBuilder = Container.Bind(scriptableService.ServiceType).WithId(identifier.ServiceId)
                                           .FromInstance(scriptableService);
                else bindBuilder = Container.Bind(scriptableService.ServiceType).FromInstance(scriptableService);

                if (scriptableService is IBindType bindType)
                {
                    switch (bindType.BindType)
                    {
                        case BindType.AsCached:
                            bindBuilder.AsCached().NonLazy();

                            break;
                        case BindType.AsSingle:
                            bindBuilder.AsSingle().NonLazy();

                            break;
                        case BindType.AsTransient:
                            bindBuilder.AsTransient().NonLazy();

                            break;
                    }
                }
                else bindBuilder.AsCached().NonLazy();
            }
        }

#if UNITY_EDITOR
        public void SetScriptables(List<ScriptableObject> scriptableObjects) =>
            _scriptableObjects = scriptableObjects;
#endif
    }
}