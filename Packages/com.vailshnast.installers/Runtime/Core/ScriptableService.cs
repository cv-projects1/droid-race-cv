using System;
using UnityEngine;

namespace vailshnast.installers
{
    public class ScriptableService : ScriptableObject , IScriptableService
    {
        public Type ServiceType => GetType();
    }
}