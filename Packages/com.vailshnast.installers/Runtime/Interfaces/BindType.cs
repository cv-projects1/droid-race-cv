namespace vailshnast.installers
{
    public enum BindType
    {
        AsCached, AsSingle, AsTransient
    }
}