namespace vailshnast.installers
{
    public interface IBindType
    {
        BindType BindType { get; }
    }
}