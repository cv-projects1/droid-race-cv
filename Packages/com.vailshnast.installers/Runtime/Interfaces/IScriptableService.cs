using System;

namespace vailshnast.installers
{
    public interface IScriptableService
    {
        Type ServiceType { get; }
    }
}