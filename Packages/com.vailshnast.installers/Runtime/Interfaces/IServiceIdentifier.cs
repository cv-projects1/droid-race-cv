namespace vailshnast.installers
{
    public interface IServiceIdentifier
    {
        object ServiceId { get; }
    }
}