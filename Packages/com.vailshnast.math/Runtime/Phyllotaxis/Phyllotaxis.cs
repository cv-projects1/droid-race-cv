using System;
using UnityEngine;

namespace vailshnast.math
{
    public static class Phyllotaxis
    {
        public static Vector2 Calculate(float degree, float scale, int count)
        {
            float angle = count * (degree * Mathf.Deg2Rad);
            float radius = scale * Mathf.Sqrt(count);
            float xCoord = radius * Mathf.Cos(angle);
            float yCoord = radius * Mathf.Sin(angle);

            return new Vector2(xCoord, yCoord);
        }

        public static float GetRadius(float scale, int count) => Mathf.Sqrt(count) * scale;
    }
}