using System;
using UnityEngine;
using vailshnast.installers;

namespace vailshnast.mobile.essensials
{
    [CreateAssetMenu(fileName = "AmplitudeConfig", menuName = "StaticData/AmplitudeConfig")]
    public class AmplitudeConfig : ScriptableObject, IScriptableService
    {
        public Type ServiceType => typeof(AmplitudeConfig);

        public string API_Key;
        //public string Secret;
    }
}
