using vailshnast.mobile.essensials;

public class AmplitudeService : IAnalyticsService
{
    private readonly AmplitudeConfig _amplitudeConfig;
    
    private Amplitude _amplitude;
    private bool _initialized = false;

    public AmplitudeService(AmplitudeConfig amplitudeConfig)
    {
        _amplitudeConfig = amplitudeConfig;
    }
    
    public void Initialize()
    {
        if (_initialized) return;

#if UNITY_IOS || UNITY_ANDROID
        _amplitude = Amplitude.getInstance();
        _amplitude.setServerUrl("https://api2.amplitude.com");
        _amplitude.logging = true;
        _amplitude.trackSessionEvents(true);
        _amplitude.init(_amplitudeConfig.API_Key);
#endif
        _initialized = true;
    }

    public void LogEvent(IEventData data)
    {
        Initialize();
#if UNITY_IOS || UNITY_ANDROID
        _amplitude.logEvent(data.EventName);
#endif
    }

    public void LogEvent(IEventParamData eventParamData)
    {
        Initialize();
#if UNITY_IOS || UNITY_ANDROID
        _amplitude.logEvent(eventParamData.EventName,eventParamData.EventParams);
#endif
    }
}