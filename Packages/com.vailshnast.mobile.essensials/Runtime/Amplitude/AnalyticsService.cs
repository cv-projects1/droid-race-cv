using System.Collections.Generic;

public interface IAnalyticsService
{
    void LogEvent(IEventData eventData);
    void LogEvent(IEventParamData eventParamData);
    void Initialize();
}

public interface IEventData
{
    string EventName { get; set; }
}

public interface IEventParamData
{
    string EventName { get; set; }
    IDictionary<string, object> EventParams { get; set; }
}

public class EventData : IEventData
{
    public string EventName { get; set; }
}

public class EventParamData : IEventParamData
{
    public string EventName { get; set; }
    public IDictionary<string, object> EventParams { get; set; }
}