using System;
using UnityEngine;
using vailshnast.installers;

namespace vailshnast.mobile.essensials
{
    [CreateAssetMenu(fileName = "IOSConfig", menuName = "StaticData/IOSConfig")]
    public class IOSConfig : ScriptableObject, IScriptableService
    {
        public Type ServiceType => typeof(IOSConfig);

        [Multiline] public string PrivacyAndTermsText;

        public string ButtonName = "Continue",
                      TermsURL,
                      PrivacyURL,
                      PrivacyHighlightText,
                      TermsHighlightText;

        public int Spaces = 7, FontSize = 20;
    }
}
