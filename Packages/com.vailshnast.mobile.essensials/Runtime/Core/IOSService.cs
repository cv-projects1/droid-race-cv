#if UNITY_IOS
using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using UnityEngine;
using AOT;
using Nrjwolf.Tools;

// Include the IosSupport namespace if running on iOS:
using Unity.Advertisement.IosSupport;
using UnityEngine.iOS;
#endif

namespace vailshnast.mobile.essensials
{
    public class IOSService : IPlatformService
    {
        private readonly IOSConfig _iosConfig;

        public IOSService(IOSConfig iosConfig)
        {
            _iosConfig = iosConfig;
        }

        public void Initialize(bool isFirstLaunch)
        {
#if UNITY_IOS
            if(isFirstLaunch)
            ShowPrivacyPolicy(InitializeAtt);
#endif
        }

#if UNITY_IOS
        private void InitializeAtt()
        {
            // check with iOS to see if the user has accepted or declined tracking
            var status = ATTrackingStatusBinding.GetAuthorizationTrackingStatus();
            var currentVersion = new Version(Device.systemVersion);
            var ios14 = new Version("14.5");

            Debug.Log($"App Tracking Transparency STATUS: {status} ver: {currentVersion}");
            // Check the user's consent status.
            // If the status is undetermined, display the request request:
            if (status == ATTrackingStatusBinding.AuthorizationTrackingStatus.NOT_DETERMINED && currentVersion >= ios14)
            {
                Debug.Log("Unity iOS Support: Requesting iOS App Tracking Transparency native dialog.");
                ATTrackingStatusBinding.RequestAuthorizationTracking(OnAuthorizationTrackingReceived);
            }
        }

        private void OnAuthorizationTrackingReceived(int status)
        {
            Debug.Log($"App Tracking Transparency recieved {status}");
        }

        private void ShowPrivacyPolicy(Action onContinue = null)
        {
            Debug.Log("Trying to Show Privacy Policy");
            
            var productName = Application.productName;
            var privacyTermsText = _iosConfig.PrivacyAndTermsText.Replace("PRODUCT NAME", productName);

            IOSNativeAlert.ShowPrivacyPolicyMessage(_iosConfig.Spaces, _iosConfig.ButtonName, 0,
                                                    privacyTermsText, _iosConfig.PrivacyURL,
                                                    _iosConfig.TermsURL, _iosConfig.PrivacyHighlightText,
                                                    _iosConfig.TermsHighlightText, _iosConfig.FontSize, onContinue);

        }
#endif
    }
}