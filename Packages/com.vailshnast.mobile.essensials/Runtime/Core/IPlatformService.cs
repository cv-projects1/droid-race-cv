using System.Threading.Tasks;

namespace vailshnast.mobile.essensials
{
    public interface IPlatformService
    {
        void Initialize(bool isFirstLaunch);
    }
}