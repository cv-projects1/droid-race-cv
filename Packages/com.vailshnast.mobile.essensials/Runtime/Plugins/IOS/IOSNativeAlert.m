//
//  IOSNativeAlert.m
//  Alert
//
//  Created by Nrjwolf on 04/12/2019.
//  Copyright © 2019 Nrjwolf. All rights reserved.
//

extern UIViewController *UnityGetGLViewController();

static NSString *ToNSString(char* string) {
    return [NSString stringWithUTF8String:string];
}

// Unity button delegate delegate
typedef void (*MonoPAlertButtonDelegate)(const char* buttonId);
static MonoPAlertButtonDelegate _onButtonClick = NULL;

// Register unity message handler
FOUNDATION_EXPORT void IOSRegisterMessageHandler(MonoPAlertButtonDelegate onButtonClick)
{
    _onButtonClick = onButtonClick;
}

// Send message to unity
void SendMessageToUnity(const char* buttonId) {
    NSLog(@"Clicked %s", buttonId);
    if(_onButtonClick != NULL) {
        _onButtonClick(buttonId);
    }
}

void CreateTextView(UIAlertController* alert, NSString* text, NSString* privacyURL, NSString* termsURL, NSString* privacyButton, NSString* termsButton, float fontSize)
{
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectZero];
    textView.translatesAutoresizingMaskIntoConstraints = NO;
    textView.editable = FALSE;
    textView.dataDetectorTypes = UIDataDetectorTypeAll;
    textView.userInteractionEnabled = TRUE;
    textView.backgroundColor = [UIColor clearColor];
    textView.scrollEnabled = TRUE;
    //textView.textAlignment = NSTextAlignmentJustified;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    
    NSRange privacyRange = [text rangeOfString:(privacyButton)];
    NSRange termsRange = [text rangeOfString:(termsButton)];

    [attributedString addAttribute:NSLinkAttributeName value: [NSURL URLWithString:privacyURL] range:privacyRange];
    [attributedString addAttribute:NSLinkAttributeName value: [NSURL URLWithString:termsURL] range:termsRange];
        
    textView.attributedText = attributedString;
    
    NSLayoutConstraint *leadConstraint = [NSLayoutConstraint constraintWithItem:alert.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:-8.0];
    NSLayoutConstraint *trailConstraint = [NSLayoutConstraint constraintWithItem:alert.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:8.0];

    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:alert.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeTop multiplier:1.0 constant:1.0];
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:alert.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:64.0];
    
    [textView setFont:[UIFont systemFontOfSize:fontSize]];
    [textView setTextAlignment:NSTextAlignmentCenter];
    [alert.view addSubview:textView];
    [NSLayoutConstraint activateConstraints:@[leadConstraint, trailConstraint, topConstraint, bottomConstraint]];
}

void _IOSShowPrivacyPolicy (int alertStyle, int spaces, char* buttonId, int buttonsStyle,
                            char* privacyAndTermsText, char* privacyURL, char* termsURL, char* privacyButton, char* termsButton, int fontSize)
{
    
    NSString *original = @"\n";
    // Capacity does not limit the length, it's just an initial capacity
    NSMutableString *result = [NSMutableString stringWithCapacity:[original length] * spaces];

    int i;
    for (i = 0; i < spaces; i++)
        [result appendString:original];
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle : result message : ToNSString(@"") preferredStyle : (UIAlertControllerStyle)alertStyle];

    NSString *buttonTitle = ToNSString(buttonId);
    
    
    UIAlertActionStyle style = (UIAlertActionStyle)buttonsStyle;
    UIAlertAction * button = [UIAlertAction actionWithTitle: buttonTitle style:style handler:^(UIAlertAction * action)
    {
        NSString* buttonId = [NSString stringWithFormat:@"%@%d", buttonTitle, 0];
        SendMessageToUnity((char*)[buttonId UTF8String]);
    }];
    
    [alert addAction:button];
        
    alert.view.autoresizesSubviews = YES;
        
    CreateTextView(alert,ToNSString(privacyAndTermsText), ToNSString(privacyURL),ToNSString(termsURL),
                   ToNSString(privacyButton),ToNSString(termsButton), fontSize);

    dispatch_async(dispatch_get_main_queue(), ^{
        [UnityGetGLViewController() presentViewController:alert animated:YES completion:nil];
    });
}

// Alert function
void _IOSShowAlertMsg (int alertStyle, char* title, char* message, char* buttons[], int buttonsStyle[], int buttonsLength) {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle : ToNSString(title) message : ToNSString(message) preferredStyle : (UIAlertControllerStyle)alertStyle];

    // Add buttons
    if (buttons && buttonsLength > 0) {
        for (int i = 0; i < buttonsLength; i++) {
            NSString *buttonTitle = ToNSString(buttons[i]);
            UIAlertActionStyle style = (UIAlertActionStyle)buttonsStyle[i];
            UIAlertAction * button = [UIAlertAction actionWithTitle: buttonTitle style:style handler:^(UIAlertAction * action) {
                NSString* buttonId = [NSString stringWithFormat:@"%@%d", buttonTitle, i];
                SendMessageToUnity((char*)[buttonId UTF8String]);
            }];
            [alert addAction:button];
        }
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        [UnityGetGLViewController() presentViewController:alert animated:YES completion:nil];
    });
}


/// Show something like android toast
void _IOSShowToast (char* message, BOOL isLongDuration) {
    NSString *messageString = ToNSString(message);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                message:messageString
                                                            preferredStyle:UIAlertControllerStyleAlert];

    [UnityGetGLViewController() presentViewController:alert animated:YES completion:nil];
    int duration = isLongDuration ? 1 : .5;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
}
