namespace vailshnast.observer
{
    public interface IObserver
    {
        void OnObjectChanged(IObservable observable);
    }
    
    public interface IObserver<in T>
    {
        void OnObjectChanged(T observable);
    }
}