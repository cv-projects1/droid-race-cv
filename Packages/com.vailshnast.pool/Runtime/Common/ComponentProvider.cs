using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace vailshnast.pool
{
    public sealed class ComponentProvider
    {
        private readonly Dictionary<Type, IPoolComponent> _componentsMap = new Dictionary<Type, IPoolComponent>();
        private readonly Dictionary<Type, Dictionary<object, IPoolComponent>> _identifierComponentsMap = new Dictionary<Type, Dictionary<object, IPoolComponent>>();
        
        public void Register(IPoolComponent poolComponent)
        {
            var primaryKey = poolComponent.ComponentType;
            var secondaryKey = (poolComponent as IPoolComponentIdentifier)?.ComponentId;
            
            if (secondaryKey != null)
            {
                if (_identifierComponentsMap.ContainsKey(primaryKey))
                    _identifierComponentsMap[primaryKey].Add(secondaryKey, poolComponent);
                else
                {
                    var secondaryMap = new Dictionary<object, IPoolComponent>();
                    secondaryMap.Add(secondaryKey, poolComponent);
                    _identifierComponentsMap.Add(primaryKey, secondaryMap);
                }
            }
            else
            {
                if (_componentsMap.ContainsKey(primaryKey))
                {
                    var sb = new StringBuilder();
                    sb.AppendLine($"Component with type {primaryKey.Name} already registered.");
                    sb.AppendLine($"Try to implement {nameof(IPoolComponentIdentifier)} interface for bind two or more components with same type");
                    throw new Exception(sb.ToString());
                }

                _componentsMap.Add(primaryKey, poolComponent);
            }
        }

        public T Resolve<T>(object id = null) where T : Component
        {
            var primaryKey = typeof(T);
            var secondaryKey = id;

            if (secondaryKey == null) return (T) _componentsMap[primaryKey];
            
            var secondaryMap = _identifierComponentsMap[primaryKey];
            return (T) secondaryMap[secondaryKey];
        }
    }
}