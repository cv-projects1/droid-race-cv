using System;

namespace vailshnast.pool
{
    public interface IPoolComponent
    {
        Type ComponentType { get; }
    }
    
    public interface IPoolComponentIdentifier
    {
        object ComponentId { get; }
    }
}