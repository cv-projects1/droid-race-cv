﻿using System;
using UnityEngine;

namespace vailshnast.pool
{
    public abstract class PoolBehaviour : MonoBehaviour , IPoolObject, IPoolComponent
    {
        public abstract Type ComponentType { get; }
        
        protected PoolObject PoolObject = null;

        public T ResolveComponent<T>() where T : Component => PoolObject.ResolveComponent<T>();
        public void Destroy() => PoolObject.Destroy();
        
        public virtual void PostAwake(PoolObject poolObject) => PoolObject = poolObject;
        public virtual void OnReuseObject(PoolObject poolObject) { /* do nothing */ }
        public virtual void OnDisposeObject(PoolObject poolObject) { /* do nothing */ }
    }
}
