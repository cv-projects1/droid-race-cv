using System;
using UnityEngine;

namespace vailshnast.pool
{
    [Serializable]
    public sealed class PoolObject
    {
        public Transform Transform { get; }
        public GameObject GameObject { get; }
        public int InstanceId { get; }
        public bool IsInsidePool { get; private set; }
        internal Transform PoolParent { get; }

        public event Action OnDestroyed;
        public event Action OnDestroyedOneTime;

        private readonly IPoolObject[] _poolObjectScripts;
        private readonly ComponentProvider _componentProvider;

        internal PoolObject(GameObject instance, Transform poolParent)
        {
            GameObject = instance;
            InstanceId = instance.GetInstanceID();
            Transform = instance.transform;
            
            _poolObjectScripts = instance.GetComponentsInChildren<IPoolObject>(true);
            _componentProvider = new ComponentProvider();

            IsInsidePool = true;
            PoolParent = poolParent;

            foreach (var poolObjectScript in _poolObjectScripts)
            {
                if (poolObjectScript is IPoolComponent poolComponent)
                    _componentProvider.Register(poolComponent);
            }
            
            foreach (var poolObjectScript in _poolObjectScripts)
                poolObjectScript.PostAwake(this);
        }
        
        internal void Initialize()
        {
            GameObject.SetActive(true);
            IsInsidePool = false;
            
            foreach (var iPoolObject in _poolObjectScripts)
                iPoolObject.OnReuseObject(this);
        }

        public T ResolveComponent<T>(object componentId = null) where  T: Component
        {
            return _componentProvider.Resolve<T>(componentId);
        }

        /// <summary>
        /// Return object back into pool.
        /// </summary>
        public void Destroy()
        {
            if (IsInsidePool) return;
            
            GameObject.SetActive(false);
            
            Transform.position = Vector3.zero;
            Transform.rotation = Quaternion.identity;
            Transform.SetParent(PoolParent);
            IsInsidePool = true;

            foreach (var poolObjectScript in _poolObjectScripts)
                poolObjectScript.OnDisposeObject(this);
            
            OnDestroyed?.Invoke();
            
            OnDestroyedOneTime?.Invoke();
            OnDestroyedOneTime = delegate { };
        }
    }
}