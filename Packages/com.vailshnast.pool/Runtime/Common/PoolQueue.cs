using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace vailshnast.pool
{
    internal sealed class PoolQueue
    {
        public Queue<PoolObject> PoolObjects => _poolObjects;
        private readonly Queue<PoolObject> _poolObjects = new Queue<PoolObject>();

        private readonly GameObject _prefab;
        private readonly Transform _poolParent;

        private readonly int _increaseSizeBy;
        private bool FlexibleSize => _increaseSizeBy > 0;

        private readonly DiContainer _diContainer;

        internal PoolQueue(GameObject prefab, int startSize, int increaseSizeBy, Transform poolRoot, DiContainer diContainer)
        {
            _diContainer = diContainer;

            _prefab = prefab;
            _increaseSizeBy = increaseSizeBy;

            _poolParent = new GameObject($"Pool_{prefab.name}").transform;
            _poolParent.parent = poolRoot;

            for (var i = 0; i < startSize; i++) _poolObjects.Enqueue(CreatePoolObject(_prefab));
        }

        internal PoolObject GetPoolObject()
        {
            var poolObject = _poolObjects.Dequeue();
            _poolObjects.Enqueue(poolObject);

            if (!poolObject.IsInsidePool && !FlexibleSize) poolObject.Destroy();
            else if (!poolObject.IsInsidePool && FlexibleSize)
            {
                poolObject = CreatePoolObject(_prefab);
                _poolObjects.Enqueue(poolObject);

                for (var i = 0; i < _increaseSizeBy - 1; i++)
                    _poolObjects.Enqueue(CreatePoolObject(_prefab));
            }

            poolObject.Initialize();

            return poolObject;
        }

        internal void DisposePool()
        {
            if (_poolObjects.Count == 0) return;

            while (_poolObjects.Count != 0)
                Object.Destroy(_poolObjects.Dequeue().GameObject);
        }

        private PoolObject CreatePoolObject(GameObject prefab)
        {
            var instance = _diContainer.InstantiatePrefab(prefab, _poolParent);
            instance.SetActive(false);

            var poolObject = new PoolObject(instance, _poolParent);
            return poolObject;
        }

        public void ReturnObjectsToPool()
        {
            foreach (var value in _poolObjects)
            {
                if (!value.IsInsidePool)
                    value.Destroy();
            }
        }
    }
}