using UnityEngine;

namespace vailshnast.pool
{
    public static class PoolManagerExtension
    {
        public static PoolObject InstantiateFromPool(this PoolService poolService, GameObject prefab,
            Vector3 position)
        {
            var poolObject = poolService.InstantiateFromPool(prefab);
            poolObject.Transform.position = position;
            return poolObject;
        }

        public static PoolObject InstantiateFromPool(this PoolService poolService, GameObject prefab,
            Vector3 position, Quaternion rotation)
        {
            var poolObject = poolService.InstantiateFromPool(prefab, position);
            poolObject.Transform.rotation = rotation;
            return poolObject;
        }

        public static PoolObject InstantiateFromPool(this PoolService poolService, GameObject prefab,
            Transform parent)
        {
            var poolObject = poolService.InstantiateFromPool(prefab);
            poolObject.Transform.parent = parent;
            return poolObject;
        }
    }
}