using System;
using UnityEngine;

namespace vailshnast.pool
{
    public interface IPoolService
    {
        /// <summary>
        /// Initializes Pool service
        /// </summary>
        void Initialize();
        
        /// <summary>
        /// Create pool using certain prefab.
        /// </summary>
        /// <param name="prefab">Pool prefab.</param>
        /// <param name="startSize">Initial pool size.</param>
        /// <param name="increaseSizeBy">Increase size if pool was ended on amount.</param>
        /// <exception cref="Exception">thrown exception if pool already exist.</exception>
        void CreatePool(GameObject prefab, int startSize, int increaseSizeBy = 0);

        /// <summary>
        /// Grabbing object from pool.
        /// </summary>
        /// <param name="prefab">Prefab was being used for creating pool.</param>
        /// <returns>Return container for pool object</returns>
        /// <exception cref="Exception">thrown exception if pool not exist.</exception>
        PoolObject InstantiateFromPool(GameObject prefab);

        /// <summary>
        /// Grabbing object from pool.
        /// </summary>
        /// <param name="instanceId"></param>
        /// <returns>Return container for pool object</returns>
        /// <exception cref="Exception">thrown exception if pool not exist.</exception>
        PoolObject InstantiateFromPool(int instanceId);

        /// <summary>
        /// Grabbing object from pool.
        /// </summary>
        /// <param name="instanceId"></param>
        /// <returns>Return pooled component</returns>
        /// <exception cref="Exception">thrown exception if pool not exist.</exception>
        T InstantiateFromPool<T>(int instanceId) where T : PoolBehaviour;

        /// <summary>
        /// Grabbing object from pool.
        /// </summary>
        /// <param name="prefab">Prefab was being used for creating pool.</param>
        /// <returns>Return pooled component</returns>
        /// <exception cref="Exception">thrown exception if pool not exist.</exception>
        T InstantiateFromPool<T>(GameObject prefab) where T : PoolBehaviour;

        /// <summary>
        /// Disposing pool by prefab.
        /// </summary>
        /// <param name="prefab">Prefab was being used for creating pool.</param>
        /// <exception cref="Exception">thrown exception if prefab doesnt exist.</exception>
        void DisposePool(GameObject prefab);

        /// <summary>
        /// Disposing pool by instanceId.
        /// </summary>
        /// <param name="instanceId">Instance ID of gameobject to dispose</param>
        /// <exception cref="Exception">thrown exception if prefab doesnt exist.</exception>
        void DisposePool(int instanceId);

        /// <summary>
        /// Returning All Objects To Corresponding pools
        /// </summary>
        void ReturnAllObjectsToPools();
    }
}