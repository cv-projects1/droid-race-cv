﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using vailshnast.serviceparent;
using Zenject;

namespace vailshnast.pool
{
    public sealed class PoolService :  IPoolService
    {
        private readonly Dictionary<int, PoolQueue> _poolMap = new Dictionary<int, PoolQueue>();
        private readonly DiContainer _diContainer;
        private readonly IServicesParent _servicesParent;
        private Transform _poolParent;

        public PoolService(DiContainer diContainer , IServicesParent servicesParent)
        {
            _diContainer = diContainer;
            _servicesParent = servicesParent;
        }

        public void Initialize() 
        {   
            _poolParent = new GameObject($"Pool Service").transform;
            _poolParent.SetParent(_servicesParent.Parent);
        }
        
        /// <summary>
        /// Create pool using certain prefab.
        /// </summary>
        /// <param name="prefab">Pool prefab.</param>
        /// <param name="startSize">Initial pool size.</param>
        /// <param name="increaseSizeBy">Increase size if pool was ended on amount.</param>
        /// <exception cref="Exception">thrown exception if pool already exist.</exception>
        public void CreatePool(GameObject prefab, int startSize, int increaseSizeBy = 0)
        {
            if (prefab == null) throw new ArgumentException($"Parameter {nameof(prefab)} cannot be null.");
            if (startSize <= 0) throw new ArgumentException($"Parameter {nameof(startSize)} should be greater then zero.");
            if (increaseSizeBy < 0) throw new ArgumentException($"Parameter {nameof(increaseSizeBy)} cannot be less then zero.");
            
            var poolKey = prefab.GetInstanceID();

            if (_poolMap.ContainsKey(poolKey))
                throw new Exception($"[PoolService] Pool {prefab.name} already exist.");

            var pool = new PoolQueue(prefab, startSize, increaseSizeBy, _poolParent, _diContainer);
            _poolMap.Add(poolKey, pool);
        }
        
        /// <summary>
        /// Grabbing object from pool.
        /// </summary>
        /// <param name="prefab">Prefab was being used for creating pool.</param>
        /// <returns>Return container for pool object</returns>
        /// <exception cref="Exception">thrown exception if pool not exist.</exception>
        public PoolObject InstantiateFromPool(GameObject prefab)
        {
            if (prefab == null) throw new ArgumentException($"Parameter {nameof(prefab)} cannot be null.");

            var poolKey = prefab.GetInstanceID();

            if (!_poolMap.ContainsKey(poolKey))
                throw new Exception($"[PoolService] Pool {prefab.name} not found.");

            return _poolMap[poolKey].GetPoolObject();
        }

        /// <summary>
        /// Grabbing object from pool.
        /// </summary>
        /// <param name="instanceId"></param>
        /// <returns>Return container for pool object</returns>
        /// <exception cref="Exception">thrown exception if pool not exist.</exception>
        public PoolObject InstantiateFromPool(int instanceId)
        {
            if (!_poolMap.ContainsKey(instanceId))
                throw new Exception($"[PoolService] Pool id: {instanceId} not found.");

            return _poolMap[instanceId].GetPoolObject();
        }

        /// <summary>
        /// Grabbing object from pool.
        /// </summary>
        /// <param name="instanceId"></param>
        /// <returns>Return pooled component</returns>
        /// <exception cref="Exception">thrown exception if pool not exist.</exception>
        public T InstantiateFromPool<T>(int instanceId) where T : PoolBehaviour
        {
            var poolObject = InstantiateFromPool(instanceId);
            return poolObject.ResolveComponent<T>();
        }

        /// <summary>
        /// Grabbing object from pool.
        /// </summary>
        /// <param name="prefab">Prefab was being used for creating pool.</param>
        /// <returns>Return pooled component</returns>
        /// <exception cref="Exception">thrown exception if pool not exist.</exception>
        public T InstantiateFromPool<T>(GameObject prefab) where T : PoolBehaviour
        {
            var poolObject = InstantiateFromPool(prefab);
            return poolObject.ResolveComponent<T>();
        }
        
        /// <summary>
        /// Disposing pool by prefab.
        /// </summary>
        /// <param name="prefab">Prefab was being used for creating pool.</param>
        /// <exception cref="Exception">thrown exception if prefab doesnt exist.</exception>
        public void DisposePool(GameObject prefab)
        {
            var poolKey = prefab.GetInstanceID();

            if (!_poolMap.ContainsKey(poolKey))
                throw new Exception($"[PoolService] Pool {prefab.name} not found.");

            _poolMap[poolKey].DisposePool();
            _poolMap.Remove(poolKey);
        }

        /// <summary>
        /// Disposing pool by instanceId.
        /// </summary>
        /// <param name="instanceId">Instance ID of gameobject to dispose</param>
        /// <exception cref="Exception">thrown exception if prefab doesnt exist.</exception>
        public void DisposePool(int instanceId)
        {
            var poolKey = instanceId;

            if (!_poolMap.ContainsKey(poolKey))
                throw new Exception($"[PoolService] Pool {instanceId} not found.");

            _poolMap[poolKey].DisposePool();
            _poolMap.Remove(poolKey);
        }
        
        /// <summary>
        /// Returning All Objects To Corresponding pools
        /// </summary>
        public void ReturnAllObjectsToPools()
        {
            foreach (var pool in _poolMap.Values) pool.ReturnObjectsToPool();
        }
    }
}