using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector.Editor;
using TNRD.CodeGeneration;
using UnityEditor;
using UnityEngine;

namespace vailshnast.pooledfactories.editor
{
    [CustomEditor(typeof(BasePooledFactoryConfig), true)]
#if ODIN_INSPECTOR_3
    public class BaseFactoryConfigEditor : OdinEditor
#else
    public class BaseFactoryConfigEditor : Editor
#endif
    {
        private BasePooledFactoryConfig _baseFactoryConfig;
#if ODIN_INSPECTOR
        protected override void OnEnable()
        {
            base.OnEnable();
            _baseFactoryConfig = (BasePooledFactoryConfig) target;
        }
#else
        public void OnEnable()
        {
            _baseFactoryConfig = (BasePooledFactoryConfig) target;
        }
#endif

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button($"Generate constants"))
            {
                GenerateConstants();
                EditorUtility.SetDirty(target);
                AssetDatabase.SaveAssets();
            }
        }

        private void GenerateConstants()
        {
            var generator = new Generator(_baseFactoryConfig.ConstantsNamespace);
            var tagsClass = new Class(_baseFactoryConfig.ConstantsClassName) {IsStatic = true};

            var query = _baseFactoryConfig.PoolSettings.Select(x => x.Prefab.name)
                                          .GroupBy(x => x)
                                          .Where(g => g.Count() > 1)
                                          .Select(y => y.Key)
                                          .ToList();

            if (query.Count > 0)
            {
                Debug.LogError($"There are prefabs with duplicate names, abandon constants generating");
                return;
            }

            foreach (var prefab in _baseFactoryConfig.PoolSettings)
            {
                var field = new Field(prefab.Prefab.name,
                                      prefab.Prefab.name,
                                      typeof(string)) {IsConst = true};

                tagsClass.AddField(field);
            }

            var path = $"{_baseFactoryConfig.ConstantsFolderPath}/{_baseFactoryConfig.ConstantsClassName}.cs";
            path = path.Replace("Assets", "");
            path = string.Concat(Application.dataPath, path);
            generator.AddClass(tagsClass);
            generator.SaveToFile(path);
            AssetDatabase.Refresh();
        }
    }
}