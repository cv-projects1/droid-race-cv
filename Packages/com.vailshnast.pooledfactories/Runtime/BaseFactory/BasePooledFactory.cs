using System.Collections.Generic;
using vailshnast.pool;

namespace vailshnast.pooledfactories
{
    public abstract class BasePooledFactory : IBasePooledFactory
    {
        private readonly Dictionary<string, int> _keyMap;
        private readonly IPoolService _poolService;

        protected BasePooledFactory(IPoolService poolService)
        {
            _poolService = poolService;
            _keyMap = new Dictionary<string, int>();
        }

        public void Initialize()
        {
            var poolSettingsList = GetPoolSettingsList();
            foreach (var poolSettings in poolSettingsList)
            {
                var pooledGameObject = poolSettings.Prefab.gameObject;
                
                _keyMap.Add(pooledGameObject.name, 
                            pooledGameObject.gameObject.GetInstanceID());
                
                _poolService.CreatePool(pooledGameObject,
                                        poolSettings.StartPoolSize,
                                        poolSettings.IncreaseBy);
            } 
        }

        public abstract List<PoolSettings> GetPoolSettingsList();
        
        public T CreatePoolObject<T>(string key) where T : PoolBehaviour
        {
            _keyMap.TryGetValue(key, out var instanceId);
            return _poolService.InstantiateFromPool<T>(instanceId);
        }
    }
}