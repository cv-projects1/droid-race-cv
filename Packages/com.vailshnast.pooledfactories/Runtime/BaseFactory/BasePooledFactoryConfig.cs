using System;
using System.Collections.Generic;
using UnityEngine;
using vailshnast.installers;
using vailshnast.pool;

namespace vailshnast.pooledfactories
{
    public abstract class BasePooledFactoryConfig : ScriptableObject , 
                                                    IScriptableService, 
                                                    IBasePooledFactoryConfig
    {
        public abstract Type ServiceType { get; }
        
        [SerializeField] private List<PoolSettings> poolSettings = new List<PoolSettings>();
        public List<PoolSettings> PoolSettings => poolSettings;
#if UNITY_EDITOR
        public string ConstantsClassName;
        public string ConstantsNamespace;
#if ODIN_INSPECTOR_3
        [Sirenix.OdinInspector.FolderPath]
#endif
        public string ConstantsFolderPath;
#endif
    }
}