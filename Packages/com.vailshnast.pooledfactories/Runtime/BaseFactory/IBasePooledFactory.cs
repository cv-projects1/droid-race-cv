using System.Collections.Generic;
using vailshnast.pool;

namespace vailshnast.pooledfactories
{
    public interface IBasePooledFactory
    {
        void Initialize();
        T CreatePoolObject<T>(string key) where T : PoolBehaviour;
    }
}