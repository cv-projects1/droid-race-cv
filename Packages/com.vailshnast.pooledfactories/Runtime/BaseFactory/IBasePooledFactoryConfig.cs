using System.Collections.Generic;

namespace vailshnast.pooledfactories
{
    public interface IBasePooledFactoryConfig
    {
        List<PoolSettings> PoolSettings { get; }
    }
}