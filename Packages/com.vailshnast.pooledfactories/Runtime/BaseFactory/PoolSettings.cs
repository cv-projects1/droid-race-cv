using System;
using vailshnast.pool;

namespace vailshnast.pooledfactories
{
    [Serializable]
    public class PoolSettings
    {
        public PoolBehaviour Prefab;
        public int StartPoolSize;
        public int IncreaseBy;
    }
}