using System;
using Entitas;
using UnityEngine;
using vailshnast.entitasview;
using vailshnast.pool;

namespace vailshnast.pooledfactories
{
    public class PooledEntityObject : PoolBehaviour
    {
        [SerializeField] private EntityView entityEntityView;
        public override Type ComponentType => typeof(PooledEntityObject);
        public EntityView EntityView => entityEntityView;

        public override void PostAwake(PoolObject poolObject)
        {
            base.PostAwake(poolObject);
            EntityView.Initialize();
        }
    }
}