using System;

namespace vailshnast.postprocessbuild
{
    [Serializable]
    public class AssemblyJson
    {
        public string name;
        public string rootNamespace;

        public string[] references;
    }
}