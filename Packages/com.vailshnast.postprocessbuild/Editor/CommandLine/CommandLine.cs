using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using UnityEngine;

namespace vailshnast.postprocessbuild
{
    public static class CommandLine
    {
        private static bool displayedConsoleConfigurationWarning;

        public static void RunAsync(
            string toolPath,
            string arguments,
            CommandLine.CompletionHandler completionDelegate,
            string workingDirectory = null,
            Dictionary<string, string> envVars = null,
            CommandLine.IOHandler ioHandler = null)
        {
            Action job = (Action) (() =>
            {
                CommandLine.Result result = CommandLine.Run(toolPath, arguments, workingDirectory, envVars, ioHandler);
                RunOnMainThread.Run((Action) (() => completionDelegate(result)));
            });

            if (ExecutionEnvironment.InBatchMode)
                RunOnMainThread.Run(job);
            else
                new Thread(new ThreadStart(job.Invoke)).Start();
        }

        public static CommandLine.Result Run(
            string toolPath,
            string arguments,
            string workingDirectory = null,
            Dictionary<string, string> envVars = null,
            CommandLine.IOHandler ioHandler = null)
        {
            return CommandLine.RunViaShell(toolPath, arguments, workingDirectory, envVars, ioHandler);
        }

        public static CommandLine.Result RunViaShell(
            string toolPath,
            string arguments,
            string workingDirectory = null,
            Dictionary<string, string> envVars = null,
            CommandLine.IOHandler ioHandler = null,
            bool useShellExecution = false,
            bool stdoutRedirectionInShellMode = true)
        {
            bool flag = false;
            Encoding inputEncoding = Console.InputEncoding;
            Encoding outputEncoding = Console.OutputEncoding;

            try
            {
                System.Type type = Encoding.UTF8.GetType();

                if (inputEncoding.GetType() != type)
                    Console.InputEncoding = Encoding.UTF8;

                if (outputEncoding.GetType() != type)
                    Console.OutputEncoding = Encoding.UTF8;
            }
            catch (Exception ex)
            {
                if (!CommandLine.displayedConsoleConfigurationWarning)
                {
                    UnityEngine.Debug.LogWarning((object) string.Format(
                                                     "Unable to set console input / output encoding from {0} & {1} to {2} (e.g en_US.UTF8-8). Some commands may fail. {3}",
                                                     (object) inputEncoding, (object) outputEncoding,
                                                     (object) Encoding.UTF8, (object) ex));

                    flag = true;
                }
            }

            if (Application.platform == RuntimePlatform.WindowsEditor && toolPath.Contains("'"))
            {
                useShellExecution = true;
                stdoutRedirectionInShellMode = true;
            }
            else if (!toolPath.StartsWith("\"") && !toolPath.StartsWith("'"))
                toolPath = FileUtils.NormalizePathSeparators(toolPath);

            string path1 = (string) null;
            string path2 = (string) null;

            if (useShellExecution && stdoutRedirectionInShellMode)
            {
                path1 = Path.GetTempFileName();
                path2 = Path.GetTempFileName();
                string str1 = toolPath;
                string str2;
                string str3;
                string str4;

                if (Application.platform == RuntimePlatform.WindowsEditor)
                {
                    str2 = "cmd.exe";
                    str3 = "/c \"";
                    str4 = "\"";
                }
                else
                {
                    str2 = "bash";
                    str3 = "-l -c '";
                    str4 = "'";
                    str1 = toolPath.Replace("'", "'\\''");
                }

                arguments = string.Format("{0}\"{1}\" {2} 1> {3} 2> {4}{5}", (object) str3, (object) str1,
                                          (object) arguments, (object) path1, (object) path2, (object) str4);

                toolPath = str2;
            }

            Process process = new Process();
            process.StartInfo.UseShellExecute = useShellExecution;
            process.StartInfo.Arguments = arguments;

            if (useShellExecution)
            {
                process.StartInfo.CreateNoWindow = false;
                process.StartInfo.RedirectStandardOutput = false;
                process.StartInfo.RedirectStandardError = false;
            }
            else
            {
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;

                if (envVars != null)
                {
                    foreach (KeyValuePair<string, string> envVar in envVars)
                        process.StartInfo.EnvironmentVariables[envVar.Key] = envVar.Value;
                }
            }

            process.StartInfo.RedirectStandardInput = !useShellExecution && ioHandler != null;
            process.StartInfo.FileName = toolPath;
            process.StartInfo.WorkingDirectory = workingDirectory ?? Environment.CurrentDirectory;
            process.Start();

            if (ioHandler != null)
                ioHandler(process, process.StandardInput, CommandLine.StreamData.Empty);

            List<string>[] stdouterr = new List<string>[2]
            {
                new List<string>(),
                new List<string>()
            };

            if (useShellExecution)
            {
                process.WaitForExit();

                if (stdoutRedirectionInShellMode)
                {
                    stdouterr[0].Add(File.ReadAllText(path1));
                    stdouterr[1].Add(File.ReadAllText(path2));
                    File.Delete(path1);
                    File.Delete(path2);
                }
            }
            else
            {
                AutoResetEvent complete = new AutoResetEvent(false);

                CommandLine.AsyncStreamReader[] fromStreams = CommandLine.AsyncStreamReader.CreateFromStreams(
                    new Stream[2]
                    {
                        process.StandardOutput.BaseStream,
                        process.StandardError.BaseStream
                    }, 1);

                CommandLine.AsyncStreamReaderMultiplexer readerMultiplexer =
                    new CommandLine.AsyncStreamReaderMultiplexer(fromStreams,
                                                                 (CommandLine.AsyncStreamReader.Handler) (data =>
                                                                 {
                                                                     stdouterr[data.handle].Add(data.text);

                                                                     if (ioHandler == null)
                                                                         return;

                                                                     ioHandler(process, process.StandardInput, data);
                                                                 }),
                                                                 (CommandLine.AsyncStreamReaderMultiplexer.
                                                                     CompletionHandler) (() => complete.Set()));

                foreach (CommandLine.AsyncStreamReader asyncStreamReader in fromStreams)
                    asyncStreamReader.Start();

                process.WaitForExit();
                complete.WaitOne();
            }

            CommandLine.Result result = new CommandLine.Result()
            {
                stdout = string.Join(string.Empty, stdouterr[0].ToArray()),
                stderr = string.Join(string.Empty, stdouterr[1].ToArray()),
                exitCode = process.ExitCode
            };

            result.message =
                CommandLine.FormatResultMessage(toolPath, arguments, result.stdout, result.stderr, result.exitCode);

            try
            {
                if (Console.InputEncoding != inputEncoding)
                    Console.InputEncoding = inputEncoding;

                if (Console.OutputEncoding != outputEncoding)
                    Console.OutputEncoding = outputEncoding;
            }
            catch (Exception ex)
            {
                if (!CommandLine.displayedConsoleConfigurationWarning)
                {
                    UnityEngine.Debug.LogWarning((object) string.Format(
                                                     "Unable to restore console input / output  encoding to {0} & {1}. {2}",
                                                     (object) inputEncoding, (object) outputEncoding, (object) ex));

                    flag = true;
                }
            }

            if (flag)
                CommandLine.displayedConsoleConfigurationWarning = true;

            return result;
        }

        public static string[] SplitLines(string multilineString) => Regex.Split(multilineString, "\r\n|\r|\n");

        private static string FormatResultMessage(
            string toolPath,
            string arguments,
            string stdout,
            string stderr,
            int exitCode)
        {
            return string.Format("{0} '{1} {2}'\nstdout:\n{3}\nstderr:\n{4}\nexit code: {5}\n",
                                 exitCode != 0 ? (object) "Failed to run" : (object) "Successfully executed",
                                 (object) toolPath, (object) arguments, (object) stdout, (object) stderr,
                                 (object) exitCode);
        }

        public static string GetExecutableExtension() =>
            Application.platform == RuntimePlatform.WindowsEditor ? ".exe" : string.Empty;

        public static string FindExecutable(string executable)
        {
            string toolPath = Application.platform != RuntimePlatform.WindowsEditor ? "which" : "where";

            try
            {
                CommandLine.Result result = CommandLine.Run(toolPath, executable, Environment.CurrentDirectory);

                if (result.exitCode == 0)
                    return CommandLine.SplitLines(result.stdout)[0];
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.Log((object) ("'" + toolPath +
                                                "' command is not on path.  Unable to find executable '" + executable +
                                                "' (" + ex.ToString() + ")"));
            }

            return (string) null;
        }

        public class Result
        {
            public string stdout;
            public string stderr;
            public int exitCode;
            public string message;
        }

        public delegate void CompletionHandler(CommandLine.Result result);

        public class StreamData
        {
            public int handle;
            public string text = string.Empty;
            public byte[] data;
            public bool end;

            public StreamData(int handle, string text, byte[] data, bool end)
            {
                this.handle = handle;
                this.text = text;
                this.data = data;
                this.end = end;
            }

            public static CommandLine.StreamData Empty =>
                new CommandLine.StreamData(0, string.Empty, (byte[]) null, false);
        }

        public delegate void IOHandler(
            Process process,
            StreamWriter stdin,
            CommandLine.StreamData streamData);

        private class AsyncStreamReader
        {
            private AutoResetEvent readEvent;
            private int handle;
            private Stream stream;
            private byte[] buffer;
            private volatile bool complete;

            public AsyncStreamReader(int handle, Stream stream, int bufferSize)
            {
                this.readEvent = new AutoResetEvent(false);
                this.handle = handle;
                this.stream = stream;
                this.buffer = new byte[bufferSize];
            }

            public event CommandLine.AsyncStreamReader.Handler DataReceived;
            public int Handle => this.handle;

            public void Start()
            {
                if (this.complete)
                    return;

                new Thread(new ThreadStart(this.Read)).Start();
            }

            private void Read()
            {
                while (!this.complete)
                {
                    this.stream.BeginRead(this.buffer, 0, this.buffer.Length, (AsyncCallback) (asyncResult =>
                    {
                        int length = this.stream.EndRead(asyncResult);

                        if (!this.complete)
                        {
                            this.complete = length == 0;

                            if (this.DataReceived != null)
                            {
                                byte[] numArray = new byte[length];
                                Array.Copy((Array) this.buffer, (Array) numArray, numArray.Length);

                                this.DataReceived(new CommandLine.StreamData(
                                                      this.handle, Encoding.UTF8.GetString(numArray), numArray,
                                                      this.complete));
                            }
                        }

                        this.readEvent.Set();
                    }), (object) null);

                    this.readEvent.WaitOne();
                }
            }

            public static CommandLine.AsyncStreamReader[] CreateFromStreams(
                Stream[] streams,
                int bufferSize)
            {
                CommandLine.AsyncStreamReader[] asyncStreamReaderArray =
                    new CommandLine.AsyncStreamReader[streams.Length];

                for (int handle = 0; handle < streams.Length; ++handle)
                    asyncStreamReaderArray[handle] =
                        new CommandLine.AsyncStreamReader(handle, streams[handle], bufferSize);

                return asyncStreamReaderArray;
            }

            public delegate void Handler(CommandLine.StreamData streamData);
        }

        private class AsyncStreamReaderMultiplexer
        {
            private AutoResetEvent queuedItem;
            private Queue queue;
            private HashSet<int> activeStreams;

            public AsyncStreamReaderMultiplexer(
                CommandLine.AsyncStreamReader[] readers,
                CommandLine.AsyncStreamReader.Handler handler,
                CommandLine.AsyncStreamReaderMultiplexer.CompletionHandler complete = null)
            {
                this.queuedItem = new AutoResetEvent(false);
                this.queue = Queue.Synchronized(new Queue());
                this.activeStreams = new HashSet<int>();

                foreach (CommandLine.AsyncStreamReader reader in readers)
                {
                    reader.DataReceived += new CommandLine.AsyncStreamReader.Handler(this.HandleRead);
                    this.activeStreams.Add(reader.Handle);
                }

                this.DataReceived += handler;

                if (complete != null)
                    this.Complete += complete;

                new Thread(new ThreadStart(this.PollQueue)).Start();
            }

            public event CommandLine.AsyncStreamReaderMultiplexer.CompletionHandler Complete;
            public event CommandLine.AsyncStreamReader.Handler DataReceived;

            public void Shutdown()
            {
                lock ((object) this.activeStreams)
                    this.activeStreams.Clear();

                this.queuedItem.Set();
            }

            private void HandleRead(CommandLine.StreamData streamData)
            {
                this.queue.Enqueue((object) streamData);
                this.queuedItem.Set();
            }

            private void PollQueue()
            {
                while (this.activeStreams.Count > 0)
                {
                    this.queuedItem.WaitOne();

                    while (this.queue.Count > 0)
                    {
                        CommandLine.StreamData streamData = (CommandLine.StreamData) this.queue.Dequeue();

                        if (streamData.end)
                        {
                            lock ((object) this.activeStreams)
                                this.activeStreams.Remove(streamData.handle);
                        }

                        if (this.DataReceived != null)
                            this.DataReceived(streamData);
                    }
                }

                if (this.Complete == null)
                    return;

                this.Complete();
            }

            public delegate void CompletionHandler();
        }

        public class LineReader
        {
            private Dictionary<int, List<CommandLine.StreamData>> streamDataByHandle =
                new Dictionary<int, List<CommandLine.StreamData>>();

            public LineReader(CommandLine.IOHandler handler = null)
            {
                if (handler == null)
                    return;

                this.LineHandler += handler;
            }

            public event CommandLine.IOHandler LineHandler;
            public event CommandLine.IOHandler DataHandler;

            public List<CommandLine.StreamData> GetBufferedData(int handle)
            {
                List<CommandLine.StreamData> streamDataList;

                return this.streamDataByHandle.TryGetValue(handle, out streamDataList)
                    ? new List<CommandLine.StreamData>((IEnumerable<CommandLine.StreamData>) streamDataList)
                    : new List<CommandLine.StreamData>();
            }

            public void Flush()
            {
                foreach (List<CommandLine.StreamData> streamDataList in this.streamDataByHandle.Values)
                    streamDataList.Clear();
            }

            public static CommandLine.StreamData Aggregate(
                List<CommandLine.StreamData> dataStream)
            {
                List<string> stringList = new List<string>();
                int length = 0;
                int handle = 0;
                bool end = false;

                foreach (CommandLine.StreamData streamData in dataStream)
                {
                    stringList.Add(streamData.text);
                    length += streamData.data.Length;
                    handle = streamData.handle;
                    end |= streamData.end;
                }

                string text = string.Join(string.Empty, stringList.ToArray());
                byte[] data = new byte[length];
                int destinationIndex = 0;

                foreach (CommandLine.StreamData streamData in dataStream)
                {
                    Array.Copy((Array) streamData.data, 0, (Array) data, destinationIndex, streamData.data.Length);
                    destinationIndex += streamData.data.Length;
                }

                return new CommandLine.StreamData(handle, text, data, end);
            }

            public void AggregateLine(Process process, StreamWriter stdin, CommandLine.StreamData data)
            {
                if (this.DataHandler != null)
                    this.DataHandler(process, stdin, data);

                bool flag1 = false;

                if (data.data != null)
                {
                    data.text = data.text.Replace("\r\n", "\n").Replace("\r", "\n");
                    List<CommandLine.StreamData> streamDataList = this.GetBufferedData(data.handle);
                    streamDataList.Add(data);
                    bool flag2 = false;

                    while (!flag2)
                    {
                        List<CommandLine.StreamData> dataStream = new List<CommandLine.StreamData>();
                        int num1 = 0;
                        int count = streamDataList.Count;
                        flag2 = true;

                        foreach (CommandLine.StreamData streamData1 in streamDataList)
                        {
                            bool flag3 = data.end && ++num1 == count;
                            dataStream.Add(streamData1);
                            int num2 = streamData1.text.Length;

                            if (!flag3)
                            {
                                num2 = streamData1.text.IndexOf("\n");

                                if (num2 >= 0)
                                    dataStream.Remove(streamData1);
                                else
                                    continue;
                            }

                            CommandLine.StreamData streamData2 = CommandLine.LineReader.Aggregate(dataStream);
                            dataStream.Clear();

                            if (!flag3)
                            {
                                streamData2.text += streamData1.text.Substring(0, num2 + 1);

                                dataStream.Add(new CommandLine.StreamData(
                                                   data.handle, streamData1.text.Substring(num2 + 1), streamData1.data,
                                                   false));

                                flag2 = false;
                            }

                            if (this.LineHandler != null)
                                this.LineHandler(process, stdin, streamData2);

                            flag1 = true;
                        }

                        streamDataList = dataStream;
                    }

                    this.streamDataByHandle[data.handle] = streamDataList;
                }

                if (flag1 || this.LineHandler == null)
                    return;

                this.LineHandler(process, stdin, CommandLine.StreamData.Empty);
            }
        }
    }
}