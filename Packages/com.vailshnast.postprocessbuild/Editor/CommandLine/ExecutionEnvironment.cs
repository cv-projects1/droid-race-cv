using System;
using System.Globalization;
using UnityEngine;

namespace vailshnast.postprocessbuild
{
    public class ExecutionEnvironment
    {
        private const float DEFAULT_UNITY_VERSION_MAJOR_MINOR = 5.4f;
        private static float unityVersionMajorMinor = -1f;

        public static bool InBatchMode => Environment.CommandLine.ToLower().Contains("-batchmode");

        public static bool ExecuteMethodEnabled => Environment.CommandLine.ToLower().Contains("-executemethod");

        internal static bool InteractiveMode => (Environment.CommandLine.ToLower().Contains("-gvh_noninteractive") ? 1 : (ExecutionEnvironment.InBatchMode ? 1 : 0)) == 0;

        public static float VersionMajorMinor
        {
            get
            {
                if ((double) ExecutionEnvironment.unityVersionMajorMinor >= 0.0)
                    return ExecutionEnvironment.unityVersionMajorMinor;
                float result = 5.4f;
                string unityVersion = Application.unityVersion;
                if (!string.IsNullOrEmpty(unityVersion))
                {
                    int num = unityVersion.IndexOf('.');
                    if (num > 0 && unityVersion.Length > num + 1 && !float.TryParse(unityVersion.Substring(0, num + 2), NumberStyles.Any, (IFormatProvider) CultureInfo.InvariantCulture, out result))
                        result = 5.4f;
                }
                ExecutionEnvironment.unityVersionMajorMinor = result;
                return result;
            }
        }
    }
}