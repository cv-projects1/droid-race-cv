using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using Logger = UnityEngine.Logger;

namespace vailshnast.postprocessbuild
{
    public class FileUtils
    {
        internal const string META_EXTENSION = ".meta";
        public static readonly string ASSETS_FOLDER = "Assets";
        private static readonly string PACKAGES_FOLDER = "Packages";
        private static Regex PACKAGES_ASSETDB_PATH_REGEX = new Regex("^(Packages[/\\\\][^/\\\\]+)[/\\\\](.*)?$");

        private static Regex PACKAGES_PHYSICAL_PATH_REGEX =
            new Regex("^(Library[/\\\\]PackageCache[/\\\\])([^/\\\\]+)(@[^/\\\\]+)[/\\\\](.*)?$");

        public static string ProjectDirectory => Directory.GetParent(Path.GetFullPath(Application.dataPath)).FullName;
        public static string ProjectTemporaryDirectory => Path.Combine(FileUtils.ProjectDirectory, "Temp");

        public static string FormatError(string summary, List<string> errors) => errors.Count > 0
            ? string.Format("{0}\n{1}", (object) summary, (object) string.Format("\n", (object[]) errors.ToArray()))
            : string.Empty;

        public static List<string> DeleteExistingFileOrDirectory(string path, bool includeMetaFiles = true)
        {
            List<string> stringList = new List<string>();

            if (includeMetaFiles)
            {
                if (!path.EndsWith(".meta"))
                    stringList.AddRange((IEnumerable<string>) FileUtils.DeleteExistingFileOrDirectory(path + ".meta"));
            }

            try
            {
                if (Directory.Exists(path))
                {
                    if (!FileUtil.DeleteFileOrDirectory(path))
                    {
                        new DirectoryInfo(path).Attributes &= ~FileAttributes.ReadOnly;

                        foreach (string fileSystemEntry in Directory.GetFileSystemEntries(path))
                            stringList.AddRange(
                                (IEnumerable<string>) FileUtils.DeleteExistingFileOrDirectory(
                                    fileSystemEntry, includeMetaFiles));

                        Directory.Delete(path);
                    }
                }
                else if (File.Exists(path))
                {
                    if (!FileUtil.DeleteFileOrDirectory(path))
                    {
                        File.SetAttributes(path, File.GetAttributes(path) & ~FileAttributes.ReadOnly);
                        File.Delete(path);
                    }
                }
            }
            catch (Exception ex)
            {
                stringList.Add(string.Format("{0} ({1})", (object) path, (object) ex));
            }

            return stringList;
        }

        public static void CopyDirectory(string sourceDir, string targetDir)
        {
            Func<string, string> func =
                (Func<string, string>) (path => Path.Combine(targetDir, path.Substring(sourceDir.Length + 1)));

            foreach (string directory in Directory.GetDirectories(sourceDir, "*", SearchOption.AllDirectories))
                Directory.CreateDirectory(func(directory));

            foreach (string file in Directory.GetFiles(sourceDir, "*", SearchOption.AllDirectories))
            {
                if (!file.EndsWith(".meta"))
                    File.Copy(file, func(file));
            }
        }

        public static string CreateTemporaryDirectory(bool useSystemTempPath = false, int retry = 100)
        {
            string path1 = !useSystemTempPath ? FileUtils.ProjectTemporaryDirectory : Path.GetTempPath();

            while (retry-- > 0)
            {
                string path = Path.Combine(path1, Path.GetRandomFileName());

                if (!File.Exists(path))
                {
                    Directory.CreateDirectory(path);

                    return path;
                }
            }

            return (string) null;
        }

        public static void MoveDirectory(string sourceDir, string targetDir)
        {
            sourceDir = Path.GetFullPath(sourceDir);
            targetDir = Path.GetFullPath(targetDir);
            FileUtils.DeleteExistingFileOrDirectory(targetDir);

            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                Directory.CreateDirectory(targetDir);
                FileUtils.CopyDirectory(sourceDir, targetDir);
                FileUtils.DeleteExistingFileOrDirectory(sourceDir);
            }
            else
                Directory.Move(sourceDir, targetDir);
        }

        public static void MoveFile(string sourceFile, string targetFile)
        {
            sourceFile = Path.GetFullPath(sourceFile);
            targetFile = Path.GetFullPath(targetFile);
            FileUtils.DeleteExistingFileOrDirectory(targetFile);

            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                File.Copy(sourceFile, targetFile);
                FileUtils.DeleteExistingFileOrDirectory(sourceFile);
            }
            else
                File.Move(sourceFile, targetFile);
        }

        public static string NormalizePathSeparators(string path) =>
            path?.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);

        public static string PosixPathSeparators(string path) => path?.Replace("\\", "/");

        public static string FindPathUnderDirectory(string directory, string pathToFind)
        {
            directory = FileUtils.NormalizePathSeparators(directory);

            if (directory.EndsWith(Path.DirectorySeparatorChar.ToString()))
                directory = directory.Substring(0, directory.Length - 1);

            List<string> stringList = new List<string>();

            foreach (string directory1 in Directory.GetDirectories(directory, "*", SearchOption.AllDirectories))
            {
                string str = FileUtils.NormalizePathSeparators(directory1.Substring(directory.Length + 1));

                if (str.EndsWith(Path.DirectorySeparatorChar.ToString() + pathToFind) || str.Contains(
                    Path.DirectorySeparatorChar.ToString() + pathToFind + (object) Path.DirectorySeparatorChar))
                    stringList.Add(str);
            }

            if (stringList.Count == 0)
                return (string) null;

            stringList.Sort((Comparison<string>) ((lhs, rhs) => lhs.Length - rhs.Length));

            return stringList[0];
        }

        public static string[] SplitPathIntoComponents(string path) =>
            FileUtils.NormalizePathSeparators(path).Split(Path.DirectorySeparatorChar);

        public static string FindDirectoryByCaseInsensitivePath(string pathToFind)
        {
            string path1 = ".";
            string[] strArray = FileUtils.SplitPathIntoComponents(pathToFind);

            for (int index = 0; index < strArray.Length && path1 != null; ++index)
            {
                string path2 = path1;
                string strA = strArray[index];
                string lowerInvariant = strArray[index].ToLowerInvariant();
                path1 = (string) null;
                List<KeyValuePair<int, string>> keyValuePairList = new List<KeyValuePair<int, string>>();

                foreach (string directory in Directory.GetDirectories(path2))
                {
                    string fileName = Path.GetFileName(directory);

                    if (fileName.ToLowerInvariant() == lowerInvariant)
                    {
                        keyValuePairList.Add(new KeyValuePair<int, string>(
                                                 Math.Abs(string.CompareOrdinal(strA, fileName)),
                                                 index != 0 ? directory : Path.GetFileName(directory)));

                        break;
                    }
                }

                if (keyValuePairList.Count != 0)
                {
                    keyValuePairList.Sort((Comparison<KeyValuePair<int, string>>) ((lhs, rhs) => lhs.Key - rhs.Key));
                    path1 = keyValuePairList[0].Value;
                }
                else
                    break;
            }

            return FileUtils.NormalizePathSeparators(path1);
        }
        
        public static bool IsUnderDirectory(string path, string directory)
        {
            if (string.IsNullOrEmpty(directory))
                return false;

            if (!directory.EndsWith(Path.DirectorySeparatorChar.ToString()))
                directory += (string) (object) Path.DirectorySeparatorChar;

            return FileUtils.NormalizePathSeparators(path.ToLower()).StartsWith(directory.ToLower());
        }

        public static FileUtils.PackageDirectoryType GetPackageDirectoryType(string path)
        {
            if (FileUtils.PACKAGES_ASSETDB_PATH_REGEX.IsMatch(path))
                return FileUtils.PackageDirectoryType.AssetDatabasePath;

            return FileUtils.PACKAGES_PHYSICAL_PATH_REGEX.IsMatch(path)
                ? FileUtils.PackageDirectoryType.PhysicalPath
                : FileUtils.PackageDirectoryType.None;
        }

        public static bool IsUnderPackageDirectory(string path) =>
            FileUtils.GetPackageDirectoryType(path) != FileUtils.PackageDirectoryType.None;

        public static string GetPackageDirectory(
            string path,
            FileUtils.PackageDirectoryType directoryType = FileUtils.PackageDirectoryType.None)
        {
            Match match1 = FileUtils.PACKAGES_ASSETDB_PATH_REGEX.Match(path);
            string path1;

            if (match1.Success)
            {
                path1 = match1.Groups[1].Value;

                if (directoryType == FileUtils.PackageDirectoryType.PhysicalPath)
                    path1 = Path.GetFullPath(path1).Substring(FileUtils.ProjectDirectory.Length + 1);
            }
            else
            {
                Match match2 = FileUtils.PACKAGES_PHYSICAL_PATH_REGEX.Match(path);

                path1 = !match2.Success
                    ? string.Empty
                    : (directoryType != FileUtils.PackageDirectoryType.AssetDatabasePath
                        ? match2.Groups[1].Value + match2.Groups[2].Value + match2.Groups[3].Value
                        : Path.Combine(FileUtils.PACKAGES_FOLDER, match2.Groups[2].Value));
            }

            return path1;
        }

        public static bool GetRelativePathFromAssetsOrPackagesFolder(
            string path,
            out string basePath,
            out string relativePath)
        {
            if (FileUtils.IsUnderDirectory(path, FileUtils.ASSETS_FOLDER))
            {
                basePath = FileUtils.ASSETS_FOLDER;
                relativePath = path.Length < basePath.Length + 1 ? string.Empty : path.Substring(basePath.Length + 1);

                return true;
            }

            if (FileUtils.IsUnderPackageDirectory(path))
            {
                basePath = FileUtils.GetPackageDirectory(path);
                relativePath = path.Length < basePath.Length + 1 ? string.Empty : path.Substring(basePath.Length + 1);

                return true;
            }

            basePath = string.Empty;
            relativePath = path;

            return false;
        }

        public static FileUtils.RemoveAssetsResult RemoveAssets(
            IEnumerable<string> filenames,
            Logger logger = null)
        {
            FileUtils.RemoveAssetsResult removeAssetsResult = new FileUtils.RemoveAssetsResult();
            HashSet<string> stringSet = new HashSet<string>();

            using (IEnumerator<string> enumerator = filenames.GetEnumerator())
            {
                label_10:

                while (enumerator.MoveNext())
                {
                    string current = enumerator.Current;

                    if (File.Exists(current))
                    {
                        if (AssetDatabase.DeleteAsset(current))
                            removeAssetsResult.Removed.Add(current);
                        else
                            removeAssetsResult.RemoveFailed.Add(current);

                        string directoryName = Path.GetDirectoryName(current);

                        while (true)
                        {
                            if (!string.IsNullOrEmpty(directoryName) && !Path.IsPathRooted(directoryName) &&
                                (string.Compare(directoryName, FileUtils.ASSETS_FOLDER) != 0 &&
                                 string.Compare(directoryName, FileUtils.PACKAGES_FOLDER) != 0))
                            {
                                stringSet.Add(directoryName);
                                directoryName = Path.GetDirectoryName(directoryName);
                            }
                            else
                                goto label_10;
                        }
                    }
                    else
                        removeAssetsResult.Missing.Add(current);
                }
            }

            List<string> stringList1 = new List<string>((IEnumerable<string>) stringSet);
            stringList1.Sort((Comparison<string>) ((lhs, rhs) => string.Compare(rhs, lhs)));
            List<string> stringList2 = new List<string>();

            foreach (string path in stringList1)
            {
                if (Directory.GetFiles(path).Length == 0 && Directory.GetDirectories(path).Length == 0 &&
                    !AssetDatabase.DeleteAsset(path))
                {
                    stringList2.Add(path);
                    removeAssetsResult.RemoveFailed.Add(path);
                }
            }

            if (logger != null)
            {
                List<string> stringList3 = new List<string>();

                if (removeAssetsResult.Removed.Count > 0)
                    stringList3.Add(string.Format("Removed:\n{0}",
                                                  (object) string.Join("\n", removeAssetsResult.Removed.ToArray())));

                if (removeAssetsResult.RemoveFailed.Count > 0)
                    stringList3.Add(string.Format("Failed to Remove:\n{0}",
                                                  (object) string.Join(
                                                      "\n", removeAssetsResult.RemoveFailed.ToArray())));

                if (removeAssetsResult.Missing.Count > 0)
                    stringList3.Add(string.Format("Missing:\n{0}",
                                                  (object) string.Join("\n", removeAssetsResult.Missing.ToArray())));

                if (stringList2.Count > 0)
                    stringList3.Add(string.Format("Failed to Remove Folders:\n{0}",
                                                  (object) string.Join("\n", stringList2.ToArray())));

                if (stringList3.Count > 0)
                    logger.Log(string.Join("\n", stringList3.ToArray()), LogLevel.Verbose);
            }

            return removeAssetsResult;
        }

        public static bool CreateFolder(string path)
        {
            if (AssetDatabase.IsValidFolder(path))
                return true;

            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            string directoryName = Path.GetDirectoryName(path);

            return FileUtils.CreateFolder(directoryName) &&
                   !string.IsNullOrEmpty(AssetDatabase.CreateFolder(directoryName, directoryInfo.Name));
        }

        public static string ReplaceBaseAssetsOrPackagesFolder(string path, string newBase)
        {
            string str = path;
            string relativePath;

            if (FileUtils.GetRelativePathFromAssetsOrPackagesFolder(path, out string _, out relativePath))
                str = Path.Combine(newBase, relativePath);

            return str;
        }

        public enum PackageDirectoryType
        {
            None,
            AssetDatabasePath,
            PhysicalPath,
        }

        public class RemoveAssetsResult
        {
            public RemoveAssetsResult()
            {
                this.Removed = new List<string>();
                this.RemoveFailed = new List<string>();
                this.Missing = new List<string>();
            }

            public List<string> Removed { get; set; }
            public List<string> RemoveFailed { get; set; }
            public List<string> Missing { get; set; }
            public bool Success => this.RemoveFailed.Count == 0 && this.Missing.Count == 0;
        }
    }
}
