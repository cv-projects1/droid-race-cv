namespace vailshnast.postprocessbuild
{
    public enum LogLevel
    {
        Debug,
        Verbose,
        Info,
        Warning,
        Error,
    }
}