using UnityEditorInternal;
using UnityEngine;

namespace vailshnast.postprocessbuild
{
    [CreateAssetMenu(fileName = "IL2CPPStrippingConfig",menuName = "PostProcessingВuild/IL2CPPStrippingConfig")]
    public class IL2CPPStrippingConfig : ScriptableObject
    {
        public AssemblyDefinitionAsset MainPreserveAssembly;
        public bool PreserveMainAssemblyDependencies = true;
    }
}
