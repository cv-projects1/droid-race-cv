using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor.UnityLinker;
using UnityEditorInternal;
using UnityEngine;

namespace vailshnast.postprocessbuild
{
    public class IL2CPPStrippingPostProcessor : IUnityLinkerProcessor
    {
        public int callbackOrder { get; }

        public string GenerateAdditionalLinkXmlFile(BuildReport report, UnityLinkerBuildPipelineData data)
        {
            var il2CPPStrippingConfig = PostBuildExtensions.GetProjectConfig<IL2CPPStrippingConfig>();
            
            if (ValidateConfig(il2CPPStrippingConfig)) return string.Empty;
            var configJson = il2CPPStrippingConfig.MainPreserveAssembly.GetJsonFromAssembly();
            
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("<linker>");
            
            foreach (var assemblyGuid in configJson.references)
            {
                var assemblyJson = GetAssemblyJson(assemblyGuid);
                
                stringBuilder.AppendLine($"<assembly fullname=\"{assemblyJson.name}\" preserve=\"all\"/>");
            }
            stringBuilder.AppendLine("</linker>");

            var filePathName = Path.Combine(data.inputDirectory, "MainAssemblyStripping.xml");
            File.WriteAllText(filePathName, stringBuilder.ToString());

            // Return the file path & name
            return filePathName;
        }

        private AssemblyJson GetAssemblyJson(string assemblyGuid)
        {
            var correctGuid = assemblyGuid.Replace($"GUID:", string.Empty);
            var assemblyAssetPath = AssetDatabase.GUIDToAssetPath(correctGuid);
            var assemblyAsset = AssetDatabase.LoadAssetAtPath<AssemblyDefinitionAsset>(assemblyAssetPath);
            var assemblyJson = assemblyAsset.GetJsonFromAssembly();
            return assemblyJson;
        }

        private bool ValidateConfig(IL2CPPStrippingConfig il2CPPStrippingConfig)
        {
            if (il2CPPStrippingConfig == null)
            {
                Debug.LogError($"Project doesnt contains il2cpp config");
                return true;
            }

            if (!il2CPPStrippingConfig.PreserveMainAssemblyDependencies) return true;
            if (il2CPPStrippingConfig.MainPreserveAssembly != null) return false;

            Debug.LogError($"il2cpp config assembly asset is null");
            return true;
        }

   

        public void OnBeforeRun(BuildReport report, UnityLinkerBuildPipelineData data)
        {
            
        }

        public void OnAfterRun(BuildReport report, UnityLinkerBuildPipelineData data)
        {
           
        }
    }
}