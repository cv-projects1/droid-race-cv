using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace vailshnast.postprocessbuild
{
    public static class PostBuildExtensions
    {
        public static T GetProjectConfig<T>() where T : ScriptableObject
        {
            var guids = AssetDatabase.FindAssets("t:ScriptableObject");
            var toSelect = new List<ScriptableObject>();
            
            foreach (var guid in guids)
            {
                var path = AssetDatabase.GUIDToAssetPath(guid);

                var toCheck = AssetDatabase.LoadAllAssetsAtPath(path)
                                           .ToList()
                                           .OfType<ScriptableObject>()
                                           .Where(x => x is T);
                    
                toSelect.AddRange(toCheck);
            }

            if (toSelect.Count <= 1)
                return toSelect.Count switch
                {
                    1 => (T) toSelect[0],
                    0 => null,
                    _ => null
                };

            Debug.LogError($"There are more than two {nameof(T)} configs in project, returning first");

            return (T) toSelect[0];
        }
        
        public static AssemblyJson GetJsonFromAssembly(this AssemblyDefinitionAsset asset)
            => JsonUtility.FromJson<AssemblyJson>(asset.text);
    }
}