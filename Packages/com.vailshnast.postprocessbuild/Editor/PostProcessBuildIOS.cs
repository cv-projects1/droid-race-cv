using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using UnityEngine;

namespace vailshnast.postprocessbuild
{
    public class PostProcessBuildIOS : IPostprocessBuildWithReport
    {
        public int callbackOrder => 999;

        public void OnPostprocessBuild(BuildReport report)
        {
            var summary = report.summary;
            var path = report.summary.outputPath;
            var platform = summary.platform;

            if (platform == BuildTarget.iOS)
            {
                var config = PostBuildExtensions.GetProjectConfig<PostProcessBuildPlatformConfig>();
                ModifyFrameworks(path);
                UpdatePlist(path);

                if (config != null)
                {
                    ModifySettings(config);
                    InstallPods(config, path);
                    OpenXcodeProject(config, path);
                    AddAppTransparencyTracking(config, path); 
                }
            }
        }

        private void AddAppTransparencyTracking(PostProcessBuildPlatformConfig config, string path)
        {
#if UNITY_IOS
            if(!config.AddAppTransparencyDescription) return;
            // Retrieve the plist file from the Xcode project directory:
            string plistPath = path + "/Info.plist";
            PlistDocument plistObj = new PlistDocument();


            // Read the values from the plist file:
            plistObj.ReadFromString(File.ReadAllText(plistPath));

            // Set values from the root object:
            PlistElementDict plistRoot = plistObj.root;

            var trackingString = "NSUserTrackingUsageDescription";
            // Set the description key-value in the plist:
            if (!plistRoot.values.ContainsKey(trackingString))
                plistRoot.SetString(trackingString, config.AppTransparencyDescription);

            // Save changes to the plist:
            File.WriteAllText(plistPath, plistObj.WriteToString());
#endif
        }

        private void OpenXcodeProject(PostProcessBuildPlatformConfig config, string path)
        {
            if (!config.OpenXcodeProject) return;

            var envVars = new Dictionary<string, string>()
            {
                {
                    "LANG",
                    (Environment.GetEnvironmentVariable("LANG") ?? "en_US.UTF-8").Split('.')[0] + ".UTF-8"
                },
                {
                    "PATH",
                    "/usr/local/bin:" + (Environment.GetEnvironmentVariable("PATH") ?? string.Empty)
                }
            };

            var projectName = "Unity-iPhone.xcworkspace";
            CommandLine.Result result = null;

            result = config.RunInShell
                ? CommandLine.RunViaShell("open", projectName, path, envVars)
                : CommandLine.Run("open", projectName, path, envVars);

            if (result.exitCode == 0)
                Debug.Log($"Successfully opened XcodeProject {result.message}\n{result.stderr}\n{result.stdout}");
            else Debug.LogError($"Failed to open XcodeProject {result.message}\n{result.stderr}\n{result.stdout}");
        }

        private void InstallPods(PostProcessBuildPlatformConfig config, string path)
        {
            if (!config.PodInstall) return;

            Dictionary<string, string> envVars = new Dictionary<string, string>()
            {
                {
                    "LANG",
                    (Environment.GetEnvironmentVariable("LANG") ?? "en_US.UTF-8").Split('.')[0] + ".UTF-8"
                },
                {
                    "PATH",
                    "/usr/local/bin:" + (Environment.GetEnvironmentVariable("PATH") ?? string.Empty)
                }
            };

            CommandLine.Result result = null;

            result = config.RunInShell
                ? CommandLine.RunViaShell("pod", "install", path, envVars)
                : CommandLine.Run("pod", "install", path, envVars);

            if (result == null)
            {
                Debug.LogError($"PostBuild: pod not installed result is null");

                return;
            }

            if (result.exitCode == 0)
                Debug.Log(
                    $"PostBuild: pod installed {result.message} {result.exitCode} {result.stdout} {result.stderr}");
            else
                Debug.LogError(
                    $"PostBuild: pod not installed {result.message} {result.exitCode} {result.stdout} {result.stderr}");
        }

        private void ModifySettings(PostProcessBuildPlatformConfig config)
        {
            if (config == null) return;
            if (!config.AutoSign) return;

            PlayerSettings.iOS.appleDeveloperTeamID = config.AppleDeveloperTeamID;
            PlayerSettings.iOS.appleEnableAutomaticSigning = true;

            //PlayerSettings.iOS.buildNumber = buildParams.RevisionNumber.ToString();
        }

        private void ModifyFrameworks(string path)
        {
#if UNITY_IOS
            string projPath = PBXProject.GetPBXProjectPath(path);
            var project = new PBXProject();
            project.ReadFromFile(projPath);
            string mainTargetGuid = project.GetUnityMainTargetGuid();

            foreach (var targetGuid in new[] {mainTargetGuid, project.GetUnityFrameworkTargetGuid()})
            {
                project.SetBuildProperty(targetGuid, "ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES", "NO");
            }

            project.SetBuildProperty(mainTargetGuid, "ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES", "NO");
            project.WriteToFile(projPath);
#endif
        }

        private void UpdatePlist(string pathToBuildProject)
        {
#if UNITY_IOS
            var plistPath = pathToBuildProject + "/Info.plist";
            var plistDocument = new PlistDocument();
            plistDocument.ReadFromFile(plistPath);
            PlistElementDict rootDict = plistDocument.root;

            // We're declaring that we USE encryption but only those that falls into "Exemptions" that allows us to skip registration of app in US Trade export regulator (SNAP-R).
            // Examples of Exemptions: use of HTTPS, use of encryption for own data protection (if 'common known' algorithms used).
            // When this key is set in Info.Plist, Apple skips dialogs that asks about encryption usage or its state changes.
            rootDict.SetBoolean("ITSAppUsesNonExemptEncryption", false);
            plistDocument.WriteToFile(plistPath);
#endif
        }
    }
}