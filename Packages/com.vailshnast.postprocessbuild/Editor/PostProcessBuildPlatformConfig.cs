using UnityEngine;

namespace vailshnast.postprocessbuild
{
    [CreateAssetMenu(fileName = "PostProcessBuildPlatformConfig", menuName = "PostProcessingВuild/PostProcessBuildPlatformConfig", order = 0)]
    public class PostProcessBuildPlatformConfig : ScriptableObject
    {
        public string AppleDeveloperTeamID;

        public bool AutoSign = true,
                    PodInstall = true,
                    RunInShell = true,
                    OpenXcodeProject = true,
                    AddAppTransparencyDescription = true;

        public string AppTransparencyDescription =
            "Your data will be used to provide you a better and personalized ad experience.";
    }
}