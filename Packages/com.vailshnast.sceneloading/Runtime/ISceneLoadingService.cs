using System;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;

namespace vailshnast.sceneloading
{
    public interface ISceneLoadingService
    {
        void LoadScene(string sceneName,
                       Action onSceneLoaded = null,
                       LoadSceneMode loadSceneMode = LoadSceneMode.Single);

        IEnumerator LoadSceneAsync(string sceneName, 
                                       LoadSceneMode loadSceneMode = LoadSceneMode.Single);
    }
}