using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using vailshnast.coroutine;

namespace vailshnast.sceneloading
{
    public class SceneLoadingService : ISceneLoadingService
    {
        private readonly ICoroutineService _coroutineService;

        public SceneLoadingService(ICoroutineService coroutineService)
        {
            _coroutineService = coroutineService;
        }

        public void LoadScene(string sceneName, 
                              Action onSceneLoaded = null,
                              LoadSceneMode loadSceneMode = LoadSceneMode.Single) =>
            _coroutineService.Invoke(LoadYourAsyncScene(sceneName, onSceneLoaded, loadSceneMode));

        private IEnumerator LoadYourAsyncScene(string sceneName,
                                               Action onSceneLoaded = null,
                                               LoadSceneMode loadSceneMode = LoadSceneMode.Single)
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, loadSceneMode);

            // Wait until the asynchronous scene fully loads
            while (!asyncLoad.isDone)
                yield return null;

            onSceneLoaded?.Invoke();
        }
        
        public IEnumerator LoadSceneAsync(string sceneName, 
                                              LoadSceneMode loadSceneMode = LoadSceneMode.Single)
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, loadSceneMode);

            // Wait until the asynchronous scene fully loads
            while (!asyncLoad.isDone)
                yield return null;
        }
    }
}