using System;

namespace vailshnast.scriptabledatabase
{
    [Serializable]
    public class DatabaseKey<TKey, TValue>
    {
        public TKey Key;
        public TValue Value;
    }
}