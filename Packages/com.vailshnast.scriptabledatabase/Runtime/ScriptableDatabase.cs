using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace vailshnast.scriptabledatabase
{
    public abstract class ScriptableDatabase<TKey,TValue> : ScriptableObject
    {
        [SerializeField] private List<GenericKey> _resources
            = new List<GenericKey>();

        private Dictionary<TKey, TValue> _valuesMap;
        
        public void Initialize()
        {
            _valuesMap = new Dictionary<TKey, TValue>();

            foreach (var resource in _resources) 
                _valuesMap.Add(resource.Key, resource.Value);
        }

        public Dictionary<TKey, TValue>.ValueCollection GetDatabaseValues() 
            => _valuesMap.Values;

        public TValue GetValue(TKey key) 
            => _valuesMap[key];

        [Serializable]
        public class GenericKey : DatabaseKey<TKey,TValue> { }
    }
}