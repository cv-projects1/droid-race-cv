using UnityEngine;

namespace vailshnast.serviceparent
{
    public interface IServicesParent
    {
        Transform Parent { get; }
        void Initialize();
    }
}