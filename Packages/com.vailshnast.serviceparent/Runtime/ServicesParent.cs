using UnityEngine;
using Zenject;

namespace vailshnast.serviceparent
{
    public class ServicesParent : IServicesParent
    {
        private readonly DiContainer _diContainer;
        public Transform Parent { get; private set; }

        public void Initialize() => 
            Parent = _diContainer.CreateEmptyGameObject($"Services").transform;
                
        public ServicesParent(DiContainer diContainer) 
            => _diContainer = diContainer;
    }
}