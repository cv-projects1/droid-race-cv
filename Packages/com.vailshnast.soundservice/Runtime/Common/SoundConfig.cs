using System;
using UnityEngine;
using UnityEngine.Audio;
using vailshnast.installers;

namespace vailshnast.soundservice
{
    [CreateAssetMenu(fileName = "SoundConfig", menuName = "StaticData/SoundConfig")]
    public class SoundConfig : ScriptableObject, IScriptableService
    {
        public Type ServiceType => typeof(SoundConfig);
        public int InitialPoolSize = 5;
        public int IncreaseBy = 1;
        public AudioMixer AudioMixer;
        public SoundPlayer SoundPlayerPrefab;
    }
}
