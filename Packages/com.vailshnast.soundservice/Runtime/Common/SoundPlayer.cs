using System;
using UnityEngine;
using vailshnast.pool;
using vailshnast.timer;
using Zenject;
using Random = UnityEngine.Random;

namespace vailshnast.soundservice
{
    [RequireComponent(typeof(AudioSource))]
    public class SoundPlayer : MonoBehaviour, IPoolObject, IPoolComponent
    {
        Type IPoolComponent.ComponentType => typeof(SoundPlayer);
        [SerializeField] private AudioSource _audioSource = null;
        private PoolObject _poolObject;
        private bool _isPlaying;
        private const float PlayingAdditionOffset = 0.2f;
        private ITimerService _timerService;

        [Inject]
        public void Construct(ITimerService timerService) =>
            _timerService = timerService;

        public void Play(SoundSettings settings)
        {
            if (_isPlaying) return;

            _audioSource.clip = settings.ClipsToPlay[Random.Range(0, settings.ClipsToPlay.Length - 1)];

            //_audioSource.pitch = settings.Pitch.Random();
            _audioSource.volume = Random.Range(settings.Volume.x, settings.Volume.y);
            _audioSource.outputAudioMixerGroup = settings.AudioMixerGroup;
            _audioSource.spatialBlend = settings.Is3DSound ? 1f : 0f;
            _audioSource.spread = settings.Is3DSound ? -360f : 0f;
            _audioSource.minDistance = settings.Distance3D.x;
            _audioSource.maxDistance = settings.Distance3D.y;

            //_audioSource.SetCustomCurve(AudioSourceCurveType.CustomRolloff, settings.VolumeChangeByDistance);
            _audioSource.Play();
            DisableOnSoundEnded();
            _isPlaying = true;
        }

        public SoundPlayer AtPosition(Vector3 worldPosition)
        {
            if (_isPlaying) return null;

            transform.position = worldPosition;

            return this;
        }

        public SoundPlayer AtLocalPosition(Vector3 localPosition)
        {
            if (_isPlaying) return null;

            transform.localPosition = localPosition;

            return this;
        }

        public SoundPlayer FollowAt(Transform target, bool resetPosition = true)
        {
            if (_isPlaying) return null;

            transform.parent = target;
            if (resetPosition) AtLocalPosition(Vector3.zero);

            return this;
        }

        public SoundPlayer SetLoop()
        {
            if (_isPlaying) return null;

            _audioSource.loop = true;

            return this;
        }

        public void Terminate()
        {
            if (!_isPlaying) return;

            _poolObject.Destroy();
        }

        private void DisableOnSoundEnded()
        {
            if (_audioSource.loop) return;

            var playTime = _audioSource.clip.length + PlayingAdditionOffset;
            _timerService.CreateTimer(playTime, () => _poolObject.Destroy(),
                                      null, false, false, null);
        }

        void IPoolObject.PostAwake(PoolObject poolObject)
        {
            _poolObject = poolObject;
            _audioSource.loop = false;
            _audioSource.playOnAwake = false;
            _audioSource.Stop();
        }

        void IPoolObject.OnReuseObject(PoolObject poolObject)
        {
            /* do nothing */
        }

        void IPoolObject.OnDisposeObject(PoolObject poolObject)
        {
            if (_audioSource.isPlaying)
                _audioSource.Stop();

            _audioSource.loop = false;
            _isPlaying = false;
        }
    }
}