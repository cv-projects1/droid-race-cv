namespace vailshnast.soundservice
{
    public interface ISoundService
    {
        void Initialize();
        SoundPlayer GetSoundPlayer();
    }
}