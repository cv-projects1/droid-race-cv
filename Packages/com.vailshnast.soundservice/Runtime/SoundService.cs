﻿using vailshnast.pool;
using Zenject;

namespace vailshnast.soundservice
{
    public class SoundService : ISoundService
    {
        private readonly IPoolService _poolService;
        private readonly SoundConfig _soundConfig;

        public SoundService(IPoolService poolService , SoundConfig soundConfig)
        {
            _poolService = poolService;
            _soundConfig = soundConfig;
        }

        public void Initialize()
        {
            _poolService.CreatePool(_soundConfig.SoundPlayerPrefab.gameObject,
                                    _soundConfig.InitialPoolSize,
                                    _soundConfig.IncreaseBy);
        }

        public SoundPlayer GetSoundPlayer()
        {
            var poolObject = _poolService.InstantiateFromPool(_soundConfig.SoundPlayerPrefab
                                                                          .gameObject);

            return poolObject.ResolveComponent<SoundPlayer>();
        }
    }
}