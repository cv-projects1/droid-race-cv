using UnityEngine;

 
 public class AutoStretchSprite : MonoBehaviour
 {
     [SerializeField] private SpriteRenderer _spriteRenderer;
     
     public void Resize(Camera cam, bool keepAspect = false)
     {
         transform.localScale = new Vector3(1, 1, 1);
 
         // example of a 640x480 sprite
         Vector2 size = _spriteRenderer.sprite.bounds.size;
         float width = size.x;  // 4.80f
         float height = size.y; // 6.40f
 
         // and a 2D camera at 0,0,-10
         float worldScreenHeight = cam.orthographicSize * 2f; // 10f
         float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width; // 10f
 
         Vector3 imgScale = new Vector3(1f, 1f, 1f);
 
         // do we scale according to the image, or do we stretch it?
         if (keepAspect)
         {
             Vector2 ratio = new Vector2(width / height, height / width);
             if ((worldScreenWidth / width) > (worldScreenHeight / height))
             {
                 // wider than tall
                 imgScale.x = worldScreenWidth / width;
                 imgScale.y = imgScale.x * ratio.y; 
             }
             else
             {
                 // taller than wide
                 imgScale.y = worldScreenHeight / height;
                 imgScale.x = imgScale.y * ratio.x;             
             }
         }
         else
         {
             imgScale.x = worldScreenWidth / width;
             imgScale.y = worldScreenHeight / height;
         }
 
         // apply change
         transform.localScale = imgScale;
     }
 }