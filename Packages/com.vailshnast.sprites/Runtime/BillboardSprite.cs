using UnityEngine;
using Zenject;

namespace vailshnast.sprites
{
    public class BillboardSprite : MonoBehaviour
    {
        [SerializeField] private Transform _cameraTransform;
        [SerializeField] private Vector3 _dir = -Vector2.up;
        
        public void Update() => transform.LookAt(_cameraTransform.transform.position, _dir);
        
        public void SetCameraTransform(Transform cam) => _cameraTransform = cam;
    }
}
