using UnityEngine;


public class ScaleSpriteToScreen : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private Camera _camera;

    [SerializeField] private Vector2 _fitSettings;

    public void Resize() => ResizeSpriteToScreen(_fitSettings.x, _fitSettings.y);
    
    private void ResizeSpriteToScreen(float fitToScreenWidth, float fitToScreenHeight)
    {
        var spriteRenderer = _spriteRenderer;
        var spriteTransform = _spriteRenderer.transform;

        spriteTransform.localScale = new Vector3(1,1,1);
 
        float width = spriteRenderer.sprite.bounds.size.x;
        float height = spriteRenderer.sprite.bounds.size.y;
         
        float worldScreenHeight = (float)(_camera.orthographicSize * 2.0);
        float worldScreenWidth = (float)(worldScreenHeight / Screen.height * Screen.width);
         
        if (fitToScreenWidth != 0)
        {
            Vector2 sizeX = new Vector2(worldScreenWidth / width / fitToScreenWidth,spriteTransform.localScale.y);
            spriteTransform.localScale = sizeX;
        }
         
        if (fitToScreenHeight != 0)
        {
            Vector2 sizeY = new Vector2(spriteTransform.localScale.x, worldScreenHeight / height / fitToScreenHeight);
            spriteTransform.localScale = sizeY;
        }
    }
}
