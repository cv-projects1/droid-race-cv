using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace vailshnast.sprites
{
    public static class SpriteTools
    {
        private static void ResizeSpriteToScreen(SpriteRenderer spriteRenderer, Camera camera,  float fitToScreenWidth, float fitToScreenHeight)
        {
            var spriteTransform = spriteRenderer.transform;

            spriteTransform.localScale = new Vector3(1,1,1);

            Vector2 spriteSize = spriteRenderer.sprite.bounds.size;
            float width = spriteSize.x;
            float height = spriteSize.y;
         
            float worldScreenHeight = (float)(camera.orthographicSize * 2.0);
            float worldScreenWidth = (float)(worldScreenHeight / Screen.height * Screen.width);
         
            if (fitToScreenWidth != 0)
            {
                Vector2 sizeX = new Vector2(worldScreenWidth / width / fitToScreenWidth,spriteTransform.localScale.y);
                spriteTransform.localScale = sizeX;
            }
         
            if (fitToScreenHeight != 0)
            {
                Vector2 sizeY = new Vector2(spriteTransform.localScale.x, worldScreenHeight / height / fitToScreenHeight);
                spriteTransform.localScale = sizeY;
            }
        }
        public static void AutoStretchSprite(Transform transform, SpriteRenderer spriteRenderer, Camera cam, bool keepAspect = false)
        {
            transform.localScale = new Vector3(1, 1, 1);
 
            // example of a 640x480 sprite
            Vector2 size = spriteRenderer.sprite.bounds.size;
            float width = size.x;  // 4.80f
            float height = size.y; // 6.40f
 
            // and a 2D camera at 0,0,-10
            float worldScreenHeight = cam.orthographicSize * 2f;                       // 10f
            float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width; // 10f
 
            Vector3 imgScale = new Vector3(1f, 1f, 1f);
 
            // do we scale according to the image, or do we stretch it?
            if (keepAspect)
            {
                Vector2 ratio = new Vector2(width / height, height / width);
                if ((worldScreenWidth / width) > (worldScreenHeight / height))
                {
                    // wider than tall
                    imgScale.x = worldScreenWidth / width;
                    imgScale.y = imgScale.x * ratio.y; 
                }
                else
                {
                    // taller than wide
                    imgScale.y = worldScreenHeight / height;
                    imgScale.x = imgScale.y * ratio.x;             
                }
            }
            else
            {
                imgScale.x = worldScreenWidth / width;
                imgScale.y = worldScreenHeight / height;
            }
 
            // apply change
            transform.localScale = imgScale;
        }
    }
}
