namespace vailshnast.statemachines
{
    public interface IBaseParamState<in T> : IState where T : IStateParam
    {
        public void EnterState(T param);
    }
}