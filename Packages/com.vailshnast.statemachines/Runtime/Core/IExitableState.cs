namespace vailshnast.statemachines
{
    public interface IExitableState
    {
        void ExitState();
    }
}