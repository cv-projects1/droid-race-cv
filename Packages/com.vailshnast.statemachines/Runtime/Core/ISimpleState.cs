namespace vailshnast.statemachines
{
    public interface ISimpleState : IState
    {
        void EnterState();
    }
}