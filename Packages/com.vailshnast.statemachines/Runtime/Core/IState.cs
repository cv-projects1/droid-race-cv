namespace vailshnast.statemachines
{
    public interface IState
    {
        void Initialize(IStateMachine stateMachine);
    }
}