using System;
using Zenject;

namespace vailshnast.statemachines
{
    public interface IStateMachine
    {
        event Action<Type> OnStateEntered,
                           OnStateExited,
                           OnBeforeStateEntered,
                           OnBeforeStateExited;
        
        void Initialize();

        void EnterState<TState, TParam>(TParam param)
            where TParam : IStateParam where TState : IBaseParamState<TParam>;

        void EnterState<TState>() where TState : ISimpleState;
        bool IsActiveStateTypeOf<T>() where T : IState;
        bool IsPreviousStateTypeOf<T>() where T : IState;
    }
}