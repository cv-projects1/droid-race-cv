using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace vailshnast.statemachines
{
    public abstract class BaseStateMachine : IStateMachine
    {
        private IState _activeState;
        private IState _previousState;
        
        private readonly Dictionary<Type, IState> _statesMap;
        private readonly DiContainer _diContainer;
        public event Action<Type> OnStateEntered, OnStateExited;
        public event Action<Type> OnBeforeStateEntered;
        public event Action<Type> OnBeforeStateExited;
        public bool IsActiveStateTypeOf<T>() where T : IState => _activeState is T;
        public bool IsPreviousStateTypeOf<T>() where T : IState => _previousState is T;

        public BaseStateMachine(DiContainer diContainer)
        {
            _diContainer = diContainer;
            _statesMap = new Dictionary<Type, IState>();
        }

        public abstract void Initialize();

        public void EnterState<TState, TParam>(TParam param)
            where TParam : IStateParam where TState : IBaseParamState<TParam>
        {
            var state = ChangeState<TState>();

            OnBeforeStateEntered?.Invoke(state.GetType());
            state.EnterState(param);
            OnStateEntered?.Invoke(state.GetType());
        }

        public void EnterState<TState>() where TState : ISimpleState
        {
            var state = ChangeState<TState>();
            
            OnBeforeStateEntered?.Invoke(state.GetType());
            state.EnterState();
            OnStateEntered?.Invoke(state.GetType());
        }

        private TState ChangeState<TState>() where TState : IState
        {
            ExitState();

            _statesMap.TryGetValue(typeof(TState), out var state);

#if UNITY_EDITOR
            if (state == null)
            {
                Debug.LogError($"{GetType()} doesnt have state of type {typeof(TState)}");
                return default;
            }
#endif
            _previousState = _activeState;
            _activeState = state;

            return (TState) state;
        }

        private void ExitState()
        {
            OnBeforeStateExited?.Invoke(_activeState.GetType());

            if (_activeState is IExitableState exitableState)
                exitableState.ExitState();
            
            OnStateExited?.Invoke(_activeState.GetType());
        }

        protected void AddState<T>() where T : IState
        {
            var state = _diContainer.Instantiate<T>();
            _statesMap[typeof(T)] = state;
            state.Initialize(this);
        }
    }
}