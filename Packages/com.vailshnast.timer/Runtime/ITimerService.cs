using System;

namespace vailshnast.timer
{
    public interface ITimerService
    {
        Timer CreateTimer(float duration, Action onComplete, Action<TimerData> onUpdate,
                          bool isLooped, bool usesRealTime, object owner);

        void CancelAllTimers();
        void CancelTimer(Timer timer);
        void CancelTimers(object owner);
    }
}