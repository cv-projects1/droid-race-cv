using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.PlayerLoop;
using Zenject;

namespace vailshnast.timer
{
    /// <summary>
    /// Manages updating all the <see cref="Timer"/>s that are running in the application.
    /// This will be instantiated the first time you create a timer -- you do not need to add it into the
    /// scene manually.
    /// </summary>
    public class TimerService : ITimerService , ITickable
    {
        private List<Timer> _timers = new List<Timer>();

        // buffer adding timers so we don't edit a collection during iteration
        private List<Timer> _timersToAdd = new List<Timer>();
        
        public Timer CreateTimer(float duration, Action onComplete, Action<TimerData> onUpdate,
                                 bool isLooped, bool usesRealTime, object owner)
        {
            var timer = new Timer(duration, onComplete, onUpdate, isLooped, usesRealTime, owner);
            _timersToAdd.Add(timer);

            return timer;
        }

        public void CancelAllTimers()
        {
            foreach (Timer timer in _timers) timer.Cancel();

            _timers = new List<Timer>();
            _timersToAdd = new List<Timer>();
        }

        public void CancelTimer(Timer timer) => timer.Cancel();

        public void CancelTimers(object owner)
        {
            var ownerTimers =_timers.Where(x => x.Owner == owner).ToList();

            for (int i = 0; i < ownerTimers.Count; i++)
            {
                var ownerTimer = ownerTimers[i];
                ownerTimer.Cancel();
            }
        }
        
        private void UpdateAllTimers()
        {
            if (_timersToAdd.Count > 0)
            {
                _timers.AddRange(_timersToAdd);
                _timersToAdd.Clear();
            }

            foreach (Timer timer in _timers) timer.Update();

            _timers.RemoveAll(t => t.IsDone);
        }

        public void Tick() => UpdateAllTimers();
    }
}