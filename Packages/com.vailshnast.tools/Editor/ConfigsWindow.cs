#if UNITY_EDITOR
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using vailshnast.installers;

namespace vailshnast.tools
{
    using UnityEditor;
    using Sirenix.Utilities;
    using Sirenix.Utilities.Editor;
    using Sirenix.OdinInspector.Editor;

    public class ConfigsWindow : OdinMenuEditorWindow
    {
        [MenuItem("Tools/ProjectTools/ConfigsWindow")]
        private static void OpenWindow()
        {
            var window = GetWindow<ConfigsWindow>();
            window.position = GUIHelper.GetEditorWindowRect().AlignCenter(800, 600);
        }

        protected override OdinMenuTree BuildMenuTree()
        {
            var tree = new OdinMenuTree(true)
            {
                Config = {DrawSearchToolbar = true}
            };

            var scriptableObject = AssetUtilities.GetAllAssetsOfType<ScriptableObject>().OfType<IScriptableService>()
                                                 .ToList();

            foreach (var t in scriptableObject)
                tree.Add(t.GetType().Name, t);

            tree.EnumerateTree()
                .AddThumbnailIcons()
                .SortMenuItemsByName();

            return tree;
        }

        protected override void OnBeginDrawEditors()
        {
            OdinMenuTreeSelection selected = this.MenuTree.Selection;

            SirenixEditorGUI.BeginHorizontalToolbar();
            {
                GUILayout.FlexibleSpace();

                if (SirenixEditorGUI.ToolbarButton("Save Assets"))
                {
                    AssetDatabase.SaveAssets();
                }

            }
            SirenixEditorGUI.EndHorizontalToolbar();
        }
    }

    #region Processors
    /*public class LevelPointsListProccessor : OdinAttributeProcessor<List<LevelPointSettings>>
    {
        public override void ProcessSelfAttributes(InspectorProperty property, List<Attribute> attributes)
        {
            base.ProcessSelfAttributes(property, attributes);
            attributes.Add(new ColorBoxGroup("LevelElements", 0, 0, 1));
            attributes.Add(new ColorBoxGroup("LevelElements", 0, 0, 1));
            attributes.Add(new TableListAttribute());
        }
    }

    public class LevelSettingsAttributeProccessor : OdinAttributeProcessor<LevelSettings>
    {
        private const string LevelPointGenerator = "LevelPointGenerator";

        public override void ProcessChildMemberAttributes(InspectorProperty parentProperty, MemberInfo member,
                                                          List<Attribute> attributes)
        {
            base.ProcessChildMemberAttributes(parentProperty, member, attributes);

            if (member.Name == LevelPointGenerator)
            {
                attributes.Add(new ColorBoxGroup("LevelElements", 0, 0, 1));

                //attributes.Add(new TableListAttribute());
                attributes.Add(new HideReferenceObjectPickerAttribute());
                attributes.Add(new TypeFilterAttribute("@LevelSettingsAttributeProccessor.GetGenerators()"));
                attributes.Add(new LabelWidthAttribute(200));
                attributes.Add(new ShowInInspectorAttribute());
            }
        }

        private static IEnumerable<Type> GetGenerators()
        {
            return AssemblyUtilities.GetTypes(AssemblyTypeFlags.CustomTypes)
                                    .Where(x => !x.IsAbstract && !x.IsInterface &&
                                                x.InheritsFrom(typeof(ILevelPointGenerator)));
        }
    }

    public class FieldSettingsProcessor : OdinAttributeProcessor<FieldSettings>
    {
        public override void ProcessSelfAttributes(InspectorProperty property, List<Attribute> attributes)
        {
            base.ProcessSelfAttributes(property, attributes);
            attributes.Add(new ColorBoxGroup("Field Settings", 1, 0, 0));
            attributes.Add(new HideReferenceObjectPickerAttribute());
            attributes.Add(new HideLabelAttribute());
        }

        public override void ProcessChildMemberAttributes(InspectorProperty parentProperty, MemberInfo member,
                                                          List<Attribute> attributes)
        {
            base.ProcessChildMemberAttributes(parentProperty, member, attributes);

            if (member.Name == "ElementSpeed")
            {
                attributes.Add(new InfoBoxAttribute("@FieldSettingsProcessor.GetSpeed($value)", InfoMessageType.None));

                //
            }
            
            if (member.Name == "FieldElementsGenerator")
            {
                attributes.Add(new ColorBoxGroup("Field Settings/FieldElements", 0, 0, 1));
                
                attributes.Add(new TypeFilterAttribute("@FieldSettingsProcessor.GetGenerators()"));
                attributes.Add(new LabelWidthAttribute(200));
                attributes.Add(new ShowInInspectorAttribute());
            }

            if (member.Name == "RandomInvalidElementsCount") 
                attributes.Add(new PropertyRangeAttribute(0, 5));

            if(member.Name == "ValidElementsCount")
                attributes.Add(new PropertyRangeAttribute(0, 5));

            attributes.Add(new ColorBoxGroup("Field Settings", 1, 0, 0));
            attributes.Add(new LabelWidthAttribute(200));
        }

        public static string GetSpeed(float speed)
        {
            return $"{speed} seconds to reach end";
        }
        
        private static IEnumerable<Type> GetGenerators()
        {
            return AssemblyUtilities.GetTypes(AssemblyTypeFlags.CustomTypes)
                                    .Where(x => !x.IsAbstract && !x.IsInterface &&
                                                x.InheritsFrom(typeof(ILevelPointGenerator)));
        }
    }

    public class LockPickAttributeProcessor : OdinAttributeProcessor<LockpickSettings>
    {
        public override void ProcessSelfAttributes(InspectorProperty property, List<Attribute> attributes)
        {
            base.ProcessSelfAttributes(property, attributes);
            attributes.Add(new HideReferenceObjectPickerAttribute());
            attributes.Add(new HideLabelAttribute());
        }

        public override void ProcessChildMemberAttributes(InspectorProperty parentProperty, MemberInfo member,
                                                          List<Attribute> attributes)
        {
            base.ProcessChildMemberAttributes(parentProperty, member, attributes);

            if (member.Name == "RotationSpeed")
            {
                attributes.Add(new InfoBoxAttribute("@LockPickAttributeProcessor.GetRotationHelp($value)",
                                                    InfoMessageType.None));

                attributes.Add(new LabelWidthAttribute(200));

                //attributes.Add(new HideLabelAttribute());
            }

            attributes.Add(new ColorBoxGroup("LockpickSettings", 0, 1, 0));
        }

        public static string GetRotationHelp(float speed)
        {
            return $"{speed} = [{speed} circles / 1 sec] or [1 circles / {1f / speed} sec]";
        }
    }

    public class LevelDataAttributeProccessor : OdinAttributeProcessor<LevelData>
    {
        private const string LevelSettings = "LevelSettings";

        public override void ProcessChildMemberAttributes(InspectorProperty parentProperty, MemberInfo member,
                                                          List<Attribute> attributes)
        {
            base.ProcessChildMemberAttributes(parentProperty, member, attributes);

            if (member.Name == LevelSettings)
            {
                attributes.Add(new FoldoutGroupAttribute("Level"));
                attributes.Add(new BoxGroupAttribute("Level/LevelSettings"));
                attributes.Add(new HideLabelAttribute());
                attributes.Add(new ShowInInspectorAttribute());
                attributes.Add(new HideReferenceObjectPickerAttribute());
            }
        }
    }

    public class LevelDataSetProccessor : OdinAttributeProcessor<LevelSetData>
    {
        private const string LevelList = "LevelList";

        public override void ProcessChildMemberAttributes(InspectorProperty parentProperty, MemberInfo member,
                                                          List<Attribute> attributes)
        {
            base.ProcessChildMemberAttributes(parentProperty, member, attributes);

            if (member.Name == LevelList)
            {
                attributes.Add(new ListDrawerSettingsAttribute()
                {
                    AddCopiesLastElement = true, 
                    OnBeginListElementGUI = $"@LevelDataSetProccessor.OnBeginList($index)", 
                    //OnEndListElementGUI = $"@LevelDataSetProccessor.OnBeginList($index)",
                });
            }
   
        }
        
        public static void OnBeginList(int index)
        {
            SirenixEditorGUI.Title($"Level {index + 1}","",TextAlignment.Left,true);
        }

        public static void OnEndList(int index)
        {
        }
    }*/
    #endregion
}
#endif