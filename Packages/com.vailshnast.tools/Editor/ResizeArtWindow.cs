﻿using System.Collections.Generic;
using System.IO;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

namespace vailshnast.tools
{
    public class ResizeArtWindow : OdinEditorWindow
    {
        private SerializedObject serializedObject;
        private Vector2 m_ScrollPosition;
        public List<Texture2D> textures;
        public float Scale = 0.75f;
        public ResizeType ResizeType;
        public Vector2 ResizeResolution;

        [FolderPath]
        public string path;

        [MenuItem("Tools/ResizeArt")]
        static void InitWindow()
        {
            ResizeArtWindow window =
                (ResizeArtWindow)GetWindow(typeof(ResizeArtWindow), false, "Resize Art Window");

        

            //window.position = new Rect(500, 500, 800, 650);
            window.Show();
        }

        [Button]
        private void FindByPath() => textures = GetTextures(path.Substring(path.IndexOf("Assets")));

        [Button]
        public void Resize()
        {
            for (int i = 0; i < textures.Count; i++)
            {
                string path = /*= "Assets/Art/Locations" + paths[i]*/AssetDatabase.GetAssetPath(textures[i]);
                Texture2D tex = AssetDatabase.LoadAssetAtPath<Texture2D>(path);
                if (tex != null)
                {
                    if (ResizeType == ResizeType.ScaleByKoef)
                        ScaleTexture(tex, (int) (tex.width * Scale), (int) (tex.height * Scale));
                    else
                    {
                        ScaleTexture(tex, (int)(ResizeResolution.x), (int)(ResizeResolution.y));
                    }
                    Debug.Log(tex.name);
                }
                else
                {
                    Debug.LogError("null tex at path \n" + path);
                }
            }
            //Utilities.SimpleCancelableProgressBarOperation("Resizing Textures", textures, ResizeTexture);
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
        }

        private void ScaleTexture(Texture2D source, int targetWidth, int targetHeight)
        {
            string assetPath = AssetDatabase.GetAssetPath(source);
            var tImporter = AssetImporter.GetAtPath(assetPath) as TextureImporter;
            if (tImporter != null)
            {
                tImporter.textureType = TextureImporterType.Sprite;

                tImporter.textureCompression = TextureImporterCompression.Uncompressed;
                tImporter.isReadable = true;

                AssetDatabase.ImportAsset(assetPath);
                //AssetDatabase.Refresh();
            }

            Texture2D result = new Texture2D(targetWidth, targetHeight, source.format, true);
            Color[] rpixels = result.GetPixels(0);
            float incX = (1.0f / (float)targetWidth);
            float incY = (1.0f / (float)targetHeight);
            for (int px = 0; px < rpixels.Length; px++)
            {
                rpixels[px] = source.GetPixelBilinear(incX * ((float)px % targetWidth), incY * ((float)Mathf.Floor(px / targetWidth)));
            }

            List<TextureFormat> viableFormats = new List<TextureFormat>(){TextureFormat.ARGB32,TextureFormat.RGBA32,TextureFormat.RGB24,TextureFormat.Alpha8};

            if (viableFormats.Contains(result.format) == false)
            {
                Debug.LogError("Wrong Format" + result.format.ToString() + " ", source);
                return;
            }
            //else Debug.Log("good format: " + result.format);

            result.SetPixels(rpixels, 0);
            result.Apply();

            byte[] bytes = result.EncodeToPNG();
            Object.DestroyImmediate(result);

            // For testing purposes, also write to a file in the project folder
            File.WriteAllBytes(AssetDatabase.GetAssetPath(source), bytes);
            if (tImporter != null)
            {
                tImporter.textureType = TextureImporterType.Sprite;

                tImporter.isReadable = false;

                AssetDatabase.ImportAsset(assetPath);
                //AssetDatabase.Refresh();
            }
            //AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(source));
        }
        
        public List<Texture2D> GetTextures(string folderPath)
        {
            Debug.Log(folderPath);
            string[] guids =
                AssetDatabase.FindAssets("t:texture",
                                         new[] { folderPath }); //FindAssets uses tags check documentation for more info

            List<Texture2D> textures = new List<Texture2D>();
            //Utilities.SimpleCancelableProgressBarOperation("Loading Prefabs", guids, x => LoadPrefab(prefabs, x));
            for (int i = 0; i < guids.Length; i++) //probably could get optimized 
            {
                string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                textures.Add(AssetDatabase.LoadAssetAtPath<Texture2D>(path));
            }

            return textures;

        }
    }

    public enum ResizeType
    {
        ScaleByKoef,
        SetWidthAndHeight
    }
}