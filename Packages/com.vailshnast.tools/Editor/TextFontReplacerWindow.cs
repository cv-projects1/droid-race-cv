using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using TMPro;
using UnityEditor;
using UnityEngine;

namespace vailshnast.tools
{
    public class TextFontReplacerWindow : OdinEditorWindow
    {
        public GameObject[] Prefabs;
        public TMP_FontAsset FontAsset;
        public Material Material;
        
        [MenuItem("Tools/ProjectTools/TextFontReplacerWindow")]
        private static void OpenWindow()
        {
            var window = GetWindow<TextFontReplacerWindow>();
            //window.position = GUIHelper.GetEditorWindowRect().AlignCenter(800, 600);
        }
            
        [Button]
        public void Replace()
        {
            var tmpList = new List<TMP_Text>();

            foreach (var prefab in Prefabs) 
                tmpList.AddRange(prefab.GetComponentsInChildren<TMP_Text>(true));

            foreach (var text in tmpList)
            {
                text.font = FontAsset;
                text.fontSharedMaterial = Material;
            }

            foreach (var prefab in Prefabs) 
                EditorUtility.SetDirty(prefab);
            
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
        }
    }
}