using System.Diagnostics;
using UnityEditor;
using UnityEngine;

namespace vailshnast.tools
{
    public class UnityMenuCommands
    {
        [MenuItem("Tools/ProjectCommands/Jenny/ExecuteJenny")]
        private static void OpenWindow() => JennyHelper.ExecuteJenny();

        [MenuItem("Tools/ProjectCommands/Clear Prefs")]
        private static void ClearPrefs() => PlayerPrefs.DeleteAll();
        
#if UNITY_EDITOR_WIN
        [MenuItem(JennyAutoExecute)]
        private static void ToggleAction()
        {
            JennyStartup.IsEnabled = !JennyStartup.IsEnabled;
        }

        [MenuItem(JennyAutoExecute, true)]
        private static bool ToggleActionValidate()
        {
            Menu.SetChecked(JennyAutoExecute, JennyStartup.IsEnabled);
            return true;
        }
        
        private const string JennyAutoExecute = "Tools/ProjectCommands/Jenny/JennyAutoExecute";
#endif
    }

    public static class JennyHelper
    {
        public static void ExecuteJenny()
        {
            var folderPath = Application.dataPath;
            folderPath = folderPath.Replace("Assets", "Jenny/Jenny.exe");

            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = folderPath,
                    Arguments = "server",
                }
            };

            process.Start();
        }
 
    }
    
    

#if UNITY_EDITOR_WIN
    [InitializeOnLoad]
    public class JennyStartup
    {
        private const string JennyEnabled = "JennyEnabled";

        public static bool IsEnabled
        {
            get => EditorPrefs.GetBool(JennyEnabled, true);
            set
            {
                if (value) EditorPrefs.DeleteKey(JennyEnabled);
                else EditorPrefs.SetBool(JennyEnabled, value); // clear value if it's equals defaultValue
            }
        }
        private const string EditorKey = "EditorStarted";

        static JennyStartup()
        {
            if (!IsEnabled) return;
            if (SessionState.GetBool(EditorKey, false)) return;

            JennyHelper.ExecuteJenny();
            SessionState.SetBool(EditorKey, true);
        }
    }
#endif
}

