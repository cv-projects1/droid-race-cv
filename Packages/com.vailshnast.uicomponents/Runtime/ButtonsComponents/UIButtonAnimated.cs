﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace vailshnast.uicomponents
{
    [RequireComponent(typeof(Button))]
    public class UIButtonAnimated : UIButtonComponent
    {
        //TODO:RETURN AFTER
        [SerializeField/*, BoxGroup("Settings")*/] private Vector3 _strength = new Vector3(-0.2f, -0.2f, 1);
        [SerializeField, /*, BoxGroup("Settings")*/] private float _duration = 0.2f, _elasticity = 1;
        [SerializeField, /*, BoxGroup("Settings")*/] private int _vibrato = 10;

        private Tween _animationTween;

        public event UnityAction OnButtonAnimated;
        
        private void Animate()
        {
            if (_animationTween != null && _animationTween.IsPlaying()) return;
        
            transform.localScale = Vector3.one;

            if (_animationTween == null)
                _animationTween = transform.DOPunchScale(_strength, _duration, _vibrato, _elasticity)
                                           .SetAutoKill(false).OnComplete(() => OnButtonAnimated?.Invoke());
            else _animationTween.Restart();
        }

        protected override void OnButtonClick() => Animate();
        public override void AddSubscriber(UnityAction action) => OnButtonAnimated += action;
    }
}
