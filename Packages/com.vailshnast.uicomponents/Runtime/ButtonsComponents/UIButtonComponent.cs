﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using vailshnast.attributes;

namespace vailshnast.uicomponents
{
    [RequireComponent(typeof(Button))]
    public class UIButtonComponent : MonoBehaviour
    {
        [SerializeField,GetComponent] protected Button _button;
        private void Start() => _button.onClick.AddListener(OnButtonClick);

        protected virtual void OnButtonClick() { }

        public virtual void AddSubscriber(UnityAction action) =>
            _button.onClick.AddListener(action);
    }
}

