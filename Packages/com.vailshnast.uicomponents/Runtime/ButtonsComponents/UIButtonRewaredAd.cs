﻿/*
using AngryCat.Services.ProjectContext;
using I2.Loc;
using UnityEngine;
using UnityEngine.Advertisements;
using Zenject;

namespace AngryCat.UI.UIComponents.ButtonsComponents
{
    public class UIButtonRewaredAd : UIButtonComponent , IUnityAdsListener
    {
        [SerializeField] private Localize _adTextLocalize;
        [SerializeField , TermsPopup] private string _adReadyTerm, _adNotReadyTerm;
        
        [Inject] private UnityAdsService _unityAdsManager;

        private void Awake()
        {
            OnButtonPressedEvent += ShowAd;
            Advertisement.AddListener(this);
        }

        private void OnEnable() => SetAdText();
        
        private void SetAdText() => _adTextLocalize.SetTerm(_unityAdsManager.IsRewardedAdReady ? _adReadyTerm : _adNotReadyTerm);
        
        private void ShowAd() => _unityAdsManager.ShowRewardedAd();

        public void OnUnityAdsReady(string placementId)
        {
            Debug.Log ("ad ready");
            SetAdText();
        }

        public void OnUnityAdsDidError(string message) { }

        public void OnUnityAdsDidStart(string placementId) { }

        public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
        {
            switch (showResult)
            {
                case ShowResult.Finished:
                    // Reward the user for watching the ad to completion.
                    Debug.Log("rewarded");

                    break;
                case ShowResult.Skipped:
                    Debug.Log("The ad skipped");

                    break;
                case ShowResult.Failed:
                    Debug.Log("The ad did not finish due to an error.");

                    break;
            }
            SetAdText();
        }
    }
}
*/
