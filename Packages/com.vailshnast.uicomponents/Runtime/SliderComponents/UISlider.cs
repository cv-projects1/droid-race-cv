﻿using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace vailshnast.uicomponents.SliderComponents
{
    public class UISlider : Slider
    {
        public UnityEvent onClickEvent;
        
        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            onClickEvent?.Invoke();
        }
    }
}
