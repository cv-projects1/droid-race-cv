﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace vailshnast.uicomponents.TabComponents
{
    public abstract class BaseTab : MonoBehaviour,IPointerDownHandler
    {
        //[SerializeField] private  UnityEvent _OnTabSelected,_OnTabDeselected;
        public event Action<BaseTab> OnTabPressed, OnTabSelected;
        public void OnPointerDown(PointerEventData eventData)
        {
            OnTabPressed?.Invoke(this);
        }

        public void Deselect()
        {
            OnDeselect();
            OnTabSelected?.Invoke(this);
        }

        public void Select()
        {
            OnSelect();
            OnTabSelected?.Invoke(this);
        }

        protected abstract void OnDeselect();
        protected abstract void OnSelect();
    }
}
