﻿namespace vailshnast.uicomponents.TabComponents
{
    public class SelfTab : BaseTab
    {
        protected override void OnDeselect() => gameObject.SetActive(false);
        protected override void OnSelect() => gameObject.SetActive(true);
    }
}
