﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace vailshnast.uicomponents.TabComponents
{
    public class TabGroup : MonoBehaviour
    {
        [SerializeField] private List<BaseTab> _tabs;
        [SerializeField] private BaseTab _onEnableSelectedTab;
        private void Awake()
        {
            for (int i = 0; i < _tabs.Count; i++) _tabs[i].OnTabPressed += SelectTab;
        }

        private void OnEnable()
        {
            if (_onEnableSelectedTab != null)
                SelectTab(_onEnableSelectedTab);
        }

        private void OnDestroy()
        {
            for (int i = 0; i < _tabs.Count; i++) _tabs[i].OnTabPressed -= SelectTab;
        }

        public void SelectTab(BaseTab baseTab)
        {
            for (int i = 0; i < _tabs.Count; i++) if(_tabs[i] != baseTab) _tabs[i].Deselect();
            baseTab.Select();
        }

#if UNITY_EDITOR
        /*[Button]*///TODO RETURN AFTER
        private void FillEverything()
        {
            _tabs = GetComponentsInChildren<BaseTab>(true).ToList();
            if (_tabs.Count > 0) _onEnableSelectedTab = _tabs[0];
        }
        /*[Button]*/
        private void FillTabs()
        {
            _tabs = GetComponentsInChildren<BaseTab>(true).ToList();
            //if (_tabs.Count > 0) _onEnableSelectedTab = _tabs[0];
        }
#endif
    }
}
