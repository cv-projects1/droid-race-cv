﻿using UnityEngine;
using UnityEngine.UI;

namespace vailshnast.uicomponents.UILayouts
{
    public class FlexibleLayoutGroup : LayoutGroup
    {
        [SerializeField] private int _rows, _columns;
        [SerializeField] private Vector2 _cellSize , _spacing;
        [SerializeField] private FitType _fitType;
        [SerializeField] private bool _fitX, _fitY , _fitRectHeight, _fitRectWidth, _useAligns = true;

        private bool IsCenterAlign
        {
            get
            {
                return childAlignment == TextAnchor.LowerCenter || childAlignment == TextAnchor.MiddleCenter ||
                       childAlignment == TextAnchor.UpperCenter;
            }
        }

        private bool IsRightAlign
        {
            get
            {
                return childAlignment == TextAnchor.LowerRight || childAlignment == TextAnchor.MiddleRight ||
                       childAlignment == TextAnchor.UpperRight;
            }
        }

        private bool IsMiddleAlign
        {
            get
            {
                return childAlignment == TextAnchor.MiddleLeft || childAlignment == TextAnchor.MiddleRight ||
                       childAlignment == TextAnchor.MiddleCenter;
            }
        }

        private bool IsLowerAlign
        {
            get
            {
                return childAlignment == TextAnchor.LowerLeft || childAlignment == TextAnchor.LowerRight ||
                       childAlignment == TextAnchor.LowerCenter;
            }
        }
        
        public override void CalculateLayoutInputVertical()
        {
            /*float cellWidth = (parentWidth / columns) - (spacing.x / ((float)columns / (columns - 1))) - (padding.left / (float)columns) - (padding.right / (float)columns);
            float cellHeight = (parentHeight / rows) - (spacing.y / ((float)rows / (rows - 1))) - (padding.top / (float)rows) - (padding.bottom / (float)rows);
        
            For anyone having trouble with spacing causing overflow with more than a 3x3 grid:
            float cellWidth = (parentWidth / (float)columns) - ((spacing.x / (float)columns) * 2)....
                Should be:
            float cellWidth = (parentWidth / (float)columns) - ((spacing.x / (float)columns) * (colums - 1))....*/
            
            base.CalculateLayoutInputHorizontal();

            if (_fitType == FitType.Width || _fitType == FitType.Height || _fitType == FitType.Uniform)
            {
                _fitX = true;
                _fitY = true;
                
                float sqRt = Mathf.Sqrt(transform.childCount);
                _rows = Mathf.CeilToInt(sqRt);
                _columns = Mathf.CeilToInt(sqRt);
            }

            if (_fitType == FitType.Width || _fitType == FitType.FixedColumns)
                _rows = Mathf.CeilToInt(transform.childCount / (float) _columns);

            if (_fitType == FitType.Height || _fitType == FitType.FixedRows)
                _columns = Mathf.CeilToInt(transform.childCount / (float) _rows);

            var rect = rectTransform.rect;
            
            float parentWidth = rect.width;
            float parentHeight = rect.height;

            float cellWidth = (parentWidth / _columns) - (_spacing.x / ((float) _columns / (_columns - 1)));
            float cellHeight = (parentHeight / _rows) - (_spacing.y / ((float) _rows / (_rows - 1)));

            float paddingLeft = padding.left / (float) _columns;
            float paddingRight = padding.right / (float) _columns;
            float paddingTop = padding.top / (float) _rows;
            float paddingBottom = padding.bottom / (float) _rows;

            float paddingX = -paddingLeft - paddingRight;
            float paddingY = -paddingTop - paddingBottom;


            _cellSize.x = _fitX ? cellWidth : _cellSize.x;
            _cellSize.y = _fitY ? cellHeight: _cellSize.y;

            /*_cellSize.x += paddingX;
            _cellSize.y += paddingY;*/
            
            int columnCount = 0;
            int rowCount = 0;

            for (int i = 0; i < rectChildren.Count; i++)
            {
                rowCount = i / _columns;
                columnCount = i % _columns;

                var item = rectChildren[i];

                float xSpacingAlign = 0;
                float ySpacingAlign = 0;

                if (_useAligns)
                {
                    if (IsCenterAlign)
                        xSpacingAlign = (rect.width - (_cellSize.x * _columns + _spacing.x * (_columns - 1))) / _columns;
                
                    if (IsRightAlign)
                        xSpacingAlign = (rect.width - (_cellSize.x * _columns + _spacing.x * (_columns - 1)));

                    if (IsMiddleAlign)
                        ySpacingAlign = (rect.height - (_cellSize.y * _columns + _spacing.y * (_columns - 1))) / _columns;
                
                    if (IsLowerAlign)
                        ySpacingAlign = (rect.height - (_cellSize.y * _columns + _spacing.y * (_columns - 1)));
                }

                var xPos = (_cellSize.x * columnCount) + (_spacing.x * columnCount) + xSpacingAlign /*- paddingRight + paddingLeft*/;
                var yPos = (_cellSize.y * rowCount) + (_spacing.y * rowCount) + ySpacingAlign /*- paddingBottom + paddingTop*/;

                SetChildAlongAxis(item, 0, xPos, _cellSize.x);
                SetChildAlongAxis(item, 1, yPos, _cellSize.y);
            }
            
            Vector3 newScale = rectTransform.sizeDelta;
 
            if (_fitRectWidth) newScale.x = /*padding.left + padding.right +*/ (_cellSize.x + _spacing.x) * _columns - _spacing.x;
            if (_fitRectHeight) newScale.y = /*padding.top + padding.bottom +*/ (_cellSize.y + _spacing.y) * _rows - _spacing.y;
 
            rectTransform.sizeDelta = newScale;
        }

        public override void SetLayoutHorizontal()
        {
            //throw new System.NotImplementedException();
        }

        public override void SetLayoutVertical()
        {
           //throw new System.NotImplementedException();
        }
        
    }

    public enum FitType
    {
        Uniform,
        Width,
        Height,
        FixedColumns,
        FixedRows
    }
}
