﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace vailshnast.uicomponents.UIScrollView
{
    public class ResetScrollRect : MonoBehaviour
    {
        [SerializeField] private ScrollRect _scrollRect;
        [SerializeField] private Axis _axis;
        [SerializeField, Range(0,1)] private float _pos;
        //[SerializeField] private bool _waitOneFrame = false;

        private void OnEnable()
        {
            /*if (_waitOneFrame)
            {
                this.WaitOneFrame(() =>
                {
                    Scroll(Vector2.down);
                    _scrollRect.onValueChanged.AddListener(Scroll);
                });
            }
            else
            {
                Scroll(Vector2.zero);
                _scrollRect.onValueChanged.AddListener(Scroll);
            }*/
        }

        public void Scroll(Vector2 pos)
        {
            switch (_axis)
            {
                case Axis.Horizontal:
                    _scrollRect.horizontalNormalizedPosition = _pos;

                    break;
                case Axis.Vertical:
                    _scrollRect.verticalNormalizedPosition = _pos;

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            _scrollRect.onValueChanged.RemoveListener(Scroll);
        }
    }

    public enum Axis
    {
        Horizontal = 1,
        Vertical   = 2
    }
}
