﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace vailshnast.uicomponents.UIScrollView
{
    public class SetScrollRectPosition : MonoBehaviour
    {
        [SerializeField] private ScrollRect _scrollRect;
        [SerializeField] private RectTransform _test;
        [SerializeField , Range(0,1) ] private float _horizontalPosition, _verticalPosition;
        
        protected  void OnEnable()
        {
 
        }
        IEnumerator ApplyScrollPosition( ScrollRect scrollRect, float verticalPos , float horizontalPos)
        {
            for (int i = 0; i < 2; i++)
                yield return new WaitForEndOfFrame();

            //Canvas.ForceUpdateCanvases();
            
            //scrollRect.verticalNormalizedPosition = verticalPos;
            scrollRect.horizontalNormalizedPosition = horizontalPos;
            
            //_scrollRect.StopMovement();
            /*scrollRect.SetLayoutHorizontal();
            scrollRect.SetLayoutVertical();
            scrollRect.CalculateLayoutInputHorizontal();
            scrollRect.CalculateLayoutInputVertical();*/
            
            //LayoutRebuilder.ForceRebuildLayoutImmediate( (RectTransform)scrollRect.transform );
        }
    }
}
