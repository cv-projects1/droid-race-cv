using UnityEngine;

namespace vailshnast.visualbounds
{
    public class VisualBounds : MonoBehaviour
    {
        public Vector2 offset;
        public Vector2 scaleBound = new Vector2(1, 1);
    
        [HideInInspector] public Vector2 maxXlimit;
        [HideInInspector] public Vector2 maxYlimit;
    
        private Camera _camera;
    
        public void Initialize(Camera cam)
        {
            _camera = cam;
            CalculateBounds();
        }
        public void CalculateBounds()
        {
            var ortoSize = _camera.orthographicSize;
            var cameraHalfWidth = _camera.aspect * ortoSize;
            var transformPos = transform.position;
        
            maxXlimit = new Vector2((transformPos.x + offset.x - (scaleBound.x / 2)) + cameraHalfWidth, (transformPos.x + offset.x + (scaleBound.x / 2)) - cameraHalfWidth);
            maxYlimit = new Vector2((transformPos.y + offset.y - (scaleBound.y / 2)) + ortoSize, (transformPos.y + offset.y + (scaleBound.y / 2)) - ortoSize);
        }
    }
}