using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace vailshnast.windows.editor
{
    [CustomEditor(typeof(WindowsConfig))]
    public class WindowsConfigEditor : UnityEditor.Editor
    {
        private WindowsConfig _windowsConfig;
        public void OnEnable() => _windowsConfig = (WindowsConfig) target;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button($"Load All windows"))
            {
                var guids = AssetDatabase.FindAssets("t:Prefab");
                var toSelect = new List<WindowView>();
            
                foreach (var guid in guids)
                {
                    var path = AssetDatabase.GUIDToAssetPath(guid);
                    var toCheck = AssetDatabase.LoadAllAssetsAtPath(path)
                                               .ToList()
                                               .OfType<GameObject>()
                                               .Select(x => x.GetComponent<WindowView>())
                                               .Where(x => x != null);

                    toSelect.AddRange(toCheck);
                }
                
                _windowsConfig.SetWindowViewReferences(toSelect.ToArray());
                EditorUtility.SetDirty(target);
                AssetDatabase.SaveAssets();
            }
        }
    }
}