﻿using System;
using UnityEngine;
using vailshnast.extensions;

namespace vailshnast.windows
{
    public abstract class WindowView : MonoBehaviour
    {
        [SerializeField] private RectTransform[] _safeAreaRects;
        [SerializeField] private RectTransform _mainRect;

        public event Action OnWindowClosed, OnWindowOpened;
        
        public RectTransform MainRect => _mainRect;
        
        public abstract Type WindowId { get; }
        public virtual IWindowController Controller { get; set; }
        public virtual bool IsOpened { get; protected set; }

        public virtual void Open()
        {
            gameObject.SetActive(true);
            OnOpened();
            
            IsOpened = true;
            OnWindowOpened?.Invoke();
        }
        
        public virtual void Close()
        {
            gameObject.SetActive(false);
            OnClosed();
            IsOpened = false;

            Controller = null;
            OnWindowClosed?.Invoke();
        }
        public abstract void OnCreated();
        public abstract void OnPostCreated();
        
        protected abstract void OnOpened();
        protected abstract void OnClosed();

        public virtual void ApplySafeArea(SafeAreaData safeArea, RectTransform canvasRect)
        {
            MainRect.SetAndStretchToParentSize(canvasRect);
            
            if (_safeAreaRects == null) return;

            foreach (var rect in _safeAreaRects)
            {
                if (rect == null) continue;
                rect.anchorMin = safeArea.Min;
                rect.anchorMax = safeArea.Max;
            }
        }
    }

    public abstract class BaseWindowView<T> : WindowView where T : class, IWindowController
    {
        public override Type WindowId => typeof(T);
        public T ConcreteController => (T) Controller;
    }
}