namespace vailshnast.windows
{
    public interface IWindowsConfig
    {
        WindowsContainer WindowsContainer { get; }
        WindowView[] WindowViews { get; }
    }
}