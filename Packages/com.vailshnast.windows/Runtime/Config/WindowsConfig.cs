using System;
using UnityEngine;
using vailshnast.installers;

namespace vailshnast.windows
{
    [CreateAssetMenu(fileName = "WindowsConfig",menuName = "StaticData/WindowsConfig")]
    public class WindowsConfig : ScriptableObject, IWindowsConfig , IScriptableService
    {
        public WindowsContainer WindowsContainer => _windowsContainer;
        public WindowView[] WindowViews => _windowViews;
        
        public Type ServiceType => typeof(IWindowsConfig);
        
        [SerializeField] private WindowsContainer _windowsContainer;
        [SerializeField] private WindowView[] _windowViews;
        
#if UNITY_EDITOR
        public void SetWindowViewReferences(WindowView[] windowViews) 
            => _windowViews = windowViews;
#endif
    }
}