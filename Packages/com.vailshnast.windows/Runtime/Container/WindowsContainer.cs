using UnityEngine;

namespace vailshnast.windows
{
    public class WindowsContainer : MonoBehaviour
    {
        [SerializeField] private Canvas _canvas;
        [SerializeField] private RectTransform _canvasRect;
        [SerializeField] private Camera _uiCamera;
        
        public Canvas Canvas => _canvas;
        public RectTransform CanvasRect => _canvasRect;
        public Camera UICamera => _uiCamera;
    }
}