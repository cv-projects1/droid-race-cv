using UnityEngine;

namespace vailshnast.windows
{
    public class SafeAreaData
    {
        public Vector2 Min, Max;
    }
}