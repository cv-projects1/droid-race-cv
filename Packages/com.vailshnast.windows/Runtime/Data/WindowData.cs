using DG.Tweening;

namespace vailshnast.windows
{
    public class WindowData
    {
        public WindowView WindowView;
        public Tweener OpenTween, CloseTween;
    }
}