using UnityEngine;

namespace vailshnast.windows
{
    public static class WindowsExtensions
    {
        public static SafeAreaData CalculateSafeArea(Canvas canvas)
        {
            var safeArea = Screen.safeArea;
            var anchorMin = safeArea.position;
            var anchorMax = safeArea.position + safeArea.size;
            var canvasPixelRect = canvas.pixelRect;
            anchorMin.x /= canvasPixelRect.width;
            anchorMin.y /= canvasPixelRect.height;
            anchorMax.x /= canvasPixelRect.width;
            anchorMax.y /= canvasPixelRect.height;

            return new SafeAreaData {Min = anchorMin, Max = anchorMax};
        }
    }
}