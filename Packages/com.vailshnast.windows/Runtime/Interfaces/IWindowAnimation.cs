using DG.Tweening;

namespace vailshnast.windows
{
    public interface IWindowAnimation
    {
        Tweener OpenWithAnimation();
        Tweener CloseWithAnimation();
    }
}