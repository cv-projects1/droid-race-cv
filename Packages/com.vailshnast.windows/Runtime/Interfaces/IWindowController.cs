namespace vailshnast.windows
{
    public interface IWindowController
    {
        WindowView View { get; set; }
    }
}