using System.Threading.Tasks;

namespace vailshnast.windows
{
    public interface IWindowsService
    {
        WindowsContainer WindowsContainer { get; }
        
        Task Initialize();
        
        void RequestShowWindow(IWindowController controller);
        void RequestShowAndCloseLast(IWindowController controller);
        void RequestShowAndCloseAll(IWindowController controller);
        
        void RequestShowWindowAndQueue(IWindowController forcedController,
                                       IWindowController queueController);
        
        void RequestCloseLastOpenedWindow();
        void RequestCloseWindow<T>() where T : IWindowController;
        void RequestCloseWindow(WindowView view);
        void RequestCloseAllWindows();

        TWindow GetWindow<TWindow, TController>() where TController : class, IWindowController
                                                  where TWindow : BaseWindowView<TController>;
    }
}