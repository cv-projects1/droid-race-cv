using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using vailshnast.serviceparent;
using Zenject;

namespace vailshnast.windows
{
    public sealed class WindowsService : IWindowsService
    {
        private IWindowController OpenedWindow { get; set; }
        private readonly IServicesParent _servicesParent;
        private readonly IWindowsConfig _windowsConfig;
        private readonly DiContainer _diContainer;
        private readonly Dictionary<Type, WindowData> _windowsMap;
        private WindowsContainer _windowsContainer;
        private SafeAreaData _safeArea;
        public WindowsContainer WindowsContainer => _windowsContainer;

        public WindowsService(IWindowsConfig windowsConfig,
                              IServicesParent servicesParent,
                              DiContainer diContainer)
        {
            _windowsMap = new Dictionary<Type, WindowData>();
            _windowsConfig = windowsConfig;
            _diContainer = diContainer;
            _servicesParent = servicesParent;
        }

        public Task Initialize()
        {
            CreateWindowContainer();
            InitializeWindows();

            return Task.CompletedTask;
        }

        private void CreateWindowContainer()
        {
            Transform windowsParent = new GameObject($"Windows Service").transform;
            windowsParent.SetParent(_servicesParent.Parent);

            _windowsContainer =
                _diContainer.InstantiatePrefabForComponent<WindowsContainer>(
                    _windowsConfig.WindowsContainer, windowsParent);

            _safeArea = WindowsExtensions.CalculateSafeArea(_windowsContainer.Canvas);
        }

        private void InitializeWindows()
        {
            foreach (var windowPrefab in _windowsConfig.WindowViews)
                InstantiateWindowView(windowPrefab);

            foreach (var window in _windowsMap.Values)
                window.WindowView.OnPostCreated();
        }

        private void InstantiateWindowView(WindowView windowPrefab)
        {
            var windowView = _diContainer.InstantiatePrefabForComponent<WindowView>(windowPrefab);
            
            _windowsMap.Add(windowView.WindowId, new WindowData {WindowView = windowView});
            windowView.ApplySafeArea(_safeArea, _windowsContainer.CanvasRect);
            windowView.gameObject.SetActive(false);

            var windowTransform = windowView.transform;
            windowTransform.SetParent(_windowsContainer.CanvasRect);
            windowTransform.localScale = Vector3.one;
            windowTransform.localPosition = Vector3.zero;
            windowView.OnCreated();
        }

        public TWindow GetWindow<TWindow, TController>() where TController : class, IWindowController
                                                         where TWindow : BaseWindowView<TController>
        {
            _windowsMap.TryGetValue(typeof(TController), out var windowData);
#if UNITY_EDITOR
            if (windowData == null)
                throw new NullReferenceException(
                    $"Can't find window: {typeof(TWindow)} of controller {typeof(TController)}");
#endif
            return windowData?.WindowView as TWindow;
        }

        public void RequestShowWindow(IWindowController controller)
        {
            var controllerType = controller.GetType();
            _windowsMap.TryGetValue(controllerType, out var data);
            var view = data.WindowView;

            if (view == null) throw new NullReferenceException("Can't find popup with id: " + controllerType.Name);

            view.Controller = controller;
            controller.View = view;
            OpenedWindow = controller;
            view.transform.SetAsLastSibling();
            view.Open();

            if (view is IWindowAnimation windowAnimation)
            {
                KillAnimations(view);
                _windowsMap[view.WindowId].OpenTween = windowAnimation.OpenWithAnimation();
            }
        }

        public void RequestShowAndCloseLast(IWindowController controller)
        {
            RequestCloseLastOpenedWindow();
            RequestShowWindow(controller);
        }

        public void RequestShowWindowAndQueue(IWindowController controller,
                                              IWindowController queueController)
        {
            var controllerType = controller.GetType();
            var queuedType = queueController.GetType();

            if (controller == queueController)
                throw new NullReferenceException(
                    $"Requested controller is the same as queued, abandoned {controllerType.Namespace} {queuedType.Name}");

            _windowsMap.TryGetValue(controllerType, out var openedData);
            RequestShowWindow(controller);

            openedData.WindowView.OnWindowClosed += () =>
            {
                OpenWindow();
                openedData.WindowView.OnWindowClosed -= OpenWindow;
            };

            void OpenWindow() => RequestShowWindow(queueController);
        }

        public void RequestCloseLastOpenedWindow()
        {
            if (OpenedWindow == null || OpenedWindow.View == null || !OpenedWindow.View.IsOpened) return;

            var openedView = OpenedWindow.View;

            if (OpenedWindow.View is IWindowAnimation animatedWindow)
            {
                KillAnimations(openedView);
                Tweener closeTweeen = animatedWindow.CloseWithAnimation();
                closeTweeen.onComplete += () => openedView.Close();
                _windowsMap[openedView.WindowId].CloseTween = closeTweeen;
            }
            else
                OpenedWindow.View.Close();
        }

        public void RequestShowAndCloseAll(IWindowController controller)
        {
            RequestCloseAllWindows();
            RequestShowWindow(controller);
        }

        public void RequestCloseWindow<T>() where T : IWindowController
        {
            Type type = typeof(T);
            _windowsMap.TryGetValue(type, out var data);

            if (data == null) return;

            WindowView view = data.WindowView;
            RequestCloseWindow(view);
        }

        public void RequestCloseWindow(WindowView view)
        {
            if (!view.IsOpened) return;

            if (view is IWindowAnimation animatedWindow)
            {
                KillAnimations(view);
                Tweener closeTween = animatedWindow.CloseWithAnimation();

                closeTween.onComplete += () =>
                {
                    if (view == OpenedWindow.View)
                        OpenedWindow.View = null;

                    view.Close();
                };

                _windowsMap[view.WindowId].CloseTween = closeTween;
            }
            else
            {
                if (view == OpenedWindow.View)
                    OpenedWindow.View = null;

                view.Close();
            }
        }

        public void RequestCloseAllWindows()
        {
            foreach (var entry in _windowsMap)
            {
                WindowView view = entry.Value.WindowView;
                RequestCloseWindow(view);
            }
        }

        private void KillAnimations(WindowView view)
        {
            WindowData tweens = _windowsMap[view.WindowId];
            tweens.CloseTween?.Kill();
            tweens.OpenTween?.Kill();
        }
    }
}